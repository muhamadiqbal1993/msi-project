<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<input type="hidden" name="debit" id="debit" class="form-control"> 
						<input type="hidden" name="credit" id="credit" class="form-control"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Kode</label>
									<input type="text" name="kode" class="form-control" readonly="" value="<?php echo 'PR-'.date('ymdHis')?>">
								</div>
								<div class="form-group">
									<label>No Purchase Order</label>
									<select class="form-control select2" name="no_so" id="no_so" style="width: 100%;" onchange="inputProduct()" required="">
										<option value="" >--- Pilih Sales Order ---</option>
										<?php
										$sql1=mysql_query("select * from bill where bill not in (select no_so from bill_return where status = '1') and status != '3'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['bill']?>" ><?php echo $data1['bill']?></option>

										<?php }?>

									</select>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal</label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" >
								</div>
								<div class="form-group">
									<label>Alasan Return</label>
									<input type="text" name="keterangan" id="keterangan" class="form-control" >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
										<h1>Product Line</h1>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 50%">Product</th>
													<th>Qty</th>
													<th>Harga</th>
													<th>Harga Total</th>
													<th>Action</th>
												</tr>
											</thead>
											
											<script language="javascript" type="text/javascript">

												function inputProduct()
												{
													var no_so = document.getElementById('no_so').value;
													var table = document.getElementById('tabDidik');

													for(var i = table.rows.length - 1; i > 0; i--)
													{
														table.deleteRow(i);
													}

													var xmlhttp = new XMLHttpRequest();
													xmlhttp.onreadystatechange = function() {
														if (this.readyState == 4 && this.status == 200) {
															var myArr = JSON.parse(this.responseText);

															var totalList = myArr.length;
															var totalNilai = 0;
															if(totalList == 0)
															{
																alert("Tidak Ada Product Pada PO ini");
															}
															for(var i=0;i<totalList;i++)
															{
																var rowCount = i+1;
																var row = table.insertRow(rowCount);
																var cell0 = row.insertCell(0);
																var cell1 = row.insertCell(1);
																var cell2 = row.insertCell(2);
																var cell3 = row.insertCell(3);
																var cell4 = row.insertCell(4);
																var cell5 = row.insertCell(5);

																var nilaiTotal = (parseInt(myArr[i]['qty']) * parseInt(myArr[i]['harga_jual'])).toLocaleString();
																totalNilai += parseInt(myArr[i]['qty']) * parseInt(myArr[i]['harga_jual']);

																cell0.innerHTML = rowCount;
																cell1.innerHTML = '<input type="text" name="editKode[]" class="form-control" value="'+myArr[i]['product']+" - "+myArr[i]['nampro']+'" readonly>';

																cell2.innerHTML = '<input type="number" name="editQty[]" id="editQty'+rowCount+'"  oninput="changeQty('+rowCount+')" class="form-control" value="'+myArr[i]['qty']+'">';
																cell3.innerHTML = myArr[i]['harga_jual'];
																cell4.innerHTML = nilaiTotal;
																cell5.innerHTML = "<button type='button' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

															}
															document.getElementById('nilaiReturn').innerHTML = "<strong>Rp. "+totalNilai.toLocaleString()+"</strong>";
															
														}
													};
													xmlhttp.open("GET","ajax/getOrderLine.php?no_order="+no_so,true);
													xmlhttp.send();
												}

												function changeQty(abs)
												{
													var table = document.getElementById('tabDidik');

													var stringQty = "editQty"+abs;
													var dataQty = document.getElementById(stringQty).value;

													var nilaiSatuan = table.rows[abs].cells[3].innerHTML;

													table.rows[abs].cells[4].innerHTML = (parseInt(dataQty) * parseInt(nilaiSatuan)).toLocaleString();

													ubahNilai();

												}

												function delete_row_didik(abs)
												{
													document.getElementById("tabDidik").deleteRow(abs);
													ubahNilai();
												}

												function ubahNilai()
												{
													var table = document.getElementById('tabDidik');
													var rowCount = table.rows.length - 1;

													var nilaiTotal = 0;
													for(var a=1;a<=rowCount;a++)
													{
														table.rows[a].cells[0].innerHTML = a;
														table.rows[a].cells[5].innerHTML = "<button type='button' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";

														nilaiTotal += parseInt(table.rows[a].cells[4].innerHTML.split(",").join(""));
														
													}
													document.getElementById('nilaiReturn').innerHTML = "<strong>Rp. "+nilaiTotal.toLocaleString()+"</strong>";
												}

											</script>
										</table>
									</div>
									<div class="col-md-12">
										<hr class="abu">
										<h2 style="float: right;">Total Return : <span id="nilaiReturn" style="color: red;"><strong>Rp. 0</strong></span></h2>
									</div>
								</div>
							</div>

						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}

	}
</script>
