<?php
$dataEdit = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(tanggal, '%m/%d/%Y') as new_tanggal from bill_return where no = '$id'"));

?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($dataEdit['status'] != "3"){?>
							<!--<button type="submit" form="create_data" class="btn btn-primary">Simpan</button>-->
							<a href="F_cancelPayment.php?id=<?php echo $id?>"><button type="submit" class="btn btn-danger" style="float: right;">Cancel Transaksi</button></a>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="kode" class="form-control" required="" value="<?php echo $dataEdit['id'];?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">

						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>No Payment</label>
									<input type="text" name="kode" class="form-control" value="<?php echo $dataEdit['id'];?>" readonly>
								</div>
								<div class="form-group">
									<label>Purchase Order</label>
									<select class="form-control select2" name="no_so" id="no_so" style="width: 100%;" onchange="inputProduct()" required="">
										<option value="" >--- Pilih Purchase Order ---</option>
										<option value="<?php echo $dataEdit['no_so'];?>" selected><?php echo $dataEdit['no_so'];?></option>
										<?php
										$sql1=mysql_query("select * from bill where bill not in (select no_so from bill_return where status = '1') and status != '3'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['bill']?>" ><?php echo $data1['bill']?></option>
											<?php 
										}
										?>
									</select>
								</div>
								

							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal</label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" value="<?php echo $dataEdit['new_tanggal'];?>">
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" name="keterangan" id="keterangan" class="form-control" value="<?php echo $dataEdit['alasan'];?>">
								</div>
								<!-- /.form-group -->
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
										<h1><?php echo $modulnya.' Line';?></h1>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 50%">Product</th>
													<th>Qty</th>
													<th>Harga</th>
													<th>Harga Total</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$noms = 1;
												$sql1=mysql_query("select PORD.*,P.nama as nampro from bill_return_detail PORD
													inner join product P on P.no = PORD.product
													where PORD.id_return = '".$dataEdit['id']."'");
												while($data1=mysql_fetch_array($sql1))
												{
													$totalHarga = $data1['harga'] * $data1['qty'];
													echo '
													<tr>
													<td>'.$noms.'</td>
													<td><input type="text" name="editKode[]" class="form-control" value="'.$data1['product'].' - '.$data1['nampro'].'" readonly></td>

													<td><input type="number" name="editQty[]" id="editQty'.$noms.'"  oninput="changeQty('.$noms.')" class="form-control" value="'.$data1['qty'].'"></td>

													<td>'.$data1['harga'].'</td>

													<td>'.number_format($totalHarga).'</td>  
													<td><button type="button" onclick="delete_row_didik('.$noms.')" class="btn btn-danger">Delete</button></td>
													</tr>
													';
													$noms++;
												}
												?>
												
											</tbody>
											<script language="javascript" type="text/javascript">

												function inputProduct()
												{
													var no_so = document.getElementById('no_so').value;
													var table = document.getElementById('tabDidik');

													for(var i = table.rows.length - 1; i > 0; i--)
													{
														table.deleteRow(i);
													}

													var xmlhttp = new XMLHttpRequest();
													xmlhttp.onreadystatechange = function() {
														if (this.readyState == 4 && this.status == 200) {
															var myArr = JSON.parse(this.responseText);

															var totalList = myArr.length;
															var totalNilai = 0;
															for(var i=0;i<totalList;i++)
															{
																var rowCount = i+1;
																var row = table.insertRow(rowCount);
																var cell0 = row.insertCell(0);
																var cell1 = row.insertCell(1);
																var cell2 = row.insertCell(2);
																var cell3 = row.insertCell(3);
																var cell4 = row.insertCell(4);
																var cell5 = row.insertCell(5);

																var nilaiTotal = (parseInt(myArr[i]['qty']) * parseInt(myArr[i]['harga'])).toLocaleString();
																totalNilai += parseInt(myArr[i]['qty']) * parseInt(myArr[i]['harga']);

																cell0.innerHTML = rowCount;
																cell1.innerHTML = '<input type="text" name="editKode[]" class="form-control" value="'+myArr[i]['product']+" - "+myArr[i]['nampro']+'" readonly>';

																cell2.innerHTML = '<input type="number" name="editQty[]" id="editQty'+rowCount+'"  oninput="changeQty('+rowCount+')" class="form-control" value="'+myArr[i]['qty']+'">';
																cell3.innerHTML = myArr[i]['harga'];
																cell4.innerHTML = nilaiTotal;
																cell5.innerHTML = "<button type='button' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

															}
															document.getElementById('nilaiReturn').innerHTML = "<strong>Rp. "+totalNilai.toLocaleString()+"</strong>";
															
														}
													};
													xmlhttp.open("GET","ajax/getOrderLine.php?no_order="+no_so,true);
													xmlhttp.send();
												}

												function changeQty(abs)
												{
													var table = document.getElementById('tabDidik');

													var stringQty = "editQty"+abs;
													var dataQty = document.getElementById(stringQty).value;

													var nilaiSatuan = table.rows[abs].cells[3].innerHTML;

													table.rows[abs].cells[4].innerHTML = (parseInt(dataQty) * parseInt(nilaiSatuan)).toLocaleString();

													ubahNilai();

												}

												function delete_row_didik(abs)
												{
													document.getElementById("tabDidik").deleteRow(abs);
													ubahNilai();
												}

												function ubahNilai()
												{
													var table = document.getElementById('tabDidik');
													var rowCount = table.rows.length - 1;

													var nilaiTotal = 0;
													for(var a=1;a<=rowCount;a++)
													{
														table.rows[a].cells[0].innerHTML = a;
														table.rows[a].cells[5].innerHTML = "<button type='button' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";

														nilaiTotal += parseInt(table.rows[a].cells[4].innerHTML.split(",").join(""));
														
													}
													document.getElementById('nilaiReturn').innerHTML = "<strong>Rp. "+nilaiTotal.toLocaleString()+"</strong>";
												}
											</script>
										</table>
									</div>
									<div class="col-md-12">
										<hr class="abu">
										<h2 style="float: right;">Total Return : <span id="nilaiReturn" style="color: red;"><strong>Rp. <?php echo number_format($dataEdit['nilai'])?></strong></span></h2>
									</div>
								</div>
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<?php 
				$nama_modulnya = 'purchase_return';
				include '../headfoot/history.php';
				?>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>
