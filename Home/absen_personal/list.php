<?php 
$tglDari = date("m")."/01/".date("Y");
$tglSampai =  date("m")."/31/".date("Y");
$karyawan = $user_data['email'];

if(!empty($_POST['cari']))
{
  $tglDari = $_POST['dari'];
  $tglSampai = $_POST['sampai'];
  $karyawan = $user_data['email'];
}
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">

          </div>



          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            Username sudah ada yang menggunakan , silahkan daftarkan dengan username lain
          </div>
        </div>';
      }
      ?>


      <div id="pencarian">
        <form action="." id="search_data" method="POST"> 
          <div class="col-xs-12">
            <div class="form-group">
              <div class="row">
                <div class="col-xs-6">
                  <label>Tanggal Dari </label>
                  <input type="text" name="dari" class="form-control" id="datepicker" value="<?php echo $tglDari;?>">
                  <input type="hidden" name="cari" class="form-control" value="1">
                </div>
                <div class="col-xs-6">
                  <label>Tanggal Sampai </label>
                  <input type="text" name="sampai" class="form-control" id="datepicker1" value="<?php echo $tglSampai;?>">
                </div>
              </div>
            </div>


          </div>
        </form>
        <div class="col-xs-12">
          <button type="submit" form="search_data" class="btn btn-primary">Search</button>
          <button type="submit" onclick="pencarian()" class="btn btn-warning">Sembunyikan Pencarian</button>
        </div>

      </div>
      
      <script type="text/javascript">
        function pencarian()
        {
          document.getElementById('pencarian').style.display = "none"; 
          document.getElementById('buka').style.display = ""; 
        }
        function buka()
        {
          document.getElementById('pencarian').style.display = ""; 
          document.getElementById('buka').style.display = "none";
        }
      </script>

      <div class="col-xs-12" id="buka" style="display: none">
        <button type="submit" onclick="buka()" class="btn btn-primary">Buka Pencarian</button>
      </div>
      <div class="col-xs-12">
        <hr class="abu">
      </div>

      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Kode</th>
              <th>Nama</th>
              <th>Tanggal</th>
              <th>Jam Datang</th>
              <th>Jam Pulang</th>
              <th>Keterangan</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $smpss = explode("/", $tglSampai)[2].'-'.explode("/", $tglSampai)[0].'-'.explode("/", $tglSampai)[1];
            $drss = explode("/", $tglDari)[2].'-'.explode("/", $tglDari)[0].'-'.explode("/", $tglDari)[1];

            $begin = new DateTime( $drss );
            $end = new DateTime(  $smpss );
            $end = $end->modify( '+1 day' ); 

            $interval = new DateInterval('P1D');
            $daterange = new DatePeriod($begin, $interval ,$end);

            $sear = "";
            if($karyawan != "")
            {
              $sear = "where no = '".$karyawan."'";
            }

            foreach($daterange as $date){
              $sql1=mysql_query("select * from karyawan $sear");
              while($data1=mysql_fetch_array($sql1))
              { 
                $warna = '';
                $ket = "";

                $jam_masuk = mysql_fetch_array(mysql_query("select * from absensi where id_kar = '".$data1['no']."' and tanggal like '%".$date->format("Y-m-d")."%' order by no asc limit 1"));
                $jam_pulang = mysql_fetch_array(mysql_query("select * from absensi where id_kar = '".$data1['no']."' and tanggal like '%".$date->format("Y-m-d")."%' order by no desc limit 1"));

                $sql2=mysql_query("select * from setting where no in ('1','2')");
                while($data2=mysql_fetch_array($sql2))
                { 
                  if($data2['no'] == "1")
                  {
                    if(strtotime($jam_masuk['jam'].':00') > strtotime($data2['isi'].':00'))
                    {
                      $warna = 'style="color: orange"';
                      $ket = "Datang Terlambat";
                    }
                  }
                }
                

                if(empty($jam_masuk['jam']) || empty($jam_pulang['jam']))
                {
                  $warna = 'style="color: red"';
                  $ket = "Tidak Ada Absen";
                }

                echo '
                <tr>
                 <td '.$warna.'>'.$data1['kode'].'</td>
                 <td '.$warna.'>'.$data1['nama'].'</td>
                 <td '.$warna.'>'.$date->format("d-m-Y").'</td>
                 <td '.$warna.'>'.$jam_masuk['jam'].'</td>      
                 <td '.$warna.'>'.$jam_pulang['jam'].'</td>
                 <td '.$warna.'>'.$ket.'</td>
               </tr>
               ';
             }
             
           }

           ?>
         </tbody>
       </table>
     </div>

   </div>
   <br>
   <br>
   <!-- /.box-body -->
 </div>
 <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>