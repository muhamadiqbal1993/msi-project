<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<!--<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu"> -->

					</div>
					
					<div class="box-body">
						<div class="col-md-12">
							<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
							&nbsp;&nbsp;&nbsp;
							<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
							<hr class="abu">
						</div>
						<form id="create_data" method="POST" enctype="multipart/form-data"> 
							<div class="col-md-12">
								<input type="hidden" name="input" class="form-control" value="1">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
								<input type="hidden" name="detPrice" id="detPrice" class="form-control" required="">
								<?php include 'tab/data_customer.php';?>
							</div>
						</form>
					</div>

					<br>
					<br>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>