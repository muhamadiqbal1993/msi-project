<?php
include 'conn.php';

$origin = explode("-", $_GET['route'])[0];
$dest = $_GET['dest'];
$agent = $_GET['agent'];
$berat = $_GET['berat'];
$kom_list = $_GET['kom'];
$route = $_GET['route'];
$fl_no = explode("-", $_GET['fl_no'])[0];

$txtOther = "0";
$heavy_w = "0";

$queryResult2=$connect->query("select concat(CT.kode,' ',OC.base,' ',OC.isi) as isi from other_charge  OC 
	inner join charge_type CT on CT.no = OC.type
	where OC.port = '$origin' and OC.status = '1'");

while($fetchData2=$queryResult2->fetch_assoc()){
	if($txtOther == "0")
	{
		$txtOther = $fetchData2['isi'];
	}
	else
	{
		$txtOther = $txtOther.','.$fetchData2['isi'];
	}
}

$queryResult3=$connect->query("select concat(H.base_on,' ',H.start,' ',H.end,' ',H.type,' ',H.isi) as heavy_w from heavy H
	inner join airlane A on A.no = H.airline
	where A.carier_code = '$fl_no' ");

while($fetchData3=$queryResult3->fetch_assoc()){
	if($heavy_w == "0")
	{
		$heavy_w = $fetchData3['heavy_w'];
	}
	else
	{
		$heavy_w = $heavy_w.','.$fetchData3['heavy_w'];
	}
}

//query lama
// $queryResult=$connect->query("select no,tarif,nama,start,type,price,status,IFNULL(nta, 0) as nta, IFNULL(surcharge, 0) as surcharge,IFNULL(adminfee, 0) as adminfee,heavy_w from ( select TD.*,(select concat(N.nta,'#',N.status) from nta N 
// 	inner join airlane A on A.name = N.airline
// 	where N.agent = '$agent' and A.carier_code = '$fl_no') as nta, 
// 	(select concat(S.base,'#',S.isi) from surcharge S inner join kom_list KL on KL.no = S.commodity where KL.name = '$kom_list') as surcharge ,
// 	'$txtOther' as adminfee ,
// 	'$heavy_w' as heavy_w
// 	from tarif_detail TD 
// 	inner join tarif T on T.no = TD.tarif and T.status = '1'
// 	where $berat between TD.start and TD.end and T.origin = '$origin' and T.dest = '$dest' order by price desc limit 1 ) as tbl1");

$queryResult=$connect->query("select *,'$txtOther' as adminfee,'$heavy_w' as heavy_w,IFNULL(nta, 0) as nta,IFNULL(surcharge, 0) as surcharge from (
	select *,
	(CASE WHEN transit is null then concat(origin,'-',dest) else concat(origin,'-',transit,'-',dest) END) as full_transit,
	(select price from tarif_detail where tarif = tb1.no and $berat BETWEEN start and end limit 1) as price,
	(select concat(N.nta,'#',N.status) from nta N 
	inner join airlane A on A.name = N.airline
	where N.agent = '$agent' and A.carier_code = '$fl_no') as nta,
	(SELECT (isi/100) as totals FROM liner_ppn LP
	inner join airlane A on A.no = LP.liner
	where A.carier_code = '$fl_no') as ppns,
	(select concat(S.base,'#',S.isi) from surcharge S inner join kom_list KL on KL.no = S.commodity where KL.name = '$kom_list') as surcharge
	from (
	select *,(select GROUP_CONCAT(airport SEPARATOR '-') as transit from account_airport where modul = 'tarif' and account = tarif.no group by account) as transit from tarif where status = '1'
	) as tb1
	) as tb2 where full_transit = '$route'");

$result=array();

while($fetchData=$queryResult->fetch_assoc()){
	$result[]=$fetchData;
}

echo json_encode($result);

?>


