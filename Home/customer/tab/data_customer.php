<div class="row">
	
	<!-- <input type="hidden" name="s_pic" class="form-control" value="-">
	<input type="hidden" name="c_pic" class="form-control" value="-"> -->
	<div class="col-md-6">
		<div class="form-group">
			<label>Kode Customer</label>
			<input type="text" name="kode" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Nama Customer</label>
			<input type="text" name="nama" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Alamat</label>
			<input type="text" name="alamat" class="form-control">
		</div>
		<div class="form-group">
			<label>Phone</label>
			<input type="text" name="phone" class="form-control">
		</div>
		<div class="form-group">
			<label>PIC</label>
			<input type="text" name="kontak" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>NPWP</label>
			<input type="text" name="npwp" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Nama NPWP</label>
			<input type="text" name="nama_npwp" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Photo NPWP</label>
			 <input type="file" name="photos" id="exampleInputFile">
		</div>
		
		
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Kota</label>
			<select class="form-control select2" name="kota" style="width: 100%;" required="">
				<option value="" >--- Pilih Kota</option>
				<?php
				$sql1=mysql_query("select * from kabupaten where status = '1'");
				while($data1=mysql_fetch_array($sql1))
				{
					?>
					<option value="<?php echo $data1['no']?>" ><?php echo $data1['kabupaten'];?></option>
					<?php 
				}?>
			</select>
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control">
		</div>
		<div class="form-group">
			<label>Bank</label>
			<input type="text" name="bank" class="form-control">
		</div>
		<div class="form-group">
			<label>Bank Account Name</label>
			<input type="text" name="acc" class="form-control">
		</div>
		<div class="form-group">
			<label>Bank Account No</label>
			<input type="text" name="acc_no" class="form-control">
		</div>
		<div class="form-group">
			<label>Sales</label>
			<select class="form-control select2" name="sales" style="width: 100%;">
				<option value="" >--- Pilih Sales</option>
				<?php
				$sql1=mysql_query("select * from karyawan where status = '1'");
				while($data1=mysql_fetch_array($sql1))
				{
					?>
					<option value="<?php echo $data1['no']?>" ><?php echo $data1['nama'];?></option>
					<?php 
				}?>
			</select>
		</div>
		<div class="form-group">
			<label>Type Perusahaan</label>
			<select class="form-control select2" name="tipe_perusahaan" style="width: 100%;">
				<option value="" >--- Pilih Type Perusahaan ---</option>
				<option value="1" >Perusahaan Pemerintah</option>
				<option value="2" >Perusahaan Swasta</option>
			</select>
		</div>
	</div>
</div>
