<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <?php if($addPM == "1"){?>
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu">
            <?php }?>
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat, dan secara otomatis membuat User Login dengan Id Karyawan sebagai user login, dan 123456 sebagai password
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            Username sudah ada yang menggunakan , silahkan daftarkan dengan username lain
          </div>
        </div>';
      }
      ?>


      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Kode Customer</th>
              <th>Nama Customer</th>
              <th>PIC</th>
              <th>Email</th>
              <th>Alamat</th>
              <th>Kota</th>
              <th>Status</th>
              <th>Total Order</th>
              <th>Terakhir Order</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $sql1=mysql_query("select C.*,K.kabupaten from customer C
              inner join kabupaten K on K.no = C.kota
              where C.status != '100'
              ");
            while($data1=mysql_fetch_array($sql1))
            {  
              $status = "Aktif";
              if($data1['status'] == "2")
              {
                $status = "Tidak Aktif";
              }

              $totalOrder = mysql_fetch_array(mysql_query("select count(*) as total,DATE_FORMAT(tanggal, '%d %b %Y') as tgls from invoice where customer = '".$data1['no']."'"));

              echo '
              <tr>
                <td>'.$data1['kode'].'</td>
                <td>'.$data1['nama'].'</td>
                <td>'.$data1['contact'].'</td>
                <td>'.$data1['email'].'</td>
                <td>'.$data1['alamat'].'</td>
                <td>'.$data1['kabupaten'].'</td>
                <td>'.$status.'</td>    
                <td>'.$totalOrder['total'].'</td>
                <td>'.$totalOrder['tgls'].'</td>
                <td><a href="./?id='.$data1['no'].'">Manage</a></td>
              </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>