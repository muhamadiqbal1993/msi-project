<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Tambah <?php echo $modulnya;?> Baru</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?> Data</a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
            &nbsp;&nbsp;&nbsp;
            <button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
            <hr class="abu">
          </div>
          <form id="create_data" method="POST"> 
            <input type="hidden" name="input" class="form-control" value="1">
            <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
            <div class="box-body">
              <div class="col-xs-12">
                <h3>Total Revenues & Gains</h3>
                <table id="revenue" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Account</th>
                      <th>Hapus</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $noms = 1;
                    $sql1=mysql_query("select * from template_nett_income where type = '1'");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      $accr = '<select class="form-control select2" name="coa[]" id="coa'.$noms.'" style="width: 100%;" required="">';

                      $sql12=mysql_query("select * from coa where status = '1'");
                      while($data12=mysql_fetch_array($sql12))
                      {
                        $slct = "";
                        if($data1['coa'] == $data12['no'])
                        {
                          $slct = "selected";
                        }
                        $accr .= '<option value="'.$data12['no'].'" '.$slct.'>'.$data12['id'].' - '.$data12['nama'].'</option>';
                      }
                      $accr .= '</select>';
                      echo '
                      <tr>
                        <td>'.$noms.'</td>
                        <td>'.$accr.'</td>
                        <td><button type="button" onclick="delete_rev('.$noms.')" class="btn btn-danger">Delete</button></td>
                      </tr>
                      ';
                      $noms++;
                    }
                    ?>
                    <tr>
                      <td colspan="3" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_rev()">Tambah Data</button></td>

                    </tr>
                  </tbody>

                </table>
                <script language="javascript" type="text/javascript">

                  function getProduct_rev(abs)
                  {
                    var products = '<select class="form-control select2" name="coa[]" id="coa'+abs+'" style="width: 100%;" required="">'+
                    <?php

                    $sql1=mysql_query("select * from coa where status = '1'");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      ?>
                      '<option value="<?php echo $data1['no'];?>"><?php echo $data1['id'].' - '.$data1['nama']?></option>'+

                      <?php 
                    }?>

                    '</select>';

                    return products;
                  }

                  function delete_rev(abs)
                  {
                    document.getElementById("revenue").deleteRow(abs);
                    var rowCount = document.getElementById('revenue').rows.length - 1;
                    var table = document.getElementById('revenue');
                    for(var a=1;a<rowCount;a++)
                    {
                      table.rows[a].cells[0].innerHTML = a;
                      table.rows[a].cells[2].innerHTML = "<button type='submit' onclick='delete_rev(\""+a+"\")' class='btn btn-danger'>Delete</button>";
                    }
                  }

                  function addLine_rev()
                  { 
                    var rowCount = document.getElementById('revenue').rows.length - 1;
                    var table = document.getElementById('revenue');
                    var row = table.insertRow(rowCount);
                    var cell0 = row.insertCell(0);
                    var cell1 = row.insertCell(1);
                    var cell2 = row.insertCell(2);

                    cell0.innerHTML = rowCount;
                    cell1.innerHTML = getProduct_rev(rowCount);
                    cell2.innerHTML = "<button type='submit' onclick='delete_rev(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

                    var namaField = "#coa"+rowCount;
                    $(namaField).select2();
                  }
                </script>
              </div>
              <!-- ================================================================================================ -->
              <div class="col-xs-12">
                <h3>Total Expenses & Losses</h3>
                <table id="exp" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Account</th>
                      <th>Hapus</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $noms = 1;
                    $sql1=mysql_query("select * from template_nett_income where type = '2'");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      $accr = '<select class="form-control select2" name="coa2[]" id="coa2'.$noms.'" style="width: 100%;" required="">';

                      $sql12=mysql_query("select * from coa where status = '1'");
                      while($data12=mysql_fetch_array($sql12))
                      {
                        $slct = "";
                        if($data1['coa'] == $data12['no'])
                        {
                          $slct = "selected";
                        }
                        $accr .= '<option value="'.$data12['no'].'" '.$slct.'>'.$data12['id'].' - '.$data12['nama'].'</option>';
                      }
                      $accr .= '</select>';
                      echo '
                      <tr>
                        <td>'.$noms.'</td>
                        <td>'.$accr.'</td>
                        <td><button type="button" onclick="delete_exp('.$noms.')" class="btn btn-danger">Delete</button></td>
                      </tr>
                      ';
                      $noms++;
                    }
                    ?>
                    <tr>
                      <td colspan="3" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_exp()">Tambah Data</button></td>

                    </tr>
                  </tbody>

                </table>
                <script language="javascript" type="text/javascript">

                  function getProduct_exp(abs)
                  {
                    var products = '<select class="form-control select2" name="coa2[]" id="coa2'+abs+'" style="width: 100%;" required="">'+
                    <?php

                    $sql1=mysql_query("select * from coa where status = '1'");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      ?>
                      '<option value="<?php echo $data1['no'];?>"><?php echo $data1['id'].' - '.$data1['nama']?></option>'+

                      <?php 
                    }?>

                    '</select>';

                    return products;
                  }

                  function delete_exp(abs)
                  {
                    document.getElementById("exp").deleteRow(abs);
                    var rowCount = document.getElementById('exp').rows.length - 1;
                    var table = document.getElementById('exp');
                    for(var a=1;a<rowCount;a++)
                    {
                      table.rows[a].cells[0].innerHTML = a;
                      table.rows[a].cells[2].innerHTML = "<button type='submit' onclick='delete_exp(\""+a+"\")' class='btn btn-danger'>Delete</button>";
                    }

                  }

                  function addLine_exp()
                  { 
                    var rowCount = document.getElementById('exp').rows.length - 1;
                    var table = document.getElementById('exp');
                    var row = table.insertRow(rowCount);
                    var cell0 = row.insertCell(0);
                    var cell1 = row.insertCell(1);
                    var cell2 = row.insertCell(2);

                    cell0.innerHTML = rowCount;
                    cell1.innerHTML = getProduct_exp(rowCount);
                    cell2.innerHTML = "<button type='submit' onclick='delete_exp(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

                    var namaField = "#coa2"+rowCount;
                    $(namaField).select2();
                  }
                </script>
              </div>
            </div>
          </form>
          <br>
          <br>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
</script>