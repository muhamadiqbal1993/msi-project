<?php

$nama = "";
$keterangan = "";
$status = "";
$dari = "";
$sampai = "";
$isi = "";

$sql1=mysql_query("select *,DATE_FORMAT(dari, '%m/%d/%Y') as drs,DATE_FORMAT(sampai, '%m/%d/%Y') as smp from news where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$nama = $data1['kata'];
	$status = $data1['status'];
	$dari = $data1['drs'];
	$sampai = $data1['smp'];
$isi = $data1['isi'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-12">
								<div class="form-group">
									<label>teks</label>
									<input type="hidden" name="input" class="form-control" value="2">
									<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
									<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $nama;?>">
								</div>	
								<!-- /.form-group -->

							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Dari Tanggal</label>
									<input type="text" name="dari" id="datepicker" class="form-control" required="" value="<?php echo $dari?>">
								</div>
								<div class="form-group">
									<label>Sampai Tanggal</label>
									<input type="text" name="sampai" id="datepicker1" class="form-control" required="" value="<?php echo $sampai?>">
								</div>
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Status</label>
									<select class="form-control select2" name="aktif" style="width: 100%;" required="">
										<option value="1" <?php if($status == "1"){echo "selected";}?>>Aktif</option>
										<option value="2" <?php if($status == "2"){echo "selected";}?>>Tidak Aktif</option>

									</select>
								</div>	
								
								<!-- /.form-group -->
							</div>
				<div class="col-md-12">
								<label>Isi</label>
								<textarea class="form-control" rows="3" name="isi" placeholder="Enter ..."><?php echo $isi?></textarea>
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<?php 
				$nama_modulnya = 'news';
				include '../headfoot/history.php';
				?>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>
