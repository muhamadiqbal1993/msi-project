<?php
$nama = "";
$kode = "";
$pic = "";
$alamat = "";
$alamat2 = "";
$status = "";
$phone = "";
$email = "";

$sql1=mysql_query("select * from client where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$nama = $data1['nama'];
	$kode = $data1['kode'];
	$pic = $data1['pic'];
	$alamat = $data1['alamat'];
	$alamat2 = $data1['alamat2'];
	$status = $data1['status'];
	$phone = $data1['phone'];
	$email = $data1['email'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Kode</label>
									<input type="hidden" name="input" class="form-control" value="2">
									<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
									<input type="text" name="kode" class="form-control" required="" value="<?php echo $kode;?>">
								</div>	
								<div class="form-group">
									<label>Perusahaan</label>
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $nama;?>">
								</div>	
								<div class="form-group">
									<label>Phone</label>
									<input type="text" name="phone" class="form-control" required="" value="<?php echo $phone;?>">
								</div>
								<div class="form-group">
									<label>Email</label>
									<input type="text" name="email" class="form-control" required="" value="<?php echo $email;?>">
								</div>
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>PIC</label>
									<input type="text" name="pic" class="form-control" required="" value="<?php echo $pic;?>">
								</div>
								<div class="form-group">
									<label>Alamat</label>
									<input type="text" name="alamat" class="form-control" required="" value="<?php echo $alamat;?>">
								</div>	
								<div class="form-group">
									<label>Alamat2</label>
									<input type="text" name="alamat2" class="form-control" required="" value="<?php echo $alamat2;?>">
								</div>	
								<div class="form-group">
									<label>Status</label>
									<select class="form-control select2" name="aktif" style="width: 100%;" required="">
										<option value="1" <?php if($status == "1"){echo "selected";}?>>Aktif</option>
										<option value="2" <?php if($status == "2"){echo "selected";}?>>Tidak Aktif</option>

									</select>
								</div>	
								
								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>