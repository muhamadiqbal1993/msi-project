<?php
$r_date = "";
$tgl_dari = date('Y-m-d').' 00:00:00';
$tgl_sampai = date('Y-m-d').' 23:59:00';
if(!empty($_POST['r_date']))
{
  $r_date = $_POST['r_date'];
  $tgl_dari = explode("/", explode(" - ", $_POST['r_date'])[0])[2].'-'.explode("/",explode(" - ", $_POST['r_date'])[0])[0].'-'.explode("/", explode(" - ", $_POST['r_date'])[0])[1].' 00:00:00';
  $tgl_sampai = explode("/", explode(" - ", $_POST['r_date'])[1])[2].'-'.explode("/", explode(" - ", $_POST['r_date'])[1])[0].'-'.explode("/", explode(" - ", $_POST['r_date'])[1])[1].' 23:59:00';
}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Report <?php echo $modulnya;?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Report Ledger</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="../excel/trial_balance?id=1&dari=<?php echo $tgl_dari?>&sampai=<?php echo $tgl_sampai?>"><button type="submit" class="btn btn-success">Export To Excel</button></a>

            <hr class="abu">
          </div>
          <div class="box-body">
            <div class="col-xs-12">
              <div class="row">
                <form action="." id="search_data" method="POST"> 
                  <div class="col-xs-3">
                    <div class="form-group">
                      <label>Periode</label>
                      <input type="text" name="r_date" id="reservation" class="form-control" value="<?php echo $r_date?>">
                    </div>
                  </div>
                  <div class="col-xs-5">
                    <br>
                    <button type="submit" class="btn btn-primary">Search</button>
                  </div>

                </form>
              </div>
            </div>
            <div class="col-xs-12">
              <hr class="abu">
            </div>
            <div class="col-xs-12">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align: center;background-color: LightGray">Account</th>
                    <th style="background-color: LightGray">Last</th>
                    <th style="background-color: LightGray">Debit</th>
                    <th style="background-color: LightGray">Credit</th>
                    <th style="background-color: LightGray">This</th>
                  </tr>


                </thead>
                <tbody>
                  <?php 
                  $s_total_debit = 0;
                  $s_total_credit = 0;
                  $s_total_balance = 0;
                  $limits = "tanggal between '".$tgl_dari."' and '".$tgl_sampai."'";
                  $spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                  $sql1=mysql_query("select * from coa where id not in (select parent from coa) order by id asc");
                  while($data1=mysql_fetch_array($sql1))
                  {  

                    
                    $nilaiSaldoAwal = mysql_fetch_array(mysql_query("select sum(debit-abs(credit)) as totalSemua from jurnal_item where account ='".$data1['id']."' and tanggal < '$tgl_dari';
"));


                    $nilaiDebitCredit = mysql_fetch_array(mysql_query("select sum(debit) as totalDebit,sum(credit) as totalCredit from jurnal_item where account ='".$data1['id']."' and tanggal between '$tgl_dari' and '$tgl_sampai'
"));

                    $nilaiBalance = ($nilaiSaldoAwal['totalSemua']+$nilaiDebitCredit['totalDebit']) - str_replace("-", "", $nilaiDebitCredit['totalCredit']);
                    echo '
                    <tr>
                    <td><strong>'.$data1['id'].' - '.$data1['nama'].'</strong></td>
                    <td><strong>'.number_format($nilaiSaldoAwal['totalSemua']).'</strong></td>
                    <td><strong>'.number_format($nilaiDebitCredit['totalDebit']).'</strong></td>
                    <td><strong>'.number_format($nilaiDebitCredit['totalCredit']).'</strong></td>
                    <td><strong>'.number_format($nilaiBalance).'</strong></td>
                    </tr>
                    ';

                  }

                  ?>
                </tbody>
              </table>
            </div>

            <br>
            <br>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>

  <script>
    function save_modul()
    {
      document.getElementById("create_data").action = ".";
    }
  </script>
