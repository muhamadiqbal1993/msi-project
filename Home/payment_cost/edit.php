<?php
$dataEdit = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(tanggal, '%m/%d/%Y') as new_tanggal from payment_cost where no = '$id'"));
$dataPembayaran = mysql_fetch_array(mysql_query("select * from mutasi_detail where no = '".$dataEdit['pembayaran']."'"));
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Edit <?php echo $modulnya;?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Edit Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
            &nbsp;&nbsp;&nbsp;
            <?php if($dataEdit['status'] != "3"){?>
              <!-- <button type="button" onclick="cekData()" class="btn btn-primary">Simpan</button> -->
              
              <a href="F_deleteData.php?id=<?php echo $id?>"><button type="submit" class="btn btn-danger" style="float: right;margin-left: 10px;">Cancel Payment</button></a>
              <a href="../function/laporan/voucher_out/?id=<?php echo $id?>" target="_blank"><button type="submit" class="btn btn-success" style="float: right;">Download PDF</button></a>
            <?php }?>
            <hr class="abu">
          </div>
          <script type="text/javascript">
            function cekData()
            {
              var pembayaran = document.getElementById("payment").value.split("#")[1];
              var total = document.getElementById("subtotal").value;
              if(parseInt(pembayaran) == parseInt(total))
              {
                document.getElementById("create_data").submit();
              }
              else
              {
                alert("Total Pembayaran dengan Total Pengeluaran tidak sama, periksa kembali Cost Line anda");
              }
              
            }
          </script>
          <form id="create_data" method="POST"> 
            <input type="hidden" name="input" class="form-control" value="2">
            <input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
            <input type="hidden" name="kode" class="form-control" required="" value="<?php echo $dataEdit['no_payment'];?>">
            <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">
            <input type="hidden" name="subtotal" id="subtotal" class="form-control" value="<?php echo $dataEdit['total'];?>">
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label>No Payment</label>
                  <input type="text" name="kode" class="form-control" value="<?php echo $dataEdit['no_payment'];?>" readonly>
                </div>
                <div class="form-group">
                  <label>Tanggal</label>
                  <input type="text" name="tanggal" id="datepicker1" class="form-control" value="<?php echo $dataEdit['new_tanggal'];?>">
                </div>
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">

                  <label>Kasbank</label>
                  <select class="form-control select2" name="akun" id="akun" style="width: 100%;" required="">
                     <option value="">--- Pilih Kasbank ---</option>

                    <?php
                    $sql1=mysql_query("select * from kasbank where status = '1' ");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      ?>
                      <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $dataEdit['coa']){echo "selected";}?>><?php echo $data1['no_acc'].' - '.$data1['nama_acc']?></option>
                      <?php 
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Keterangan</label>
                  <input type="text" name="keterangan" class="form-control" value="<?php echo $dataEdit['keterangan'];?>">
                </div>
                <!-- /.form-group -->
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <hr class="abu">
                    <h1><?php echo $modulnya.' Line';?></h1>
                    <hr class="abu">
                  </div>
                  <div class="col-md-12">
                    <table id="tabDidik" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th style="width: 5%" >No</th>
                          <th style="width: 20%">Account</th>
                          <th>Keterangan</th>
                          <th>Qty</th>
                          <th>Price</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        $noms = 1;
                        $sql1=mysql_query("select * from payment_cost_detail where type = 'payment_cost' and id_transaksi = '".$dataEdit['no_payment']."'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          $accr = '<select class="form-control select2" name="coa[]" id="coa'.$noms.'" style="width: 100%;" required="">';

                          $sql12=mysql_query("select * from coa where status = '1'");
                          while($data12=mysql_fetch_array($sql12))
                          {
                            $slct = "";
                            if($data1['account'] == $data12['id'])
                            {
                              $slct = "selected";
                            }
                            $accr .= '<option value="'.$data12['id'].'" '.$slct.'>'.$data12['id'].' - '.$data12['nama'].'</option>';
                          }
                          $accr .= '</select>';
                          echo '
                          <tr>
                          <td>'.$noms.'</td>
                          <td>'.$accr.'</td>
                          <td><input type="text" name="editketerangan[]" class="form-control" value="'.$data1['ref'].'"></td>
                          <td><input type="number" name="editQty[]" id="editQty'.$noms.'" oninput="ubahTotal()" class="form-control" value="'.$data1['status'].'"></td>
                          <td><input type="number" name="editPrice[]" id="editPrice'.$noms.'" oninput="ubahTotal()" class="form-control" value="'.$data1['credit'].'"></td>  
                          <td><button type="button" onclick="delete_row_didik('.$noms.')" class="btn btn-danger">Delete</button></td>
                          </tr>
                          ';
                          $noms++;
                        }
                        ?>
                        <tr>
                          <td colspan="6" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button><span style="float: right;"><h4 style="color: white" id="total"><strong> Total : Rp. <?php echo number_format($dataEdit['total'])?></strong></h4></span></td>
                        </tr>
                      </tbody>
                      <script language="javascript" type="text/javascript">

                        function getProduct(abs)
                        {
                          var products = '<select class="form-control select2" name="coa[]" id="coa'+abs+'" style="width: 100%;" required="">'+
                          <?php

                          $sql1=mysql_query("select * from coa where status = '1'");
                          while($data1=mysql_fetch_array($sql1))
                          {
                            ?>
                            '<option value="<?php echo $data1['id'];?>"><?php echo $data1['id'].' - '.$data1['nama']?></option>'+

                            <?php 
                          }?>

                          '</select>';

                          return products;
                        }

                        function delete_row_didik(abs)
                        {
                          document.getElementById("tabDidik").deleteRow(abs);
                          var rowCount = document.getElementById('tabDidik').rows.length - 1;
                          var table = document.getElementById('tabDidik');
                          for(var a=1;a<rowCount;a++)
                          {
                            table.rows[a].cells[0].innerHTML = a;
                            table.rows[a].cells[5].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
                          }
                          ubahTotal();
                        }

                        function addLine_didik()
                        { 
                          var rowCount = document.getElementById('tabDidik').rows.length - 1;
                          var table = document.getElementById('tabDidik');
                          //alert(rowCount);
                          var row = table.insertRow(rowCount);
                          var cell0 = row.insertCell(0);
                          var cell1 = row.insertCell(1);
                          var cell2 = row.insertCell(2);
                          var cell3 = row.insertCell(3);
                          var cell4 = row.insertCell(4);
                          var cell5 = row.insertCell(5);
                          cell0.innerHTML = rowCount;
                          cell1.innerHTML = getProduct(rowCount);
                          cell2.innerHTML = '<input type="text" name="editketerangan[]" class="form-control">';
                          cell3.innerHTML = '<input type="number" name="editQty[]" id="editQty'+rowCount+'" oninput="ubahTotal()" class="form-control" value="0">';
                          cell4.innerHTML = '<input type="number" name="editPrice[]" id="editPrice'+rowCount+'" oninput="ubahTotal()" class="form-control" value="0">';
                          cell5.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

                          var namaField = "#coa"+rowCount;
                          $(namaField).select2();
                        }

                        function ubahTotal()
                        {
                          var rowCount = document.getElementById('tabDidik').rows.length - 1;
                          var table = document.getElementById('tabDidik');
                          var total = 0;
                          for(var a=1;a<rowCount;a++)
                          {
                            var editQty = 'editQty'+a;
                            var editPrice = 'editPrice'+a;
                            var qty = document.getElementById(editQty).value;
                            var harga = document.getElementById(editPrice).value;
                            total = parseInt(total) + (parseInt(qty) * parseInt(harga));
                            //alert(qty+" - "+harga);
                          }
                          document.getElementById("subtotal").value = total;
                          document.getElementById('total').innerHTML = "<strong>Total : Rp. "+total.toLocaleString()+"</strong>";
                        }
                      </script>
                    </table>
                  </div>
                </div>
              </div>
              <!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
            </div>
          </form>
          <br>
          <br>
          <!-- /.box-body -->
        </div>
        <?php 
        $nama_modulnya = 'payment_cost';
        include '../headfoot/history.php';
        ?>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
  function bukaPassword()
  {
    var aa = document.getElementById("bukaPas").readOnly;
    if(aa)
    {
      document.getElementById("bukaPas").readOnly = false;
    }
    else
    {
      document.getElementById("bukaPas").readOnly = true;
    }
  }
</script>
