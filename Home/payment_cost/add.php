<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Tambah <?php echo $modulnya;?> Baru</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
            &nbsp;&nbsp;&nbsp;
            <button type="submit" form="create_data" class="btn btn-primary">Simpan</button>
            <hr class="abu">
          </div>

          <form id="create_data" method="POST"> 
            <input type="hidden" name="input" class="form-control" value="1">
            <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
            
            <input type="hidden" name="subtotal" id="subtotal" class="form-control"> 
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kode Transaksi</label>
                  <input type="text" name="kode" class="form-control" value="<?php echo date('ymdHis')?>" readonly> 
                </div>
                <div class="form-group">
                  <label>Tanggal</label>
                  <input type="text" name="tanggal" id="datepicker1" class="form-control" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kasbank</label>
                  <select class="form-control select2" name="akun" id="akun" style="width: 100%;" required="">
                    <option value="">--- Pilih Kasbank ---</option>

                   <?php
                   $sql1=mysql_query("select * from kasbank where status = '1' ");
                   while($data1=mysql_fetch_array($sql1))
                   {
                    ?>
                    <option value="<?php echo $data1['no']?>" ><?php echo $data1['no_acc'].' - '.$data1['nama_acc']?></option>
                    <?php 
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label>Keterangan</label>
                <input type="text" name="keterangan" class="form-control" >
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-12">
                  <hr class="abu">
                  <h1><?php echo $modulnya.' Line';?></h1>
                  <hr class="abu">
                </div>
                <div class="col-md-12">
                  <table id="tabDidik" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width: 5%" >No</th>
                        <th style="width: 20%">Account</th>
                        <th>Keterangan</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <thead>
                      <tr>
                        <th colspan="7" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button><span style="float: right;"><h4 style="color: white" id="total"><strong> Total : Rp. 0</strong></h4></span></th>
                      </tr>
                    </thead>
                    <script language="javascript" type="text/javascript">

                      function getProduct(abs)
                      {
                        var products = '<select class="form-control select2" name="coa[]" id="coa'+abs+'" style="width: 100%;" required="">'+
                        <?php

                        $sql1=mysql_query("select * from coa where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          '<option value="<?php echo $data1['id'];?>"><?php echo $data1['id'].' - '.$data1['nama']?></option>'+

                          <?php 
                        }?>

                        '</select>';

                        return products;
                      }

                      function delete_row_didik(abs)
                      {
                        document.getElementById("tabDidik").deleteRow(abs);
                        var rowCount = document.getElementById('tabDidik').rows.length - 1;
                        var table = document.getElementById('tabDidik');
                        for(var a=1;a<rowCount;a++)
                        {
                          table.rows[a].cells[6].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
                          ubahTotal();                          
                        }
                        ubahTotal();
                      }

                      function addLine_didik()
                      { 
                        var rowCount = document.getElementById('tabDidik').rows.length - 1;
                        var table = document.getElementById('tabDidik');
                        var row = table.insertRow(rowCount);
                        var cell0 = row.insertCell(0);
                        var cell1 = row.insertCell(1);
                        var cell2 = row.insertCell(2);
                        var cell3 = row.insertCell(3);
                        var cell4 = row.insertCell(4);
                        var cell5 = row.insertCell(5);
                        var cell6 = row.insertCell(6);

                        cell0.innerHTML = rowCount;
                        cell1.innerHTML = getProduct(rowCount);
                        cell2.innerHTML = '<input type="text" name="editketerangan[]" class="form-control">';
                        cell3.innerHTML = '<input type="number" name="editQty[]" id="editQty'+rowCount+'" oninput="ubahTotal()" class="form-control" value="0">';
                        cell4.innerHTML = '<input type="number" name="editPrice[]" id="editPrice'+rowCount+'" oninput="ubahTotal()" class="form-control" value="0">';
                        cell5.innerHTML = '<input type="text" name="editPrice[]" id="editTotal'+rowCount+'" disabled class="form-control" value="0">';
                        cell6.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

                        var namaField = "#coa"+rowCount;
                        $(namaField).select2();
                      }

                      function ubahTotal()
                      {
                        var rowCount = document.getElementById('tabDidik').rows.length - 1;
                        var table = document.getElementById('tabDidik');
                        var total = 0;
                        for(var a=1;a<rowCount;a++)
                        {
                          var nomorIds = table.rows[a].cells[0].innerHTML;
                          var editQty = 'editQty'+nomorIds;
                          var editPrice = 'editPrice'+nomorIds;
                          var editTotal = 'editTotal'+nomorIds;
                          var qty = document.getElementById(editQty).value;
                          var harga = document.getElementById(editPrice).value;
                          let hasilNya = parseInt(qty) * parseInt(harga);
                          document.getElementById(editTotal).value = hasilNya.toLocaleString();
                          total = parseInt(total) + hasilNya;
                            //alert(qty+" - "+harga);
                        }
                        document.getElementById("subtotal").value = total;
                        document.getElementById('total').innerHTML = "<strong>Total : Rp. "+total.toLocaleString()+"</strong>";
                      }
                    </script>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </form>
        <br>
        <br>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
</script>
