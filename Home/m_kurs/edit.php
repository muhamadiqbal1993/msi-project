<?php

$dataEdit = mysql_fetch_array(mysql_query("select * from kurs where no = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<input type="hidden" name="input" class="form-control" value="2">
							<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
							<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
							<div class="col-md-6">
								<div class="form-group">
									<label>Kode Kurs</label>
									<input type="text" name="kode" class="form-control" required="" value="<?php echo $dataEdit['kode'];?>" readonly>
								</div>	
								
								<div class="form-group">
									<label>Nama Kurs</label>
									<input type="text" name="nama" class="form-control" value="<?php echo $dataEdit['nama'];?>">
								</div>	
								<div class="form-group">
									<label>Status</label>
									<select class="form-control select2" name="aktif" style="width: 100%;" required="">
										<option value="1" <?php if($dataEdit['status'] == "1"){echo "selected";}?>>Aktif</option>
										<option value="2" <?php if($dataEdit['status'] == "2"){echo "selected";}?>>Tidak Aktif</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Logo Kurs</label>
									<input type="text" name="logo" class="form-control" required="" value="<?php echo $dataEdit['logo'];?>">
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" name="keterangan" class="form-control" required="" value="<?php echo $dataEdit['keterangan'];?>">
								</div>
								
								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<?php 
				$nama_modulnya = 'kurs';
				include '../headfoot/history.php';
				?>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>


