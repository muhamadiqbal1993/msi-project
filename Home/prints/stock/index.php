<?php
include '../../function/init.php';
$brand = "";
$rak = "";
$gudang = "";
$kodnamket = "";
$wheres = "";

$namGud = "Semua";
$namRak = "Semua";
$namBrand = "Semua";

if(!empty($_GET['nama']))
{
	$kodnamket = $_GET['nama'];
	$wheres .= " and P.nama like '%$kodnamket%'";
}
if(!empty($_GET['brand']))
{
	$brand = $_GET['brand'];
	$wheres .= " and B.no = '$brand'";
}
if(!empty($_GET['gudang']))
{
	$gudang = $_GET['gudang'];
	$wheres .= " and R.gudang = '$gudang'";
}
if(!empty($_GET['rak']))
{
	$rak = $_GET['rak'];
	$wheres .= " and rak = '$rak'";
}
?>
<html>
<head>
	<title>Struk Pembayaran</title>
	<style>

		#tabel
		{
			font-size:15px;
			border-collapse:collapse;
		}
		#tabel  td
		{
			padding-left:5px;
			border: 1px solid black;
		}
	</style>
</head>
<body style='font-family:tahoma; font-size:8pt;'>
	<center>
		<table style='width:350px; font-size:15pt; font-family:calibri; border-collapse: collapse;' border = '0'>
			<tr>
				<td width='70%' align='CENTER' vertical-align:top'><span style='color:black;'>
					<b></b></br><span style="font-size:12pt"><strong>Laporan Stock</strong></span></span></br>
				</td>
			</tr>

		</table>
		<style>
			hr { 
				display: block;
				margin-top: 0.5em;
				margin-bottom: 0.5em;
				margin-left: auto;
				margin-right: auto;
				border-style: inset;
				border-width: 1px;
			} 
		</style>

		<table cellspacing='0' cellpadding='0' style='width:350px; font-size:12pt; font-family:calibri;  border-collapse: collapse;' border='0'>
			<tr align='center'>
				<td colspan='4'><hr></td>
			</tr>
			<tr>
				<td style='vertical-align:top;padding-top: 10px'>Gudang</td>
				<td style='text-align:right; vertical-align:top;padding-top: 10px'><?php echo $namGud?></td>
			</tr>
			<tr>
				<td style='vertical-align:top;padding-top: 10px'>Rak</td>
				<td style='text-align:right; vertical-align:top;padding-top: 10px'><?php echo $namRak?></td>
			</tr>

			<tr>
				<td style='vertical-align:top;padding-top: 10px'>Brand</td>
				<td style='text-align:right; vertical-align:top;padding-top: 10px'><?php echo $namBrand?></td>
			</tr>

			<tr align='center'>
				<td colspan='4'><hr></td></tr>
				<td width='80%'>Product</td>
				<td width='20%' style="text-align: right;">Stock</td><tr>
					<td colspan='4'><hr></td></tr>
				</tr>
				<?php
				$totalsemua = 0;
				$sql1=mysql_query("select P.*,R.nama as namrak,B.nama as nambrand from product P
					inner join rak R on R.no = P.rak
					inner join brand B on B.no = P.kategory
					where P.status = '1' $wheres limit 200");
				while($data1=mysql_fetch_array($sql1))
				{


					?>
					<tr>
						<td style='vertical-align:top;padding-top: 10px'><?php echo $data1['nama'];?></td>
						<td style='text-align:right; vertical-align:top;padding-top: 10px'><?php echo number_format($data1['type']);?></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan='4'><hr></td>
				</tr>
			</table>

			<table style='width:350px; font-size:15pt; font-family:calibri; border-collapse: collapse;'>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td style="font-size:12pt"><?php echo $user_data['nama']?></td>
					<td style="text-align: right;font-size:12pt"><?php echo date('d-m-Y H:i:s')?></td>
				</tr>

			</table>


		</center>
		<script type="text/javascript">
			window.print();
		</script>
	</body>
	</html>
