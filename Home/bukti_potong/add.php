<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Tambah <?php echo $modulnya;?> Baru</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
            &nbsp;&nbsp;&nbsp;
            <button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Upload</button>
            <hr class="abu">
          </div>
          <form id="create_data" method="POST" enctype="multipart/form-data">
            <div class="box-body">
              <input type="hidden" name="input" class="form-control" value="2">
              <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
              <div class="col-md-12">

                <div class="form-group">
                  <label><strong>Pilih File</strong></label>
                  <input type="file" name="photos" id="photos" required>
                </div> 
                <div class="form-group">
                  <label>Account</label>
                  <select class="form-control select2" name="bill[]"  multiple="multiple" style="width: 100%;" required="">
                    <?php
                    $sql1=mysql_query("select I.*,BP.no as bukti,
                      DATE_FORMAT(I.tanggal, '%d %b %Y') as tgls,
                      DATE_FORMAT(I.due, '%d %b %Y') as dues,
                      C.nama as namcus from bill I
                      inner join vendor C on C.no = I.vendor
                      left outer join bukti_potong BP on BP.id_transaksi = I.bill
                      where I.status != '5' and BP.no is null");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      ?>
                      <option value="<?php echo $data1['bill']?>" ><?php echo $data1['bill'].' - '.$data1['namcus']?></option>
                      <?php 
                    }
                    ?>
                  </select>
                </div>
                
              </div>

            </div>
          </form>
          <br>
          <br>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
</script>
