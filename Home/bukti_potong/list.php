<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Bulk Upload Bukti Potong</button>
            </form>
            <hr class="abu">
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
              Data Berhasil Dibuat.
              </div>
              </div>';
            }
            if($status == "2")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
              </div>
              </div>';
            }
            if($status == "3")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Warning!</h4>
              '.$modulnya.' sudah ada yang menggunakan , silahkan ganti dengan data yang berbeda
              </div>
              </div>';
            }
            ?>


            <div class="col-xs-12">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No Purchase Order</th>
                    <th>Vendor</th>
                    <th>Tanggal</th>
                    <th>Nilai</th>
                    <th>Status</th>
                    <th>File Bukti Potong</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  $sql1=mysql_query("select I.*,BP.no as bukti,BP.bukti as file_bukti,
                    DATE_FORMAT(I.tanggal, '%d %b %Y') as tgls,
                    DATE_FORMAT(I.due, '%d %b %Y') as dues,
                    C.nama as namcus from bill I
                    inner join vendor C on C.no = I.vendor
                    left outer join bukti_potong BP on BP.id_transaksi = I.bill
                    where I.status != '5'");
                  while($data1=mysql_fetch_array($sql1))
                  {  
                    $status = "<span style='color:red'><strong>Belum Upload</strong></span>";
                    $upls = '<a href="#" data-toggle="modal" data-target="#popUpload'.$data1['no'].'">Upload Bukti</a>';
                    $file = "";
                    if($data1['bukti'] != "")
                    {
                     $status = "<span style='color:green'><strong>Sudah Upload</strong></span>";
                     $file = "<a href='../images/".$data1['file_bukti']."' target='_blank'>Download File Bukti</a>";
                     $upls = "<a href='F_deleteData.php?id=".$data1['bill']."' style='color:red'>Hapus Bukti</a>";
                   } 
                   
                   echo '
                   <tr>
                   <td>'.$no.'</td>  
                   <td>'.$data1['bill'].'</td>
                   <td>'.$data1['namcus'].'</td>
                   <td>'.$data1['tgls'].'</td>
                   <td>'.number_format($data1['grandtotal']).'</td>
                   <td>'.$status.'</td>   
                   <td>'.$file.'</td>      
                   <td>'.$upls.'</td>
                   </tr>
                   ';
                   $no++;
                 }
                 ?>
               </tbody>
             </table>
           </div>

         </div>
         <br>
         <br>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </section>

</div>

<?php 

$sql1=mysql_query("select I.*,BP.no as bukti,
  DATE_FORMAT(I.tanggal, '%d %b %Y') as tgls,
  DATE_FORMAT(I.due, '%d %b %Y') as dues,
  C.nama as namcus from bill I
  inner join vendor C on C.no = I.vendor
  left outer join bukti_potong BP on BP.id_transaksi = I.bill
  where I.status != '5' and BP.no is null
  ");
while($data1=mysql_fetch_array($sql1))
{ 
  ?>

  <div class="modal fade" id="popUpload<?php echo $data1['no']?>">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title"><strong><?php echo 'Upload Bukti Potong '.$data1['bill'];?></strong></h5>
          </div>

          <div class="modal-body">
            <form id="create_data" action="." method="POST" enctype="multipart/form-data">
              <input type="hidden" name="input" class="form-control" value="1">
              <input type="hidden" name="bill" class="form-control" value="<?php echo $data1['bill']?>">
              <div class="form-group">
                <label><strong>Pilih File</strong></label>
                <input type="file" name="photos" id="photos" required>
              </div> 
            </form>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="submit" form="create_data" class="btn btn-primary pull-right" >Simpan</button>
          </div>


        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <?php 
  }
  ?>
