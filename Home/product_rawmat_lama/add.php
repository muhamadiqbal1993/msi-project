<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="hidden" name="input" class="form-control" value="1">

									<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
									<input type="text" name="nama" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Telepon</label>
									<input type="number" name="phone" class="form-control" >
								</div>
								<div class="form-group">
									<label>Group User</label>
									<select class="form-control select2" name="group" style="width: 100%;" required="">
										<?php
										$sql1=mysql_query("select * from group_divisi where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['nama']?></option>

											<?php }?>

										</select>
									</div>
									<!-- /.form-group -->

									<!-- /.form-group -->
								</div>
								<!-- /.col -->
								<div class="col-md-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" name="email" class="form-control" >
									</div>
									<div class="form-group">
										<label>Username</label>
										<input type="text" name="username" class="form-control" required="">
									</div>
									<!-- /.form-group -->
									<div class="form-group">
										<label>Password</label>
										<input type="text" name="password" class="form-control" required="">
									</div>
									<!-- /.form-group -->
								</div>
								<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
							</div>
						</form>
						<br>
						<br>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
	</div>

	<script>
		function save_modul()
		{
			document.getElementById("create_data").action = ".";
		}
	</script>