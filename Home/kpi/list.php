<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <?php if($addPM == "1"){?>
             <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu"> 
            <?php }?>
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            Username sudah ada yang menggunakan , silahkan daftarkan dengan username lain
          </div>
        </div>';
      }
      ?>


      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Karyawan</th>
              <th>Tgl Penilaian</th>
              <th>Nilai</th>
              <th>Periode</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $sql1 = mysql_query("select *,DATE_FORMAT(kpi_tgl_penilaian, '%d %M %Y') as new_tanggal,DATE_FORMAT(kpi_periode_start, '%d %M %Y') as daris,DATE_FORMAT(kpi_periode_end, '%d %M %Y') as sampais from kpi");
            while($data1=mysql_fetch_array($sql1))
            {  
              $karyawan = mysql_fetch_array(mysql_query("select * from karyawan where no='".$data1['kpi_employee']."'"));

              echo '
              <tr>
                <td>'.$karyawan['nama'].'</td>
                <td>'.$data1['new_tanggal'].'</td>
                <td>'.$data1['kpi_nilai_sp'].'</td>
                <td>'.$data1['daris'].' -'.$data1['sampais'].'</td>     
                <td><a href="./?id='.$data1['kpi_id'].'">Manage</a></td>
              </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>