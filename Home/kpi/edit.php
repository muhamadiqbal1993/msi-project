<?php


$dataEdit = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(kpi_periode_start, '%m/%d/%Y') as daris,DATE_FORMAT(kpi_periode_end, '%m/%d/%Y') as sampais from kpi where kpi_id = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						
						<div class="box-body">
							<div class="col-md-6">

							<!-- <div class="form-group">
								<label>Kode</label>
								<input type="text" name="kode" class="form-control" required="" value="<?php echo $kode?>" readonly>
							</div> -->

							<div class="form-group">
								<label>Karyawan</label>
								<select class="form-control select2" name="employee" style="width: 100%;" required="">
									<option value="" >--- Pilih Karyawan ---</option>
									<?php
									$sql1=mysql_query("select * from karyawan");
									while($data1=mysql_fetch_array($sql1))
									{
										?>
										<option value="<?php echo $data1['no']?>" <?php if($dataEdit['kpi_employee'] == $data1['no']){echo "selected";}?>><?php echo $data1['nama']?> - <?php echo $data1['kode']?></option>

										<?php 
									}
									?>

								</select>
							</div>
							<div class="form-group">
								<label>Periode</label>
								<div class="row">
									<div class="col-md-6"><input type="text" name="periode_start" id="datepicker1" placeholder="Mulai Periode" class="form-control" value="<?php echo $dataEdit['daris']?>" required></div>
									<div class="col-md-6"><input type="text" name="periode_end" id="datepicker" placeholder="Berakhir Periode" class="form-control" value="<?php echo $dataEdit['sampais']?>" required></div>
								</div>
							</div>
						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Atasan Langsung</label>
								<select class="form-control select2" name="atasan" style="width: 100%;" required="">
									<option value="" >--- Pilih Karyawan ---</option>
									<?php
									$sql1=mysql_query("select * from karyawan");
									while($data1=mysql_fetch_array($sql1))
									{
										?>
										<option value="<?php echo $data1['no']?>" <?php if($dataEdit['kpi_atasan'] == $data1['no']){echo "selected";}?>><?php echo $data1['nama']?> - <?php echo $data1['kode']?></option>

										<?php 
									}
									?>

								</select>
							</div>
						</div>
						

						<div class="col-md-12">
							<hr class="abu">
							<h3><strong>Aspek Penilaian</strong></h3>
							<hr class="abu">

							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Aspek Penilaian</th>
										<th>Indikator Penilaian</th>
										<th>Target</th>
										<th style="width: 10%;">Nilai</th>
										<th style="width: 40%;">Keterangan</th>
									</tr>
								</thead>
								<tbody>
									<?php 

									$sql1=mysql_query("select * from kpi_indikator where status = '1' order by aspek asc");
									while($data1=mysql_fetch_array($sql1))
									{  
										$dataNilai = mysql_fetch_array(mysql_query("select * from kpi_detail where kpi_id = '$id' and kpi_indikator = '".$data1['no']."'"));

										$aspek = "Aspek Individu";
										if($data1['aspek'] == "2")
										{
											$aspek = "Aspek Kinerja";
										}
										echo '
										<tr>
										<td>'.$aspek.'</td>
										<td>'.$data1['nama'].'</td>
										<td><input type="text" name="d_target[]" class="form-control" value="'.$dataNilai['kpi_target'].'" readonly></td>
										<td>
										<input type="number" name="d_nilai[]" class="form-control" value="'.$dataNilai['nilai'].'">
										<input type="hidden" name="d_kode[]" class="form-control" value="'.$data1['no'].'">
										</td>      
										<td><input type="text" name="d_komentar[]" class="form-control" value="'.$dataNilai['komentar'].'"></td>
										</tr>
										';
									}
									?>
								</tbody>
							</table>

						</div>
					</div>
				</form>
				<br>
				<br>
				<!-- /.box-body -->
			</div>

			<?php 
			$nama_modulnya = 'kpi';
			include '../headfoot/history.php';
			?>

			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>