<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<input type="hidden" name="input" class="form-control" value="1">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">
								<div class="form-group">
									<label>Bulan</label>
									<select class="form-control select2" name="bulan" style="width: 100%;" required="">
										<option value="Januari" >Januari</option>
										<option value="Februari">Februari</option>
										<option value="Maret" >Maret</option>
										<option value="April" >April</option>
										<option value="Mei" >Mei</option>
										<option value="Juni" >Juni</option>
										<option value="Juli" >Juli</option>
										<option value="Agustus" >Agustus</option>
										<option value="September" >September</option>
										<option value="Oktober" >Oktober</option>
										<option value="November" >November</option>
										<option value="Desember" >Desember</option>
									</select>
								</div>
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								
								<div class="form-group">
									<label>Tahun</label>
									<select class="form-control select2" name="tahun" style="width: 100%;" required="">
										<option value="2023" >2023</option>
										<option value="2024">2024</option>
										<option value="2025" >2025</option>
										<option value="2026" >2026</option>
										<option value="2027" >2027</option>
										<option value="2028" >2028</option>
										<option value="2029" >2029</option>
										<option value="2030" >2030</option>
										<option value="2031" >2031</option>
										<option value="2032" >2032</option>
										<option value="2033" >2033</option>
										<option value="2034" >2034</option>
									</select>
								</div>
								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>