<?php
$dataEdit = mysql_fetch_array(mysql_query("select * from slip_gaji where no = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
				
						<hr class="abu">
					</div>
					
					<div class="box-body">
						<form id="create_data" method="POST"> 
							<div class="col-md-6">
								<input type="hidden" name="input" class="form-control" value="2">
								<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
								<div class="form-group">
									<label>Bulan</label>
									<input type="text" name="bulan" class="form-control" required="" value="<?php echo $dataEdit['bulan']?>" readonly>
								</div>
								
								
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Tahun</label>
									<input type="text" name="tahun" class="form-control" required="" value="<?php echo $dataEdit['tahun']?>" readonly>
								</div>
								<!-- /.form-group -->
							</div>
						</form>
						<div class="col-md-12">
							<hr>
							<h3>Data Karyawan</h3>
						</div>
						<div class="col-xs-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Kode Karyawan</th>
										<th>Nama Karyawan</th>
										<th>Total Gaji</th>
										<th>Print Slip Gaji</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$noms = 1;
									$sql1=mysql_query("select K.*,SGK.total from slip_gaji_kar SGK
										inner join karyawan K on K.no = SGK.id_kar
										where SGK.id_slip = '".$dataEdit['id']."'");
									while($data1=mysql_fetch_array($sql1))
									{  
										echo '
										<tr>
										<td>'.$data1['kode'].'</td>
										<td>'.$data1['nama'].'</td>  
										<td>'.number_format($data1['total']).'</td>
										<td><a href="../function/laporan/slip_gaji/?id='.$data1['no'].'" target="_blank">Print Slip Gaji</a></td>
										<td><a href="./?ids='.$data1['no'].'">View Detail</a></td>
										</tr>
										';
									}
									?>
								</tbody>
							</table>
						</div>
						<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->

					</div>
					

					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'komponen';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>