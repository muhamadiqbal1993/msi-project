<?php
$dataEditDetail = mysql_fetch_array(mysql_query("select SGK.*,K.nama as namkar,K.kode as kodkar from slip_gaji_kar SGK
	inner join karyawan K on K.no = SGK.id_kar
	where SGK.no = '$id'"));
$dataEdit = mysql_fetch_array(mysql_query("select * from slip_gaji where id = '".$dataEditDetail['id_slip']."'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<button type="submit" onclick="history.back()" class="btn btn-warning">Kembali</button>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<a href="../function/laporan/slip_gaji/?id=<?php echo $id?>" target="_blank"><button type="submit" class="btn btn-success" style="float: right;">Print Slip Gaji</button></a>
						<hr class="abu">
					</div>
					<?php
					if(!empty($_GET['s']))
					{
						echo '<div class="col-xs-12">
						<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-check"></i> Berhasil!</h4>
						Data Berhasil Diedit
						</div>
						</div>';
					}
					?>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<input type="hidden" name="input" class="form-control" value="2">
								<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
								<div class="form-group">
									<label>Bulan</label>
									<input type="text" name="bulan" class="form-control" required="" value="<?php echo $dataEdit['bulan']?>" readonly>
								</div>
								<div class="form-group">
									<label>Karyawan</label>
									<input type="text" name="karyawan" class="form-control" required="" value="<?php echo $dataEditDetail['kodkar'].' - '.$dataEditDetail['namkar']?>" readonly>
								</div>
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Tahun</label>
									<input type="text" name="tahun" class="form-control" required="" value="<?php echo $dataEdit['tahun']?>" readonly>
								</div>
								<!-- /.form-group -->
							</div>
							<div class="col-md-12">
								<hr>
								<h3>Detail Komponen</h3>
							</div>
							<div class="col-xs-12">
								<table id="tableKom" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Nama Komponen</th>
											<th>Type</th>
											<th>Nilai</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$noms = 1;
										$totals = 0;
										$sql1=mysql_query("select SGKD.* ,K.nama as namkom ,K.dapat,K.no as nokom from slip_gaji_kar_det SGKD
											inner join komponen K on K.no = SGKD.id_kom
											where id_slip = '".$dataEdit['id']."' and id_kar='".$dataEditDetail['id_kar']."'");
										while($data1=mysql_fetch_array($sql1))
										{  
											$tipe = "Penambah";
											if($data1['dapat'] == "2")
											{
												$tipe = "Pengurang";
												$totals -= $data1['nilai'];
											}
											else
											{
												$totals += $data1['nilai'];
											}
											echo '
											<tr>
											<td><input type="text" name="nama[]" id="nama'.$noms.'" class="form-control" required="" value="'.$data1['nokom'].' - '.$data1['namkom'].'" readonly></td>
											<td><input type="text" name="tipe[]" id="tipe'.$noms.'" class="form-control" required="" value="'.$tipe.'" readonly></td>
											<td><input type="number" name="nilai[]" id="nilai'.$noms.'" oninput="cekNilai()" class="form-control" required="" value="'.$data1['nilai'].'"></td>
											</tr>
											';
											$noms++;
										}
										?>
										<tr>
											<td style="background-color: silver;" colspan="2">Total</td>
											<td style="background-color: silver;"><input type="text" id="nilaiTotal" name="nilaiTotal" class="form-control" required="" value="<?php echo number_format($totals)?>" readonly></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</form>
					

					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'komponen';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>

	function cekNilai()
	{
		var totalSemua = 0;
		var rowCount = document.getElementById('tableKom').rows.length;
		var table = document.getElementById('tableKom');
		for(var a=1;a<rowCount;a++)
		{
			if(a != rowCount)
			{
				var t_nilai = 'nilai'+a;
				var t_tipe = 'tipe'+a;
				var nilais = document.getElementById(t_nilai).value;
				var tipe = document.getElementById(t_tipe).value;

				if(tipe == "Penambah")
				{
					totalSemua = parseInt(nilais) + parseInt(totalSemua);
				}
				else
				{
					totalSemua =parseInt(totalSemua) -  parseInt(nilais);
				}
			}
			document.getElementById("nilaiTotal").value = totalSemua.toLocaleString('en-US');
		}
		
		
	}

	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>