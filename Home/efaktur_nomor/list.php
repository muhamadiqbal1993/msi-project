<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <?php if($addPM == "1"){?>
            <a href="../efaktur_generate"><button type="button" class="btn btn-primary">Generate Nomor Efaktur</button></a>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#popDownload" style="float: right;">Download Bulk CSV</button>

            <hr class="abu">
            <?php }?>
          </div>
<!-- 
          <form method="POST" id="downloadBulk">
            <input type="hidden" name="type" value="input">
          </form>
        -->
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <?php 
          if($status == "1")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
              Data Berhasil Dibuat.
            </div>
          </div>';
        }
        if($status == "2")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
            Ada Masalah Dengan Server , Segera Hubungi Administrator.
          </div>
        </div>';
      }
      if($status == "3")
      {
        echo '<div class="col-xs-12">
        <div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> Warning!</h4>
          Folder sudah ada yang menggunakan , silahkan ganti dengan data yang berbeda
        </div>
      </div>';
    }
    ?>


    <div class="col-xs-12">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Nomor Faktur</th>
		<th>Sales Order</th>
            <th>Tanggal Terbit</th>
            <th>Nilai Transaksi</th>
            <th>Nilai Pajak</th>
            <th>Status</th>
            <th>Action</th>
            <th>Download CSV</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $noms = "1";
          $sql1=mysql_query("select E.*,DATE_FORMAT(E.tanggal_terbit, '%d %b %Y') as tgls,I.inv from efaktur E
		left outer join invoice I on I.efaktur = E.no
		order by E.no desc");
          while($data1=mysql_fetch_array($sql1))
          {  
            $status = "<strong>Free</strong>";
            if($data1['status'] == "2")
            {
              $status = "<strong style='color:blue'>Terdaftar</strong>";
            }
            if($data1['status'] == "3")
            {
              $status = "<strong style='color:green'>Sudah Upload</strong>";
            }
            if($data1['status'] == "4")
            {
              $status = "<strong style='color:red'>Batal</strong>";
            }

            $pengganti = "050.";
            if($data1['pengganti'] == "1")
            {
              $pengganti = "051.";
            }

            $d_csv = "";
            if($data1['total'] != "0")
            {
              $d_csv = '<a href="../excel/csv/?id='.$data1['no'].'" target="_blank">Download CSV</a>';
            }

            echo '
            <tr>
              <td>'.$noms.'</td>
              <td>'.$pengganti.$data1['id'].'</td>
		<td>'.$data1['inv'].'</td>
              <td>'.$data1['tgls'].'</td>
              <td>'.number_format($data1['total']).'</td>
              <td>'.number_format($data1['pajak']).'</td>
              <td>'.$status.'</td>      
              <td><a href="./?id='.$data1['no'].'">Manage</a></td>
              <td>'.$d_csv.'</td>
            </tr>
            ';
            $noms++;
          }

          ?>
        </tbody>
      </table>
    </div>

  </div>
  <br>
  <br>
  <!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>

<div class="modal fade" id="popDownload">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h5 class="modal-title"><strong>Download CSV Efaktur</strong></h5>
        </div>

        <div class="modal-body">
          <table id="dataDownload" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nomor Faktur</th>
                <th>Tanggal Terbit</th>
                <th>Nilai Transaksi</th>
                <th>Nilai Pajak</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $noms = "1";
              $dets = "";
              $sql1=mysql_query("select *,DATE_FORMAT(tanggal_terbit, '%d %b %Y') as tgls from efaktur where status = '1' and total!='0'");
              while($data1=mysql_fetch_array($sql1))
              {  
                $status = "<strong>Free</strong>";

                $pengganti = "010.";
                if($data1['pengganti'] == "1")
                {
                  $pengganti = "011.";
                }

                $d_csv = "";
                if($data1['total'] != "0")
                {
                  $d_csv = '<a href="../excel/csv/?id='.$data1['no'].'" target="_blank">Download CSV</a>';
                }

                echo '
                <tr>
                  <td>'.$noms.'</td>
                  <td>'.$pengganti.$data1['id'].'</td>
                  <td>'.$data1['tgls'].'</td>
                  <td>'.number_format($data1['total']).'</td>
                  <td>'.number_format($data1['pajak']).'</td>
                  <td><a href="#" onclick="deleteFaktur('.$noms.')">Hapus</a></td>
                </tr>
                ';
                $dets .= $data1['id']."##";
                $noms++;
              }

              ?>
            </tbody>
          </table>
        </div>

        <script type="text/javascript">
          function deleteFaktur(abs)
          {
            var dets = "";
            document.getElementById("dataDownload").deleteRow(abs);
            var rowCount = document.getElementById('dataDownload').rows.length;
            var table = document.getElementById('dataDownload');
            for(var a=1;a<rowCount;a++)
            {
              dets += table.rows[a].cells[1].innerHTML+"##";
              table.rows[a].cells[0].innerHTML = a;
              table.rows[a].cells[5].innerHTML = "<a href='#'' onclick='deleteFaktur(\""+a+"\")'>Hapus</a>";
            }
            document.getElementById("faktur").value = dets;
          }
        </script>

        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <form method="POST" action='../excel/csv_multi/'>
            <input type="hidden" name="faktur" id="faktur" value="<?php echo $dets?>">
            <button type="submit" class="btn btn-primary pull-right">Download</button>
          </form>
        </div>
      </div>
    </div>
  </div>
