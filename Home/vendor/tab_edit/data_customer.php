<div class="row">
	
	<!-- <input type="hidden" name="s_pic" class="form-control" value="-">
	<input type="hidden" name="c_pic" class="form-control" value="-"> -->
	<div class="col-md-6">
		<div class="form-group">
			<label>Kode Vendor</label>
			<input type="text" name="kode" class="form-control" required="" value="<?php echo $dataEdit['kode'];?>" readonly>
		</div>
		<div class="form-group">
			<label>Nama vendor</label>
			<input type="text" name="nama" class="form-control" required="" value="<?php echo $dataEdit['nama'];?>">
		</div>
		<div class="form-group">
			<label>Alamat</label>
			<input type="text" name="alamat" class="form-control" value="<?php echo $dataEdit['alamat'];?>">
		</div>
		<div class="form-group">
			<label>Phone</label>
			<input type="text" name="phone" class="form-control" value="<?php echo $dataEdit['phone'];?>">
		</div>
		<div class="form-group">
			<label>Contact</label>
			<input type="text" name="kontak" class="form-control" required="" value="<?php echo $dataEdit['contact'];?>"> 
		</div>
		<div class="form-group">
			<label>NPWP</label>
			<input type="text" name="npwp" class="form-control" required="" value="<?php echo $dataEdit['npwp'];?>"> 
		</div>
		<div class="form-group">
			<label>Nama NPWP</label>
			<input type="text" name="nama_npwp" class="form-control" required="" value="<?php echo $dataEdit['nama_npwp'];?>"> 
		</div>
		
		
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Kota</label>
			<select class="form-control select2" name="kota" style="width: 100%;" required="">
				<option value="" >--- Pilih Kota</option>
				<?php
				$sql1=mysql_query("select * from kabupaten where status = '1'");
				while($data1=mysql_fetch_array($sql1))
				{
					?>
					<option value="<?php echo $data1['no']?>" <?php if($dataEdit['kota'] == $data1['no']){echo "selected";}?>><?php echo $data1['kabupaten'];?></option>
					<?php 
				}?>
			</select>
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" value="<?php echo $dataEdit['email'];?>">
		</div>
		<div class="form-group">
			<label>Bank</label>
			<input type="text" name="bank" class="form-control" value="<?php echo $dataEdit['bank'];?>">
		</div>
		<div class="form-group">
			<label>Bank Account Name</label>
			<input type="text" name="acc" class="form-control" value="<?php echo $dataEdit['bank_acc'];?>">
		</div>
		<div class="form-group">
			<label>Bank Account No</label>
			<input type="text" name="acc_no" class="form-control" value="<?php echo $dataEdit['bank_no'];?>">
		</div>
		<div class="form-group">
			<label>Penanggung Jawab</label>
			<select class="form-control select2" name="sales" style="width: 100%;">
				<option value="" >--- Pilih Penanggung Jawab</option>
				<?php
				$sql1=mysql_query("select * from karyawan where status = '1'");
				while($data1=mysql_fetch_array($sql1))
				{
					?>
					<option value="<?php echo $data1['no']?>" <?php if($dataEdit['marketing'] == $data1['no']){echo "selected";}?>><?php echo $data1['nama'];?></option>
					<?php 
				}?>
			</select>
		</div>
		<div class="form-group">
			<label>Status</label>
			<select class="form-control select2" name="aktif" style="width: 100%;" required="">
				<option value="1" <?php if($dataEdit['status'] == "1"){echo "selected";}?>>Aktif</option>
				<option value="2" <?php if($dataEdit['status'] == "2"){echo "selected";}?>>Tidak Aktif</option>

			</select>
		</div>
	</div>
</div>