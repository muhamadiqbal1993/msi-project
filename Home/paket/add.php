<?php
$ids = date('ymdhis');
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					
					<div class="box-body">
						<form id="create_data" method="POST" enctype="multipart/form-data">
							<div class="col-md-2">
								<p></p>
								<p></p>
								<img id="photo1"/>
								<p></p>
								<div class="btn btn-default btn-file" id="photo62">
									<i class="fa fa-image"></i> Pilih Photo
									<input type="file" name="photos" onchange="viewImage(this);">

								</div>
								<p></p>
								<p></p>
								<p></p>
								<p></p>
							</div>
							<div class="col-md-5">
								<input type="hidden" name="input" class="form-control" value="1">
								<input type="hidden" name="detail" id="detail" class="form-control" required="">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 

								<div class="form-group">
									<label>Barcode</label>
									<input type="text" name="barcode" class="form-control" required="" value="<?php echo $ids?>">
								</div>
								<div class="form-group">
									<label>Nama Paket</label>
									<input type="text" name="nama" class="form-control" >
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label>Harga</label>
									<input type="number" name="harga" class="form-control" >
								</div>
								<div class="form-group">
									<label>Potongan Harga</label>
									<input type="number" name="potongan" class="form-control" >
								</div>
								
							</div>

							<div class="col-md-12">
								<hr class="abu">
								<div class="form-group">
									<h2>List Item Paket</h2>
								</div>
							</div>

							<div class="col-xs-12">
								<a href="#" id="tambahBtn" onclick="addLine()">+ Tambah Item</a>
								<p></p>
								<table id="so_table" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th style="width: 5%">No</th>
											<th>Product</th>
											<th>Qty</th>
											<th>Action</th>
										</tr>
									</thead>
									<script language="javascript" type="text/javascript">

										function getProduct(abs)
										{
											var products = '<select class="form-control select2" multiple="multiple" id="product_edits'+abs+'" name="l_product'+abs+'[]" style="width: 100%;">'+
											<?php
											$sql1=mysql_query("select * from product where status = '1'");
											while($data1=mysql_fetch_array($sql1))
											{
												?>
												'<option value="<?php echo $data1['kode'];?>"><?php echo $data1['kode'].' - '.$data1['nama']?></option>'+

												<?php 
											}?>
											'</select>';
											return products;
										}

										function delete_row(abs)
										{
											document.getElementById("so_table").deleteRow(abs);
											var rowCount = document.getElementById('so_table').rows.length;
											var table = document.getElementById('so_table');
											for(var a=1;a<rowCount;a++)
											{
												table.rows[a].cells[0].innerHTML = '<input type="number" name="l_nom[]" class="form-control" value="'+a+'" readonly>';
												table.rows[a].cells[3].innerHTML = "<button type='button' onclick='delete_row(\""+a+"\")' class='btn btn-danger'>Hapus</button>";
											}
										}

										function addLine()
										{ 
											var rowCount = document.getElementById('so_table').rows.length;
											var table = document.getElementById('so_table');
											var row = table.insertRow(rowCount);
											var cell0 = row.insertCell(0);
											var cell1 = row.insertCell(1);
											var cell2 = row.insertCell(2);
											var cell3 = row.insertCell(3);
											cell0.innerHTML = '<input type="number" name="l_nom[]" class="form-control" value="'+rowCount+'" readonly>';
											cell1.innerHTML = getProduct(rowCount);
											cell2.innerHTML = '<input type="number" id="editQty" name="l_qty[]" class="form-control">';
											cell3.innerHTML = "<button type='button' onclick='delete_row(\""+rowCount+"\")' class='btn btn-danger'>Hapus</button>";

											var nams = '#product_edits'+rowCount;
											$(nams).select2();
										}

									</script>
								</table>
								<a href="#" id="tambahBtn1" onclick="addLine()">+ Tambah Item</a>
							</div>

						</form>
						<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
					</div>
					
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}

	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(150)
				.height(150);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>