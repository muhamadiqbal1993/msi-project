<div class="row">
	<div class="col-md-12">
		<h1>Data Pribadi</h1>
		<hr class="abu">
	</div>
	<!-- <input type="hidden" name="s_pic" class="form-control" value="-">
	<input type="hidden" name="c_pic" class="form-control" value="-"> -->
	<div class="col-md-6">
		<div class="form-group">
			<label>No KTP</label>
			<input type="text" name="ktp" class="form-control" required="" value="<?php echo $ktp;?>">
		</div>
		<div class="form-group">
			<label>Kelamin</label>
			<select class="form-control select2" name="kelamin" style="width: 100%;" required="">
				<option value="laki" <?php if($kelamin == "laki"){echo "selected";}?>>Laki - Laki</option>
				<option value="perempuan" <?php if($kelamin == "perempuan"){echo "selected";}?>>Perempuan</option>
			</select>
		</div>
		<div class="form-group">
			<label>Tempat Lahir</label>
			<input type="text" name="tempat_lahir" class="form-control" required="" value="<?php echo $tempat_lahir;?>">
		</div>
		<div class="form-group">
			<label>Tanggal Lahir</label>
			<input type="text" name="tgl_lahir" id="datepicker" class="form-control" required="" value="<?php echo $tgl_lahir;?>">
		</div>
		<div class="form-group">
			<label>Agama</label>
			<select class="form-control select2" name="agama" style="width: 100%;" required="">
				<option value="budha" <?php if($agama == "budha"){echo "selected";}?>>Budha</option>
				<option value="hindu" <?php if($agama == "hindu"){echo "selected";}?>>Hindu</option>
				<option value="islam" <?php if($agama == "islam"){echo "selected";}?>>Islam</option>
				<option value="katholik" <?php if($agama == "katholik"){echo "selected";}?>>Katholik</option>
				<option value="protestan" <?php if($agama == "protestan"){echo "selected";}?>>Protestan</option>
			</select>
		</div>
		
		
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Status Pernikahan</label>
			<select class="form-control select2" name="nikah" style="width: 100%;" required="">
				<option value="lajang" <?php if($nikah == "lajang"){echo "selected";}?>>Lajang</option>
				<option value="menikah" <?php if($nikah == "menikah"){echo "selected";}?>>Menikah</option>
				<option value="bercerai" <?php if($nikah == "bercerai"){echo "selected";}?>>Bercerai</option>
				<option value="pisah" <?php if($nikah == "pisah"){echo "selected";}?>>Pasangan Meninggal</option>
			</select>
		</div>
		<div class="form-group">
			<label>Phone 1</label>
			<input type="text" name="phone1" class="form-control" value="<?php echo $phone1;?>">
		</div>
		<div class="form-group">
			<label>Phone 2</label>
			<input type="text" name="phone2" class="form-control" value="<?php echo $phone2;?>">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" value="<?php echo $email;?>">
		</div>
		<div class="form-group">
			<label>Alamat</label>
			<input type="text" name="alamat" class="form-control" value="<?php echo $alamat;?>">
		</div>
	</div>
</div>
