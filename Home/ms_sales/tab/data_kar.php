<div class="row">
	<div class="col-md-12">
		<h1>Data Karyawan</h1>
		<hr class="abu">
	</div>
	<!-- <input type="hidden" name="s_pic" class="form-control" value="-">
	<input type="hidden" name="c_pic" class="form-control" value="-"> -->
	<div class="col-md-12">
		<div class="form-group">
			<label>Photo Karyawan</label>
			<br>
			<div class="btn btn-default btn-file" id="photo62" >
				<p style="text-align:center;"><img id="photo1" src="../images/image.png" style="height: 150px;width: 150px"/></p>
				<input type="file" name="photos" id="photos" onchange="viewImage1(this);" >
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Kode Karyawan</label>
			<input type="text" name="kode" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Nama Karyawan</label>
			<input type="text" name="nama" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Bank Rekening</label>
			<input type="text" name="nama_bank" class="form-control">
		</div>
		<div class="form-group">
			<label>No Rekening</label>
			<input type="text" name="norek" class="form-control">
		</div>
		<div class="form-group">
			<label>Tanggal Join</label>
			<input type="text" name="tgl_join" id="datepicker1" class="form-control" required="">
		</div>
		
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Status Karyawan</label>
			<select class="form-control select2" name="status_kar" style="width: 100%;" required="">
				<?php
				$sql1=mysql_query("select * from status_karyawan where status = '1'");
				while($data1=mysql_fetch_array($sql1))
				{
					?>
					<option value="<?php echo $data1['no']?>" ><?php echo $data1['kode'].' - '.$data1['nama'];?></option>
					<?php 
				}?>
			</select>
		</div>
		<div class="form-group">
			<label>Jabatan</label>
			<select class="form-control select2" name="jabatan" id="jabatan" style="width: 100%;" required="">
				<?php
				$sql1=mysql_query("select * from group_divisi where status = '1' and departemen = '9'");
				while($data1=mysql_fetch_array($sql1))
				{
					?>
					<option value="<?php echo $data1['no']?>" ><?php echo $data1['kode'].' - '.$data1['nama'];?></option>
					<?php 
				}?>
			</select>
		</div>
		<div class="form-group">
			<label>Cuti Karyawan</label>
			<input type="text" name="cuti" class="form-control" value="0">
		</div>
		<div class="form-group">
			<label>BPJS Kesehatan</label>
			<input type="text" name="bpjs" class="form-control">
		</div>
		<div class="form-group">
			<label>BPJS Ketenagakerjaan</label>
			<input type="text" name="tku" class="form-control">
		</div>
	</div>
</div>

<script type="text/javascript">
	function viewImage1(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(200)
				.height(200);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
