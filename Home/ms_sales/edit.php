<?php

$kode = "";
$nama = "";
$nama_bank ="";
$norek ="";
$tgl_join = "";
$jabatan = "";
$status_kar = "";
$cuti = "";
$bpjs = "";
$tku = "";

$ktp = "";
$kelamin = "";
$tempat_lahir = "";
$tgl_lahir = "";
$agama = "";
$nikah = "";
$phone1 = "";
$phone2 = "";
$email = "";
$alamat = "";
$photo = "";

$sql1=mysql_query("select *,DATE_FORMAT(tgl_join, '%m/%d/%Y') as joins,DATE_FORMAT(tgl_lahir, '%m/%d/%Y') as lahir from karyawan where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$kode = $data1['kode'];
	$nama = $data1['nama'];
	$nama_bank = $data1['rekening'];
	$norek = $data1['bank_rekening'];
	$tgl_join = $data1['joins'];
	$jabatan = $data1['jabatan'];
	$status_kar = $data1['status'];
	$cuti = $data1['cuti'];
	$bpjs = $data1['bpjs'];
	$tku = $data1['bpjstku'];

	$ktp = $data1['ktp'];
	$kelamin = $data1['kelamin'];
	$tempat_lahir = $data1['tempat_lahir'];
	$tgl_lahir = $data1['lahir'];
	$agama = $data1['agama'];
	$nikah = $data1['status_nikah'];
	$phone1 = $data1['phone1'];
	$phone2 = $data1['phone2'];
	$email = $data1['email'];
	$alamat = $data1['alamat'];
	$photo = $data1['photo'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" form="create_data" onclick="save_modul()" class="btn btn-primary">Simpan</button>
						<?php }?>

						<hr class="abu">
					</div>
					<form id="create_data" method="POST" action="." enctype="multipart/form-data"> 
						<div class="box-body">
							<input type="hidden" name="input" class="form-control" value="2">
							<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
							<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
							<input type="hidden" name="detPrice" id="detPrice" class="form-control" required="">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_1" data-toggle="tab">Data Karyawan</a></li>
									<li><a href="#tab_2" data-toggle="tab">Informasi Pribadi</a></li>
									<li><a href="#tab_3" data-toggle="tab">Kontak Terdesak</a></li>
									<li><a href="#tab_4" data-toggle="tab">Pendidikan</a></li>
									<li><a href="#tab_5" data-toggle="tab">Pengalaman</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1">
										<?php include 'tab_edit/data_kar.php';?>
									</div>
									<div class="tab-pane" id="tab_2">
										<?php include 'tab_edit/pribadi.php';?>
									</div>
									<div class="tab-pane" id="tab_3">
										<?php include 'tab_edit/kontak.php';?>
									</div>
									<div class="tab-pane" id="tab_4">
										<?php include 'tab_edit/pendidikan.php';?>
									</div>
									<div class="tab-pane" id="tab_5">
										<?php include 'tab_edit/pengalaman.php';?>
									</div>
								</div>

								<!-- /.tab-content -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'karyawan';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var setujus = document.getElementById("setuju").checked;
		if(setujus == false)
		{
			alert("Checklist Jika Anda Sudah Membaca dan Memahami Term & Condition");
			$('[href="#tab_6"]').tab('show');
			
		}
		
		
	}
</script>

