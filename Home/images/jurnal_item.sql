-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 05, 2023 at 05:19 PM
-- Server version: 10.5.21-MariaDB-cll-lve
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nncp6326_bac`
--

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_item`
--

CREATE TABLE `payment_cost_detail` (
  `no` int(11) NOT NULL,
  `id_transaksi` varchar(30) DEFAULT NULL,
  `account` varchar(15) DEFAULT NULL,
  `debit` varchar(20) DEFAULT NULL,
  `credit` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `ref` varchar(50) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `jurnal_item`
--

INSERT INTO `payment_cost_detail` (`no`, `id_transaksi`, `account`, `debit`, `credit`, `type`, `ref`, `tanggal`, `status`) VALUES
(1, 'ORDER/23/987', '510021', '7219802', '0', 'sales_order', 'ORDER/23/987', '2023-08-04 15:20:24', '1'),
(2, 'ORDER/23/987', '', '0', '78553.739', 'sales_order', 'ORDER/23/987', '2023-08-04 15:20:24', '1'),
(3, 'ORDER/23/111', '520004', '695459', '0', 'sales_order', 'ORDER/23/111', '2023-08-04 15:24:04', '1'),
(4, 'ORDER/23/111', '600002', '0', '7566.823', 'sales_order', 'ORDER/23/111', '2023-08-04 15:24:04', '1'),
(10, 'PC/2023/0002', '120202', '0', '500000', 'payment_cost', 'hoho', '2023-08-04 21:15:17', '1'),
(15, 'JM-230805162616', '1', '5000001', '0', 'jurnal_manual', 'PEMAKAIAN', '2023-08-30 00:00:00', '1'),
(16, 'JM-230805162616', '1', '0', '5000001', 'jurnal_manual', 'PEMAKAIAN', '2023-08-30 00:00:00', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jurnal_item`
--
ALTER TABLE `payment_cost_detail`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jurnal_item`
--
ALTER TABLE `payment_cost_detail`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
