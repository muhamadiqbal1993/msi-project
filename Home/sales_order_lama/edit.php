<?php
$id = $_GET['id'];


if (!empty($_POST['cancel'])) 
{
  mysql_query("update invoice set status = '5' where no = '$id'");
}
$dataTrx = mysql_fetch_array(mysql_query("select *,
  DATE_FORMAT(tanggal, '%m/%d/%Y') as tgls,
  DATE_FORMAT(due, '%m/%d/%Y') as dues from invoice where no = '$id'"));
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Edit <?php echo $modulnya;?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Edit Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
             <?php if($dataTrx['status'] != "5"){?>
            &nbsp;&nbsp;&nbsp;
            <button type="submit" form="cancel_order" class="btn btn-danger">Cancel Transaksi</button>
            &nbsp;&nbsp;&nbsp;
            <?php if($dataTrx['status'] == "1"){?>
            <button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
            <?php }?>
            <button type="button" class="btn btn-success" style="float:right">Download Invoice</button>
            <?php } else {echo '<p style="color: red;float: right;font-size: 25px;padding-right: 20px"><strong>Cancel</strong></p>';}
            ?>

              <!-- <div class="btn-group" style="float: right;">
                <button type="button" class="btn btn-success">Download Invoice</button>
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="../function/laporan/inv/?id=<?php echo $id.'-'.$user_data['no']?>" target="_blank">PDF</a></li>
                  <li><a href="../excel/inv/?id=<?php echo $id?>" target="_blank">Excel</a></li>
                </ul>
              </div> -->

            </div>
          </div>
        </div>
      </div>

      <form id="cancel_order" action="./?id=<?php echo $id?>" method="POST">
        <!-- <input type="hidden" name="input" class="form-control" value="2"> -->
        <input type="hidden" name="cancel" class="form-control" value="<?php echo $id?>">
      </form>

      
      <form id="create_data" method="POST"> 
        <input type="hidden" name="input" class="form-control" value="2">
        <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
        <input type="hidden" name="idss" class="form-control" value="<?php echo $dataTrx['inv'];?>"  >

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-body">
                <div class="col-md-6">
                  <div class="form-group">
                    <label><strong>Nomor Order</strong></label>
                    <input type="text" name="no_order" class="form-control" value="<?php echo $dataTrx['inv']?>" readonly>
                  </div>
                  <div class="form-group">
                    <label><strong>Customer</strong></label>
                    <select class="form-control select2" name="customer" id="customer" style="width: 100%;" required="">
                      <option value="" >--- Pilih Customer ---</option>
                      <?php
                      $sql1=mysql_query("select * from customer where status = '1'");
                      while($data1=mysql_fetch_array($sql1))
                      {
                        ?>
                        <option value="<?php echo $data1['no']?>" <?php if($dataTrx['customer'] == $data1['no']){echo "selected";}?>><?php echo $data1['kode'].' - '.$data1['nama']?></option>
                        <?php 
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><strong>Tanggal Invoice</strong></label>
                    <input type="text" name="tanggal" id="datepicker" class="form-control" required value="<?php echo $dataTrx['tgls']?>">
                  </div>
                  <div class="form-group">
                    <label><strong>Jatuh Tempo</strong></label>
                    <input type="text" name="due" id="datepicker1" class="form-control"  value="<?php echo $dataTrx['dues']?>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label><strong>Salesman</strong></label>
                    <input type="text" name="sales2" id="sales2" class="form-control" readonly="" style="display: none">
                    <div id="sales1">
                      <select class="form-control select2" name="salesman" id="salesman"  style="width: 100%;" >
                        <option value="" >--- Pilih Salesman ---</option>
                        <?php
                        $sql1=mysql_query("select * from user where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          <option value="<?php echo $data1['no']?>" <?php if($dataTrx['salesman'] == $data1['no']){echo "selected";}?>><?php echo 'Kilat'.$data1['no'].' - '.$data1['nama']?></option>
                          <?php 
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label><strong>Account</strong></label>
                    <select class="form-control select2" name="account" style="width: 100%;" >
                      <option value="" >--- Pilih Account ---</option>
                      <?php
                      $sql1=mysql_query("select * from coa where status = '1'");
                      while($data1=mysql_fetch_array($sql1))
                      {
                        ?>
                        <option value="<?php echo $data1['id']?>" <?php if($dataTrx['account'] == $data1['id']){echo "selected";}?>><?php echo $data1['id'].' - '.$data1['nama']?></option>
                        <?php 
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><strong>Keterangan</strong></label>
                    <input type="text" name="keterangan" class="form-control" required value="<?php echo $dataTrx['keterangan']?>">
                  </div>
                </div>

                <div class="col-md-12">
                  <hr class="abu">
                </div>
                <div class="col-md-12">
                  <div class="box-body table-responsive no-padding">
                    <table id="so_table" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th style="width: 15%">AWB</th>
                          <th>Flight</th>
                          <th>Berat (Kg)</th>
                          <th>Harga Satuan</th>
                          <th>Keterangan</th>
                          <th>Biaya Tambahan</th>
                          <th style="width: 10%">Total</th>
                          <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        $noms = 1;
                        $sql1=mysql_query("select * from invoice_detail where invoice = '".$dataTrx['inv']."'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          echo '
                          <tr>
                            <td>'.$noms.'</td>
                            <td><input type="text" id="editAwb'.$noms.'" name="editAwb[]" class="form-control" value="'.$data1['awb'].'"></td>
                            <td><input type="text" id="editFlight'.$noms.'" name="editFlight[]" class="form-control" value="'.$data1['flight'].'"></td>
                            <td><input type="text" id="editBerat'.$noms.'" name="editBerat[]" oninput="getNilaiTotal('.$noms.')" class="form-control" value="'.$data1['berat'].'"></td>
                            <td><input type="number" id="editHarga'.$noms.'" name="editHarga[]" oninput="getNilaiTotal('.$noms.')" class="form-control" value="'.$data1['harga'].'"></td>  
                            <td><input type="text" id="editKeterangan'.$noms.'" name="editKeterangan[]" class="form-control" value="'.$data1['keterangan'].'"></td>
                            <td><input type="number" id="editBiaya'.$noms.'" name="editBiaya[]" oninput="getNilaiTotal('.$noms.')" class="form-control" value="'.$data1['tambahan'].'"></td>
                            <td>'.number_format($data1['total']).'</td>
                            <td><a href="javascript:void(0)" onclick="delete_row('.$noms.')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                          </tr>
                          ';
                          $noms++;
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                  <br>
                  <a href="javascript:void(0)" id="tambahBtn1" onclick="addLine()">+ Tambah Item</a>
                </div>

                <script language="javascript" type="text/javascript">

                  function addLine()
                  {
                    var rowCount = document.getElementById('so_table').rows.length;
                    var table = document.getElementById('so_table');

                    var row = table.insertRow(rowCount);
                    var cell0 = row.insertCell(0);
                    var cell1 = row.insertCell(1);
                    var cell2 = row.insertCell(2);
                    var cell3 = row.insertCell(3);
                    var cell4 = row.insertCell(4);
                    var cell5 = row.insertCell(5);
                    var cell6 = row.insertCell(6);
                    var cell7 = row.insertCell(7);
                    var cell8 = row.insertCell(8);

                    cell0.innerHTML = rowCount;
                    cell1.innerHTML = '<input type="text" id="editAwb'+rowCount+'" name="editAwb[]" class="form-control" >';
                    cell2.innerHTML = '<input type="text" id="editFlight'+rowCount+'" name="editFlight[]" class="form-control" >';
                    cell3.innerHTML = '<input type="text" id="editBerat'+rowCount+'" name="editBerat[]" oninput="getNilaiTotal('+rowCount+')" class="form-control" value="0">';
                    cell4.innerHTML = '<input type="number" id="editHarga'+rowCount+'" name="editHarga[]" oninput="getNilaiTotal('+rowCount+')" class="form-control" value="0">';
                    cell5.innerHTML = '<input type="text" id="editKeterangan'+rowCount+'" name="editKeterangan[]" class="form-control" >';
                    cell6.innerHTML = '<input type="number" id="editBiaya'+rowCount+'" name="editBiaya[]" oninput="getNilaiTotal('+rowCount+')" class="form-control" value="0">';
                    cell7.innerHTML = '';
                    cell8.innerHTML = '<a href="javascript:void(0)" onclick="delete_row('+rowCount+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
                  }

                  function getNilaiTotal(abs)
                  {
                    var table = document.getElementById('so_table');

                    var tBerat = "editBerat"+abs
                    var tHarga = "editHarga"+abs
                    var tBiaya = "editBiaya"+abs

                    var nBerat = document.getElementById(tBerat).value;
                    var nHarga = document.getElementById(tHarga).value;
                    var nBiaya = document.getElementById(tBiaya).value;

                    var totalSemua = (parseFloat(nBerat) * parseFloat(nHarga)) + parseFloat(nBiaya);

                    table.rows[abs].cells[7].innerHTML = parseInt(totalSemua).toLocaleString('en-US');

                    totalPajak();

                  }

                  function viewTotal()
                  {
                    var table = document.getElementById('so_table');
                    var rowCount = document.getElementById('so_table').rows.length;
                    var subTotal = 0;
                    for(var a=1;a<rowCount;a++)
                    {
                      subTotal += parseInt(table.rows[a].cells[7].innerHTML.split(",").join(""));
                    }
                    document.getElementById("subtotal").value = subTotal.toLocaleString('en-US');
                    var nilaiPajak = subTotal + parseInt(document.getElementById("lainya").value.split(",").join(""));
                    document.getElementById("grandtotal").value = nilaiPajak.toLocaleString('en-US');
                  }

                  function delete_row(abs)
                  {
                    document.getElementById("so_table").deleteRow(abs);
                    var rowCount = document.getElementById('so_table').rows.length;
                    var table = document.getElementById('so_table');
                    for(var a=1;a<rowCount;a++)
                    {
                      var ab = a;

                      if(abs <= a)
                      {
                        ab = a + 1;
                      }

                      var n_editAwb = "editAwb"+ab;
                      var n_editFlight= "editFlight"+ab;
                      var n_editBerat= "editBerat"+ab;
                      var n_editHarga= "editHarga"+ab;
                      var n_editKeterangan= "editKeterangan"+ab;
                      var n_editBiaya= "editBiaya"+ab;

                      var editAwb = document.getElementById(n_editAwb).value;
                      var editFlight = document.getElementById(n_editFlight).value;
                      var editBerat = document.getElementById(n_editBerat).value;
                      var editHarga = document.getElementById(n_editHarga).value;
                      var editKeterangan = document.getElementById(n_editKeterangan).value;
                      var editBiaya = document.getElementById(n_editBiaya).value;
                      var editTotal = 0;

                      table.rows[a].cells[0].innerHTML = a;
                      table.rows[a].cells[1].innerHTML = '<input type="text" id="editAwb'+a+'" name="editAwb[]" class="form-control" value="'+editAwb+'">';
                      table.rows[a].cells[2].innerHTML = '<input type="text" id="editFlight'+a+'" name="editFlight[]" class="form-control" value="'+editFlight+'">';
                      table.rows[a].cells[3].innerHTML = '<input type="text" id="editBerat'+a+'" name="editBerat[]" oninput="getNilaiTotal('+a+')" class="form-control" value="'+editBerat+'">';
                      table.rows[a].cells[4].innerHTML = '<input type="number" id="editHarga'+a+'" name="editHarga[]" oninput="getNilaiTotal('+a+')" class="form-control" value="'+editHarga+'">';
                      table.rows[a].cells[5].innerHTML = '<input type="text" id="editKeterangan'+a+'" name="editKeterangan[]" class="form-control" value="'+editKeterangan+'">';
                      table.rows[a].cells[6].innerHTML = '<input type="number" id="editBiaya'+a+'" name="editBiaya[]" oninput="getNilaiTotal('+a+')" class="form-control" value="'+editBiaya+'">';
                      table.rows[a].cells[8].innerHTML = '<a href="javascript:void(0)" onclick="delete_row('+a+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
                      totalPajak();
                    }
                    totalPajak();
                  }

                  function delete_row_pajak(abs)
                  {
                    document.getElementById("pajak_table").deleteRow(abs);
                    var rowCount = document.getElementById('pajak_table').rows.length;
                    var table = document.getElementById('pajak_table');
                    for(var a=1;a<rowCount;a++)
                    {
                      table.rows[a].cells[0].innerHTML = a;
                      table.rows[a].cells[3].innerHTML = '<a href="javascript:void(0)" onclick="delete_row_pajak('+a+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
                      totalPajak();
                    }
                    totalPajak();
                  }

                  function getProductPajak(abs)
                  {
                    var pajaks = '<select class="form-control select2" id="product_editss'+abs+'" name="editPajak[]" onchange="getNilaiPajak('+abs+')" style="width: 100%;" required="">'+
                    <?php

                    $sql1=mysql_query("select * from pajak where status = '1'");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      ?>
                      '<option value="<?php echo $data1['no'].'##'.$data1['nilai'];?>"><?php echo $data1['nama']?></option>'+

                      <?php 
                    }?>

                    '</select>';

                    return pajaks;
                  }

                  function getNilaiPajak(abs)
                  {
                //alert(abs);
                var cellValue = "product_editss"+abs
                var table = document.getElementById('pajak_table');
                var isiNilai = document.getElementById(cellValue).value;
                if(isiNilai == "")
                {
                  table.rows[abs].cells[2].innerHTML = 0;
                }
                else
                {
                  table.rows[abs].cells[2].innerHTML = isiNilai.split("##")[1];
                }


                totalPajak();

              }

              function totalPajak()
              {
                var nilaiSubtotal = parseInt(document.getElementById("subtotal").value.split(",").join(""));

                var rowCount = document.getElementById('pajak_table').rows.length;
                var table = document.getElementById('pajak_table');
                var nilaiPajak = 0;
                for(var a=1;a<rowCount;a++)
                {
                  var persentasePajak = nilaiSubtotal * (parseFloat(table.rows[a].cells[2].innerHTML) / 100);
                  nilaiPajak += persentasePajak ;
                  //viewTotal();
                }
                document.getElementById("lainya").value = nilaiPajak.toLocaleString('en-US');
                viewTotal();
              }

              function addLinePajak()
              {
                var rowCount = document.getElementById('pajak_table').rows.length;
                var table = document.getElementById('pajak_table');

                var row = table.insertRow(rowCount);
                var cell0 = row.insertCell(0);
                var cell1 = row.insertCell(1);
                var cell2 = row.insertCell(2);
                var cell3 = row.insertCell(3);


                cell0.innerHTML = rowCount;
                cell1.innerHTML = getProductPajak(rowCount);
                cell2.innerHTML = '';
                cell3.innerHTML = '<a href="javascript:void(0)" onclick="delete_row_pajak('+rowCount+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
                var nams = '#product_editss'+rowCount;
                $(nams).select2();
                $(nams).val(""); 
                $(nams).change();
              }

            </script>
            <div class="col-md-12">
              <br>
            </div>
          </div>

        </div>

      </div>

    </div>

    <div class="row">
      <div class="col-xs-7">
        <div class="box">
          <div class="box-body">
            <div class="col-md-12">
              <h5><strong>Pajak Tambahan</strong></h5>
              <p></p>
              <div class="box-body table-responsive no-padding">
                <table id="pajak_table" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th style="width: 40%">Nama Pajak</th>
                      <th>Nilai (Dalam %)</th>
                      <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $noms = 1;
                    $sql1=mysql_query("select * from invoice_pajak where invoice = '".$dataTrx['inv']."'");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      $accr = '<select class="form-control select2" id="product_editss'.$noms.'" name="editPajak[]" onchange="getNilaiPajak('.$noms.')" style="width: 100%;" required="">';

                      $sql12=mysql_query("select * from pajak where status = '1'");
                      while($data12=mysql_fetch_array($sql12))
                      {
                        $slct = "";
                        if($data1['pajak'] == $data12['no'])
                        {
                          $slct = "selected";
                        }
                        $accr .= '<option value="'.$data12['no'].'##'.$data1['nilai'].'" '.$slct.'>'.$data12['nama'].'</option>';
                      }
                      $accr .= '</select>';

                      echo '
                      <tr>
                        <td>'.$noms.'</td>
                        <td>'.$accr.'</td>
                        <td>'.number_format($data1['nilai']).'</td>
                        <td><a href="javascript:void(0)" onclick="delete_row_pajak('.$noms.')" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                      </tr>
                      ';
                      $noms++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              <br>
              <a href="javascript:void(0)" id="tambahBtn22" onclick="addLinePajak()">+ Tambah Item</a>
            </div>
          </div>
          <br>
          <br>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-xs-5">
        <div class="box">
          <div class="box-body">
            <div class="col-md-12">
              <table width="100%">
                <tbody>
                  <tr>
                    <td width="30%"><input type="text" class="form-control" value="SUBTOTAL :" readonly></td>
                    <td width="70%"><input type="text" id="subtotal" name="subtotal" class="form-control" value="<?php echo number_format($dataTrx['total'])?>" readonly style="text-align: right; "></td>
                  </tr>
                  <tr>
                    <td width="30%"><input type="text" class="form-control" value="BIAYA LAINYA :" readonly></td>
                    <td width="70%"><input type="text" id="lainya" name="lainya" class="form-control" value="<?php echo number_format($dataTrx['lainya'])?>" readonly style="text-align: right; " ></td>
                  </tr>
                </tbody>
              </table>
              <hr class="abu">
              <table width="100%">
                <tbody>
                  <tr>
                    <td width="30%"><input type="text" class="form-control" value="GRAND TOTAL :" readonly style="font-weight:bold;"></td>
                    <td width="70%"><input type="text" id="grandtotal" name="grandtotal" class="form-control" value="<?php echo number_format($dataTrx['grandtotal'])?>" readonly style="text-align: right;font-weight:bold; "></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <br>

          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>

    </div>

  </form>


  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <div class="col-md-12">
            <h5><strong>Info Pembayaran</strong></h5>
            <p></p>
            <div class="box-body table-responsive no-padding">
              <table id="pajak_table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Tanggal Bayar</th>
                    <th>No Payment</th>
                    <th>Bank Mutasi</th>
                    <th>Nilai</th>
                  </tr>
                </thead>
                    <!-- <?php
                    $total_nya = "";
                    $detailss = "";
                    $nom = 1;
                    $sql2=mysql_query("select PD.*,concat('PYNT/',DATE_FORMAT(P.tanggal, '%y'),'/',LPAD(P.no, 4, '0')) as pynt,K.no_acc,K.nama_acc,K.bank,DATE_FORMAT(M.tanggal, '%d %M %Y') as tgls,MD.nominal,P.no as id_pay,M.no as id_mut from pembayaran_detail PD
                      inner join pembayaran P on P.id = PD.id_trx
                      inner join mutasi_detail MD on MD.no = P.bayar
                      inner join mutasi M on M.id = MD.id_trx
                      inner join kasbank K on K.no = M.kasbank
                      where PD.inv = '".$dataTrx['no']."' and P.status = '1' and P.type = 'INV'");
                    while($data2=mysql_fetch_array($sql2))
                    {

                      ?>
                      <tr>
                        <td><?php echo $data2['tgls'];?></td>
                        <td><a href="../b_invoice/?id=<?php echo $data2['id_pay']?>"><?php echo $data2['pynt'];?></a></td>
                        <td><a href="../mutasi_kas/?kd=<?php echo $data2['id_mut']?>"><?php echo $data2['bank'].' '.$data2['no_acc'].' - '.$data2['nama_acc'];?></a></td>
                        <td><?php echo number_format($data2['nominal']);?></td>
                      </tr>
                      <?php 
                      $nom++;
                    }
                    ?> -->
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php 
      $nama_modulnya = 'sales_order';
      include '../headfoot/history.php';
      ?>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
</div>
<!-- /.row -->
</div>
</section>
</div>

<script>
  view_total();
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
  function bukaPassword()
  {
    var aa = document.getElementById("bukaPas").readOnly;
    if(aa)
    {
      document.getElementById("bukaPas").readOnly = false;
    }
    else
    {
      document.getElementById("bukaPas").readOnly = true;
    }
  }
</script>
