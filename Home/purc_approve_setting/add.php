<?php
$dataEdit = mysql_fetch_array(mysql_query("select DATE_FORMAT(tanggal, '%m/%d/%Y') as new_tanggal from setting_account where no = '1'"));
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">

						<button type="button" onclick="save_modul()" class="btn btn-primary">Simpan</button>

					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="box">
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<input type="hidden" name="debit" id="debit" class="form-control"> 
						<input type="hidden" name="credit" id="credit" class="form-control"> 
						<div class="box-body">

							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">

										<h1>Approval Purchase Level</h1>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >Approval Ke</th>
													<th style="width: 80%">Divisi</th>
													<th >Action</th>

												</tr>
											</thead>
											<tbody>
												<?php 
												$noms = 1;
												$sql1=mysql_query("select * from bill_approve_setting");
												while($data1=mysql_fetch_array($sql1))
												{

													$acc_debit = '<select class="form-control select2" name="divisi[]" id="divisi'.$noms.'" style="width: 100%;" required="">';

													$sql12=mysql_query("select * from group_divisi where status = '1'");
													while($data12=mysql_fetch_array($sql12))
													{
														$slct = "";
														if($data1['divisi'] == $data12['no'])
														{
															$slct = "selected";
														}
														$acc_debit .= '<option value="'.$data12['no'].'" '.$slct.'>'.$data12['nama'].'</option>';
													}
													$acc_debit .= '</select>';

													echo '
													<tr>
													<td>'.$data1['urutan'].'</td>
													<td>'.$acc_debit.'</td>
													<td><button type="button" onclick="delete_row_didik('.$noms.')" class="btn btn-danger">Delete</button></td>
													</tr>
													';
													$noms++;
												}
												?>
											</tbody>
											<thead>
												<tr>
													<th colspan="3" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button></th>
													
												</tr>
											</thead>
											<script language="javascript" type="text/javascript">

												function getProduct(abs)
												{
													var products = '<select class="form-control select2" name="divisi[]" id="divisi'+abs+'" style="width: 100%;" required="">'+
													<?php

													$sql1=mysql_query("select * from group_divisi where status = '1'");
													while($data1=mysql_fetch_array($sql1))
													{
														?>
														'<option value="<?php echo $data1['no'];?>"><?php echo $data1['nama']?></option>'+

														<?php 
													}?>

													'</select>';

													return products;
												}

												function delete_row_didik(abs)
												{
													document.getElementById("tabDidik").deleteRow(abs);
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													for(var a=1;a<rowCount;a++)
													{
														table.rows[a].cells[0].innerHTML = a;
														table.rows[a].cells[2].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
													}
													
												}

												function addLine_didik()
												{ 
													
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													var row = table.insertRow(rowCount);
													var cell0 = row.insertCell(0);
													var cell1 = row.insertCell(1);
													var cell2 = row.insertCell(2);
													
													cell0.innerHTML = rowCount;
													cell1.innerHTML = getProduct(rowCount);
													
													
													cell2.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

													var namaField = "#divisi"+rowCount;
													$(namaField).select2();
												}

												
												
											</script>

										</table>
									</div>
								</div>
							</div>

						</div>
					</form>
					<br>
					<br>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}

	}
</script>
