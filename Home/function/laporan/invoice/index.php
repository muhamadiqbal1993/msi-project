<?php
include "../../connect.php";
require('../fpdf17/fpdf.php');
//Menampilkan data dari tabel di database
$id = $_GET['id'];

$tgl_skr = date('d F Y');
$customer = "";
$alamat = "";
$alamat2 = "";
$pic = "";
$invoice = "";
$no_inv = "";
$tanggal = "";

$dataInvoice = mysql_fetch_array(mysql_query("select I.*,C.nama as namcus,C.alamat as namal,C.phone as namphone,DATE_FORMAT(I.tanggal, '%d %M %Y') as tgls,DATE_FORMAT(I.due, '%d %M %Y') as dues from invoice I
	inner join customer C on C.no = I.customer
	where I.no='$id'"));

$dataKantor = mysql_fetch_array(mysql_query("select * from setting_web"));

$tampil= "select IDR.*,P.nama as nampro from invoice_detail_retail IDR
inner join product P on P.no = IDR.produk
where IDR.id_order = '".$dataInvoice['inv']."'";
$result=mysql_query($tampil);
//Inisiasi untuk membuat header kolom
$c1 = "";
$c2 = "";
$c3 = "";
$c4 = "";
$c5 = "";
$angka = 1;
//For each row, add the field to the corresponding column


$pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
$pdf->AddPage();

//Menambahkan Gambar
$pdf->Image('logo.png',5,5,-300);

$pdf->Ln(20);
$pdf->SetFont('Arial','B',11);

$pdf->Cell(0,0,'INVOICE',0,1,'C');
$pdf->SetFont('Arial','B',10);


$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(25,5,'No Invoice ');
$pdf->Cell(70,5,' : '.$dataInvoice['inv']);
$pdf->Cell(17,5,'No PO ');
$pdf->Cell(80,5,' : '.$dataInvoice['inv']);
$pdf->Ln(7);
$pdf->Cell(-5);
$pdf->Cell(25,5,'Tgl. Invoice ');
$pdf->Cell(70,5,' : '.$dataInvoice['tgls']);
$pdf->Cell(17,5,'Customer ');
$pdf->Cell(80,5,' : '.$dataInvoice['namcus']);


//Fields Name position
$Y_Fields_Name_position = 60;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(230,230,230);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(10,8,'NO',1,0,'C',1);

$pdf->SetX(15);
$pdf->Cell(105,8,'DESCRIPTION ',1,0,'C',1);

$pdf->SetX(120);
$pdf->Cell(15,8,'QTY',1,0,'C',1);

$pdf->SetX(135);
$pdf->Cell(35,8,'UNIT PRICE',1,0,'C',1);

$pdf->SetX(170);
$pdf->Cell(35,8,'TOTAL PRICE',1,0,'C',1);


$pdf->Ln();
$Y_Table_Position = 68;
$total_semua = 0;
$pdf->SetFont('Arial','',9);
while($row = mysql_fetch_array($result))
{   
	$c1 = '   '.$angka;
	$c2 = '   '.$row["nampro"];
	$c3 = '   '.$row["qty"];
	$c4 = '   '.number_format($row["harga_jual"]);
	$c5 = '   '.number_format($row["total"]);

	$pdf->SetFont('Arial','',9);
	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(5);
	$pdf->Cell(10,7,$c1,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(15);
	$pdf->Cell(105,7,$c2,1,'L');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(120);
	$pdf->Cell(15,7,$c3,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(135);
	$pdf->Cell(35,7,$c4,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(170);
	$pdf->Cell(35,7,$c5,1,'C');

	$Y_Table_Position = $Y_Table_Position + 7;
	$angka++;
	$total_semua = $total_semua + $row["no"] * $row["no"];
}

$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(130,7,'Terbilang        ',1,'','L','');

$pdf->SetFont('Arial','',10);
$pdf->SetX(135);
$pdf->Cell(35,7,'Sub-total        ',1,'','R','');
$pdf->SetX(170);
$pdf->Cell(35,7,number_format($dataInvoice['total']),1,'','R','');

$Y_Table_Position = $Y_Table_Position + 7;
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(130,7,penyebut($dataInvoice['total']).' Rupiah',1,'','L','');

$pdf->SetFont('Arial','',10);
$pdf->SetX(135);
$pdf->Cell(35,7,'PPN 11%        ',1,'','R','');
$pdf->SetX(170);
$pdf->Cell(35,7,number_format($dataInvoice['lainya']),1,'','R','');

$pdf->SetFont('Arial','B',11);
$Y_Table_Position = $Y_Table_Position + 7;
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(165,7,'Total        ',1,'','R','');
$pdf->SetX(170);
$pdf->Cell(35,7,number_format($dataInvoice['grandtotal']),1,'','R','');

$Y_Table_Position = $Y_Table_Position + 7;
$pdf->SetFont('Arial','',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(165,7,'DP-20%        ',1,'','R','');
$pdf->SetX(170);
$pdf->Cell(35,7,number_format($dataInvoice['total']*0.20),1,'','R','');



$pdf->SetFont('Arial','B',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(55,5,'NPWP '.$dataKantor['nama_npwp']);
$pdf->SetFont('Arial','',10);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(55,5,$dataKantor['no_npwp']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(55,5,"Pembayaran Harap Ditransfer Ke Rekening");
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(55,5,"BANK BNI Cabang Tangerang");
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(55,5,"Atas Nama : PT MAJU SELARAS INSTRUMINDO");
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(55,5,"No Rekening : 8 8 8 8 0 1 1 0 1 5");


//============================================================================

$pdf->Output();

function penyebut($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = penyebut($nilai - 10). " Belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " Seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " Seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
	}     
	return $temp;
}


?>

