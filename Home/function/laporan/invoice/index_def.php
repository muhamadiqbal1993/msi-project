<?php
include "../../connect.php";
require('../fpdf17/fpdf.php');
//Menampilkan data dari tabel di database
$id = $_GET['id'];

$tgl_skr = date('d F Y');
$customer = "";
$alamat = "";
$alamat2 = "";
$pic = "";
$invoice = "";
$no_inv = "";
$tanggal = "";

$dataInvoice = mysql_fetch_array(mysql_query("select I.*,C.nama as namcus,DATE_FORMAT(I.tanggal, '%d %b %Y') as tgls from invoice I
    inner join customer C on C.no = I.customer
    where I.no='$id'"));

$tampil= "select * from invoice_detail where invoice = '".$dataInvoice['inv']."'";
$result=mysql_query($tampil);
//Inisiasi untuk membuat header kolom
$c1 = "";
$c2 = "";
$c3 = "";
$c4 = "";
$c5 = "";
$angka = 1;
//For each row, add the field to the corresponding column


$pdf = new FPDF('L','mm',array(210,297)); //L For Landscape / P For Portrait
$pdf->AddPage();

//Menambahkan Gambar
$pdf->Image('logo.png',5,10,-650);

$pdf->SetFont('Arial','B',17);
$pdf->Cell(20);
$pdf->Cell(235,1,'PT. PT. SYSTEM DEMO');
$pdf->Cell(30,1,'INVOICE');
$pdf->SetFont('Arial','B',10);
$pdf->Ln(5);
$pdf->Cell(20);
$pdf->Cell(100,5,'Jl PT. SYSTEM DEMO,');
$pdf->SetFont('Arial','',9);
$pdf->Ln(5);
$pdf->Cell(20);
$pdf->Cell(100,5,'Kec. Pahandut, Kota. Palangkaraya, Kalimantan Tengah 74874');
$pdf->Ln(5);
$pdf->Cell(20);
$pdf->Cell(100,5,'Telp : 082149111401');
$pdf->SetFont('Arial','B',9);
$pdf->Ln(8);
$pdf->Cell(-5);
$pdf->Cell(20,5,'Kepada');
$pdf->Cell(15,5,' : '.$dataInvoice['namcus']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(20,5,'No Invoice');
$pdf->Cell(15,5,' : '.$dataInvoice['inv']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(20,5,'Tanggal');
$pdf->Cell(15,5,' : '.$dataInvoice['tgls']);

//Fields Name position
$Y_Fields_Name_position = 53;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(230,230,230);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(10,8,'No',1,0,'C',1);

$pdf->SetX(15);
$pdf->Cell(40,8,'AWB',1,0,'C',1);

$pdf->SetX(55);
$pdf->Cell(30,8,'Flight',1,0,'C',1);

$pdf->SetX(85);
$pdf->Cell(30,8,'Tanggal',1,0,'C',1);

$pdf->SetX(115);
$pdf->Cell(40,8,'Tujuan',1,0,'C',1);

$pdf->SetX(155);
$pdf->Cell(20,8,'Koli',1,0,'C',1);

$pdf->SetX(175);
$pdf->Cell(20,8,'Kilo',1,0,'C',1);

$pdf->SetX(195);
$pdf->Cell(25,8,'Tarif',1,0,'C',1);

$pdf->SetX(220);
$pdf->Cell(25,8,'Prepaid',1,0,'C',1);

$pdf->SetX(245);
$pdf->Cell(22,8,'Admin',1,0,'C',1);

$pdf->SetX(267);
$pdf->Cell(25,8,'Jumlah',1,0,'C',1);


$pdf->Ln();
$Y_Table_Position = 61;
$total_semua = 0;
$pdf->SetFont('Arial','',10);
while($row = mysql_fetch_array($result))
{   
    $prep = $row["berat"] * $row["harga"];
    $c1 = '   '.$angka;
    $c2 = '   '.$row["awb"];
    $c3 = '   '.$row["flight"];
    $c4 = '   '.$dataInvoice['tgls'];
    $c5 = '  '.$row["dest"];
    $c6 = '  '.$row["koli"];
    $c7 = '   '.$row["berat"];
    $c8 = '   '.number_format($row["harga"]);
    $c9 = '   '.number_format($prep);
    $c10 = '  '.number_format($row["tambahan"]);
    $c11 = '  '.number_format($row["total"]);

    if($Y_Table_Position > 184)
    {
     $pdf->AddPage();
     $pdf->Image('logo.png',5,10,-650);

     $pdf->SetFont('Arial','B',17);
     $pdf->Cell(20);
     $pdf->Cell(235,1,'PT. BORNEO ALFA CARGO');
     $pdf->Cell(30,1,'INVOICE');
     $pdf->SetFont('Arial','B',10);
     $pdf->Ln(5);
     $pdf->Cell(20);
     $pdf->Cell(100,5,'Jl Adonis Samad/Bakung Menara nomor 43,');
     $pdf->SetFont('Arial','',9);
     $pdf->Ln(5);
     $pdf->Cell(20);
     $pdf->Cell(100,5,'Kec. Pahandut, Kota. Palangkaraya, Kalimantan Tengah 74874');
     $pdf->Ln(5);
     $pdf->Cell(20);
     $pdf->Cell(100,5,'Telp : 082149111401');
     $pdf->SetFont('Arial','B',9);
     $pdf->Ln(8);
     $pdf->Cell(-5);
     $pdf->Cell(20,5,'Kepada');
     $pdf->Cell(15,5,' : '.$dataInvoice['namcus']);
     $pdf->Ln(5);
     $pdf->Cell(-5);
     $pdf->Cell(20,5,'No Invoice');
     $pdf->Cell(15,5,' : '.$dataInvoice['inv']);
     $pdf->Ln(5);
     $pdf->Cell(-5);
     $pdf->Cell(20,5,'Tanggal');
     $pdf->Cell(15,5,' : '.$dataInvoice['tgls']);

//Fields Name position
     $Y_Fields_Name_position = 53;

//First create each Field Name
//Gray color filling each Field Name box
     $pdf->SetFillColor(230,230,230);
//Bold Font for Field Name
     $pdf->SetFont('Arial','B',10);
     $pdf->SetY($Y_Fields_Name_position);

     $pdf->SetX(5);
     $pdf->Cell(10,8,'No',1,0,'C',1);

     $pdf->SetX(15);
     $pdf->Cell(40,8,'AWB',1,0,'C',1);

     $pdf->SetX(55);
     $pdf->Cell(30,8,'Flight',1,0,'C',1);

     $pdf->SetX(85);
     $pdf->Cell(30,8,'Tanggal',1,0,'C',1);

     $pdf->SetX(115);
     $pdf->Cell(40,8,'Tujuan',1,0,'C',1);

     $pdf->SetX(155);
     $pdf->Cell(20,8,'Koli',1,0,'C',1);

     $pdf->SetX(175);
     $pdf->Cell(20,8,'Kilo',1,0,'C',1);

     $pdf->SetX(195);
     $pdf->Cell(25,8,'Tarif',1,0,'C',1);

     $pdf->SetX(220);
     $pdf->Cell(25,8,'Prepaid',1,0,'C',1);

     $pdf->SetX(245);
     $pdf->Cell(22,8,'Admin',1,0,'C',1);

     $pdf->SetX(267);
     $pdf->Cell(25,8,'Jumlah',1,0,'C',1);

     $Y_Table_Position = 61;
 }
$pdf->SetFont('Arial','',10);
 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(5);
 $pdf->Cell(10,7,$c1,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(15);
 $pdf->Cell(40,7,$c2,1,'L');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(55);
 $pdf->Cell(30,7,$c3,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(85);
 $pdf->Cell(30,7,$c4,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(115);
 $pdf->Cell(40,7,$c5,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(155);
 $pdf->Cell(20,7,$c6,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(175);
 $pdf->Cell(20,7,$c7,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(195);
 $pdf->Cell(25,7,$c8,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(220);
 $pdf->Cell(25,7,$c9,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(245);
 $pdf->Cell(22,7,$c10,1,'C');

 $pdf->SetY($Y_Table_Position);
 $pdf->SetX(267);
 $pdf->Cell(25,7,$c11,1,'C');

 $Y_Table_Position = $Y_Table_Position + 7;
 $angka++;
 $total_semua = $total_semua + $row["no"] * $row["no"];
}

if($Y_Table_Position > 184)
{
 $pdf->AddPage();
 $pdf->Image('logo.png',5,10,-650);

 $pdf->SetFont('Arial','B',17);
 $pdf->Cell(20);
 $pdf->Cell(235,1,'PT. SYSTEM DEMO');
 $pdf->Cell(30,1,'INVOICE');
 $pdf->SetFont('Arial','B',10);
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Jl DEMO SYETEM, Menara nomor 43,');
 $pdf->SetFont('Arial','',9);
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Kec. Kalideres, Kota. Kalideres, Kalimantan Tengah 74874');
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Telp : 0888788788787');
 $pdf->SetFont('Arial','B',9);
 $pdf->Ln(8);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'Kepada');
 $pdf->Cell(15,5,' : '.$dataInvoice['namcus']);
 $pdf->Ln(5);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'No Invoice');
 $pdf->Cell(15,5,' : '.$dataInvoice['inv']);
 $pdf->Ln(5);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'Tanggal');
 $pdf->Cell(15,5,' : '.$dataInvoice['tgls']);

 $Y_Fields_Name_position = 53;

 $Y_Table_Position = 61;
}

$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(240,7,'Total        ',1,'','R','');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(245);
$pdf->Cell(47,7,'  Rp. '.number_format($dataInvoice['total']),1,'C');

$Y_Table_Position = $Y_Table_Position + 7;

if($Y_Table_Position > 184)
{
 $pdf->AddPage();
 $pdf->Image('logo.png',5,10,-650);

 $pdf->SetFont('Arial','B',17);
 $pdf->Cell(20);
 $pdf->Cell(235,1,'PT. SYSTEM DEMO');
 $pdf->Cell(30,1,'INVOICE');
 $pdf->SetFont('Arial','B',10);
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Jl DEMO SYETEM, Menara nomor 43,');
 $pdf->SetFont('Arial','',9);
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Kec. Kalideres, Kota. Kalideres, Kalimantan Tengah 74874');
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Telp : 082149111401');
 $pdf->SetFont('Arial','B',9);
 $pdf->Ln(8);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'Kepada');
 $pdf->Cell(15,5,' : '.$dataInvoice['namcus']);
 $pdf->Ln(5);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'No Invoice');
 $pdf->Cell(15,5,' : '.$dataInvoice['inv']);
 $pdf->Ln(5);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'Tanggal');
 $pdf->Cell(15,5,' : '.$dataInvoice['tgls']);

 $Y_Fields_Name_position = 53;

 $Y_Table_Position = 61;
}

$pdf->SetFont('Arial','',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(240,7,'PPN 1.1%        ',1,'','R','');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(245);
$pdf->Cell(47,7,'  Rp. '.number_format($dataInvoice['lainya']),1,'C');

$Y_Table_Position = $Y_Table_Position + 7;

if($Y_Table_Position > 184)
{
   $pdf->AddPage();
   $pdf->Image('logo.png',5,10,-650);

   $pdf->SetFont('Arial','B',17);
   $pdf->Cell(20);
   $pdf->Cell(235,1,'PT. SYSTEM DEMO');
   $pdf->Cell(30,1,'INVOICE');
   $pdf->SetFont('Arial','B',10);
   $pdf->Ln(5);
   $pdf->Cell(20);
   $pdf->Cell(100,5,'Jl Adonis Samad/Bakung Menara nomor 43,');
   $pdf->SetFont('Arial','',9);
   $pdf->Ln(5);
   $pdf->Cell(20);
   $pdf->Cell(100,5,'Kec. Pahandut, Kota. Palangkaraya, Kalimantan Tengah 74874');
   $pdf->Ln(5);
   $pdf->Cell(20);
   $pdf->Cell(100,5,'Telp : 082149111401');
   $pdf->SetFont('Arial','B',9);
   $pdf->Ln(8);
   $pdf->Cell(-5);
   $pdf->Cell(20,5,'Kepada');
   $pdf->Cell(15,5,' : '.$dataInvoice['namcus']);
   $pdf->Ln(5);
   $pdf->Cell(-5);
   $pdf->Cell(20,5,'No Invoice');
   $pdf->Cell(15,5,' : '.$dataInvoice['inv']);
   $pdf->Ln(5);
   $pdf->Cell(-5);
   $pdf->Cell(20,5,'Tanggal');
   $pdf->Cell(15,5,' : '.$dataInvoice['tgls']);

   $Y_Fields_Name_position = 53;

   $Y_Table_Position = 61;
}

$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(240,7,'Grand Total        ',1,'','R','');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(245);
$pdf->Cell(47,7,'  Rp. '.number_format($dataInvoice['grandtotal']),1,'C');

$Y_Table_Position = $Y_Table_Position + 7;

if($Y_Table_Position > 184)
{
 $pdf->AddPage();
 $pdf->Image('logo.png',5,10,-650);

 $pdf->SetFont('Arial','B',17);
 $pdf->Cell(20);
 $pdf->Cell(235,1,'PT. SYSTEM DEMO');
 $pdf->Cell(30,1,'INVOICE');
 $pdf->SetFont('Arial','B',10);
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Jl Adonis Samad/Bakung Menara nomor 43,');
 $pdf->SetFont('Arial','',9);
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Kec. Pahandut, Kota. Palangkaraya, Kalimantan Tengah 74874');
 $pdf->Ln(5);
 $pdf->Cell(20);
 $pdf->Cell(100,5,'Telp : 082149111401');
 $pdf->SetFont('Arial','B',9);
 $pdf->Ln(8);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'Kepada');
 $pdf->Cell(15,5,' : '.$dataInvoice['namcus']);
 $pdf->Ln(5);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'No Invoice');
 $pdf->Cell(15,5,' : '.$dataInvoice['inv']);
 $pdf->Ln(5);
 $pdf->Cell(-5);
 $pdf->Cell(20,5,'Tanggal');
 $pdf->Cell(15,5,' : '.$dataInvoice['tgls']);

 $Y_Fields_Name_position = 53;

 $Y_Table_Position = 61;
}

$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(287,7,'Terbilang : '.penyebut($dataInvoice['grandtotal']). ' Rupiah',1,'','L','');

$pdf->Ln(10);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(-5);
$pdf->Cell(140,5,'Keterangan');
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(180,5,'Mohon pembayaran ditransfer ke rekening bank berikut ini:');
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(250,5,'BNI Cabang xxxxx');
$pdf->Cell(30,1,'Hormat Saya');
$pdf->Ln(7);
$pdf->Cell(-5);
$pdf->Cell(180,5,'1234 5678 910');
$pdf->Ln(7);
$pdf->Cell(-5);
$pdf->Cell(255,5,'a.n Demo System,PT');
$pdf->Cell(30,1,'');
$pdf->Ln(14);
$pdf->Cell(-5);
$pdf->Cell(255,5,'');
$pdf->Cell(30,1,'Finance');

//============================================================================

$pdf->Output();

function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = penyebut($nilai - 10). " Belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " Seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " Seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
}


?>

