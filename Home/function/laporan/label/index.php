<?php
include "../../connect.php";
require('../code128.php');

$cariFilter = "";
$ukuran = "5025";



if($ukuran == "5025")
{
  $pdf = new PDF_Code128('L','mm',array(55,25)); //L For Landscape / P For Portrait
  $pdf->SetAutoPageBreak(false);

  $sql1=mysql_query("select P.*,R.nama as namrak,B.nama as nambrand from product P
    inner join rak R on R.no = P.rak
    left outer join brand B on B.no = P.kategory
    where P.status = '1'
    $cariFilter limit 100");
  while($data1=mysql_fetch_array($sql1))
  { 
    $txts = $data1['barcode'];
    $kata1 = substr($txts,0,30);
    $kata2 = "";
    if(strlen($txts) > 30)
    {
      $kata2 = substr($txts,30,strlen($txts));
    }
    $ket = substr($data1['nama'],0,30);
    if($data1['nama'] == "")
    {
      $ket = "-";
    }

    $pdf->AddPage();

    $pdf->SetFont('Arial','',8);
    $pdf->Code128(1,1,$data1['kode'],50,5);
    $pdf->Cell(0,-3,$data1['kode'],0,0,'C');
    $pdf->Ln(5);
    $pdf->Cell(0,-3,$kata1,0,0,'C');
    $pdf->Ln(3);
    $pdf->Cell(0,-3,$kata2,0,0,'C');
    $pdf->Ln(5);
    $pdf->Cell(0,-3,$ket,0,0,'C');
  }
}

if($ukuran == "5025A")
{
  $pdf = new PDF_Code128('L','mm',array(55,25)); //L For Landscape / P For Portrait
  $pdf->SetAutoPageBreak(false);

  $sql1=mysql_query("select P.*,R.nama as namrak,B.nama as nambrand from product P
    inner join rak R on R.no = P.rak
    left outer join brand B on B.no = P.kategory
    where P.status = '1'
    $cariFilter limit 100");
  while($data1=mysql_fetch_array($sql1))
  { 
    $txts = $data1['nama'];
    $kata1 = substr($txts,0,30);
    $kata2 = "";
    if(strlen($txts) > 30)
    {
      $kata2 = substr($txts,30,strlen($txts));
    }
    $ket = substr($data1['keterangan'],0,30);
    if($data1['keterangan'] == "")
    {
      $ket = "-";
    }

    $pdf->AddPage();

    $pdf->SetFont('Arial','',8);
    $pdf->Code128(1,1,$data1['kode'],50,5);
    $pdf->Cell(0,-3,$data1['kode'],0,0,'C');
    $pdf->Ln(4);
    $pdf->Cell(0,-3,$kata1,0,0,'C');
    $pdf->Ln(3);
    $pdf->Cell(0,-3,$kata2,0,0,'C');
    $pdf->Ln(4);
    $pdf->Cell(0,-3,$ket,0,0,'C');
    $pdf->SetFont('Arial','',7);
    $pdf->Ln(3);
    $pdf->Cell(0,-3,"Everest Motor Cinere -081285191664",0,0,'C');
  }
}

if($ukuran == "4020")
{
  $pdf = new PDF_Code128('L','mm',array(40,20)); //L For Landscape / P For Portrait
  $pdf->SetAutoPageBreak(false);

  $sql1=mysql_query("select P.*,R.nama as namrak,B.nama as nambrand from product P
    inner join rak R on R.no = P.rak
    left outer join brand B on B.no = P.kategory
    where P.status = '1'
    $cariFilter limit 100");
  while($data1=mysql_fetch_array($sql1))
  { 
    $txts = $data1['nama'];
    $kata1 = substr($txts,0,23);
    $kata2 = "";
    if(strlen($txts) > 23)
    {
      $kata2 = substr($txts,23,23);
    }
    $ket = substr($data1['keterangan'],0,23);
    if($data1['keterangan'] == "")
    {
      $ket = "-";
    }

    $pdf->AddPage();

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(0,-14,$data1['kode'],0,0,'C');
    $pdf->Ln(4);
    $pdf->SetFont('Arial','',7);
    $pdf->Cell(0,-14,$kata1,0,0,'C');
    $pdf->Ln(3);
    $pdf->Cell(0,-14,$kata2,0,0,'C');
    $pdf->Ln(4);
    $pdf->Cell(0,-14,$ket,0,0,'C');
    $pdf->Ln(3);
    $pdf->Cell(0,-14,"10.3.24",0,0,'R');
  }
}

if($ukuran == "7020")
{
  $pdf = new PDF_Code128('L','mm',array(70,20)); //L For Landscape / P For Portrait
  $pdf->SetAutoPageBreak(false);

  $sql1=mysql_query("select P.*,R.nama as namrak,B.nama as nambrand from product P
    inner join rak R on R.no = P.rak
    left outer join brand B on B.no = P.kategory
    where P.status = '1'
    $cariFilter limit 100");
  while($data1=mysql_fetch_array($sql1))
  { 
    $txts = $data1['nama'];
    $kata1 = substr($txts,0,23);
    $kata2 = "";
    if(strlen($txts) > 23)
    {
      $kata2 = substr($txts,23,23);
    }
    $ket = substr($data1['keterangan'],0,23);
    if($data1['keterangan'] == "")
    {
      $ket = "-";
    }

    $pdf->AddPage();

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(0,-14,$data1['kode'],0,0,'C');
    $pdf->Ln(4);
    $pdf->SetFont('Arial','',7);
    $pdf->Cell(0,-14,$kata1,0,0,'C');
    $pdf->Ln(3);
    $pdf->Cell(0,-14,$kata2,0,0,'C');
    $pdf->Ln(4);
    $pdf->Cell(0,-14,$ket,0,0,'C');
    $pdf->Ln(3);
    $pdf->Cell(0,-14,"10.3.24",0,0,'R');
  }
}


$pdf->Output();
?>
