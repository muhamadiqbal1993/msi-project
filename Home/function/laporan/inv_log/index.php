<?php
include "../../connect.php";
require('../fpdf17/fpdf.php');
//Menampilkan data dari tabel di database
$id = $_GET['id'];

$tgl_skr = date('d F Y');
$customer = "";
$alamat = "";
$alamat2 = "";
$pic = "";
$invoice = "";
$no_inv = "";
$tanggal = "";

$dataInvoice = mysql_fetch_array(mysql_query("select I.*,C.nama as namcus,DATE_FORMAT(I.tanggal, '%d %b %Y') as tgls,
DATE_FORMAT(I.tanggal, '%d-%b-%Y') as lins
 from invoice I
    inner join customer C on C.no = I.customer
    where I.no='$id'"));

$tampil= "select * from invoice_detail where invoice = '".$dataInvoice['inv']."'";
$result=mysql_query($tampil);
//Inisiasi untuk membuat header kolom
$c1 = "";
$c2 = "";
$c3 = "";
$c4 = "";
$c5 = "";
$angka = 1;
//For each row, add the field to the corresponding column


$pdf = new FPDF('L','mm',array(210,297)); //L For Landscape / P For Portrait
$pdf->AddPage();

//Menambahkan Gambar
//$pdf->Image('logo.jpg',10,10,-175);
$pdf->Ln(-7);
$pdf->SetFont('Arial','',10);
$pdf->Cell(-5);
$pdf->Cell(20,5,'Nomor');
$pdf->Cell(215,5,' : '.$dataInvoice['inv']);
$pdf->Cell(30,5,'Palangkaraya, '.$tgl_skr);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(20,5,'Hal');
$pdf->Cell(15,5,' : Perincian Jasa Handling');

$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(235,1,'Kepada Yth, ');
$pdf->SetFont('Arial','B',10);
$pdf->Ln(3);
$pdf->Cell(-5);
$pdf->Cell(100,5,'PT. Logistic One Solution');
$pdf->SetFont('Arial','',10);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(100,5,'Jl. S Parman Kav 22 - 24');
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(100,5,'Grand Slipi Tower 16 Floor Suite 16F');

$pdf->Ln(8);
$pdf->Cell(-5);
$pdf->Cell(100,5,'Di-');
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->SetFont('Arial','BU',10);
$pdf->Cell(100,5,'JAKARTA');
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->SetFont('Arial','',10);
$pdf->Cell(100,5,'Up.Bag Keuangan');

$pdf->Ln(8);
$pdf->Cell(-5);
$pdf->Cell(100,5,'Dengan Hormat');
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(100,5,'Dengan ini kami sampaikan perincian Jasa Handling Palangka Raya sebagai berikut:');

//Fields Name position
$Y_Fields_Name_position = 70;

//Gray color filling each Field Name box
$pdf->SetFillColor(230,230,230);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(25,8,'Tgl Delivery',1,0,'C',1);

$pdf->SetX(30);
$pdf->Cell(30,8,'Delivery Docket',1,0,'C',1);

$pdf->SetX(60);
$pdf->Cell(45,8,'Pengirim',1,0,'C',1);

$pdf->SetX(105);
$pdf->Cell(45,8,'Penerima',1,0,'C',1);

$pdf->SetX(150);
$pdf->Cell(40,8,'No SMU',1,0,'C',1);

$pdf->SetX(190);
$pdf->Cell(18,8,'Koli',1,0,'C',1);

$pdf->SetX(208);
$pdf->Cell(18,8,'Kilo',1,0,'C',1);

$pdf->SetX(226);
$pdf->Cell(26,8,'Jasa Handling',1,0,'C',1);

$pdf->SetX(252);
$pdf->Cell(25,8,'Total',1,0,'C',1);

$pdf->SetX(277);
$pdf->Cell(15,8,'Ket',1,0,'C',1);

$pdf->Ln();
$Y_Table_Position = 78;
$total_semua = 0;
$pdf->SetFont('Arial','',10);
while($row = mysql_fetch_array($result))
{   
    $prep = $row["berat"] * $row["harga"];
    $c1 = ' '.$dataInvoice["lins"];
    $c2 = '   ';
    $c3 = '   '.$row["pengirim"];
    $c4 = '  '.$row["penerima"];
    $c5 = '  '.$row["awb"];
    $c6 = '   '.$row["koli"];
    $c7 = '   '.$row["berat"];
    $c8 = '   '.number_format($row["harga"]);
    $c9 = '  '.number_format($row["total"]);
    $c10 = '  20 Kg';

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(5);
    $pdf->Cell(25,7,$c1,1,'L');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(30);
    $pdf->Cell(30,7,$c2,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(60);
    $pdf->Cell(45,7,$c3,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(105);
    $pdf->Cell(45,7,$c4,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(150);
    $pdf->Cell(40,7,$c5,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(190);
    $pdf->Cell(18,7,$c6,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(208);
    $pdf->Cell(18,7,$c7,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(226);
    $pdf->Cell(26,7,$c8,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(252);
    $pdf->Cell(25,7,$c9,1,'C');

    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(277);
    $pdf->Cell(15,7,$c10,1,'C');

    $Y_Table_Position = $Y_Table_Position + 7;
    $angka++;
    $total_semua = $total_semua + $row["no"] * $row["no"];
}

$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(240,7,'Total        ',1,'','R','');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(245);
$pdf->Cell(47,7,'  Rp. '.number_format($dataInvoice['total']),1,'C');

$Y_Table_Position = $Y_Table_Position + 7;

$pdf->SetFont('Arial','',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(240,7,'Pajak        ',1,'','R','');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(245);
$pdf->Cell(47,7,'  Rp. '.number_format($dataInvoice['lainya']),1,'C');

$Y_Table_Position = $Y_Table_Position + 7;

$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(240,7,'Grand Total        ',1,'','R','');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(245);
$pdf->Cell(47,7,'  Rp. '.number_format($dataInvoice['grandtotal']),1,'C');

$Y_Table_Position = $Y_Table_Position + 7;

$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(287,7,'Terbilang : '.penyebut($dataInvoice['grandtotal']). 'Rupiah',1,'','L','');

$pdf->SetFont('Arial','BU',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(180,5,'TRANSFER VIA');
$pdf->SetFont('Arial','B',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(250,5,'BANK BCA 657 5192 061');
$pdf->Cell(30,1,'Salam Hormat');
$pdf->Ln(7);
$pdf->Cell(-5);
$pdf->Cell(180,5,'a.n Borneo Alfa Cargo,PT');
$pdf->Ln(7);
$pdf->Cell(-5);
$pdf->Cell(245,5,'');
$pdf->Cell(30,1,'PT Borneo Alfa Cargo');

$pdf->SetFont('Arial','BU',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(180,5,'FINANCE');
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->SetFont('Arial','',10);
$pdf->Cell(180,5,'Mohon bantuan bukti transfer dapat diemailkan ke "cargoborneoalfa@gmail.com"');

//============================================================================

$pdf->Output();

function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = penyebut($nilai - 10). " Belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " Seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " Seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
}
?>


