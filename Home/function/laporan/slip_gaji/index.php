<?php
include "../../connect.php";
require('../fpdf17/fpdf.php');
//Menampilkan data dari tabel di database
$id = $_GET['id'];

$dataSLip = mysql_fetch_array(mysql_query("select SGK.*,SG.bulan,SG.tahun from slip_gaji_kar SGK
	inner join slip_gaji SG on SG.id = SGK.id_slip
	where SGK.no= '$id'"));

$dataKaryawan = mysql_fetch_array(mysql_query("select K.*,J.nama as namjab from karyawan K
	inner join group_divisi J on J.no = K.jabatan
	where K.no = '".$dataSLip['id_kar']."'"));

$lineTerima = mysql_fetch_array(mysql_query("select count(*) as total from komponen where dapat = '1'"));
$linePotong = mysql_fetch_array(mysql_query("select count(*) as total from komponen where dapat = '2'"));
$totalLine = $lineTerima['total'];
if($totalLine < $linePotong['total'])
{
	$totalLine = $linePotong['total'];
}

$dataVoucher = mysql_fetch_array(mysql_query("select PI.*,DATE_FORMAT(PI.tanggal, '%d %b %Y') as tanggal_new,K.bank,K.nama_acc from payment_in PI
	inner join mutasi_detail MD on MD.no = PI.pembayaran
	inner join mutasi M on M.id = MD.id_trx
	inner join kasbank K on K.no = M.kasbank
	where PI.no = '$id'"));
$tgl_skr = date('d F Y');
$customer = "";
$alamat = "";
$alamat2 = "";
$pic = "";
$invoice = "";
$no_inv = "";
$tanggal = "";

$tampil= "select * from user ";
//Inisiasi untuk membuat header kolom
$c1 = "";
$c2 = "";
$c3 = "";
$c4 = "";
$c5 = "";
$angka = 1;
//For each row, add the field to the corresponding column


$pdf = new FPDF('L','mm',array(148,210)); //L For Landscape / P For Portrait
$pdf->AddPage();

//Menambahkan Gambar
$pdf->Image('logo.png',5,5,-650);

$pdf->SetFont('Arial','B',15);
$pdf->Cell(20);
$pdf->Cell(5,7,'PT Borneo Alfa Cargo');
$pdf->Ln(17);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(-5);
$pdf->Cell(25,5,'Nama');
$pdf->Cell(5,5,' : '.$dataKaryawan['nama']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(25,5,'Jabatan');
$pdf->Cell(5,5,' : '.$dataKaryawan['namjab']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(25,5,'Periode');
$pdf->Cell(5,5,' : '.$dataSLip['bulan'].' - '.$dataSLip['tahun']);
//Fields Name position
$Y_Fields_Name_position = 45;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(219,197,197);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(98,8,'PENERIMAAN',1,0,'C',1);

$pdf->SetX(103);
$pdf->Cell(102,8,'POTONGAN',1,0,'C',1);


$pdf->Ln();
$Y_Table_Position = 53;
$total_semua = 0;
$t_debit = 0;
$t_credit = 0;
$pdf->SetFont('Arial','',8);

$totalTer = 0;
$totalPot = 0;

for ($i=0; $i < $totalLine; $i++) 
{ 	
	$t_lineTerima = mysql_fetch_array(mysql_query("select * from komponen where dapat = '1' limit ".$i.",1"));
	$t_linePotong = mysql_fetch_array(mysql_query("select * from komponen where dapat = '2' limit ".$i.",1"));

	$nilaiTerima = mysql_fetch_array(mysql_query("select nilai from slip_gaji_kar_det where id_slip = '".$dataSLip['id_slip']."' and id_kar = '".$dataSLip['id_kar']."' and id_kom = '".$t_lineTerima['no']."'"));
	$nilaiPotong = mysql_fetch_array(mysql_query("select nilai from slip_gaji_kar_det where id_slip = '".$dataSLip['id_slip']."' and id_kar = '".$dataSLip['id_kar']."' and id_kom = '".$t_linePotong['no']."'"));

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(5);
	$pdf->Cell(40,7,$t_lineTerima['nama'],1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(45);
	$pdf->Cell(58,7,number_format($nilaiTerima['nilai']),1,'L');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(103);
	$pdf->Cell(40,7,$t_linePotong['nama'],1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(143);
	$pdf->Cell(62,7,number_format($nilaiPotong['nilai']),1,'C');

	$totalTer += $nilaiTerima['nilai'];
	$totalPot += $nilaiPotong['nilai'];

	$Y_Table_Position = $Y_Table_Position + 7;
	$angka++;

}

$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(219,197,197);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(40,7,"TOTAL PENERIMAAN",1,'C');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(45);
$pdf->Cell(58,7,number_format($totalTer),1,'L');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(103);
$pdf->Cell(40,7,"TOTAL POTONGAN",1,'C');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(143);
$pdf->Cell(62,7,number_format($totalPot),1,'C');

$pdf->SetY($Y_Table_Position+7);
$pdf->SetX(5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(200,7,"TOTAL PENERIMAAN : Rp. ".number_format($dataSLip['total']),1,'','L','');

// $pdf->Ln(10);
// $pdf->SetFont('Arial','B',10);
// $pdf->Cell(140,5,'Keterangan');
// $pdf->Ln(5);
// $pdf->Cell(180,5,'Untuk Pembayaran Dapat Dibayarkan Ke Rekening BCA 6470411021 a/n Muhamad Iqbal');

$pdf->Ln(10);
$pdf->SetFont('Arial','B',8);
$pdf->SetX(10);
$pdf->Cell(85,5,'         PENERIMA');
$pdf->Cell(80,5,'');
$pdf->Cell(70,5,'DIREKTUR');
$pdf->Ln(20);
$pdf->SetX(12);
$pdf->Cell(75,5,'(________________)');
$pdf->Cell(80,5,'');
$pdf->Cell(20,5,'AHMAD FAIZ BURAIDAH');

//============================================================================

$pdf->Output();

function penyebut($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = penyebut($nilai - 10). " Belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " Seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " Seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
	}     
	return $temp;
}
?>
