<?php
include "../../connect.php";
require('../fpdf17/fpdf.php');
//Menampilkan data dari tabel di database
$id = $_GET['id'];

$tgl_skr = date('d F Y');
$customer = "";
$alamat = "";
$alamat2 = "";
$pic = "";
$invoice = "";
$no_inv = "";
$tanggal = "";

$dataInvoice = mysql_fetch_array(mysql_query("select I.*,C.nama as namcus,C.contact,C.alamat as namal,C.phone as namphone,C.email as namel,DATE_FORMAT(I.tanggal, '%d %M %Y') as tgls,DATE_FORMAT(I.due, '%d %M %Y') as dues from invoice I
	inner join customer C on C.no = I.customer
	where I.no='$id'"));

$pembayaran = "Pelunasan Sebelum Barang Dikirimkan";


$pemenuhan = $dataInvoice['pemenuhan'];


if($dataInvoice['pembayaran'] == "1")
{
	$pembayaran = "Down Payment 50%";
}

if($dataInvoice['pembayaran'] == "2")
{
	$pembayaran = "Down Payment 30%";
}

$tampil= "select IDR.*,P.nama as nampro,P.barcode from invoice_detail_retail IDR
inner join product P on P.no = IDR.produk
where IDR.id_order = '".$dataInvoice['inv']."'";
$result=mysql_query($tampil);
//Inisiasi untuk membuat header kolom
$c1 = "";
$c2 = "";
$c3 = "";
$c4 = "";
$c5 = "";
$angka = 1;
//For each row, add the field to the corresponding column


$pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
$pdf->AddPage();

//Menambahkan Gambar
$pdf->Image('logo.png',25,5,-200);

$pdf->Ln(25);
$pdf->SetFont('Arial','B',11);
$pdf->Ln(7);
$pdf->Cell(0,0,'QUOTATION',0,1,'C');


//Fields Name position
$Y_Fields_Name_position = 47;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(255,255,255);
//Bold Font for Field Name
$pdf->SetFont('Arial','',9);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(20,6,'To',1,0,'L',1);
$pdf->SetX(25);
$pdf->Cell(80,6,' '.$dataInvoice['namcus'],1,0,'L',1);
$pdf->SetX(105);
$pdf->Cell(20,6,'Date',1,0,'L',1);
$pdf->SetX(125);
$pdf->Cell(80,6,' '.$dataInvoice['tgls'],1,0,'L',1);

$Y_Fields_Name_position = $Y_Fields_Name_position+6;
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(20,6,'Attn',1,0,'L',1);
$pdf->SetX(25);
$pdf->Cell(80,6,' '.$dataInvoice['contact'],1,0,'L',1);
$pdf->SetX(105);
$pdf->Cell(20,6,'From',1,0,'L',1);
$pdf->SetX(125);
$pdf->Cell(80,6,' Mulyadi',1,0,'L',1);

$Y_Fields_Name_position = $Y_Fields_Name_position+6;
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(20,6,'Phone',1,0,'L',1);
$pdf->SetX(25);
$pdf->Cell(80,6,' '.$dataInvoice['namphone'],1,0,'L',1);
$pdf->SetX(105);
$pdf->Cell(20,6,'Phone',1,0,'L',1);
$pdf->SetX(125);
$pdf->Cell(80,6,' '.$dataInvoice['namphone'],1,0,'L',1);

$Y_Fields_Name_position = $Y_Fields_Name_position+6;
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(20,6,'Fax',1,0,'L',1);
$pdf->SetX(25);
$pdf->Cell(80,6,' - ',1,0,'L',1);
$pdf->SetX(105);
$pdf->Cell(20,6,'Fax',1,0,'L',1);
$pdf->SetX(125);
$pdf->Cell(80,6,' ',1,0,'L',1);

$Y_Fields_Name_position = $Y_Fields_Name_position+6;
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(20,6,'Subject',1,0,'L',1);
$pdf->SetX(25);
$pdf->Cell(80,6,' '.$dataInvoice['keterangan'],1,0,'L',1);
$pdf->SetX(105);
$pdf->Cell(20,6,'No',1,0,'L',1);
$pdf->SetX(125);
$pdf->Cell(80,6,' '.$dataInvoice['inv'],1,0,'L',1);

$Y_Fields_Name_position = $Y_Fields_Name_position+6;
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(20,6,'Email',1,0,'L',1);
$pdf->SetX(25);
$pdf->Cell(80,6,' '.$dataInvoice['namel'],1,0,'L',1);
$pdf->SetX(105);
$pdf->Cell(20,6,'Ref No',1,0,'L',1);
$pdf->SetX(125);
$pdf->Cell(80,6,' ',1,0,'L',1);

$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(20,5,'Berikut kami berikan penawaran harga untuk pengadaan peralatan Laboratorium');

$Y_Fields_Name_position = $Y_Fields_Name_position + 18;

$pdf->SetFillColor(230,230,230);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(10,8,'No',1,0,'C',1);

$pdf->SetX(15);
$pdf->Cell(60,8,'Description ',1,0,'C',1);

$pdf->SetX(75);
$pdf->Cell(40,8,'Merk / Type ',1,0,'C',1);

$pdf->SetX(115);
$pdf->Cell(10,8,'Qty',1,0,'C',1);

$pdf->SetX(125);
$pdf->Cell(30,8,'Unit Price (Rp.)',1,0,'C',1);

$pdf->SetX(155);
$pdf->Cell(20,8,'Disc (%)',1,0,'C',1);

$pdf->SetX(175);
$pdf->Cell(30,8,'Total Price (Rp.)',1,0,'C',1);


$pdf->Ln();
$Y_Table_Position = $Y_Fields_Name_position + 8;
$total_semua = 0;
$pdf->SetFont('Arial','',10);
while($row = mysql_fetch_array($result))
{   
	$c1 = '   '.$angka;
	$c2 = '   '.$row["nampro"];
	$c3 = '   '.$row["barcode"];
	$c4 = '   '.$row["qty"];
	$c5 = '   '.number_format($row["harga_jual"]);
	$c6 = '   '.$row["disc_persen"];
	$c7 = '   '.number_format($row["total"]);

	$pdf->SetFont('Arial','',10);
	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(5);
	$pdf->Cell(10,7,$c1,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(15);
	$pdf->Cell(60,7,$c2,1,'L');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(75);
	$pdf->Cell(40,7,$c3,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(115);
	$pdf->Cell(10,7,$c4,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(125);
	$pdf->Cell(30,7,$c5,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(155);
	$pdf->Cell(20,7,$c6,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(175);
	$pdf->Cell(30,7,$c7,1,'C');

	$Y_Table_Position = $Y_Table_Position + 7;
	$angka++;
	$total_semua = $total_semua + $row["no"] * $row["no"];
}

$pdf->SetFont('Arial','',10);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(170,7,'Subtotal        ',1,'','R','');
$pdf->SetX(175);
$pdf->Cell(30,7,number_format($dataInvoice['total']),1,'','R','');

$Y_Table_Position = $Y_Table_Position + 7;
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(170,7,'PPN 11%        ',1,'','R','');
$pdf->SetX(175);
$pdf->Cell(30,7,number_format($dataInvoice['lainya']),1,'','R','');

$pdf->SetFont('Arial','B',11);
$Y_Table_Position = $Y_Table_Position + 7;
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(170,7,'Total        ',1,'','R','');
$pdf->SetX(175);
$pdf->Cell(30,7,number_format($dataInvoice['grandtotal']),1,'','R','');

$pdf->SetFont('Arial','B',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(55,5,'Keterangan ');

$pdf->SetFont('Arial','',8.5);
$pdf->Ln(7);
$pdf->Cell(10);
$pdf->Cell(10,5,'1.');
$pdf->Cell(45,5,'Franco');
$pdf->Cell(15,5,' : '.$dataInvoice['franco']);

$pdf->Ln(5);
$pdf->Cell(10);
$pdf->Cell(10,5,'2.');
$pdf->Cell(45,5,'Masa Berlaku Penawaran');
$pdf->Cell(15,5,' : 30 Hari kerja setelah penawaran ini diterima');

$pdf->Ln(5);
$pdf->Cell(10);
$pdf->Cell(10,5,'3.');
$pdf->Cell(45,5,'Pemenuhan Barang');
$pdf->Cell(15,5,' : '.$pemenuhan);

$pdf->Ln(5);
$pdf->Cell(10);
$pdf->Cell(10,5,'4.');
$pdf->Cell(45,5,'Pembayaran');
$pdf->Cell(15,5,' : '.$pembayaran);

$pdf->SetFont('Arial','',10);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(55,5,'Demikian penawaran yang dapat kami sampaikan. Atas perhatian dan kerjasamanya kami mengucapkan terimakasih.');

$pdf->SetFont('Arial','B',10);
$pdf->Ln(15);
$pdf->Cell(-5);
$pdf->Cell(55,5,'Hormat Kami, ');


$pdf->SetFont('Arial','B',10);
$pdf->Ln(20);
$pdf->Cell(-5);
$pdf->Cell(55,5,'Sulaiman ');
$pdf->Ln(5);
$pdf->SetFont('Arial','',10);
$pdf->Cell(-5);
$pdf->Cell(55,5,'Direktur ');



//============================================================================

$pdf->Output();

function penyebut($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = penyebut($nilai - 10). " Belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " Seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " Seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
	}     
	return $temp;
}


?>

