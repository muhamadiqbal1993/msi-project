<?php
include "../../connect.php";
require('../fpdf17/fpdf.php');
//Menampilkan data dari tabel di database
$id = $_GET['s'];

$datavoucher_penutup = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(tanggal, '%d %M %Y') as tanggal_new from voucher_memo where no = '$id'"));
$tgl_skr = date('d F Y');
$customer = "";
$alamat = "";
$alamat2 = "";
$pic = "";
$invoice = "";
$no_inv = "";
$tanggal = "";
// $sql1=mysql_query("select IH.*,DATE_FORMAT(IH.tgl_inv, '%d %M %Y') as date, C.nama as namcli,C.alamat as alcli
//     ,C.alamat2 as alcli2,C.pic from invoice_header IH
//     inner join client C on C.no = IH.client where IH.no = '$id'");
// while($data1=mysql_fetch_array($sql1))
// { 
//     $invoice = $data1['keterangan'];
//     $customer = $data1['namcli'];
//     $alamat = $data1['alcli'];
//     $alamat2 = $data1['alcli2'];
//     $pic = $data1['pic'];
//     $no_inv = $data1['id'].str_pad($data1['no'], 5, '0', STR_PAD_LEFT);
//     $tanggal = $data1['date'];
// }

$tampil= "select * from user ";
//Inisiasi untuk membuat header kolom
$c1 = "";
$c2 = "";
$c3 = "";
$c4 = "";
$c5 = "";
$angka = 1;
//For each row, add the field to the corresponding column


$pdf = new FPDF('L','mm',array(148,210)); //L For Landscape / P For Portrait
$pdf->AddPage();

//Menambahkan Gambar
//$pdf->Image('logo.jpg',10,10,-175);

$pdf->SetFont('Arial','B',15);
$pdf->Cell(0,0,'PT Lionel Jaya Logistic');
$pdf->Ln(5);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(30);
$pdf->Cell(100,5,'JURNAL MEMORIAL');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(27,5,'Transaction No ');
$pdf->Cell(10,5,' : '.$datavoucher_penutup['id']);
$pdf->Ln(7);
$pdf->Cell(130,5,'Prepaid Exp :');
$pdf->Cell(27,5,'Date ');
$pdf->Cell(10,5,' : '.$datavoucher_penutup['tanggal_new']);
$pdf->Ln(5);
$pdf->Cell(130,5,'Referensi : '.$datavoucher_penutup['ref']);
//Fields Name position
$Y_Fields_Name_position = 35;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(219,197,197);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(10,8,'No',1,0,'C',1);

$pdf->SetX(15);
$pdf->Cell(70,8,'Account',1,0,'C',1);

$pdf->SetX(85);
$pdf->Cell(80,8,'Description',1,0,'C',1);

$pdf->SetX(165);
$pdf->Cell(20,8,'Debit',1,0,'C',1);

$pdf->SetX(185);
$pdf->Cell(20,8,'Credit',1,0,'C',1);


$pdf->Ln();
$Y_Table_Position = 43;
$total_semua = 0;
$t_debit = 0;
$t_credit = 0;
$pdf->SetFont('Arial','',8);
$sql1=mysql_query("select VD.*,C.nama as namcoa from voucher_memo_detail VD 
	inner join coa C on C.id = VD.account
	where VD.id_voucher = '$id'");
while($row=mysql_fetch_array($sql1))
{  
	$dbts = number_format($row['debit']);
	$crdts = number_format($row['credit']);

	$t_debit += $row['debit'];
	$t_credit += $row['credit'];

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(5);
	$pdf->Cell(10,7,$angka,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(15);
	$pdf->Cell(70,7,$row['account'].' - '.$row['namcoa'],1,'L');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(85);
	$pdf->Cell(80,7,$row['ket'],1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(165);
	$pdf->Cell(20,7,$dbts,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(185);
	$pdf->Cell(20,7,$crdts,1,'C');

	$Y_Table_Position = $Y_Table_Position + 7;
	$angka++;
	//$total_semua = $total_semua + $row["qty"] * $row["satuan"];
}

$pdf->SetFont('Arial','B',8);
$pdf->SetY($Y_Table_Position);
$pdf->SetX(5);
$pdf->Cell(160,7,'',1,'','R','');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(165);
$pdf->Cell(20,7,number_format($t_debit),1,'C');

$pdf->SetY($Y_Table_Position);
$pdf->SetX(185);
$pdf->Cell(20,7,number_format($t_credit),1,'C');

$pdf->SetY($Y_Table_Position+7);
$pdf->SetX(5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(200,7,penyebut($t_debit). 'Rupiah',1,'','L','');

// $pdf->Ln(10);
// $pdf->SetFont('Arial','B',10);
// $pdf->Cell(140,5,'Keterangan');
// $pdf->Ln(5);
// $pdf->Cell(180,5,'Untuk Pembayaran Dapat Dibayarkan Ke Rekening BCA 6470411021 a/n Muhamad Iqbal');

$pdf->Ln(10);
$pdf->SetFont('Arial','B',8);
$pdf->SetX(10);
$pdf->Cell(80,5,'MANAGING DIRECTOR');
$pdf->Cell(80,5,'APPROVED BY');
$pdf->Cell(70,5,'ACCOUNTING');
$pdf->Ln(20);
$pdf->SetX(12);
$pdf->Cell(75,5,'(________________)');
$pdf->Cell(80,5,'(________________)');
$pdf->Cell(20,5,'(________________)');

//============================================================================

$pdf->Output();

function penyebut($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = penyebut($nilai - 10). " Belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " Seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " Seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
	}     
	return $temp;
}
?>
