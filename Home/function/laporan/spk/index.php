<?php
include "../../connect.php";
require('../fpdf17/fpdf.php');
//Menampilkan data dari tabel di database
$id = $_GET['id'];

$tgl_skr = date('d F Y');
$customer = "";
$alamat = "";
$alamat2 = "";
$pic = "";
$invoice = "";
$no_inv = "";
$tanggal = "";

$dataInvoice = mysql_fetch_array(mysql_query("select I.*,C.nama as namcus,C.alamat as namal,C.phone as namphone,DATE_FORMAT(I.tanggal, '%d %M %Y') as tgls,DATE_FORMAT(I.due, '%d %M %Y') as dues from invoice I
	inner join customer C on C.no = I.customer
	where I.no='$id'"));

$tampil= "select IDR.*,P.nama as nampro from invoice_detail_retail IDR
inner join product P on P.no = IDR.produk
where IDR.id_order = '".$dataInvoice['inv']."'";
$result=mysql_query($tampil);
//Inisiasi untuk membuat header kolom
$c1 = "";
$c2 = "";
$c3 = "";
$c4 = "";
$c5 = "";
$angka = 1;
//For each row, add the field to the corresponding column


$pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
$pdf->AddPage();

//Menambahkan Gambar
$pdf->Image('logo.png',25,5,-200);

$pdf->Ln(25);
$pdf->SetFont('Arial','B',11);
$pdf->Ln(7);
$pdf->Cell(0,0,'SURAT PERINTAH KERJA',0,1,'C');
$pdf->Line(5, 45, 225-20, 45);
$pdf->SetFont('Arial','B',10);
$pdf->Ln(6);
$pdf->Cell(0,0,'No Surat : 015/WS - MSI/III/2024',0,1,'C');

$pdf->SetFont('Arial','',11);
$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(20,5,'Tanggal, 14 Maret 2024 ');

$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(35,5,'Nama Customer ');
$pdf->Cell(15,5,' : '.$dataInvoice['namcus']);

$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(35,5,'Alamat Customer ');
$pdf->Cell(15,5,' : '.$dataInvoice['namal']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(35,5,'');
$pdf->Cell(15,5,'   ');

$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(35,5,'Contact Person ');
$pdf->Cell(15,5,' : Customer');
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(35,5,'No HP ');
$pdf->Cell(15,5,' : '.$dataInvoice['namphone']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(35,5,'Tgl. Pemesan ');
$pdf->Cell(15,5,' : '.$dataInvoice['tgls']);
$pdf->Ln(5);
$pdf->Cell(-5);
$pdf->Cell(35,5,'Tgl. Deadline ');
$pdf->Cell(15,5,' : '.$dataInvoice['dues']);

$pdf->Ln(13);
$pdf->Cell(-5);
$pdf->Cell(20,5,'Mohon segera buatkan alat pesanan dengan rincian sebagai berikut');



//Fields Name position
$Y_Fields_Name_position = 120;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(230,230,230);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',10);
$pdf->SetY($Y_Fields_Name_position);

$pdf->SetX(5);
$pdf->Cell(10,8,'No',1,0,'C',1);

$pdf->SetX(15);
$pdf->Cell(170,8,'Nama Alat ',1,0,'C',1);

$pdf->SetX(185);
$pdf->Cell(20,8,'Qty',1,0,'C',1);


$pdf->Ln();
$Y_Table_Position = 128;
$total_semua = 0;
$pdf->SetFont('Arial','',10);
while($row = mysql_fetch_array($result))
{   
	$c1 = '   '.$angka;
	$c2 = '   '.$row["nampro"];
	$c3 = '   '.$row["qty"];

	$pdf->SetFont('Arial','',10);
	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(5);
	$pdf->Cell(10,7,$c1,1,'C');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(15);
	$pdf->Cell(170,7,$c2,1,'L');

	$pdf->SetY($Y_Table_Position);
	$pdf->SetX(185);
	$pdf->Cell(20,7,$c3,1,'C');

	$Y_Table_Position = $Y_Table_Position + 7;
	$angka++;
	$total_semua = $total_semua + $row["no"] * $row["no"];
}


$pdf->Ln(10);
$pdf->SetFont('Arial','',10);

$pdf->Ln(10);
$pdf->Cell(-5);
$pdf->Cell(55,5,'Dibuat Oleh ');
$pdf->Cell(60,5,'Mengetahui ');
$pdf->Cell(60,5,'Disetujui Oleh ');
$pdf->Cell(55,5,'Mengetahui ');

$pdf->SetFont('Arial','B',10);
$pdf->Ln(20);
$pdf->Cell(-5);
$pdf->Cell(55,5,'Sales ');
$pdf->Cell(60,5,'SPV. Purchasing ');
$pdf->Cell(60,5,'General Manager ');
$pdf->Cell(55,5,'Leader Engineering ');


//============================================================================

$pdf->Output();

function penyebut($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = penyebut($nilai - 10). " Belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " Seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " Seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
	}     
	return $temp;
}


?>

