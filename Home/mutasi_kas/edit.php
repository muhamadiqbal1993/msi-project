<?php


$dataEdit = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(tanggal, '%d %M %Y') as tgls from mutasi where no = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<button type="submit" class="btn btn-warning" onclick="history.back()">  Kembali  </button>
						&nbsp;&nbsp;&nbsp;
						<?php if($dataEdit['status'] == "1"){?>
							<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }
						?>

						<hr class="abu">
					</div>

					<div class="box-body">
						<form id="create_data" method="POST"> 
							<input type="hidden" name="input" class="form-control" value="2">
							<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
							<input type="hidden" name="detail" id="detail" class="form-control" required="">
							<input type="hidden" name="total" id="total" class="form-control" required="">
							<input type="hidden" name="user" class="form-control" value="<?php echo $user_data['no'];?>">
							<input type="hidden" name="idss" class="form-control" value="<?php echo $dataEdit['id']?>"  >
							<input type="hidden" name="no" class="form-control" value="<?php echo $dataEdit['no']?>"  >
							<div class="col-md-6">
								<div class="form-group">
									<label>Bank</label>
									<select class="form-control select2" name="bank" style="width: 100%;" disabled="">
										<?php
										$sql1=mysql_query("select * from kasbank where no = '".$dataEdit['kasbank']."'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['bank'].' '.$data1['no_acc'].' - '.$data1['nama_acc'] ?></option>
											<?php 
										}?>

									</select>
								</div>
								<div class="form-group">
									<label>Saldo Awal</label>
									<input type="text" name="saldo_awal" id="saldo_awal" class="form-control" required="" readonly value="<?php echo number_format($dataEdit['saldo_awal'])?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal</label>
									<input type="text" name="tanggal" id="datepicker" class="form-control" disabled="" value="<?php echo $dataEdit['tgls']?>">
								</div>
							</div>

						</form>




						<div class="col-md-12">
							<hr class="abu">
							<div class="form-group">
								<h2>List Transaksi</h2>
							</div>
						</div>

						<div class="col-xs-12">

							<a href="javascript:void(0)" id="tambahBtn" onclick="addLine()">+ Tambah Item</a>
							<p></p>
							<table id="so_table" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width: 5%">No</th>
										<th style="width: 20%">Nama Transaksi</th>
										<th style="width: 15%">Partner</th>
										<th style="width: 15%">Debit</th>
										<th style="width: 15%">Credit</th>
										<th style="width: 15%">Status</th>
										<th style="width: 15%">Action</th>
									</tr>
								</thead>
								<?php
								$total_nya = "";
								$detailss = "";
								$nom = 1;
								$sql2=mysql_query("select * from mutasi_detail where id_trx = '".$dataEdit['id']."'");
								while($data2=mysql_fetch_array($sql2))
								{
									$partner = "";
									if(!empty($data2['partner']))
									{
										$dataPartner = mysql_fetch_array(mysql_query("select * from vendor where kode = '".$data2['partner']."'"));
										$partner = 'VDR - ['.$dataPartner['kode'].'] '.$dataPartner['nama'];
										if ($data2['type'] == "CST")
										{
											$dataPartner = mysql_fetch_array(mysql_query("select * from customer where kode = '".$data2['partner']."'"));
											$partner = 'CST - ['.$dataPartner['kode'].'] '.$dataPartner['nama'];
										}

									}
									$stats = "Open";
									if($data2['status'] == "2")
									{
										$stats = "Reconcile";
									}

									$debit = $data2['nominal'];
									$credit = "0";
									if($data2['nominal']<0)
									{
										$credit = $data2['nominal'];
										$debit = "0";
									}

									?>
									<tr>
										<td><?php echo $nom;?></td>
										<td><?php echo $data2['keterangan'];?></td>
										<td><?php echo $partner;?></td>
										<td><?php echo $debit;?></td>
										<td><?php echo str_replace("-", "", $credit);?></td>
										<td><?php echo $stats;?></td>
										<?php if($stats == "Reconcile"){?>
											<td></td>
										<?php }else{?>
											<td>
												<a href='javascript:void(0)' onclick='delete_row(<?php echo $nom;?>)'> Hapus</a>
											</td>
										<?php }?>

									</tr>
									<?php 
									$nom++;
								}
								?>
								<script language="javascript" type="text/javascript">

									function getAccount(isi)
									{
										var products = '<select class="form-control select2" name="account_edits" id="account_edits" style="width: 100%;" required="">'+
										<?php

										$sql1=mysql_query("select 'CST' as type,kode,nama from customer UNION select 'VDR' as type,kode,nama from vendor");
										while($data1=mysql_fetch_array($sql1))
										{

											?>
											'<option value="<?php echo $data1['type'].' - ['.$data1['kode'].'] '.$data1['nama']?>"><?php echo $data1['type'].' - ['.$data1['kode'].'] '.$data1['nama']?></option>'+

											<?php 

										}?>

										'</select>';

										return products;
									}




									function jas(prod,sat)
									{
										$('#account_edits').val(prod); 
										$('#account_edits').change();
									}

									function view_total()
									{
										var totalnya = parseInt(<?php echo $dataEdit['saldo_awal']?>);
										var detail = "";
										var rowCount = document.getElementById('so_table').rows.length;
										var table = document.getElementById('so_table');
										for(var a=1;a<rowCount;a++)
										{
											var nama = table.rows[a].cells[1].innerHTML;
											var account = table.rows[a].cells[2].innerHTML;
											var debit = table.rows[a].cells[3].innerHTML;
											var credit = table.rows[a].cells[4].innerHTML;
											var status = table.rows[a].cells[5].innerHTML;
											var nominals = debit;
											if(debit == "0")
											{
												nominals = "-"+credit;
												totalnya += parseInt(nominals);
											}
											else
											{
												totalnya += parseInt(nominals);
											}
											detail = detail +','+nama+'spasinya'+account+'spasinya'+nominals+'spasinya'+status;
											
										} 
										document.getElementById("detail").value = detail;
										document.getElementById("total").value = totalnya;
										document.getElementById('endingBalance').innerHTML = "<strong>Ending Balance : Rp. "+parseInt(totalnya).toLocaleString()+"</strong>";
									}

									function delete_row(abs)
									{
										document.getElementById("so_table").deleteRow(abs);
										var rowCount = document.getElementById('so_table').rows.length;
										var table = document.getElementById('so_table');
										for(var a=1;a<rowCount;a++)
										{
											table.rows[a].cells[0].innerHTML = a;
											table.rows[a].cells[6].innerHTML = "<a href='javascript:void(0)' onclick='delete_row(\""+a+"\")'> Hapus</a>";
										}

										view_total();
									}



									function save_row(abs)
									{
										var rowCount = document.getElementById('so_table').rows.length;
										var table = document.getElementById('so_table');
										for(var a=1;a<rowCount;a++)
										{
											if(abs == a)
											{
												var account = document.getElementById("account_edits").value;
												var qty = document.getElementById("editQty").value;
												var debit = document.getElementById("editDebit").value;
												var credit = document.getElementById("editCredit").value;

												table.rows[abs].cells[1].innerHTML = qty;
												table.rows[abs].cells[2].innerHTML = account;
												table.rows[abs].cells[3].innerHTML = debit;
												table.rows[abs].cells[4].innerHTML = credit;
												table.rows[abs].cells[6].innerHTML = "<a href='javascript:void(0)' onclick='delete_row(\""+abs+"\")'> Hapus</a>";
											}
											else
											{
												if(table.rows[a].cells[5].innerHTML == "Reconcile")
												{
													table.rows[a].cells[6].innerHTML = "";
												}
												else
												{
													table.rows[a].cells[6].innerHTML = "<a href='javascript:void(0)' onclick='delete_row(\""+a+"\")'> Hapus</a>";
												}

											}
										}

										document.getElementById('tambahBtn').style.display = ""; 
										document.getElementById('tambahBtn1').style.display = ""; 
										view_total();
									}

									function edit_row(abs)
									{
										var rowCount = document.getElementById('so_table').rows.length;
										var table = document.getElementById('so_table');
										prod = "";
										for(var a=1;a<rowCount;a++)
										{
											if(abs == a)
											{
												var nama = table.rows[a].cells[1].innerHTML;
												var account = table.rows[a].cells[2].innerHTML;
												var debit = table.rows[a].cells[3].innerHTML;
												var credit = table.rows[a].cells[4].innerHTML;
												var status = table.rows[a].cells[5].innerHTML;
												prod = account;
												table.rows[a].cells[1].innerHTML = '<input type="text" id="editQty" class="form-control" value="'+nama+'">';
												table.rows[a].cells[2].innerHTML = getAccount(account);
												table.rows[a].cells[3].innerHTML = '<input type="number" id="editDebit" class="form-control" value="'+debit+'">';
												table.rows[a].cells[4].innerHTML = '<input type="number" id="editCredit" class="form-control" value="'+credit+'">';
												table.rows[a].cells[5].innerHTML = status;
												table.rows[a].cells[6].innerHTML = "<button type='submit' onclick='save_row(\""+a+"\")' class='btn btn-primary'>Simpan</button>";
											}
											else
											{
												table.rows[a].cells[6].innerHTML = "";
											}
										}
										$('#account_edits').select2();
										$('#account_edits').val(prod); 
										$('#account_edits').change();
									}

									function addLine()
									{
										document.getElementById('tambahBtn').style.display = "none"; 
										document.getElementById('tambahBtn1').style.display = "none"; 
										var rowCount = document.getElementById('so_table').rows.length;
										var table = document.getElementById('so_table');
										var row = table.insertRow(rowCount);
										var cell0 = row.insertCell(0);
										var cell1 = row.insertCell(1);
										var cell2 = row.insertCell(2);
										var cell3 = row.insertCell(3);
										var cell4 = row.insertCell(4);
										var cell5 = row.insertCell(5);
										var cell6 = row.insertCell(6);

										cell0.innerHTML = rowCount;
										cell1.innerHTML = '<input type="text" id="editQty" class="form-control">';
										cell2.innerHTML = getAccount("-");
										cell3.innerHTML = '<input type="number" id="editDebit" class="form-control" value="0">';
										cell4.innerHTML = '<input type="number" id="editCredit" class="form-control" value="0">';
										cell5.innerHTML = 'Open';
										cell6.innerHTML = "<button type='submit' onclick='save_row(\""+rowCount+"\")' class='btn btn-primary'>Simpan</button>";
										$('#account_edits').select2();


										for(var a=1;a<rowCount;a++)
										{
											if(a != rowCount)
											{
												table.rows[a].cells[6].innerHTML = "";
											}
										}
										jas("","");
									}

								</script>
							</table>
							<a href="javascript:void(0)" id="tambahBtn1" onclick="addLine()">+ Tambah Item</a>
						</div>
						<div class="col-xs-12">
							<br>
							<hr class="abu">
						</div>
						<div class="col-xs-6">
						</div>
						<div class="col-xs-6">
							<h3 style="float: right;" id="endingBalance"><strong>Ending Balance : Rp. 0</strong></h3>
						</div>
					</div>
					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'mutasi';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	view_total();
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function kbmProses()
	{
		document.getElementById("kbmProses").action = ".";
	}

</script>
