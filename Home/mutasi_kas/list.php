<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">



           

            
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            '.$modulnya.' sudah ada yang menggunakan , silahkan Buat dengan '.$modulnya.' lain
          </div>
        </div>';
      }
      ?>

      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th style="width: 5%">No</th>
              <th>Bank</th>
              <th>Balance</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $not = 1;

            $sql1=mysql_query("select * from kasbank where type = '2'");
            while($data1=mysql_fetch_array($sql1))
            {  
              $status = 'Active';
              if($data1['status'] == "2")
              {
                $status = "Closed";
              }
             
              $row = mysql_fetch_array(mysql_query("select * from mutasi where kasbank = '".$data1['no']."' order by tanggal desc limit 1"));
              $total_nya = $row['saldo_akhir'];
              echo '
              <tr>
                <td>'.$not.'</td>
                <td>'.$data1['bank'].' '.$data1['no_acc'].' - '.$data1['nama_acc'].'</td>
               
                <td>'.number_format($total_nya,2,".",",").'</td>
		 <td>'.$status.'</td>
                <td><a href="./?id='.$data1['no'].'">Manage</a></td>
              </tr>
              ';
              $not++;
            }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>
