<?php
include '../headfoot/header.php';

// $so = mysql_fetch_array(mysql_query("select count(*) as total from invoice where status = '1'"));
// $po = mysql_fetch_array(mysql_query("select count(*) as total from bill where status = '1'"));

// $efaktur = mysql_fetch_array(mysql_query("select count(*) as total from efaktur where status not in ('3','1')"));
// $bukti = mysql_fetch_array(mysql_query("select count(*) as total from bill where status != '5'"));

$currentDivisi = mysql_fetch_array(mysql_query("select departemen from group_divisi where no = '".$user_data['groups']."'"));

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
    </h1>
    <ol class="breadcrumb">
      <li class="active">Dashboard / </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <!-- <div class="row">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?php echo $po['total'];?></h3>

            <p>PO Gantung</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="../purchase_order" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
       
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?php echo $so['total'];?></h3>

            <p>SO Gantung</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="../sales_order" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
    
        <div class="small-box bg-red">
          <div class="inner">
            <h3><?php echo $efaktur['total'];?></h3>

            <p>Efaktur Upload</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="../efaktur_nomor" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
 
      <div class="col-lg-3 col-xs-6">
 
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?php echo $bukti['total'];?></h3>

            <p>Bukti Potong</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="../bukti_potong" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

    </div> -->

    <script src="ggl.js" type="text/javascript"></script>
    <div class="row">
      <?php
      $sql1=mysql_query("select * from dashboard_setting where status = '1' and department like '%".$currentDivisi['departemen']."%'");
      while($data1=mysql_fetch_array($sql1))
      { 
        ?>
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title"><?php echo "<strong>".$data1['nama'].' - '.$data1['angka']."</strong>".' '.$data1['waktu'].' Kebelakang'?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="chart_div<?php echo $data1['no']?>" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
        </div>
        <?php
      }
      ?>
    </div>

    <script type="text/javascript">

      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart1);
      function drawChart1() {
        var options = {

          hAxis: {title: '', titleTextStyle: {color: 'red'}}
        };

        <?php
        $sql1=mysql_query("select * from dashboard_setting where status = '1' and department like '%".$currentDivisi['departemen']."%'");
        while($data1=mysql_fetch_array($sql1))
        { 
          ?>
          var data<?php echo $data1['no']?> = google.visualization.arrayToDataTable([
            ['Bulan', 'Nilai'],
            <?php
            $time1 = strtotime(date('Y-m-d'));
            $wkt = "-".$data1['angka'].' day';
            $namaBulan1 = date("Y-m-d", strtotime($wkt, $time1));
            if($data1['waktu'] == "Minggu")
            {
              $time1 = strtotime(date('Y-m-d'));
              $wkt = "-".$data1['angka'].' week';
              $namaBulan1 = date("Y-m-d", strtotime($wkt, $time1));
            }
            if($data1['waktu'] == "Bulan")
            {
              $time1 = strtotime(date('Y-m'));
              $wkt = "-".$data1['angka'].' months';
              $namaBulan1 = date("Y-m", strtotime($wkt, $time1));
            }
            if($data1['waktu'] == "Tahun")
            {
              $time1 = strtotime(date('Y'));
              $wkt = "-".$data1['angka'].' year';
              $namaBulan1 = date("Y-m-d", strtotime($wkt, $time1));
            }

            for ($i = 1; $i <= $data1['angka']; $i++) 
            {

              $time = strtotime($namaBulan1);

              $wkts = "+".$i.' day';
              $namaBulan = date("Y-m-d", strtotime($wkts, $time));
              $namaBulans = date("d F Y", strtotime($wkts, $time));

              if($data1['waktu'] == "Minggu")
              {
                $wkts = "+".$i.' week';
                $namaBulan = date("Y-m-d", strtotime($wkts, $time));
                $namaBulans = date("d F Y", strtotime($wkts, $time));
              }
              if($data1['waktu'] == "Bulan")
              {
                $wkts = "+".$i.' months';
                $namaBulan = date("Y-m", strtotime($wkts, $time));
                $namaBulans = date("F Y", strtotime($wkts, $time));
              }
              if($data1['waktu'] == "Tahun")
              {
                $wkts = "+".$i.' year';
                $namaBulan = date("Y", strtotime($wkts, $time));
                $namaBulans = date("Y", strtotime($wkts, $time));
              }

              $qrs = explode("#variabel", $data1['query'])[0].$namaBulan.explode("#variabel", $data1['query'])[1];

              $penjualan = mysql_fetch_array(mysql_query($qrs));

              ?>
              ['<?php echo $namaBulans?>', <?php echo $penjualan['total']?>],
              <?php
            }
            ?>
            ]);

          var chart = new google.visualization.ColumnChart(document.getElementById('chart_div<?php echo $data1['no']?>'));
          chart.draw(data<?php echo $data1['no']?>, options);

          <?php
        }
        ?>
      }

    </script>

    <div class="row">
      <?php
      $ttlpen = 0;
      $sql1=mysql_query("select P.*,U.nama as namus,U.photo as phous,
        DATE_FORMAT(tanggal, '%d %M %Y') as daris,DATE_FORMAT(tanggal, '%H:%i') as jam from pengumuman P
        inner join user U on U.no = P.user
        where P.status = '1' and now() between P.dari and P.sampai
        order by P.no desc");
      while($data1=mysql_fetch_array($sql1))
      { 
        $ttlpen++;
        ?>
        <div class="col-md-12">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="../images/<?php echo $data1['phous'];?>" alt="User Image">
                <span class="username"><a href="#"><?php echo $data1['namus'];?></a></span>
                <span class="description"><?php echo $data1['daris'];?> - <?php echo $data1['jam'];?> </span>
              </div>
              <!-- /.user-block -->
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <h3 style="text-align: center"><strong><?php echo $data1['judul'];?></strong> </h3>
                <br>
                <br>
                <?php 
                if(!empty($data1['image']))
                {
                  ?>
                  <img class="img-responsive pad" src="../images/<?php echo $data1['image'];?>" alt="Photo" style="text-align: center;">
                  <?php 
                }
                ?>
                <p></p>
                <p><?php echo $data1['isi'];?></p>
                <p></p>
                <?php 
                if(!empty($data1['file']))
                {
                  ?>
                  <a href="#">
                    <div class="attachment-block clearfix">
                      <img class="attachment-img" src="../images/dwn.jpg" alt="Attachment Image">
                      <br>

                      <div class="attachment-pushed">
                        <h4 class="attachment-heading"><a href="../images/<?php echo $data1['file'];?>" target="_blank"><?php echo $data1['file'];?></a></h4>

                        <div class="attachment-text">
                          Klik Untuk Download File ini
                        </div>
                        <!-- /.attachment-text -->
                      </div>
                      
                      <!-- /.attachment-pushed -->
                    </div>
                  </a>
                  <?php 
                }
                ?>
                <!-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
                <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
                <span class="pull-right text-muted">127 likes - 3 comments</span> -->
              </div>
            </div>
          </div>
          <?php 
        }
        if($ttlpen == 0)
        {
          echo '
          <div class="col-md-12">
          <div class="box box-default">
          <div class="box-header with-border">
          <h3 class="box-title">Tidak Ada Pengumuman</h3>
          </div>
          <div class="box-body">
          -
          </div>
          <!-- /.box-body -->
          </div>
          </div>';
        }
        ?>

      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php
  include '../headfoot/footer.php';
?>