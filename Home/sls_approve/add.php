<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>

					</div>

					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<form id="create_data" method="POST"> 
			<input type="hidden" name="input" class="form-control" value="1">
			<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
			<div class="row">
				<div class="col-xs-12">
					<div class="box">

						<div class="box-body">

							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Nomor Order</strong></label>
									<input type="text" name="no_order" class="form-control" required readonly value="<?php echo 'INV/'.date('Y').'/'.date('m').'/'.date('His')?>">
								</div>
								<div class="form-group">
									<label><strong>Customer</strong></label>
									<select class="form-control select2" name="customer" id="customer" style="width: 100%;" required="">
										<option value="" >--- Pilih Customer ---</option>
										<?php
										$sql1=mysql_query("select * from customer where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['kode'].' - '.$data1['nama']?></option>
											<?php 
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label><strong>Tanggal Invoice</strong></label>
									<input type="text" name="tanggal" id="datepicker" class="form-control" value="<?php echo date('m/d/Y');?>" required>
								</div>
								<div class="form-group">
									<label><strong>Jatuh Tempo</strong></label>
									<input type="text" name="due" id="datepicker1" class="form-control" value="<?php echo date('m/d/Y');?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Salesman</strong></label>
									<input type="text" name="sales2" id="sales2" class="form-control" readonly="" style="display: none">
									<div id="sales1">
										<select class="form-control select2" name="salesman" id="salesman"  style="width: 100%;" >
											<option value="" >--- Pilih Salesman ---</option>
											<?php
											$sql1=mysql_query("select * from user where status = '1'");
											while($data1=mysql_fetch_array($sql1))
											{
												?>
												<option value="<?php echo $data1['no']?>" ><?php echo 'Kilat'.$data1['no'].' - '.$data1['nama']?></option>
												<?php 
											}
											?>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label><strong>Account Debit</strong></label>
											<select class="form-control select2" name="account_debit" style="width: 100%;" required="">
												<option value="" >--- Pilih Account ---</option>
												<?php
												$sql1=mysql_query("select * from coa where status = '1'");
												while($data1=mysql_fetch_array($sql1))
												{
													?>
													<option value="<?php echo $data1['id']?>" <?php if("110301" == $data1['id']){echo "selected";}?>><?php echo $data1['id'].' - '.$data1['nama']?></option>
													<?php 
												}
												?>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label><strong>Account Credit</strong></label>
											<select class="form-control select2" name="account" style="width: 100%;" required="">
												<option value="" >--- Pilih Account ---</option>
												<?php
												$sql1=mysql_query("select * from coa where status = '1'");
												while($data1=mysql_fetch_array($sql1))
												{
													?>
													<option value="<?php echo $data1['id']?>" <?php if("410101" == $data1['id']){echo "selected";}?>><?php echo $data1['id'].' - '.$data1['nama']?></option>
													<?php 
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label><strong>Keterangan</strong></label>
									<input type="text" name="keterangan" class="form-control" required>
								</div>
								<div class="form-group">
									<label><strong>Type</strong></label>
									<select class="form-control select2" name="tipe" style="width: 100%;" >
										<option value="1" >Incoming</option>
										<option value="2" >Outgoing</option>

									</select>
								</div>
							</div>

							<div class="col-md-12">
								<hr class="abu">
							</div>
							<div class="col-md-12">
								<div class="box-body table-responsive no-padding">
									<table id="so_table" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th style="width: 25%">Product</th>
												<th>Harga Satuan</th>
												<th>Qty</th>
												<th>Harga Jual</th>
												<th>Disc</th>
												<th>Nilai Disc</th>
												<th style="width: 10%">Total</th>
												<th><i class="fa fa-trash" aria-hidden="true"></i></th>
											</tr>
										</thead>
									</table>
								</div>
								<br>
								<a href="javascript:void(0)" id="tambahBtn1" onclick="addLine()">+ Tambah Item</a>
							</div>

							<script language="javascript" type="text/javascript">

								function getProduct(abs)
								{
									var products = '<select class="form-control select2" name="product[]" id="product'+abs+'" onchange=getSatuanProds("'+abs+'")  style="width: 100%;" required="">'+

									<?php

									$sql1=mysql_query("select * from product where status = '1'");
									while($data1=mysql_fetch_array($sql1))
									{
										?>
										'<option value="<?php echo $data1['no'].'#'.$data1['harga'];?>"><?php echo $data1['kode'].' - '.$data1['nama']?></option>'+

										<?php 
									}?>

									'</select>';

									return products;
								}

								function getDisc(abs)
								{
									var products = '<select class="form-control select2" name="discount[]" id="discount'+abs+'" style="width: 100%;" onchange="getNilaiDisc('+abs+')"><option value=""></option>'+

									<?php

									$sql1=mysql_query("select * from disc where status = '1'");
									while($data1=mysql_fetch_array($sql1))
									{
										?>
										'<option value="<?php echo $data1['no'].'##'.$data1['nilai'];?>"><?php echo $data1['nama'].' - '.$data1['nilai'].'%'?></option>'+

										<?php 
									}?>

									'</select>';

									return products;
								}

								function getSatuanProds(line)
								{
									var nams = "product"+line;
									var nam_sqtuan = "editHargaSatuan"+line
									var nam_jual = "editHargaJual"+line
									var txt = document.getElementById(nams).value;

									document.getElementById(nam_sqtuan).value = txt.split("#")[1].toLocaleString('en-US');
									document.getElementById(nam_jual).value = txt.split("#")[1].toLocaleString('en-US');
									getNilaiDisc(line);
								}

								function getNilaiDisc(abs)
								{
									var table = document.getElementById('so_table');
									var namDisc = "discount"+abs;
									var namQty = "editQty"+abs;
									var namJual = "editHargaJual"+abs;

									var totalDisc = 0;
									

									var qty = document.getElementById(namQty).value;
									var nilaiJual = document.getElementById(namJual).value;

									var nilaiTotal = parseInt(qty) * parseInt(nilaiJual);

									if(document.getElementById(namDisc).value != "")
									{
										var perDisc = document.getElementById(namDisc).value.split("##")[1];
										totalDisc = parseInt(nilaiTotal) * (parseInt(perDisc) / 100);
									}

									totalHarga = parseInt(nilaiTotal) - parseInt(totalDisc);

									table.rows[abs].cells[6].innerHTML = totalDisc.toLocaleString('en-US');
									table.rows[abs].cells[7].innerHTML = totalHarga.toLocaleString('en-US');

									totalPajak();
								}

								function addLine()
								{
									var rowCount = document.getElementById('so_table').rows.length;
									var table = document.getElementById('so_table');

									var row = table.insertRow(rowCount);
									var cell0 = row.insertCell(0);
									var cell1 = row.insertCell(1);
									var cell2 = row.insertCell(2);
									var cell3 = row.insertCell(3);
									var cell4 = row.insertCell(4);
									var cell5 = row.insertCell(5);
									var cell6 = row.insertCell(6);
									var cell7 = row.insertCell(7);
									var cell8 = row.insertCell(8);


									cell0.innerHTML = rowCount;
									cell1.innerHTML = getProduct(rowCount);
									cell2.innerHTML = '<input type="text" id="editHargaSatuan'+rowCount+'" name="editHargaSatuan[]" class="form-control" readonly="">';
									cell3.innerHTML = '<input type="number" id="editQty'+rowCount+'" name="editQty[]" oninput="getNilaiDisc('+rowCount+')" class="form-control" value="1">';
									cell4.innerHTML = '<input type="text" id="editHargaJual'+rowCount+'" name="editHargaJual[]" oninput="getNilaiDisc('+rowCount+')" class="form-control" value="0">';
									cell5.innerHTML = getDisc(rowCount);
									cell6.innerHTML = '0';
									cell7.innerHTML = '';
									cell8.innerHTML = '<a href="javascript:void(0)" onclick="delete_row('+rowCount+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';

									var namaField = "#product"+rowCount;
									$(namaField).val(""); 
									$(namaField).select2();
								}

								function viewTotal()
								{
									var table = document.getElementById('so_table');
									var rowCount = document.getElementById('so_table').rows.length;
									var subTotal = 0;
									var totalDisc = 0;
									for(var a=1;a<rowCount;a++)
									{
										var namQty = "editQty"+a;
										var namJual = "editHargaJual"+a;

										var qty = document.getElementById(namQty).value;
										var nilaiJual = document.getElementById(namJual).value;

										var nilaiSubTotal = parseInt(qty) * parseInt(nilaiJual);
										var nilaiDiscTotal = parseInt(table.rows[a].cells[6].innerHTML.split(",").join(""));
										
										subTotal += parseInt(nilaiSubTotal);
										totalDisc += parseInt(nilaiDiscTotal);
									}
									
									document.getElementById("subtotal").value = subTotal.toLocaleString('en-US');
									document.getElementById("t_disc").value = "-"+totalDisc.toLocaleString('en-US');
									var nilaiPajak = (parseInt(subTotal) - parseInt(totalDisc)) + parseInt(document.getElementById("lainya").value.split(",").join(""));
									document.getElementById("grandtotal").value = nilaiPajak.toLocaleString('en-US');
								}

								function delete_row(abs)
								{
									document.getElementById("so_table").deleteRow(abs);
									var rowCount = document.getElementById('so_table').rows.length;
									var table = document.getElementById('so_table');
									for(var a=1;a<rowCount;a++)
									{
										if(parseInt(a) >= parseInt(abs))
										{
											var ab = parseInt(a) + 1;

											var n_editProduct = "product"+ab;
											var n_editHargaSatuan = "editHargaSatuan"+ab;
											var n_editQty= "editQty"+ab;
											var n_editHargaJual= "editHargaJual"+ab;
											var n_editDiscount= "discount"+ab;

											var editProduct = document.getElementById(n_editProduct).value;
											var editHargaSatuan = document.getElementById(n_editHargaSatuan).value;
											var editQty = document.getElementById(n_editQty).value;
											var editHargaJual = document.getElementById(n_editHargaJual).value;
											var editDisc = document.getElementById(n_editDiscount).value;

											table.rows[a].cells[0].innerHTML = a;
											table.rows[a].cells[1].innerHTML = getProduct(a);
											table.rows[a].cells[2].innerHTML = '<input type="text" id="editHargaSatuan'+a+'" name="editHargaSatuan[]" class="form-control" value="'+editHargaSatuan+'" readonly="">';
											table.rows[a].cells[3].innerHTML = '<input type="number" id="editQty'+a+'" name="editQty[]" oninput="getNilaiDisc('+a+')" class="form-control" value="'+editQty+'">';
											table.rows[a].cells[4].innerHTML = '<input type="text" id="editHargaJual'+a+'" name="editHargaJual[]" oninput="getNilaiDisc('+a+')" class="form-control" value="'+editHargaJual+'">';
											table.rows[a].cells[5].innerHTML = getDisc(a);
											table.rows[a].cells[8].innerHTML = '<a href="javascript:void(0)" onclick="delete_row('+a+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';

											var namaField = "#product"+a;
											var namaFieldDisc = "#discount"+a;
											$(namaFieldDisc).val(editDisc); 
											$(namaField).val(editProduct); 
											$(namaField).select2();
											//totalPajak();
										}
									}
									totalPajak();
								}

								function delete_row_pajak(abs)
								{
									document.getElementById("pajak_table").deleteRow(abs);
									var rowCount = document.getElementById('pajak_table').rows.length;
									var table = document.getElementById('pajak_table');
									for(var a=1;a<rowCount;a++)
									{
										table.rows[a].cells[0].innerHTML = a;
										table.rows[a].cells[3].innerHTML = '<a href="javascript:void(0)" onclick="delete_row_pajak('+a+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
										totalPajak();
									}
									totalPajak();
								}

								function getProductPajak(abs)
								{
									var pajaks = '<select class="form-control select2" id="product_editss'+abs+'" name="editPajak[]" onchange="getNilaiPajak('+abs+')" style="width: 100%;" required="">'+
									<?php

									$sql1=mysql_query("select * from pajak where status = '1'");
									while($data1=mysql_fetch_array($sql1))
									{
										?>
										'<option value="<?php echo $data1['no'].'##'.$data1['nilai'];?>"><?php echo $data1['nama']?></option>'+

										<?php 
									}?>

									'</select>';

									return pajaks;
								}

								function getNilaiPajak(abs)
								{

									var cellValue = "product_editss"+abs
									var table = document.getElementById('pajak_table');
									var isiNilai = document.getElementById(cellValue).value;
									if(isiNilai == "")
									{
										table.rows[abs].cells[2].innerHTML = 0;
									}
									else
									{
										table.rows[abs].cells[2].innerHTML = isiNilai.split("##")[1];
									}
									totalPajak();
								}

								function totalPajak()
								{
									var nilaiSubtotal = parseInt(document.getElementById("subtotal").value.split(",").join(""));

									var rowCount = document.getElementById('pajak_table').rows.length;
									var table = document.getElementById('pajak_table');
									var nilaiPajak = 0;
									for(var a=1;a<rowCount;a++)
									{
										var persentasePajak = nilaiSubtotal * (parseFloat(table.rows[a].cells[2].innerHTML) / 100);
										nilaiPajak += persentasePajak ;

									}
									document.getElementById("lainya").value = nilaiPajak.toLocaleString('en-US');
									viewTotal();
								}

								function addLinePajak()
								{
									var rowCount = document.getElementById('pajak_table').rows.length;
									var table = document.getElementById('pajak_table');

									var row = table.insertRow(rowCount);
									var cell0 = row.insertCell(0);
									var cell1 = row.insertCell(1);
									var cell2 = row.insertCell(2);
									var cell3 = row.insertCell(3);


									cell0.innerHTML = rowCount;
									cell1.innerHTML = getProductPajak(rowCount);
									cell2.innerHTML = '';
									cell3.innerHTML = '<a href="javascript:void(0)" onclick="delete_row_pajak('+rowCount+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
									var nams = '#product_editss'+rowCount;
									$(nams).select2();
									$(nams).val(""); 
									$(nams).change();
								}

							</script>
							<div class="col-md-12">
								<br>
							</div>
						</div>

					</div>

				</div>

			</div>

			<div class="row">
				<div class="col-xs-7">
					<div class="box">
						<div class="box-body">
							<div class="col-md-12">
								<h5><strong>Pajak Tambahan</strong></h5>
								<p></p>
								<div class="box-body table-responsive no-padding">
									<table id="pajak_table" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th style="width: 40%">Nama Pajak</th>
												<th>Nilai (Dalam %)</th>
												<th><i class="fa fa-trash" aria-hidden="true"></i></th>
											</tr>
										</thead>
									</table>
								</div>
								<br>
								<a href="javascript:void(0)" id="tambahBtn22" onclick="addLinePajak()">+ Tambah Item</a>
							</div>
						</div>
						<br>
						<br>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<div class="col-xs-5">
					<div class="box">
						<div class="box-body">
							<div class="col-md-12">
								<table width="100%">
									<tbody>
										<tr>
											<td width="30%"><input type="text" class="form-control" value="SUBTOTAL :" readonly></td>
											<td width="70%"><input type="text" id="subtotal" name="subtotal" class="form-control" value="0" readonly style="text-align: right; "></td>
										</tr>
										<tr>
											<td width="30%"><input type="text" class="form-control" value="DISCOUNT :" readonly></td>
											<td width="70%"><input type="text" id="t_disc" name="t_disc" class="form-control" value="0" readonly style="text-align: right; "></td>
										</tr>
										<tr>
											<td width="30%"><input type="text" class="form-control" value="BIAYA LAINYA :" readonly></td>
											<td width="70%"><input type="text" id="lainya" name="lainya" class="form-control" value="0" readonly style="text-align: right; " ></td>
										</tr>
									</tbody>
								</table>
								<hr class="abu">
								<table width="100%">
									<tbody>
										<tr>
											<td width="30%"><input type="text" class="form-control" value="GRAND TOTAL :" readonly style="font-weight:bold;"></td>
											<td width="70%"><input type="text" id="grandtotal" name="grandtotal" class="form-control" value="0" readonly style="text-align: right;font-weight:bold; "></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<br>

						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>

			</div>
		</form>
	</section>


</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>

