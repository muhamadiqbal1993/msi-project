<?php 
$d_tgl = "";
$d_awal = "";
$d_tahun = "";
$d_urut = "";

$d_awal_d = "";
$d_tahun_d = "";
$d_urut_d = "";

if(!empty($_POST['tanggal']))
{
  $d_tgl = $_POST['tanggal'];
  $d_awal = $_POST['awal'];
  $d_tahun = $_POST['tahun'];
  $d_urut = $_POST['urut'];

  $d_awal_d = $_POST['awal_d'];
  $d_tahun_d = $_POST['tahun_d'];
  $d_urut_d = $_POST['urut_d'];
}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Tambah <?php echo $modulnya;?> Baru</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Tambah Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <form action="." method="POST" enctype="multipart/form-data">
              <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
              <div class="col-md-12">
                <div class="form-group">
                  <label>Tanggal Terbit Pajak</label>
                  <input type="text" name="tanggal" id="datepicker1" onchange="getTahun()" class="form-control" required="" value="<?php echo $d_tgl?>">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Kode Awal Faktur</label>
                  <input type="number" name="awal" class="form-control" required="" value="<?php echo $d_awal?>">
                </div>
              </div>
              <div class="col-md-1">
                <h1 style="float: center"> - </h1>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Tahun terbit</label>
                  <input type="number" name="tahun" id="tahun" class="form-control" required="" value="<?php echo $d_tahun?>" readonly>
                </div>
              </div>
              <div class="col-md-1">
                <h1 style="float: center"> . </h1>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nomor Urut</label>
                  <input type="number" name="urut" class="form-control" required="" value="<?php echo $d_urut?>">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label>Sampai</label>
                </div>
                
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Kode Awal Faktur</label>
                  <input type="number" name="awal_d" class="form-control" required="" value="<?php echo $d_awal_d?>">
                </div>
              </div>
              <div class="col-md-1">
                <h1 style="float: center"> - </h1>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Tahun terbit</label>
                  <input type="number" name="tahun_d" id="tahun_d" class="form-control" required=""  value="<?php echo $d_tahun_d?>" readonly>
                </div>
              </div>
              <div class="col-md-1">
                <h1 style="float: center"> . </h1>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nomor Urut</label>
                  <input type="number" name="urut_d" class="form-control" required="" value="<?php echo $d_urut_d?>">
                </div>
              </div>
              <div class="col-md-12">
                <br>
                <br>
                <button type="submit" class="btn btn-primary" style="width: 100%;" >Generate Faktur</button>
              </div>
            </form>

          </div>
          <br>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      function getTahun()
      {
        var tahun = document.getElementById('datepicker1').value;
        document.getElementById('tahun_d').value = tahun.substr(-2);
        document.getElementById('tahun').value = tahun.substr(-2);
        //alert(tahun);
      }
    </script>

    <?php if(!empty($_POST['tanggal'])){?>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <h3><strong>Hasil Generate</strong></h3>
            <hr class="abu">
            <table id="example4" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Tanggal Terbit</th>
                  <th>Kode</th>
                  <th>Tahun</th>
                  <th>Nomor Fakutr</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $totalCount = $_POST['urut_d'] - $_POST['urut'];
                for ($i=0; $i <= $totalCount; $i++) 
                { 
                  $noms = $_POST['urut'] + $i;
                  $stts = "<strong style='color:green'>Free</strong>";
                  $cekOrder = mysql_fetch_array(mysql_query("select * from efaktur where kode = '".$_POST['awal']."' and tahun = '".$_POST['tahun']."' and nomor = '$noms'"));
                  if($cekOrder['no'] != "")
                  {
                    $stts = "<strong style='color:red'>Sudah Ada</strong>";
                  }
                  else
                  {
                    $idss = $_POST['awal'].'-'.$_POST['tahun'].'.'.$noms;
                    $tgl_new = explode("/", $_POST['tanggal'])[2].'-'.explode("/", $_POST['tanggal'])[0].'-'.explode("/", $_POST['tanggal'])[0];
                    mysql_query("insert into efaktur values(null,'$idss','0','".$_POST['awal']."','".$_POST['tahun']."','$noms','$tgl_new','0','0','1','')");
                  }
                  
                  
                  echo '
                  <tr>
                    <td>'.$_POST['tanggal'].'</td>
                    <td>'.$_POST['awal'].'</td>
                    <td>'.$_POST['tahun'].'</td>
                    <td>'.$noms.'</td>
                    <td>'.$stts.'</td>
                  </tr>
                  ';  
                }
                ?>
              </tbody>
            </table>
          </div>
          <br>
        </div>
      </div>
    </div>
    <?php }?>
    <!-- /.row -->
  </section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
  function viewImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#photo1')
        .attr('src', e.target.result)
        .width(150)
        .height(150);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }
</script>
