<?php
$dataEdit = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(tanggal, '%m/%d/%Y') as new_tanggal from purchase_add_cost where no = '$id'"));

?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($dataEdit['status'] != "3"){?>
							<!--<button type="submit" form="create_data" class="btn btn-primary">Simpan</button>-->
							<a href="F_cancelPayment.php?id=<?php echo $id?>"><button type="submit" class="btn btn-danger" style="float: right;">Cancel Transaksi</button></a>
						<?php }?>

					</div>
				</div>
			</div>
			<form id="create_data" method="POST"> 
				<input type="hidden" name="input" class="form-control" value="2">
				<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
				<input type="hidden" name="kode" class="form-control" required="" value="<?php echo $dataEdit['id'];?>">
				<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">

				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Kode</label>
									<input type="text" name="kode" class="form-control" value="<?php echo $dataEdit['id'];?>" readonly>
								</div>

								<div class="form-group">
									<label>Purchase Order</label>
									<input type="text" name="no_so" class="form-control" value="<?php echo $dataEdit['po'];?>" readonly>
								</div>
								

							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal</label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" value="<?php echo $dataEdit['new_tanggal'];?>">
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" name="keterangan" id="keterangan" class="form-control" value="<?php echo $dataEdit['keterangan'];?>">
								</div>
								<!-- /.form-group -->
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
										<h2>Product PO</h2>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 50%">Product</th>
													<th>Qty</th>
													<th>Harga</th>
													<th>Harga Total</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$noms = 1;
												$sql1=mysql_query("select PORD.*,P.nama as nampro from bill_detail PORD
													inner join product P on P.no = PORD.product
													where PORD.bill = '".$dataEdit['po']."'");
												while($data1=mysql_fetch_array($sql1))
												{
													$totalHarga = $data1['harga_jual'] * $data1['qty'];
													echo '
													<tr>
													<td>'.$noms.'</td>
													<td>'.$data1['product'].' - '.$data1['nampro'].'</td>

													<td>'.$data1['qty'].'</td>

													<td>'.$data1['harga_jual'].'</td>

													<td>'.number_format($totalHarga).'</td>  
													
													</tr>
													';
													$noms++;
												}
												?>
												
											</tbody>
											<script language="javascript" type="text/javascript">

												ffunction inputProduct()
												{
													var no_so = document.getElementById('no_so').value;
													var table = document.getElementById('tablePO');

													for(var i = table.rows.length - 1; i > 0; i--)
													{
														table.deleteRow(i);
													}

													var xmlhttp = new XMLHttpRequest();
													xmlhttp.onreadystatechange = function() {
														if (this.readyState == 4 && this.status == 200) {
															var myArr = JSON.parse(this.responseText);

															var totalList = myArr.length;
															var totalNilai = 0;
															if(totalList == 0)
															{
																alert("Tidak Ada Product Pada PO ini");
															}
															for(var i=0;i<totalList;i++)
															{
																var rowCount = i+1;
																var row = table.insertRow(rowCount);
																var cell0 = row.insertCell(0);
																var cell1 = row.insertCell(1);
																var cell2 = row.insertCell(2);
																var cell3 = row.insertCell(3);
																var cell4 = row.insertCell(4);


																var nilaiTotal = (parseInt(myArr[i]['qty']) * parseInt(myArr[i]['harga_jual'])).toLocaleString();
																totalNilai += parseInt(myArr[i]['qty']) * parseInt(myArr[i]['harga_jual']);

																cell0.innerHTML = rowCount;
																cell1.innerHTML = myArr[i]['kodpro']+" - "+myArr[i]['nampro'];

																cell2.innerHTML = myArr[i]['qty'];
																cell3.innerHTML = myArr[i]['harga_jual'];
																cell4.innerHTML = nilaiTotal;
															}
														}
													};
													xmlhttp.open("GET","ajax/getOrderLine.php?no_order="+no_so,true);
													xmlhttp.send();
												}
											</script>
										</table>
									</div>
									
								</div>
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</div>
					
					<!-- /.box -->
				</div>
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">

										<h2>Additional Cost</h2>
										<hr class="abu">
									</div>

									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 20%">Nama Additional</th>
													<th>Keterangan</th>
													<th>Nilai</th>
												
												</tr>
											</thead>
											<tbody>
												<?php 
												$noms = 1;
												$sql1=mysql_query("select * from purchase_add_cost_detail where id = '".$dataEdit['id']."'");
												while($data1=mysql_fetch_array($sql1))
												{
													echo '
													<tr>
													<td>'.$noms.'</td>
													<td>'.$data1['nama'].'</td>

													<td>'.$data1['keterangan'].'</td>

													<td>'.number_format($data1['nilai']).'</td>

													
													
													</tr>
													';
													$noms++;
												}
												?>
											</tbody>
											<thead>
												<tr>
													<th colspan="4" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button></th>
													
												</tr>
											</thead>
											<script language="javascript" type="text/javascript">



												function delete_row_didik(abs)
												{
													document.getElementById("tabDidik").deleteRow(abs);
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													for(var a=1;a<rowCount;a++)
													{
														table.rows[a].cells[0].innerHTML = a;
														table.rows[a].cells[4].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
													}
													ubahTotal();
												}

												function addLine_didik()
												{ 
													var no_so = document.getElementById('no_so').value;
													if(no_so != "")
													{
														var keterangan = document.getElementById('keterangan').value;
														var rowCount = document.getElementById('tabDidik').rows.length - 1;
														var table = document.getElementById('tabDidik');
														var row = table.insertRow(rowCount);
														var cell0 = row.insertCell(0);
														var cell1 = row.insertCell(1);
														var cell2 = row.insertCell(2);
														var cell3 = row.insertCell(3);
														var cell4 = row.insertCell(4);

														cell0.innerHTML = rowCount;
														cell1.innerHTML = '<input type="text" name="editNama[]" class="form-control" >';
														cell2.innerHTML = '<input type="text" name="editKeterangan[]" class="form-control" >';
														cell3.innerHTML = '<input type="text" name="editNilai[]" id="editNilai'+rowCount+'" oninput="ubahDesimal('+rowCount+')" class="form-control" value="0">';

														cell4.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";
													}
													else
													{
														alert("Pilih Purchase Order terlebih dahulu");
													}
												
												}

												function ubahDesimal(ids)
												{
													var nams = "editNilai"+ids;
													var val = document.getElementById(nams).value.split(",").join("");
													document.getElementById(nams).value = parseInt(val).toLocaleString('en-US');
													ubahTotal();
												}

												function ubahTotal()
												{
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													var total = 0;

													for(var a=1;a<rowCount;a++)
													{
														var editNilai = 'editNilai'+a;
														var nilai = document.getElementById(editNilai).value.split(",").join("");
														total += parseInt(nilai);
													}


													document.getElementById('nilaiReturn').innerHTML = "<strong>Rp. "+total.toLocaleString()+"</strong>";
												}
											</script>
										</table>
									</div>
								</div>
								<div class="col-md-12">
									<hr class="abu">
									<h2 style="float: right;">Total Additional Cost : <span id="nilaiReturn" style="color: red;"><strong>Rp. <?php echo number_format($dataEdit['total'])?></strong></span></h2>
								</div>

							</div>
						</div>
					</div>
					<?php 
					$nama_modulnya = 'purchase_add_cost';
					include '../headfoot/history.php';
					?>
				</div>
			</form>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>
