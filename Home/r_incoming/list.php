<?php 
$customer = "";
$payment = "";
$client = "";
$wheres = "";
$dari = date('m/d/Y');
$sampai = date('m/d/Y');
$drs = explode("/", $dari)[2].'-'.explode("/", $dari)[0].'-'.explode("/", $dari)[1];
$smps = explode("/", $sampai)[2].'-'.explode("/", $sampai)[0].'-'.explode("/", $sampai)[1];
$caris = "";
if(!empty($_POST['cek']))
{
	$dari = $_POST['dari'];
	$sampai = $_POST['sampai'];
	$drs = explode("/", $dari)[2].'-'.explode("/", $dari)[0].'-'.explode("/", $dari)[1];
	$smps = explode("/", $sampai)[2].'-'.explode("/", $sampai)[0].'-'.explode("/", $sampai)[1];
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li class="active"><?php echo $modulnya;?></li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<!-- /.box -->

				<div class="box">
					<div class="box-header">
            <!-- <?php if($addPM == "1"){?>
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu">
            <?php }?> -->
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">  
          	<?php 
          	if($status == "1")
          	{
          		echo '<div class="col-xs-12">
          		<div class="alert alert-success alert-dismissible">
          		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          		<h4><i class="icon fa fa-check"></i> Berhasil!</h4>
          		Data '.$modulnya.' Berhasil Dibuat.
          		</div>
          		</div>';
          	}
          	if($status == "2")
          	{
          		echo '<div class="col-xs-12">
          		<div class="alert alert-danger alert-dismissible">
          		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          		<h4><i class="icon fa fa-ban"></i> Error!</h4>
          		Ada Masalah Dengan Server , Segera Hubungi Administrator.
          		</div>
          		</div>';
          	}
          	if($status == "3")
          	{
          		echo '<div class="col-xs-12">
          		<div class="alert alert-warning alert-dismissible">
          		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          		<h4><i class="icon fa fa-ban"></i> Warning!</h4>
          		Data '.$modulnya.' sudah ada yang menggunakan , silahkan daftarkan dengan data yang lain
          		</div>
          		</div>';
          	}
          	?>
          	<div id="pencarian">
          		<!-- <form action="../excel/ledger/index.php" id="search_data" method="POST">  -->
          			<form id="search_data" method="POST"> 
          				<div class="col-md-4">
          					<div class="form-group">
          						<label>Tanggal</label>
          						<input type="hidden" name="cek" class="form-control" required="" value="ada">
          						<input type="text" name="dari" id="datepicker" class="form-control" required="" value="<?php echo $dari?>">
          					</div>
          				</div>

          				<div class="col-md-8">
          					<div class="form-group">
          						<label>Customer <span style="color:red;"><i>jika customer tidak di isi maka semua customer akan terpilih</i></span></label>
          						<select class="form-control select2" name="customer[]" multiple="" style="width: 100%;">
          							<?php
          							$sql1=mysql_query("select * from customer where status = '1'");
          							while($data1=mysql_fetch_array($sql1))
          							{
          								?>
          								<option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $customer){echo "selected";}?>><?php echo $data1['kode'].' - '.$data1['nama']?></option>
          								<?php 
          							}
          							?>

          						</select>
          					</div>
          				</div>

          			</form>
          			<div class="col-xs-12">
          				<button type="submit" class="btn btn-primary" onclick="localForm()">View Data</button>
          				<!-- <button type="submit" onclick="pencarian()" class="btn btn-warning">Sembunyikan Pencarian</button> -->
          				<!-- <button type="submit" class="btn btn-success" style="float: right;" onclick="downloadExcel()">Download Excel</button> -->
          			</div>
          		</div>

          		<script type="text/javascript">

          			function localForm()
          			{
          				document.getElementById("search_data").action = ".";
          				document.getElementById("search_data").submit();
          			}
          			function downloadExcel()
          			{
          				document.getElementById('search_data').action = "../excel/ledger/index.php"; 
          				document.getElementById("search_data").submit();
          			}
          			function pencarian()
          			{
          				document.getElementById('pencarian').style.display = "none"; 
          				document.getElementById('buka').style.display = ""; 
          			}
          			function buka()
          			{
          				document.getElementById('pencarian').style.display = ""; 
          				document.getElementById('buka').style.display = "none";
          			}
          		</script>

          		<div class="col-xs-12" id="buka" style="display: none">
          			<button type="submit" onclick="buka()" class="btn btn-primary">Buka Pencarian</button>
          		</div>

          		<div class="col-xs-12">
          			<hr class="abu">
          		</div>

          		<div class="col-xs-12">
          			<table id="example3" class="table table-bordered table-striped">
          				<thead>
          					<tr>
          						<th>Tanggal</th>
          						<th>SMU</th>
          						<th>Customer</th>
          						<th>Koli</th>
          						<th>Kilo</th>
          						<th>Harga Jual</th>
          						<th>Total Harga Jual</th>
          						<th>Comodity</th>
          						<th>Tambahan</th>
          						<th>Grandtotal</th>
          					</tr>
          				</thead>
          				<tbody>
          					<?php
          					$koli = 0;
          					$kilo = 0;
          					$harga = 0;
          					$t_harga = 0;
          					$tambahan = 0;
          					$g_total = 0;

          					$sql1=mysql_query("select IDS.*,C.nama as namcus,I.keterangan from invoice_detail IDS
          						inner join invoice I on I.inv = IDS.invoice
          						inner join customer C on C.no = I.customer
          						where I.tanggal like '%$drs%'");
          					while($data1=mysql_fetch_array($sql1))
          					{
          						?>
          						<tr>
          							<td ><?php echo $dari?></td>
          							<td ><?php echo $data1['awb']?></td>
          							<td ><?php echo $data1['namcus']?></td>
          							<td ><?php echo $data1['koli']?></td>
          							<td ><?php echo $data1['berat']?></td>
          							<td ><?php echo number_format($data1['harga'])?></td>
          							<td ><?php echo number_format($data1['harga']*$data1['berat'])?></td>
          							<td ><?php echo $data1['keterangan']?></td>
          							<td ><?php echo number_format($data1['tambahan'])?></td>
          							<td ><?php echo number_format($data1['total'])?></td>
          						</tr>
          						<?php
          						$koli += $data1['koli'];
          						$kilo += $data1['berat'];
          						$harga += $data1['harga'];
          						$t_harga += $data1['harga']*$data1['berat'];
          						$tambahan += $data1['tambahan'];
          						$g_total += $data1['total'];
          					}
          					?>
          					<tr>
          						<td style="background-color: silver;text-align: center;" colspan="3"><strong>Total</strong></td>
          						<td style="background-color: silver;"><?php echo number_format($koli)?></td>
          						<td style="background-color: silver;"><?php echo number_format($kilo)?></td>
          						<td style="background-color: silver;"></td>
          						<td style="background-color: silver;"><?php echo number_format($t_harga)?></td>
          						<td style="background-color: silver;"></td>
          						<td style="background-color: silver;"><?php echo number_format($tambahan)?></td>
          						<td style="background-color: silver;"><?php echo number_format($g_total)?></td>
          					</tr>
          				</tbody>
          			</table>
          		</div>

          	</div>
          	<br>
          	<br>
          	<!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

  </div>


