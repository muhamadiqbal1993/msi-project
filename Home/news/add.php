<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST" enctype="multipart/form-data"> 
						<div class="box-body">
							<input type="hidden" name="input" class="form-control" value="1">
							<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
							<div class="col-md-4">
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Photo Display</label>
									<br>
									<div class="btn btn-default btn-file" id="photo62" >
										<p style="text-align:center;"><img id="photo1" src="../images/image.png" style="height: 150px;width: 150px"/></p>
										<input type="file" name="photos" onchange="viewImage(this);">

									</div>
								</div>
							</div>
							<div class="col-md-5">
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label>Judul</label>
									<input type="text" name="judul" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>File</label>
									<input type="file" name="filest">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Dari Tanggal</label>
									<input type="text" name="dari" id="datepicker" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Sampai Tanggal</label>
									<input type="text" name="sampai" id="datepicker1" class="form-control" required="">
								</div>
							</div>
							<div class="col-md-12">
								<label>Isi</label>
								<textarea class="form-control" rows="3" name="isi" placeholder="Enter ..."></textarea>
							</div>
							<!-- /.col -->

							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(400)
				.height(400);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>