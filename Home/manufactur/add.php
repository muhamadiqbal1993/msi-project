<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="button" onclick="save_modul()" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<input type="hidden" name="bisaProduksi" id="bisaProduksi" class="form-control" value="0"> 
						<input type="hidden" name="detail" id="detail" class="form-control" > 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>ID Bom</label>
									<input type="text" name="kode" class="form-control" readonly="" value="<?php echo 'BOM-'.date('ymdHis')?>">
								</div>
								<div class="form-group">
									<label>Product</label>
									<select class="form-control select2" name="product" id="product" style="width: 100%;">
										<option value="" >----- Pilih Product Jadi -----</option>
										<?php
										$sql1=mysql_query("select * from product");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['kode'].' - '.$data1['nama']?></option>
											<?php 
										}?>

									</select>
								</div>
								<div class="form-group">
									<label>Tanggal</label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" required>
								</div>
								
							</div>
							<div class="col-md-6">
								
								<div class="form-group">
									<label>Keterangan Tambahan</label>
									<input type="text" name="keterangan" id="keterangan" class="form-control" >
								</div>
								<div class="form-group">
									<label>Qty Produksi</label>
									<input type="number" name="qty" id="qty" class="form-control" value="0">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
									</div>
									<div class="col-md-6">
										<h3><strong><?php echo $modulnya.' Line';?></strong></h3>
									</div>
									<div class="col-md-6">
										<br>
										<button type="button" class="btn btn-success" style="float: right;" onclick="computeBOM()">Compute BOM</button>
									</div>
									<div class="col-md-12">
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 20%">Product</th>
													<th>Gudang Raw Material</th>
													<th>UOM</th>
													<th>Qty</th>
													<th>Stock Gudang</th>
													<th>Status Stock</th>
												</tr>
											</thead>

											<script language="javascript" type="text/javascript">

												function computeBOM()
												{
													var d_product = document.getElementById("product").value;
													var d_qty = document.getElementById("qty").value;
													var table = document.getElementById('tabDidik');

													for(var i = table.rows.length - 1; i > 0; i--)
													{
														table.deleteRow(i);
													}

													if(d_product == "")
													{
														alert("Product Tidak Boleh Kosong");
													}
													else
													{
														if(d_qty == "0" || d_qty == "")
														{
															alert("Qty Tidak Boleh Kosong atau 0");
														}
														else
														{
															var xmlhttp = new XMLHttpRequest();
															xmlhttp.onreadystatechange = function() {
																if (this.readyState == 4 && this.status == 200) 
																{
																	var myArr = JSON.parse(this.responseText);
																	var totalList = myArr.length;
																	if(totalList == "0")
																	{
																		alert("Product Ini Belum Memiliki BOM, Silahkan buat BOM terlebih dahulu");
																	}
																	else
																	{
																		document.getElementById("detail").value = "";
																		for (var i = 0; i < myArr.length; i++) 
																		{
																			var row = table.insertRow(i + 1);

																			var statusStock = "<a style='color:green'><strong>Stock Tersedia</strong></a>";

																			if(parseInt(myArr[i]['stock']) < parseInt(myArr[i]['qty_rawmat']))
																			{
																				statusStock = "<a style='color:red'><strong>Stock Tidak Tersedia</strong></a>";
																				document.getElementById("bisaProduksi").value = "1";
																			}

																			var cell0 = row.insertCell(0);
																			var cell1 = row.insertCell(1);
																			var cell2 = row.insertCell(2);
																			var cell3 = row.insertCell(3);
																			var cell4 = row.insertCell(4);
																			var cell5 = row.insertCell(5);
																			var cell6 = row.insertCell(6);
																			

																			cell0.innerHTML = i+1;
																			cell1.innerHTML = myArr[i]['namprod'];
																			cell2.innerHTML = myArr[i]['namgud'];
																			cell3.innerHTML = myArr[i]['satuan'];
																			cell4.innerHTML = myArr[i]['qty_rawmat'];
																			cell5.innerHTML = myArr[i]['stock'];
																			cell6.innerHTML = statusStock;

																			document.getElementById("detail").value += "!#!"+myArr[i]['namprod']+"##"+myArr[i]['namgud']+"##"+myArr[i]['satuan']+"##"+myArr[i]['qty_rawmat']+"##"+myArr[i]['stock']+"##"+myArr[i]['harga_flyer'];
																			
																		}
																	}
																}
															};
															xmlhttp.open("GET","js/getDetailBom.php?product="+d_product+"&qty="+d_qty,true);
															xmlhttp.send();
														}
													}
												}
											</script>
										</table>
									</div>
								</div>
							</div>

						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var table = document.getElementById('tabDidik');

		if(table.rows.length == "1")
		{
			alert("Anda Belum Melakukan Compute BOM");
		}
		else
		{
			var stockReady = document.getElementById('bisaProduksi').value;
			
			if(stockReady == "1")
			{
				alert("Anda tidak dapat melanjutkan Produksi, Karena beberapa barang Raw Material anda tidak mempunyai stock");
			}
			else
			{
				document.getElementById("create_data").submit();
			}
		}
		
	}
</script>
