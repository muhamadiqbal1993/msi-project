<?php
$dataEdit = mysql_fetch_array(mysql_query("select * from efaktur where no = '$id'"));
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Edit <?php echo $modulnya;?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Edit Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
            &nbsp;&nbsp;&nbsp;
            <?php if($dataEdit['total'] != "0"){?>
            <button type="submit" form="actionAdd" class="btn btn-success">Sudah Upload</button>
            &nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-primary">Pengganti</button>
            <button type="button" class="btn btn-danger" style="float: right">Pembatalan</button>
            <?php }?>
            <hr class="abu">
          </div>
	 <form id="actionAdd" action="F_deleteData.php" method="POST">
            <input type="hidden" name="status" class="form-control" value="3">
            <input type="hidden" name="ids" class="form-control" value="<?php echo $id?>">
          </form>
          <form id="create_data" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="input" class="form-control" value="2">
            <input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
            <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nomor Faktur</label>
                  <input type="text" name="nama" class="form-control" required="" value="<?php echo $dataEdit['id'];?>" readonly>
                </div>
                <div class="form-group">
                 <label>Tanggal Terbit</label>
                 <input type="text" name="nama" class="form-control" required="" value="<?php echo $dataEdit['tanggal_terbit'];?>" readonly>
               </div>
             </div>

             <div class="col-md-6">
              <div class="form-group">
                <label>Nilai Transaksi</label>
                <input type="text" name="urutan" class="form-control" required="" value="<?php echo $dataEdit['total'];?>" readonly>
              </div>
              <div class="form-group">
                <label>Nilai Pajak</label>
                <input type="number" name="nilai" class="form-control" required="" value="<?php echo $dataEdit['pajak'];?>" readonly>
              </div>
            </div>
          </div>
        </form>
        <br>
        <br>
      </div>
      <?php 
      $nama_modulnya = 'level_sales';
      include '../headfoot/history.php';
      ?>
    </div>
  </div>
</section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }

  function viewImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#photo1')
        .attr('src', e.target.result)
        .width(200)
        .height(200);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }
</script>
