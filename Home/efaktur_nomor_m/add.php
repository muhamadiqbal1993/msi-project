<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-md-6">

								<div class="form-group">
									<label>Nama Level</label>
									<input type="text" name="nama" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Icon</label>
									<p></p>
									<img id="photo1"/>
									<p></p>
									<div class="btn btn-default btn-file" id="photo62">
										<i class="fa fa-image"></i> Pilih Icon
										<input type="file" name="photos" onchange="viewImage(this);">

									</div>
									<p></p>
									<p></p>
									<p></p>
									<p></p>
									
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Urutan Level</label>
									<input type="text" name="urutan" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Nominal Penjualan</label>
									<input type="number" name="nilai" class="form-control" required="">
								</div>
							</div>
							<!-- /.col -->

							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(150)
				.height(150);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>