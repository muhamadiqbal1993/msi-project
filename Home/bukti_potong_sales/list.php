<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li class="active"><?php echo $modulnya;?></li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<!-- /.box -->

				<div class="box">
					<div class="box-header">
						<!-- <form action="." method="post">
							<input type="hidden" name="type" value="input">
							<button type="submit" class="btn btn-primary">Bulk Upload Bukti Potong</button>
						</form>
						<hr class="abu"> -->
					</div>

					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<?php 
						if($status == "1")
						{
							echo '<div class="col-xs-12">
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<h4><i class="icon fa fa-check"></i> Berhasil!</h4>
								Data Berhasil Dibuat.
							</div>
						</div>';
					}
					if($status == "2")
					{
						echo '<div class="col-xs-12">
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-ban"></i> Error!</h4>
							Ada Masalah Dengan Server , Segera Hubungi Administrator.
						</div>
					</div>';
				}
				if($status == "3")
				{
					echo '<div class="col-xs-12">
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-ban"></i> Warning!</h4>
						'.$modulnya.' sudah ada yang menggunakan , silahkan ganti dengan data yang berbeda
					</div>
				</div>';
			}
			?>


			<div class="col-xs-12">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>No Sales Order</th>
							<th>Customer</th>
							<th>Tanggal</th>
							<th>Nilai Transaksi</th>
							<th>Nilai Bukti Potong</th>
							<th>File Bukti Potong</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$no = 1;
						$sql1=mysql_query("select I.*,C.nama as namcus,DATE_FORMAT(I.tanggal, '%d %b %Y') as tgls,BPS.nilai as bukti_potong,BPS.bukti from invoice I
							inner join customer C on C.no = I.customer
							left outer join bukti_potong_sales BPS on BPS.id_transaksi = I.inv
							where I.status not in ('5')");
						while($data1=mysql_fetch_array($sql1))
						{  
							$status = "<span style='color:red'><strong>Belum Upload</strong></span>";
							$upls = ' <a href="./?id='.$data1['no'].'">Upload Bukti</a>';
							$file = "";
							if($data1['bukti'] != "")
							{
								$status = "<span style='color:green'><strong>Sudah Upload</strong></span>";
								$file = "<a href='../images/".$data1['bukti']."' target='_blank'>Download File Bukti</a>";
								$upls = "<a href='F_deleteData.php?id=".$data1['inv']."' style='color:red'>Hapus Bukti</a>";
							} 
							else
							{
								if($data1['status'] == "2")
								{
									$status = "<span style='color:green'><strong>Sudah Lunas</strong></span>";
									$file = "<span style='color:orange'><strong>Tanpa Bukti Potong</strong></span>";
									$upls = "";
								} 
							}
							

							echo '
							<tr>
								<td>'.$no.'</td>  
								<td>'.$data1['inv'].'</td>
								<td>'.$data1['namcus'].'</td>
								<td>'.$data1['tgls'].'</td>
								<td>'.number_format($data1['grandtotal']).'</td>
								<td>'.number_format($data1['bukti_potong']).'</td>
								<td>'.$file.'</td> 
								<td>'.$status.'</td>   
								<td>'.$upls.'</td>
							</tr>
							';
							$no++;
						}
						?>
					</tbody>
				</table>
			</div>

		</div>
		<br>
		<br>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>

