<?php

$kode = "";
$nama = "";
$jabatan = "";
$status = "";

$sql1=mysql_query("select K.*,GD.nama as namjab,SK.nama as namsta from karyawan K
	inner join group_divisi GD on GD.no = K.jabatan
	inner join status_karyawan SK on SK.no = K.status
	where K.no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$kode = $data1['kode'];
	$nama = $data1['nama'];
	$jabatan = $data1['namjab'];
	$status = $data1['namsta'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<input type="hidden" name="detKontak" id="detKontak" class="form-control">
					</form>
					<div class="box-body">
						<div class="col-md-6">

							<div class="form-group">
								<label>Kode</label>
								<input type="text" name="kode" class="form-control" required="" value="<?php echo $kode?>" readonly>
							</div>
							<div class="form-group">
								<label>Nama Karyawan</label>
								<input type="text" name="nama" class="form-control" required="" value="<?php echo $nama?>" readonly>
							</div>

							<!-- /.form-group -->

						</div>
						<!-- /.col -->
						<div class="col-md-6">
							<div class="form-group">
								<label>Jabatan</label>
								<input type="text" name="jabatan" class="form-control" required="" value="<?php echo $jabatan?>" readonly>
							</div>
							<div class="form-group">
								<label>Status Karyawan</label>
								<input type="text" name="status" class="form-control" required="" value="<?php echo $status?>" readonly>
							</div>


							<!-- /.form-group -->
						</div>
						<div class="col-md-12">
							<h1>Komponen Gaji</h1>
							<hr class="abu">
						</div>

						<div class="col-md-12">
							<input type="hidden" name="detKontak" id="detKontak" class="form-control">
							<p></p>
							<table id="tabKontak" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width: 5%">No</th>
										<th>Nama</th>
										<th>Nilai</th>
										<th>Action</th>
									</tr>
								</thead>
								<?php
								$total_nya = "";
								$detailss = "";
								$nom = 1;
								$sql2=mysql_query("select * from komponen where status = '1'");
								while($data2=mysql_fetch_array($sql2))
								{
									$dataGaji = mysql_fetch_array(mysql_query("select * from salary where id_komponen = '".$data2['no']."' and id_kar='$id'"));
									?>
									<tr>
										<td><?php echo $nom;?></td>
										<td><?php echo $data2['nama'];?></td>
										<td><?php echo $dataGaji['nilai'];?></td>
										<td><a href="#" onclick="edit_row(<?php echo $nom;?>)"> Edit </a> </td>
									</tr>

									<?php 
									$nom++;
								}
								?>
								<script language="javascript" type="text/javascript">
									document.getElementById("detKontak").value = "<?php echo $detailss?>";
									function view_total()
									{
										var detDimensi = "";
										var totals = 0;
										var rowCount = document.getElementById('tabKontak').rows.length;
										var table = document.getElementById('tabKontak');
										for(var a=1;a<rowCount;a++)
										{
											var nama = table.rows[a].cells[1].innerHTML;
											var nilai = table.rows[a].cells[2].innerHTML;
											detDimensi = detDimensi+","+nama+"spasinya"+nilai;
										}
										document.getElementById("detKontak").value = detDimensi;
									}

									function edit_row(abs)
									{
										var rowCount = document.getElementById('tabKontak').rows.length;
										var table = document.getElementById('tabKontak');
										for(var a=1;a<rowCount;a++)
										{
											if(abs == a)
											{
												var nilai = table.rows[a].cells[2].innerHTML;

												table.rows[a].cells[2].innerHTML = '<input type="number" id="editNilai" class="form-control" value="'+nilai+'">';
												table.rows[a].cells[3].innerHTML = "<button type='submit' onclick='save_row(\""+a+"\")' class='btn btn-primary'>Simpan</button>";
											}
											else
											{
												table.rows[a].cells[3].innerHTML = "";
											}
										}
									}

									function save_row(abs)
									{
										var rowCount = document.getElementById('tabKontak').rows.length;
										var table = document.getElementById('tabKontak');
										for(var a=1;a<rowCount;a++)
										{
											if(abs == a)
											{
												var nilai = document.getElementById("editNilai").value;

												table.rows[abs].cells[2].innerHTML = nilai;
												table.rows[abs].cells[3].innerHTML = "<a href='#' onclick='edit_row(\""+abs+"\")'> Edit </a>";
											}
											else
											{
												table.rows[a].cells[3].innerHTML = "<a href='#' onclick='edit_row(\""+a+"\")'> Edit </a>";
											}
										}
										view_total();
									}

								</script>
							</table>
						</div>
					</div>

					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'salary';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>