<?php 
$brand = "";
$rak = "";
$gudang = "";
$kodnamket = "";
$wheres = "";

if(!empty($_POST['kodnamket']))
{
  $kodnamket = $_POST['kodnamket'];
  $wheres .= " and KP.no = '$kodnamket'";
}
if(!empty($_POST['brand']))
{
 $brand = $_POST['brand'];
 $wheres .= " and B.no = '$brand'";
}
if(!empty($_POST['gudang']))
{
 $gudang = $_POST['gudang'];
 $wheres .= " and R.gudang = '$gudang'";
}
if(!empty($_POST['rak']))
{
 $rak = $_POST['rak'];
 $wheres .= " and rak = '$rak'";
}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">

            <div id="pencarian">
              <!-- <form action="../excel/ledger/index.php" id="search_data" method="POST">  -->
                <form id="search_data" method="POST"> 

                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Kategory</label>

                      <select class="form-control select2" name="kodnamket" style="width: 100%;">
                        <option value="">--- Semua Kategory ---</option>
                        <?php
                        $sql1=mysql_query("select * from kategory_product where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $kodnamket){echo "selected";}?>><?php echo $data1['nama']?></option>
                          <?php 
                        }
                        ?>

                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Brand</label>
                      <select class="form-control select2" name="brand" style="width: 100%;">
                        <option value="">--- Semua Brand ---</option>
                        <?php
                        $sql1=mysql_query("select * from brand where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $brand){echo "selected";}?>><?php echo $data1['nama']?></option>
                          <?php 
                        }
                        ?>

                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Lokasi</label>
                      <select class="form-control select2" name="rak" style="width: 100%;">
                        <option value="">--- Semua Lokasi ---</option>
                        <?php
                        $sql1=mysql_query("select * from rak where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $rak){echo "selected";}?>><?php echo $data1['nama']?></option>
                          <?php 
                        }
                        ?>

                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Gudang</label>
                      <select class="form-control select2" name="gudang" style="width: 100%;">
                        <option value="">--- Semua Gudang ---</option>
                        <?php
                        $sql1=mysql_query("select * from gudang where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $gudang){echo "selected";}?>><?php echo $data1['nama']?></option>
                          <?php 
                        }
                        ?>

                      </select>
                    </div>
                  </div>
                </form>
                <div class="col-xs-12">
                  <button type="submit" class="btn btn-primary" form="search_data">Cari Data</button>
                  <!-- <button type="submit" onclick="pencarian()" class="btn btn-warning">Sembunyikan Pencarian</button> -->
                  <a href="../prints/stock/?gudang=<?php echo $gudang?>&nama=<?php echo $kodnamket?>&brand=<?php echo $brand?>&rak=<?php echo $rak?>" target="_blank"><button type="button" class="btn btn-success" style="float: right;" >Print Stock</button></a>
                  <hr class="abu">
                </div>
              </div>



            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <?php 
              if($status == "1")
              {
                echo '<div class="col-xs-12">
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data '.$modulnya.' Berhasil Dibuat.
                </div>
                </div>';
              }
              if($status == "2")
              {
                echo '<div class="col-xs-12">
                <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                Ada Masalah Dengan Server , Segera Hubungi Administrator.
                </div>
                </div>';
              }
              if($status == "3")
              {
                echo '<div class="col-xs-12">
                <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Warning!</h4>
                Data '.$modulnya.' sudah ada yang menggunakan , silahkan daftarkan dengan data yang lain
                </div>
                </div>';
              }
              ?>


              <script type="text/javascript">

                function ilang()
                {
                  if(document.getElementById('tambah').style.display == 'none')
                  {
                    document.getElementById('tambah').style.display = "block";
                    document.getElementById('barcode').focus();
                  }
                  else
                  {
                    document.getElementById('tambah').style.display = "none";
                  }

                }

                function checkTable()
                {
                  var idss = document.getElementById('barcode').value;
                  var xmlhttp = new XMLHttpRequest();
                  xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      var myArr = JSON.parse(this.responseText);

                      var totalList = myArr.length;
                      if (myArr.length == 0)
                      {
                        alert("Product Tidak ditemukan");
                        document.getElementById('barcode').value = "";
                        document.getElementById('hasil').style.display = "none";
                      }
                      else
                      {
                        document.getElementById('hasil').style.display = "block";
                        document.getElementById('product').value = "["+myArr[0]['kode']+"] "+myArr[0]['nama'];
                        document.getElementById('idproduct').value = myArr[0]['no'];
                        document.getElementById('qty').focus();
                      }

                    }
                  };
                  xmlhttp.open("GET","checkProduct.php?ids="+idss,true);
                  xmlhttp.send();
                }
              </script>

      <!-- <div class="modal fade" id="pop-create-data">
        <div class="modal-dialog ">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><strong><?php echo '['.$dataProduct['kode'].'] '.$dataProduct['nama'];?></strong></h4>
              </div>
              <form id="simpanss" method="POST"> 
                <input type="hidden" name="input" class="form-control" value="1">
                <input type="hidden" name="product" class="form-control" required="" value="<?php echo $id;?>">
                <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">
                <input type="hidden" name="kodeid" class="form-control" value="<?php echo $kodeid;?>"> 
                <div class="modal-body">
                  <div class="form-group">
                    <label>Tanggal</label>
                    <input type="date" name="tanggal" class="form-control" required="" value="<?php echo date('Y-m-d')?>" >
                  </div>
                  <div class="form-group">
                    <label>Type Transaksi</label>
                    <select class="form-control select2" name="type" style="width: 100%;" required="">
                      <option value="1" >Masuk</option>
                      <option value="2" >Keluar</option>
                    </select>
                  </div>


                  <div class="form-group">
                    <label>Qty</label>
                    <input type="number" name="qty" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" name="keterangan" class="form-control">
                  </div>
                </div>
              </form>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" form="simpanss" class="btn btn-primary">Input</button>
              </div>
            </div>
          </div>
        </div> -->

        <div class="col-xs-12">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Gudang</th>
                <th>Lokasi</th>
                <th>Kategory</th>
                <th>Brand</th>
                <th>Nama</th>
                <th>Stock</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $noms = 1;
              $querys = "select P.*,R.nama as namrak,B.nama as nambrand,KP.nama as namkat,G.nama as namgud from product P
              left outer join rak R on R.no = P.rak
              left outer join brand B on B.no = P.kategory
              left outer join kategory_product KP on KP.no = B.kategory
              left outer join gudang G on G.no = R.gudang
              where P.status = '1' $wheres limit 100";
              //echo $querys;
              $sql1=mysql_query($querys);
              while($data1=mysql_fetch_array($sql1))
              {
                echo '
                <tr>
                <td>'.$noms.'</td>
                <td>'.$data1['kode'].'</td>
                <td>'.$data1['namgud'].'</td>
                <td>'.$data1['namrak'].'</td>
                <td>'.$data1['namkat'].'</td>
                <td>'.$data1['nambrand'].'</td>
                <td>'.$data1['nama'].'</td>
                <td>'.$data1['type'].' '.$data1['satuan'].'</td>
                <td><a href="./?id='.$data1['no'].'">History</a></td>
                </tr>';
                $noms++;
              }
              ?>

            </tbody>
          </table>
        </div>

      </div>
      <br>
      <br>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>
