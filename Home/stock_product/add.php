<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Hari</label>
									<select class="form-control select2" name="hari" style="width: 100%;" required="">
										<option value="Senin" >Senin</option>
										<option value="Selasa" >Selasa</option>
										<option value="Rabu" >Rabu</option>
										<option value=Kamis >Kamis</option>
										<option value="Jumat" >Jumat</option>
										<option value="Sabtu" >Sabtu</option>
										<option value="Minggu" >Minggu</option>
									</select>
								</div>
								<div class="form-group">
									<label>Nama Sesi</label>
									<input type="text" name="nama" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Kolam</label>
									<select class="form-control select2" name="kolam" style="width: 100%;" required="">
										<option value="Semua" >Semua</option>
										<?php
										$sql1=mysql_query("select * from m_kolam where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Jam Mulai</label>
									<input type="text" name="jam_mulai" class="form-control timepicker1" required="">
								</div>
								<div class="form-group">
									<label>Jam Berhenti</label>
									<input type="text" name="jam_berhenti" class="form-control timepicker1" required="">
								</div>
							</div>
							
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>