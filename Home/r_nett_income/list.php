<?php 
$customer = "";
$payment = "";
$client = "";
$wheres = "";
$dari = date('m/d/Y');
$sampai = date('m/d/Y');
$drs = explode("/", $dari)[2].'-'.explode("/", $dari)[0].'-'.explode("/", $dari)[1];
$smps = explode("/", $sampai)[2].'-'.explode("/", $sampai)[0].'-'.explode("/", $sampai)[1];
$caris = "";
if(!empty($_POST['cek']))
{
	$dari = $_POST['dari'];
	$sampai = $_POST['sampai'];
	$drs = explode("/", $dari)[2].'-'.explode("/", $dari)[0].'-'.explode("/", $dari)[1];
	$smps = explode("/", $sampai)[2].'-'.explode("/", $sampai)[0].'-'.explode("/", $sampai)[1];
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li class="active"><?php echo $modulnya;?></li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<!-- /.box -->

				<div class="box">
					<div class="box-header">
            <!-- <?php if($addPM == "1"){?>
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu">
            <?php }?> -->
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">  
          	<?php 
          	if($status == "1")
          	{
          		echo '<div class="col-xs-12">
          		<div class="alert alert-success alert-dismissible">
          		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          		<h4><i class="icon fa fa-check"></i> Berhasil!</h4>
          		Data '.$modulnya.' Berhasil Dibuat.
          		</div>
          		</div>';
          	}
          	if($status == "2")
          	{
          		echo '<div class="col-xs-12">
          		<div class="alert alert-danger alert-dismissible">
          		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          		<h4><i class="icon fa fa-ban"></i> Error!</h4>
          		Ada Masalah Dengan Server , Segera Hubungi Administrator.
          		</div>
          		</div>';
          	}
          	if($status == "3")
          	{
          		echo '<div class="col-xs-12">
          		<div class="alert alert-warning alert-dismissible">
          		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          		<h4><i class="icon fa fa-ban"></i> Warning!</h4>
          		Data '.$modulnya.' sudah ada yang menggunakan , silahkan daftarkan dengan data yang lain
          		</div>
          		</div>';
          	}
          	?>
          	<div id="pencarian">
          		<!-- <form action="../excel/ledger/index.php" id="search_data" method="POST">  -->
          			<form id="search_data" method="POST"> 
          				<div class="col-md-2">
          					<div class="form-group">
          						<label>Dari Tanggal</label>
          						<input type="hidden" name="cek" class="form-control" required="" value="ada">
          						<input type="text" name="dari" id="datepicker" class="form-control" required="" value="<?php echo $dari?>">
          					</div>
          				</div>
          				<div class="col-md-2">
          					<div class="form-group">
          						<label>Sampai Tanggal</label>
          						<input type="text" name="sampai" id="datepicker1" class="form-control" required="" value="<?php echo $sampai?>">
          					</div>
          				</div>

          			</form>
          			<div class="col-xs-12">
          				<button type="submit" class="btn btn-primary" onclick="localForm()">View Data</button>
          				<!-- <button type="submit" onclick="pencarian()" class="btn btn-warning">Sembunyikan Pencarian</button> -->
          				<!-- <button type="submit" class="btn btn-success" style="float: right;" onclick="downloadExcel()">Download Excel</button> -->
          			</div>
          		</div>

          		<script type="text/javascript">

          			function localForm()
          			{
          				document.getElementById("search_data").action = ".";
          				document.getElementById("search_data").submit();
          			}
          			function downloadExcel()
          			{
          				document.getElementById('search_data').action = "../excel/ledger/index.php"; 
          				document.getElementById("search_data").submit();
          			}
          			function pencarian()
          			{
          				document.getElementById('pencarian').style.display = "none"; 
          				document.getElementById('buka').style.display = ""; 
          			}
          			function buka()
          			{
          				document.getElementById('pencarian').style.display = ""; 
          				document.getElementById('buka').style.display = "none";
          			}
          		</script>

          		<div class="col-xs-12" id="buka" style="display: none">
          			<button type="submit" onclick="buka()" class="btn btn-primary">Buka Pencarian</button>
          		</div>

          		<div class="col-xs-12">
          			<hr class="abu">
          		</div>

          		<div class="col-xs-12">
          			<table id="example3" class="table table-bordered table-striped">
          				<thead>
          					<tr>
          						<th>Description</th>
          						<th>Balance</th>
          					</tr>
          				</thead>
          				<tbody>
          					<?php 
          					$totalSemua = 0;
          					$sql11=mysql_query("select * from template_nett_income order by type asc");
          					while($dataMaster=mysql_fetch_array($sql11))
          					{
          						$totalBal = 0;
          						$dataC = mysql_fetch_array(mysql_query("select * from coa where no = '".$dataMaster['coa']."'"));

          						$dataHeader = mysql_fetch_array(mysql_query("select abs(sum(debit-credit)) as total from jurnal_item where account ='".$dataC['id']."' and tanggal between '$drs' and '$smps' order by tanggal asc;"));
          						echo '
          						<tr>
          						<td style="background-color: grey;"><strong>'.$dataC['id'].' - '.$dataC['nama'].'</strong></td>
          						<td style="background-color: grey;"><strong>'.number_format($dataHeader['total']).'</strong></td>
          						</tr>
          						';
          						$totalBal = $dataHeader['total'];
          						$sql1=mysql_query("select * from coa where parent = '".$dataC['id']."'");
          						while($dataCoa=mysql_fetch_array($sql1))
          						{

          							if($dataMaster['type'] == "1")
          							{
          								$qrss= "select abs(sum(debit-credit)) as total from jurnal_item where account ='".$dataCoa['id']."' and tanggal between '$drs' and '$smps' order by tanggal asc;";

          								$dataJM=mysql_fetch_array(mysql_query($qrss));


          								$dataBalance = $dataJM['total'];
          								echo '
          								<tr>
          								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$dataCoa['id'].' - '.$dataCoa['nama'].'</td>
          								<td>'.number_format($dataBalance).'</td>
          								</tr>
          								';

          								$totalBal+=$dataBalance;
          							}
          							else
          							{
          								$qrss= "select abs(sum(debit-credit)) as total from jurnal_item where account ='".$dataCoa['id']."' and tanggal between '$drs' and '$smps' order by tanggal asc;";

          								$dataJM=mysql_fetch_array(mysql_query($qrss));


          								$dataBalance = -$dataJM['total'];
          								echo '
          								<tr>
          								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$dataCoa['id'].' - '.$dataCoa['nama'].'</td>
          								<td>'.number_format(-$dataBalance).'</td>
          								</tr>
          								';

          								$totalBal+=$dataBalance;
          							}
          							
          						}

          						echo '
          						<tr>
          						<td style="background-color: cyan;"><strong> Total '.$dataC['id'].' - '.$dataC['nama'].'</strong></td>
          						<td style="background-color: cyan;"><strong>'.number_format($totalBal).'</strong></td>
          						</tr>
          						';
          						echo '
          						<tr>
          						<td colspan="6"></td>
          						</tr>
          						';
          						$totalSemua += $totalBal;
          					}
          					echo '
          					<tr>
          					<td style="background-color: lightgreen;"><strong> Total Nett Income</strong></td>
          					<td style="background-color: lightgreen;"><strong>'.number_format($totalSemua).'</strong></td>
          					</tr>
          					';
          					?>
          				</tbody>
          			</table>
          		</div>

          	</div>
          	<br>
          	<br>
          	<!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

  </div>


