<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<input type="hidden" name="input" class="form-control" value="1">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">
								<input type="hidden" name="id_kar" class="form-control" value="<?php echo $user_data['email'];?>"> 
								<div class="form-group">
									<label>Jenis Izin</label>
									<select class="form-control select2" name="izin" style="width: 100%;" required="">
										<?php
										$sql1=mysql_query("select * from jenis_izin where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								<div class="form-group">
									<label>Tanggal Dari</label>
									<input type="text" name="dari" id="datepicker" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Tanggal Sampai</label>
									<input type="text" name="sampai" id="datepicker1" class="form-control" required="">
								</div>
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Total Hari</label>
									<select class="form-control select2" name="hari" style="width: 100%;" required="">
										<option value="0.5" >0.5 Hari</option>
										<option value="1" >1 Hari</option>
										<option value="2" >2 Hari</option>
										<option value="3" >3 Hari</option>
										<option value="4" >4 Hari</option>
										<option value="5" >5 Hari</option>
										<option value="6" >6 Hari</option>
										<option value="7" >7 Hari</option>
										<option value="90" >90 Hari</option>
									</select>
								</div>
								<div class="form-group">
									<label>Keterangan Izin</label>
									<input type="text" name="keterangan" class="form-control" required="">
								</div>
								
								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>