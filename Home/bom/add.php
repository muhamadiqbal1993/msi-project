<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="button" onclick="save_modul()" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<input type="hidden" name="debit" id="debit" class="form-control"> 
						<input type="hidden" name="credit" id="credit" class="form-control"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>ID Bom</label>
									<input type="text" name="kode" class="form-control" readonly="" value="<?php echo 'BOM-'.date('ymdHis')?>">
								</div>
								<div class="form-group">
									<label>Product</label>
									<select class="form-control select2" name="product" style="width: 100%;">
										<option value="" >----- Pilih Product Jadi -----</option>
										<?php
										$sql1=mysql_query("select * from product");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['kode'].' - '.$data1['nama']?></option>
											<?php 
										}?>

									</select>
								</div>
								<div class="form-group">
									<label>Qty Produksi</label>
									<input type="text" name="qty" id="qty" class="form-control" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Gudang Raw Material</label>
									<select class="form-control select2" name="g_rawmat" style="width: 100%;">
										<option value="" >----- Pilih Gudang Raw Material -----</option>
										<?php
										$sql1=mysql_query("select * from gudang");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['id'].' - '.$data1['nama']?></option>
											<?php 
										}?>

									</select>
								</div>
								<div class="form-group">
									<label>Gudang Penerimaan Barang Jadi </label>
									<select class="form-control select2" name="g_barang_jadi" style="width: 100%;">
										<option value="" >----- Pilih Gudang Penerimaan Barang Jadi -----</option>
										<?php
										$sql1=mysql_query("select * from gudang");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['id'].' - '.$data1['nama']?></option>
											<?php 
										}?>

									</select>
								</div>
								<div class="form-group">
									<label>Keterangan Tambahan</label>
									<input type="text" name="keterangan" id="keterangan" class="form-control" >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
										<h1><?php echo $modulnya.' Line';?></h1>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 20%">Product</th>
													<th>UOM</th>
													<th>Qty</th>
													<th>Action</th>
												</tr>
											</thead>
											<thead>
												<tr>
													<th colspan="5" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button></th>
													
												</tr>
											</thead>
											<script language="javascript" type="text/javascript">

												function getProduct(abs)
												{
													var products = '<select class="form-control select2" name="r_product[]" id="r_product'+abs+'" onchange="getUom('+abs+')" style="width: 100%;" required=""><option value="" >----- Pilih Product Raw Material -----</option>'+
													<?php

													$sql1=mysql_query("select * from product where status = '1' and rawmat = '2'");
													while($data1=mysql_fetch_array($sql1))
													{
														?>
														'<option value="<?php echo $data1['no'].'#'.$data1['satuan'];?>"><?php echo $data1['kode'].' - '.$data1['nama']?></option>'+

														<?php 
													}?>

													'</select>';

													return products;
												}

												function getUom(abs)
												{
													var idUom = "r_product"+abs;
													var valueUom = "editUom"+abs;
													var dataUom = document.getElementById(idUom).value.split("#");

													document.getElementById(valueUom).value = dataUom[1];
												}

												function delete_row_didik(abs)
												{
													document.getElementById("tabDidik").deleteRow(abs);
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													for(var a=1;a<rowCount;a++)
													{
														table.rows[a].cells[0].innerHTML = a;
														table.rows[a].cells[4].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
													}
													ubahTotal();
												}

												function addLine_didik()
												{ 
												
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													var row = table.insertRow(rowCount);
													var cell0 = row.insertCell(0);
													var cell1 = row.insertCell(1);
													var cell2 = row.insertCell(2);
													var cell3 = row.insertCell(3);
													var cell4 = row.insertCell(4);
											
													cell0.innerHTML = rowCount;
													cell1.innerHTML = getProduct(rowCount);
													cell2.innerHTML = '<input type="text" name="editUom[]" id="editUom'+rowCount+'" class="form-control" readonly>';
													cell3.innerHTML = '<input type="text" name="editQty[]" class="form-control" value="0">';
													cell4.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

													var namaField = "#r_product"+rowCount;
													$(namaField).select2();
												}

												
											</script>
										</table>
									</div>
								</div>
							</div>

						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}

	}
</script>
