<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
            <hr class="abu">
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
              Data Berhasil Dibuat.
              </div>
              </div>';
            }
            if($status == "2")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
              </div>
              </div>';
            }
            if($status == "3")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Warning!</h4>
              '.$modulnya.' sudah ada yang menggunakan , silahkan ganti dengan data yang berbeda
              </div>
              </div>';
            }
            ?>


            <div class="col-xs-12">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No Purchase Order</th>
                    <th>Vendor</th>
                    <th>Tanggal</th>
                    <th>Jatuh Tempo</th>
                    <th>Tanggal Buat</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  $sql1=mysql_query("select I.*,
                    DATE_FORMAT(I.tanggal, '%d %b %Y') as tgls,
                    DATE_FORMAT(I.due, '%d %b %Y') as dues,
                    C.nama as namcus from bill I
                    inner join vendor C on C.no = I.vendor
                    where I.status = '0'");
                  while($data1=mysql_fetch_array($sql1))
                  {  
                   $status = "<strong>Waiting Approval</strong>";

                   $dataApprove = mysql_fetch_array(mysql_query("select count(*)+1 as total from bill_approve where bill = '".$data1['no']."'"));
                   $cekDivisi = mysql_fetch_array(mysql_query("select divisi from bill_approve_setting where urutan = '".$dataApprove['total']."'"));

                   if($cekDivisi['divisi'] == $user_data['groups'])
                   {
                     echo '
                     <tr>
                     <td>'.$no.'</td>  
                     <td>'.$data1['bill'].'</td>
                     <td>'.$data1['namcus'].'</td>
                     <td>'.$data1['tgls'].'</td>
                     <td>'.$data1['dues'].'</td>
                     <td>'.$data1['tanggal_create'].'</td>
                     <td>'.number_format($data1['grandtotal']).'</td>
                     <td>'.$status.'</td>      
                     <td><a href="./?id='.$data1['no'].'">Manage</a></td>
                     </tr>
                     ';
                     $no++;
                   }
                 }
                 ?>
               </tbody>
             </table>
           </div>

         </div>
         <br>
         <br>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </section>

</div>
