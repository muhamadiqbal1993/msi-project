<?php
$dataEdit = mysql_fetch_array(mysql_query("select * from brand where no = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Kode Brand</label>
									<input type="text" name="kode" class="form-control" required="" value="<?php echo $dataEdit['id'];?>" disabled>
								</div>
								<div class="form-group">
                                    <label><strong>Kategory Product</strong></label>
                                    <select class="form-control select2" name="kategory" id="kategory" style="width: 100%;" required="">
                                        <option value="" >--- Pilih Kategory ---</option>
                                        <?php
                                        $sql1=mysql_query("select * from kategory_product where status = '1'");
                                        while($data1=mysql_fetch_array($sql1))
                                        {
                                            ?>
                                            <option value="<?php echo $data1['no']?>" <?php if($dataEdit['kategory'] == $data1['no']){echo "selected";}?>><?php echo $data1['kode'].' - '.$data1['nama']?></option>
                                            <?php 
                                        }
                                        ?>
                                    </select>
                                </div>
								<div class="form-group">
									<label>Nama Brand</label>
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $dataEdit['nama'];?>">
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Keterangan Tambahan</label>
									<input type="text" name="keterangan" class="form-control" required="" value="<?php echo $dataEdit['keterangan'];?>">
								</div>
								<div class="form-group">
										<label>Status</label>
										<select class="form-control select2" name="aktif" style="width: 100%;" required="">
											<option value="1" <?php if($dataEdit['status'] == "1"){echo "selected";}?>>Aktif</option>
											<option value="2" <?php if($dataEdit['status'] == "2"){echo "selected";}?>>Tidak Aktif</option>

										</select>
									</div>
							</div>
						</div>
					</form>
					<br>
					<br>
				</div>
				<?php 
				$nama_modulnya = 'brand';
				include '../headfoot/history.php';
				?>
			</div>
		</div>
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}

	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(200)
				.height(200);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
