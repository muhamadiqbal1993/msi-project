<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					
					<div class="box-body">
						<form id="create_data" method="POST"> 
							<input type="hidden" name="input" class="form-control" value="1">
							<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
							<input type="hidden" name="detail" id="detail" class="form-control" required="">
							<input type="hidden" name="harga" id="harga" class="form-control" >
							<input type="hidden" name="idss" class="form-control" value="<?php echo date('ymdhis')?>"  >
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Partner</strong></label>
									<select class="form-control select2" name="partner" style="width: 100%;" required="">
										<option value="" >--- Pilih Partner ---</option>
										<?php
										$sql1=mysql_query("select 'CST' as type,kode,nama from customer UNION select 'VDR' as type,kode,nama from vendor2 UNION select 'MTR' as type,kode,nama from mitra");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['kode']?>" ><?php echo $data1['type'].' - ['.$data1['kode'].'] '.$data1['nama']?></option>
											<?php 
										}
										?>
									</select>
								</div>
								

							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Pembayaran</strong></label>
									<select class="form-control select2" name="pembayaran" id="pembayaran" onchange="view_total()" style="width: 100%;" >
										<option value="" >--- Pilih Pembayaran ---</option>
										<?php
										$sql1=mysql_query("select * from mutasi_detail where status = '1' and nominal < 0");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no'].'#'.$data1['nominal']?>" ><?php echo $data1['keterangan'].'  | '.number_format($data1['nominal'])?></option>
											<?php 
										}
										?>
									</select>
								</div>
								

							</div>
						</form>

						
						<div class="col-md-12">
							<hr class="abu">
						</div>

						<div class="col-md-12">
							<a href="javascript:void(0)" id="tambahBtn" onclick="addLine()">+ Tambah Item</a>
							<p></p>
							<div class="box-body table-responsive no-padding">
								<table id="so_table" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th style="width: 20%">No Bill</th>
											<th>Partner</th>
											<th>Tanggal</th>
											<th>Nilai Invoice</th>
											<th>Action</th>
										</tr>
									</thead>
								</table>
							</div>
							
							<br>
							<a href="javascript:void(0)" id="tambahBtn1" onclick="addLine()">+ Tambah Item</a>
						</div>
						

						<script language="javascript" type="text/javascript">


							function getProduct(isi)
							{
								var products = '<select class="form-control select2" onchange=getSatuanProd() id="product_edits" style="width: 100%;" required="">'+
								<?php

								$sql1=mysql_query("select *,(nilai - bayar) as sisa from (
									select I.*,C.nama as namcus,concat('BILL/',DATE_FORMAT(I.tanggal, '%y'),'/',LPAD(I.no, 4, '0')) as no_inv,DATE_FORMAT(I.tanggal, '%d %M %Y') as tgls,
									IFNULL((select sum(ABS(MD.nominal)) as total_bayar from pembayaran_detail PD 
									inner join pembayaran P on P.id = PD.id_trx
									inner join mutasi_detail MD on MD.no = P.bayar
									where PD.inv = I.no and P.status = '1' and P.type_partner != 'CST'),0) as bayar from bill I 
									left outer join vendor2 C on C.kode = I.vendor
    								left outer join mitra M on M.kode = I.vendor
									) as tb1 where bayar < nilai");
								while($data1=mysql_fetch_array($sql1))
								{
									?>
									'<option value="<?php echo $data1['no_inv']."#".$data1['namcus']."#".$data1['sisa']."#".$data1['tgls'];?>"><?php echo $data1['no_inv']?></option>'+

									<?php 
								}?>

								'</select>';

								return products;
							}

							function getSatuanProd()
							{
								var txt = document.getElementById("product_edits").value;
								var rowCount = document.getElementById('so_table').rows.length;
								if(txt != "")
								{
									var table = document.getElementById('so_table');
									var ada = "0";
									for(var a=1;a<rowCount;a++)
									{
										var product = document.getElementById("product_edits").value.split("#")[0];
										if(product == table.rows[a].cells[1].innerHTML)
										{
											alert("Invoice Ini Sudah ada Dilist");
											ada = "1";
										}

									}
									if(ada == "0")
									{
										document.getElementById("editPartner").value = txt.split("#")[1];
										document.getElementById("editTanggal").value = txt.split("#")[3];
										document.getElementById("editNilai").value = parseInt(txt.split("#")[2]).toLocaleString();
										save_row(rowCount-1);
									}
									
								}
								
							}

							function view_total()
							{
								var totalnya = 0;
								var detail = "";
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								var balance = document.getElementById("pembayaran").value.split("#")[1];
								var hrgs = 0;
								for(var a=1;a<rowCount;a++)
								{
									var idss = table.rows[a].cells[1].innerHTML;
									var harga = parseInt(table.rows[a].cells[4].innerHTML.split(",").join(""));
									hrgs += harga;
									detail = detail +','+idss;
								}	
								var sisa = parseInt(balance) + parseInt(hrgs); 
								document.getElementById("detail").value = detail;
								document.getElementById("harga").value = hrgs;
								document.getElementById('nilaiBalance').innerHTML = "<strong>Payment Balance : Rp. "+parseInt(sisa).toLocaleString()+"</strong>";
							}

							function delete_row(abs)
							{
								document.getElementById("so_table").deleteRow(abs);
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								for(var a=1;a<rowCount;a++)
								{
									table.rows[a].cells[0].innerHTML = a;
									table.rows[a].cells[5].innerHTML = "<a href='#' onclick='delete_row(\""+a+"\")'> Hapus</a>";
									view_total();
								}
								view_total();
							}
							
							function cancel_row(abs)
							{
								document.getElementById("so_table").deleteRow(abs);
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								for(var a=1;a<rowCount;a++)
								{
									table.rows[a].cells[0].innerHTML = a;
									table.rows[a].cells[5].innerHTML = "<a href='#' onclick='delete_row(\""+a+"\")'> Hapus</a>";
									view_total();
								}
								document.getElementById('tambahBtn').style.display = ""; 
								document.getElementById('tambahBtn1').style.display = ""; 
								view_total();
							}

							function save_row(abs)
							{
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								for(var a=1;a<rowCount;a++)
								{
									if(abs == a)
									{
										var product = document.getElementById("product_edits").value.split("#")[0];
										var partner = document.getElementById("editPartner").value;
										var tanggal = document.getElementById("editTanggal").value;
										var nilai = document.getElementById("editNilai").value;

										table.rows[abs].cells[1].innerHTML = product;
										table.rows[abs].cells[2].innerHTML = partner;
										table.rows[abs].cells[3].innerHTML = tanggal;
										table.rows[abs].cells[4].innerHTML = nilai;
										table.rows[abs].cells[5].innerHTML = "<a href='#' onclick='delete_row(\""+abs+"\")'> Hapus</a>";
									}
									else
									{
										table.rows[a].cells[5].innerHTML = "<a href='#' onclick='delete_row(\""+a+"\")'> Hapus</a>";
									}
								}

								document.getElementById('tambahBtn').style.display = ""; 
								document.getElementById('tambahBtn1').style.display = ""; 
								view_total();
							}

							function addLine()
							{
								document.getElementById('tambahBtn').style.display = "none"; 
								document.getElementById('tambahBtn1').style.display = "none"; 
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');

								var row = table.insertRow(rowCount);
								var cell0 = row.insertCell(0);
								var cell1 = row.insertCell(1);
								var cell2 = row.insertCell(2);
								var cell3 = row.insertCell(3);
								var cell4 = row.insertCell(4);
								var cell5 = row.insertCell(5);


								cell0.innerHTML = rowCount;
								cell1.innerHTML = getProduct("-");
								cell2.innerHTML = '<input type="text" id="editPartner" class="form-control" readonly>';
								cell3.innerHTML = '<input type="text" id="editTanggal" class="form-control" readonly>';
								cell4.innerHTML = '<input type="text" id="editNilai" class="form-control" readonly>';
								cell5.innerHTML = "<a href='#' onclick='cancel_row(\""+rowCount+"\")'> Cancel</a>";
								$('#product_edits').select2();
								$('#satuans').select2();

								for(var a=1;a<rowCount;a++)
								{
									if(a != rowCount)
									{
										table.rows[a].cells[5].innerHTML = "";
									}
								}
								$('#product_edits').val(""); 
								$('#product_edits').change();

							}

						</script>
						<div class="col-md-12">
							<hr class="abu">
						</div>
						
						<div class="col-md-12">
							
							<h3 id="nilaiBalance" style="float:right;"><strong>Payment Balance : Rp. 0</strong></h3>


							<br><br><br><br>
						</div>


					</div>
				</div>

				<br>
				<br>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>
