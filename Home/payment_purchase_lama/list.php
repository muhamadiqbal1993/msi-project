<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
            <hr class="abu">
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            '.$modulnya.' sudah ada yang menggunakan , silahkan ganti dengan data yang berbeda
          </div>
        </div>';
      }
      ?>


      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Tanggal</th>
              <th>Partner</th>
              <th>Nilai</th>
              <th>User Create</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $no = 1;
            $sql1=mysql_query("select P.*,U.nama as namus,MD.nominal,C.nama as namven,M.nama as nammit,DATE_FORMAT(P.tanggal, '%d %M %Y') as tgls from pembayaran P
		inner join mutasi_detail MD on MD.no = P.bayar
		left outer join vendor2 C on C.kode = P.partner
		left outer join mitra M on M.kode = P.partner
		inner join user U on U.no = P.user 
		where P.status = '1' and P.type = 'BILL'");
            while($data1=mysql_fetch_array($sql1))
            {  
              $status = "Success";
              if($data1['status'] == "2")
              {
                $status = "Cancel";
              } 

              echo '
              <tr>
                <td>'.$no.'</td>  
                <td>'.'PYNT/'.date('y').'/'.str_pad($data1['no'], 4, '0', STR_PAD_LEFT).'</td>
                <td>'.$data1['tgls'].'</td>
                <td>['.$data1['partner'].'] '.$data1['namven'].'</td>
                <td>'.number_format($data1['nominal']).'</td>
		<td>'.$data1['namus'].'</td>
                <td>'.$status.'</td>      
                <td><a href="./?id='.$data1['no'].'">Manage</a></td>
              </tr>
              ';
              $no++;
            }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>
