<?php

$dataEdit = mysql_fetch_array(mysql_query("select * from setting_web limit 1"));
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Edit <?php echo $modulnya;?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Edit Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
           
            <?php if($editPM == "1"){?>
            <button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
            <?php }?>
            <hr class="abu">
          </div>
          <form id="create_data" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="input" class="form-control" value="2">
            <input type="hidden" name="no" class="form-control" required="" value="<?php echo $dataEdit['no'];?>">
            <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
            <div class="box-body">
              <div class="col-md-6">

                <div class="form-group">
                  <label>Title Web</label>
                  <input type="text" name="title" class="form-control" required="" value="<?php echo $dataEdit['title'];?>">
                </div>
                <div class="form-group">
                  <label>Nama Web</label>
                  <input type="text" name="nama" class="form-control" required="" value="<?php echo $dataEdit['nama'];?>">
                </div>
                <div class="form-group">
                  <label>Inisial Web</label>
                  <input type="text" name="inisial" class="form-control" required="" value="<?php echo $dataEdit['inisial'];?>">
                </div>
                <div class="form-group">
                  <label>Icon</label>
                  
                  <p></p>
                  <img id="photo1" src="../images/<?php echo $dataEdit['logo'];?>" style="height: 200px;width: 200px"/>
                  <p></p>
                  <div class="btn btn-default btn-file" id="photos">
                    <i class="fa fa-image"></i> Pilih Photo
                    <input type="file" name="photos" onchange="viewImage(this);">
                  </div>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>No NPWP</label>
                  <input type="text" name="no_npwp" class="form-control" required="" value="<?php echo $dataEdit['no_npwp'];?>">
                </div>
                <div class="form-group">
                  <label>Nama NPWP</label>
                  <input type="text" name="nama_npwp" class="form-control" required="" value="<?php echo $dataEdit['nama_npwp'];?>">
                </div>
                <div class="form-group">
                  <label>Alamat NPWP</label>
                  <input type="text" name="alamat_npwp" class="form-control" required="" value="<?php echo $dataEdit['alamat_npwp'];?>">
                </div>
              </div>
            </div>
          </form>
          <br>
          <br>
        </div>
        
      </div>
    </div>
  </section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }

  function viewImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#photo1')
        .attr('src', e.target.result)
        .width(200)
        .height(200);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }
</script>
