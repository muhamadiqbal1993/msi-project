<?php
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_DEPRECATED);

include '../function/connect_ajax.php';

$db = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$table = <<<EOT
(select K.*,P.kabupaten as namprov,CONCAT('<a href="./?id=',K.no,'">Manage</a>') as manage,
(CASE
WHEN K.status = "1" THEN "Aktif" ELSE "Tidak Aktif"
END ) as status_new
 from kecamatan K
inner join kabupaten P on P.no = K.kabupaten ) viewData
EOT;
$primaryKey = 'no';

$columns = array(
	array( 'db' => 'no',  'dt' => 0 ),
	array( 'db' => 'namprov', 'dt' => 1 ),
	array( 'db' => 'kecamatan', 'dt' => 2 ),
	array( 'db' => 'status_new',  'dt' => 3 ),
	array( 'db' => 'manage',  'dt' => 4 ),
	);
// SQL server connection information
$sql_details = array(
	'user' => $dbuser,
	'pass' => $dbpass,
	'db'   => $dbname,
	'host' => $dbhost
	);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );
// require '../../config/ssp.class.php';
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	);

	?>


