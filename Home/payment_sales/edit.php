<?php
$id = $_GET['id'];
$dataTrx = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(tanggal, '%m/%d/%Y') as tgls,DATE_FORMAT(tanggal, '%d %M %Y') as tgl_new from pembayaran where no = '$id'"));
$dataMutasi = mysql_fetch_array(mysql_query("select * from mutasi_detail where no = '".$dataTrx['bayar']."'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<form id="create_data" method="POST"> 
			<input type="hidden" name="input" class="form-control" value="2">
			<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
			<input type="hidden" name="detail" id="detail" class="form-control" required="">
			<input type="hidden" name="harga" id="harga" class="form-control" value="<?php echo $dataTrx['total'];?>">
			<input type="hidden" name="idss" class="form-control" value="<?php echo $dataTrx['inv'];?>"  >
		</form>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($dataTrx['status'] == '1'){?>
						<a href="F_deleteData.php?id=<?php echo $id?>"><input type="submit" class="btn btn-danger" value="Cancel Payment" style="float: right;"/></a>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<form id="create_data" method="POST"> 
							<input type="hidden" name="input" class="form-control" value="1">
							<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
							<input type="hidden" name="detail" id="detail" class="form-control" required="">
							<input type="hidden" name="harga" id="harga" class="form-control" >
							<input type="hidden" name="idss" class="form-control" value="<?php echo date('ymdhis')?>"  >
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Tanggal</strong></label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" readonly value="<?php echo $dataTrx['tgls']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Keterangan</strong></label>
									<input type="text" name="keterangan"  class="form-control" readonly value="<?php echo $dataTrx['keterangan']?>">
								</div>
								<div class="form-group">

									<label>Kasbank</label>
									<select class="form-control select2" name="akun" id="akun" style="width: 100%;" required="">
										<option value="">--- Pilih Kasbank ---</option>

										<?php
										$sql1=mysql_query("select * from kasbank where status = '1' ");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $dataEdit['coa']){echo "selected";}?>><?php echo $data1['no_acc'].' - '.$data1['nama_acc']?></option>
											<?php 
										}
										?>
									</select>
								</div>
							</div>
						</form>

						
						<div class="col-md-12">
							<hr class="abu">
						</div>

						<div class="col-md-12">
							<a href="javascript:void(0)" id="tambahBtn" onclick="addLine()">+ Tambah Item</a>
							<p></p>
							<div class="box-body table-responsive no-padding">
								<table id="so_table" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th style="width: 20%">Account</th>
											<th style="width: 20%">Sales order</th>
											<th>Partner</th>
											<th>Tanggal</th>
											<th>Nilai Invoice</th>
											<th>Sisa Bayar</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$total_nya = "";
										$detailss = "";
										$nom = 1;
										$sql2=mysql_query("select * from pembayaran_detail where id_trx = '".$dataTrx['id']."'");
										while($data2=mysql_fetch_array($sql2))
										{
											$dataLine = mysql_fetch_array(mysql_query("select I.*,C.nama as namcus,DATE_FORMAT(I.tanggal, '%d %M %Y') as tgls from invoice I
												inner join customer C on C.no = I.customer
												where I.inv = '".$data2['inv']."'"));

											if($data2['inv'] == "Adjust Selisih Pembayaran")
											{
												$nams = "Adjustment Selisih Pembayaran ".$dataTrx['no_payment'];
												$dataAcc = mysql_fetch_array(mysql_query("select C.id,C.nama from jurnal_item JI
													inner join coa C on C.id = JI.account
													where JI.ref = '$nams'"));
												?>
												<tr>
													<td style="background-color: peachpuff"><?php echo $nom;?></td>
													<td style="background-color: peachpuff"><?php echo $dataAcc['id'].' - '.$dataAcc['nama']?></td>
													<td style="background-color: peachpuff"><?php echo $data2['inv']?></td>
													<td style="background-color: peachpuff">Jurnal Manual</td>
													<td style="background-color: peachpuff"><?php echo $dataTrx['tgl_new'];?></td>
													<td style="background-color: peachpuff"><?php echo number_format($data2['nilai']);?></td>
													<td style="background-color: peachpuff">0</td>
													<td style="background-color: peachpuff">
														<a href="javascript:void(0)" onclick="delete_row(<?php echo $nom;?>)"> Hapus</a>
													</td>
												</tr>
												<?php 
											}
											else
											{
												$nams = "Pembayaran INV ".$data2['inv'];
												$dataAcc = mysql_fetch_array(mysql_query("select C.id,C.nama from jurnal_item JI
													inner join coa C on C.id = JI.account
													where JI.ref = '$nams'"));
												?>
												<tr>
													<td><?php echo $nom;?></td>
													<td><?php echo $dataAcc['id'].' - '.$dataAcc['nama']?></td>
													<td><?php echo $data2['inv']?></td>
													<td><?php echo $dataLine['namcus'];?></td>
													<td><?php echo $dataLine['tgls'];?></td>
													<td><?php echo number_format($dataLine['grandtotal']);?></td>
													<td><?php echo number_format($dataLine['grandtotal']-$data2['nilai']);?></td>
													<td>
														<a href="javascript:void(0)" onclick="delete_row(<?php echo $nom;?>)"> Hapus</a>
													</td>
												</tr>
												<?php 
											}
											$nom++;
										}
										?>
									</tbody>
								</table>
							</div>

							<br>
							<a href="javascript:void(0)" id="tambahBtn1" onclick="addLine()">+ Tambah Item</a>
						</div>


						<script language="javascript" type="text/javascript">


							function getProduct(isi)
							{
								var products = '<select class="form-control select2" onchange=getSatuanProd() id="product_edits" style="width: 100%;" required="">'+
								<?php

								$sql1=mysql_query("select I.*,DATE_FORMAT(I.tanggal, '%d %M %Y') as tgls,C.nama as namcus from invoice I
									inner join customer C on C.no = I.customer
									where I.status = '1'");
								while($data1=mysql_fetch_array($sql1))
								{
									?>
									'<option value="<?php echo $data1['inv']."#".$data1['namcus']."#".$data1['grandtotal']."#".$data1['tgls'];?>"><?php echo $data1['inv']?></option>'+

									<?php 
								}?>

								'</select>';

								return products;
							}

							function getSatuanProd()
							{
								var txt = document.getElementById("product_edits").value;
								var rowCount = document.getElementById('so_table').rows.length;
								if(txt != "")
								{
									var table = document.getElementById('so_table');
									var ada = "0";
									for(var a=1;a<rowCount;a++)
									{
										var product = document.getElementById("product_edits").value.split("#")[0];
										if(product == table.rows[a].cells[1].innerHTML)
										{
											alert("Invoice Ini Sudah ada Dilist");
											document.getElementById("so_table").deleteRow(rowCount-1);
											document.getElementById('tambahBtn').style.display = ""; 
											document.getElementById('tambahBtn1').style.display = ""; 
											ada = "1";
										}
									}
									if(ada == "0")
									{
										document.getElementById("editPartner").value = txt.split("#")[1];
										document.getElementById("editTanggal").value = txt.split("#")[3];
										document.getElementById("editNilai").value = parseInt(txt.split("#")[2]).toLocaleString();
										save_row(rowCount-1);
									}
								}
							}

							function view_total()
							{
								var totalnya = 0;
								var detail = "";
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								var balance = document.getElementById("pembayaran").value.split("#")[1];
								var hrgs = 0;
								for(var a=1;a<rowCount;a++)
								{
									var idss = table.rows[a].cells[1].innerHTML;
									var harga = parseInt(table.rows[a].cells[4].innerHTML.split(",").join(""));
									var sisa = parseInt(table.rows[a].cells[5].innerHTML.split(",").join(""));
									hrgs += harga;
									detail = detail +','+idss+'###'+harga+'###'+sisa;
								} 
								var sisa = parseInt(balance) - parseInt(hrgs); 
								document.getElementById("detail").value = detail;
								document.getElementById("harga").value = hrgs;
								if(parseInt(sisa) < 0)
								{
									document.getElementById('nilaiBalance').innerHTML = "<strong>Payment Balance : Rp. 0</strong>";
								}
								else
								{
									document.getElementById('nilaiBalance').innerHTML = "<strong>Payment Balance : Rp. "+parseInt(sisa).toLocaleString()+"</strong>";
								}
							}

							function delete_row(abs)
							{
								document.getElementById("so_table").deleteRow(abs);
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								for(var a=1;a<rowCount;a++)
								{
									table.rows[a].cells[0].innerHTML = a;
									table.rows[a].cells[6].innerHTML = "<a href='javascript:void(0)' onclick='delete_row(\""+a+"\")'> Hapus</a>";
									view_total();
								}
								view_total();
							}

							function cancel_row(abs)
							{
								document.getElementById("so_table").deleteRow(abs);
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								for(var a=1;a<rowCount;a++)
								{
									table.rows[a].cells[0].innerHTML = a;
									table.rows[a].cells[6].innerHTML = "<a href='javascript:void(0)' onclick='delete_row(\""+a+"\")'> Hapus</a>";
									view_total();
								}
								document.getElementById('tambahBtn').style.display = ""; 
								document.getElementById('tambahBtn1').style.display = ""; 
								view_total();
							}

							function save_row(abs)
							{
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								for(var a=1;a<rowCount;a++)
								{
									if(abs == a)
									{
										var product = document.getElementById("product_edits").value.split("#")[0];
										var partner = document.getElementById("editPartner").value;
										var tanggal = document.getElementById("editTanggal").value;
										var nilai = document.getElementById("editNilai").value;
										var nilaiBalance = document.getElementById("nilaiBalance").innerHTML.split("Payment Balance : Rp. ")[1].split(",").join("");
										if(parseInt(nilaiBalance) > parseInt(nilai.split(",").join("")))
										{
											var nilaiHasil = 0;
										}
										else
										{
											var nilaiHasil =  parseInt(nilai.split(",").join("")) - parseInt(nilaiBalance);
										}


										table.rows[abs].cells[1].innerHTML = product;
										table.rows[abs].cells[2].innerHTML = partner;
										table.rows[abs].cells[3].innerHTML = tanggal;
										table.rows[abs].cells[4].innerHTML = nilai;
										table.rows[abs].cells[5].innerHTML = nilaiHasil.toLocaleString('en-US');;
										table.rows[abs].cells[6].innerHTML = "<a href='javascript:void(0)' onclick='delete_row(\""+abs+"\")'> Hapus</a>";
									}
									else
									{
										table.rows[a].cells[6].innerHTML = "<a href='javascript:void(0)' onclick='delete_row(\""+a+"\")'> Hapus</a>";
									}
								}

								document.getElementById('tambahBtn').style.display = ""; 
								document.getElementById('tambahBtn1').style.display = ""; 
								view_total();
							}

							function addLine()
							{
								if(document.getElementById('pembayaran').value == "")
								{
									alert('Pilih Pembayaran Dahulu')
								} 
								else
								{
									document.getElementById('tambahBtn').style.display = "none"; 
									document.getElementById('tambahBtn1').style.display = "none"; 
									var rowCount = document.getElementById('so_table').rows.length;
									var table = document.getElementById('so_table');

									var row = table.insertRow(rowCount);
									var cell0 = row.insertCell(0);
									var cell1 = row.insertCell(1);
									var cell2 = row.insertCell(2);
									var cell3 = row.insertCell(3);
									var cell4 = row.insertCell(4);
									var cell5 = row.insertCell(5);
									var cell6 = row.insertCell(6);

									cell0.innerHTML = rowCount;
									cell1.innerHTML = getProduct("-");
									cell2.innerHTML = '<input type="text" id="editPartner" class="form-control" readonly>';
									cell3.innerHTML = '<input type="text" id="editTanggal" class="form-control" readonly>';
									cell4.innerHTML = '<input type="text" id="editNilai" class="form-control" readonly>';
									cell5.innerHTML = '<input type="text" id="editSisa" class="form-control" readonly>';
									cell6.innerHTML = "<a href='javascript:void(0)' onclick='cancel_row(\""+rowCount+"\")'> Cancel</a>";
									$('#product_edits').select2();
									$('#satuans').select2();

									for(var a=1;a<rowCount;a++)
									{
										if(a != rowCount)
										{
											table.rows[a].cells[6].innerHTML = "";
										}
									}
									$('#product_edits').val(""); 
									$('#product_edits').change();
								}
							}

						</script>
						<div class="col-md-12">
							<hr class="abu">
						</div>
						<div class="col-md-12">
							<h3 id="nilaiBalance" style="float:right;"><strong>Payment Balance : Rp. 0</strong></h3>
							<br><br><br><br>
						</div>
					</div>
				</div>
				<?php 
				$nama_modulnya = 'payment';
				include '../headfoot/history.php';
				?>

				<br>
				<br>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>

		<!-- /.row -->
	</section>
</div>

<script>
	view_detail();
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>
