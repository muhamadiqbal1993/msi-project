<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <?php if($addPM == "1"){?>
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu">
            <?php }?>
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            Username sudah ada yang menggunakan , silahkan daftarkan dengan username lain
          </div>
        </div>';
      }
      ?>


      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
<th>Penambah / Pengurang</th>
              <th>Kode</th>
              <th>Nama Komponen</th>
  
              
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $sql1=mysql_query("select * from komponen
              ");
            while($data1=mysql_fetch_array($sql1))
            {  
              $status = "Aktif";
              $absen = "Iya";
              $telat = "Iya";
              $dapat = "Penambah";
              if($data1['status'] == "2")
              {
                $status = "Tidak Aktif";
              }
              if($data1['absen'] == "2")
              {
                $absen = "Tidak";
              }
              if($data1['telat'] == "2")
              {
                $telat = "Tidak";
              }

              if($data1['dapat'] == "2")
              {
                $dapat = "Pengurang";
              }
      

              echo '
              <tr>
<td>'.$dapat.'</td>
                <td>'.$data1['kode'].'</td>
                <td>'.$data1['nama'].'</td>
           
                
                <td>'.$status.'</td>      
                <td><a href="./?id='.$data1['no'].'">View</a></td>
              </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>