<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
            <hr class="abu">
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            '.$modulnya.' sudah ada yang menggunakan , silahkan ganti dengan data yang berbeda
          </div>
        </div>';
      }
      ?>


      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>ID Transaksi</th>
              <th>Tanggal</th>
              <th>Keterangan</th>
              <th>Asal Transaksi</th>
              <th>Type Transaksi</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $noms = 1;
            $sql1=mysql_query("select *,DATE_FORMAT(tanggal, '%d %M %Y') as new_tanggal from history_stock group by kode order by tanggal desc");
            while($data1=mysql_fetch_array($sql1))
            {
    		$status = "<span style='color:blue'><strong>Pending</strong></span>";
		if($data1['status'] == "2")
		{
			$status = "<span style='color:green'><strong>Validate</strong></span>";
		}
		if($data1['status'] == "3")
		{
			$status = "<span style='color:red'><strong>Cancel</strong></span>";
		}
		
              echo '
              <tr>
                <td>'.$noms.'</td>
                <td>'.$data1['kode'].'</td>
                <td>'.$data1['new_tanggal'].'</td>
                <td>'.$data1['keterangan'].'</td>
                <td>'.$data1['no_transaksi'].'</td>
                <td>'.$data1['type_transaksi'].'</td>
                <td>'.$status.'</td>    
                <td><a href="./?id='.$data1['kode'].'">Manage</a></td>
              </tr>
              ';
              $noms++;
            }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>
