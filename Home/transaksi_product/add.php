<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="button" onclick="save_modul()" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<input type="hidden" name="debit" id="debit" class="form-control"> 
						<input type="hidden" name="credit" id="credit" class="form-control"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Kode</label>
									<input type="text" name="kode" class="form-control" readonly="" value="<?php echo 'DLV-'.date('ymdHis')?>">
								</div>
								<div class="form-group">
									<label>Tanggal</label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" >
								</div>
								<div class="form-group">
									<label>Tanggal Kirim</label>
									<input type="text" name="tanggal_kirim" id="datepicker" class="form-control" >
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" name="keterangan" id="keterangan" class="form-control" >
								</div>
								<div class="form-group">
                                    <label><strong>Gudang Asal</strong></label>
                                    <select class="form-control select2" name="asal" id="asal" style="width: 100%;" required="">
                                        <option value="" >--- Pilih Gudang Asal ---</option>
                                        <?php
                                        $sql1=mysql_query("select * from gudang where status = '1'");
                                        while($data1=mysql_fetch_array($sql1))
                                        {
                                            ?>
                                            <option value="<?php echo $data1['no']?>" ><?php echo $data1['id'].' - '.$data1['nama']?></option>
                                            <?php 
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><strong>Gudang Tujuan</strong></label>
                                    <select class="form-control select2" name="tujuan" id="tujuan" style="width: 100%;" required="">
                                        <option value="" >--- Pilih Gudang Tujuan ---</option>
                                        <?php
                                        $sql1=mysql_query("select * from gudang where status = '1'");
                                        while($data1=mysql_fetch_array($sql1))
                                        {
                                            ?>
                                            <option value="<?php echo $data1['no']?>" ><?php echo $data1['id'].' - '.$data1['nama']?></option>
                                            <?php 
                                        }
                                        ?>
                                    </select>
                                </div>
								
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
										<h1><?php echo $modulnya.' Line';?></h1>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 40%">Product</th>
													<th>Qty</th>

													<th>Action</th>
												</tr>
											</thead>
											<thead>
												<tr>
													<th colspan="4" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button></th>
													
												</tr>
											</thead>
											<script language="javascript" type="text/javascript">

												function getProduct(abs)
												{
													var products = '<select class="form-control select2" name="coa[]" id="coa'+abs+'" style="width: 100%;" required="">'+
													<?php

													$sql1=mysql_query("select * from product where status = '1'");
													while($data1=mysql_fetch_array($sql1))
													{
														?>
														'<option value="<?php echo $data1['no'];?>"><?php echo $data1['kode'].' - '.$data1['nama']?></option>'+

														<?php 
													}?>

													'</select>';

													return products;
												}

												function delete_row_didik(abs)
												{
													document.getElementById("tabDidik").deleteRow(abs);
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													for(var a=1;a<rowCount;a++)
													{
														table.rows[a].cells[0].innerHTML = a;
														table.rows[a].cells[3].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
													}
													ubahTotal();
												}

												function addLine_didik()
												{ 
													var keterangan = document.getElementById('keterangan').value;
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													var row = table.insertRow(rowCount);
													var cell0 = row.insertCell(0);
													var cell1 = row.insertCell(1);
													var cell2 = row.insertCell(2);
													var cell3 = row.insertCell(3);
													
													cell0.innerHTML = rowCount;
													cell1.innerHTML = getProduct(rowCount);
													cell2.innerHTML = '<input type="qty" name="editQty[]" class="form-control" value="0">';
													
													cell3.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

													var namaField = "#coa"+rowCount;
													$(namaField).select2();
												}

												function ubahDebitDesimal(ids)
												{
													var nams = "editDebit"+ids;
													var val = document.getElementById(nams).value.split(",").join("");
													document.getElementById(nams).value = parseInt(val).toLocaleString('en-US');
													ubahTotal();
												}


												function ubahCreditDesimal(ids)
												{
													var nams = "editCredit"+ids;
													var val = document.getElementById(nams).value.split(",").join("");
													document.getElementById(nams).value = parseInt(val).toLocaleString('en-US');
													ubahTotal();
												}

												function ubahTotal()
												{
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													var debit = 0;
													var credit = 0;
													for(var a=1;a<rowCount;a++)
													{
														var editDebit = 'editDebit'+a;
														var editCredit = 'editCredit'+a;
														var debits = document.getElementById(editDebit).value.split(",").join("");
														var credits = document.getElementById(editCredit).value.split(",").join("");
														debit = parseInt(debit) + parseInt(debits);
														credit = parseInt(credit) + parseInt(credits);
													}

													document.getElementById("debit").value = debit;
													document.getElementById("credit").value = credit;
													var txtMinus = parseInt(debit) - parseInt(credit);
													document.getElementById('txtDebit').innerHTML = "<strong>Rp. "+debit.toLocaleString('en-US')+"</strong>";
													document.getElementById('txtCredit').innerHTML = "<strong>Rp. "+credit.toLocaleString('en-US')+"</strong>";
													document.getElementById('txtMinus').innerHTML = "<strong>Rp. "+txtMinus.toLocaleString('en-US')+"</strong>";
												}
											</script>
										</table>
									</div>
								</div>
							</div>

						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}

	}
</script>	
