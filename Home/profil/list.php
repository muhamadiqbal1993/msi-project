<?php

$nama = $user_data['nama'];
$email = $user_data['email'];
$phone = $user_data['phone'];
$username = $user_data['user'];
$password = $user_data['password'];
$photo = $user_data['photo'];

$folder = "../images/";

if(!empty($_POST['profil']))
{
  $status = "1";
  $noms = $user_data['no'];
  $nama = $_POST['nama'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];
  $username = $_POST['username'];
  $password = $_POST['password'];

  if(empty($_FILES['photos']['tmp_name']))
  {
    $querys = "update user set 
    nama='$nama',
    email='$email',
    phone='$phone',
    user='$username',
    password='$password'
    where no='$noms'";
    
  }
  else
  {
    $file = $_FILES['photos']['tmp_name']; 
    $imagename_file = $_FILES['photos']['name'];
    $newfolder_file = $folder . basename($imagename_file);
    move_uploaded_file($file, $newfolder_file);
    $photo = $imagename_file;

    $querys = "update user set 
    nama='$nama',
    email='$email',
    phone='$phone',
    user='$username',
    photo='$imagename_file',
    password='$password'
    where no='$noms'";
  }
  
  mysql_query($querys);
  
  
}

?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
            <hr class="abu">
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Profil Berhasil Dirubah.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            Kode sudah ada yang menggunakan , silahkan ganti dengan data yang berbeda
          </div>
        </div>';
      }
      ?>

      <form id="create_data" enctype="multipart/form-data" method="POST">   
        <div class="col-xs-2">
          <p></p>
          <p></p>
          <img src="../images/<?php echo $photo;?>" class="img" alt="Photo Profil" style="width:200px;height:250px;">
          <p></p>
          <input type="file" name="photos" id="exampleInputFile">
        </div>

        <div class="col-md-10">

          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" value="<?php echo $nama;?>">
            <input type="hidden" name="profil" class="form-control" value="1">
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" name="phone" class="form-control" value="<?php echo $phone;?>">
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $username;?>">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" class="form-control" value="<?php echo $password;?>">
          </div>

        </div>
      </form>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>
<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
</script>