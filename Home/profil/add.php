<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">

								<div class="form-group">
									<label>Nama Vendor</label>
									<input type="hidden" name="input" class="form-control" value="1">
									<input type="text" name="nama" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Kode</label>
									<input type="text" name="kode" class="form-control" >
								</div>
								<div class="form-group">
									<label>PIC</label>
									<input type="text" name="pic" class="form-control" >
								</div>
								<div class="form-group">
									<label>Phone</label>
									<input type="text" name="phone" class="form-control" >
								</div>


							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Email</label>
									<input type="email" name="email" class="form-control" >
								</div>
								<div class="form-group">
									<label>Alamat</label>
									<input type="text" name="alamat" class="form-control" >
								</div>
								<div class="form-group">
									<label>Alamat2</label>
									<input type="text" name="alamat2" class="form-control" >
								</div>

							</div>

							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>