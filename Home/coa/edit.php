<?php

$parent = "";
$nama = "";
$namkep = "";
$alias = "";
$ids = "";
$status = "";

$sql1=mysql_query("select * from coa where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$data_parent = mysql_fetch_array(mysql_query("select *,concat(id,' - ',nama) as nampar from coa where id = '".$data1['parent']."'"));
	$parent = $data1['parent'];
	$nama = $data1['nama'];
	$namkep = $data_parent['nampar'];
	$alias =  $data1['acc_type'];
	$ids = $data1['id'];
	$status = $data1['status'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<input type="hidden" name="input" class="form-control" value="2">
							<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
							<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
							<div class="col-md-6">
								<div class="form-group">
									<label>Id</label>
									<input type="number" name="ids" class="form-control" required="" value="<?php echo $ids;?>" readonly>
								</div>	
								<div class="form-group">
									<label>Parent</label>
									<select class="form-control select2" name="parent" style="width: 100%;">
										<option value="" ></option>
										<?php
										$sql1=mysql_query("select * from coa");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['id']?>" <?php if($parent == $data1['id']){echo "selected";}?>><?php echo $data1['id'].' - '.$data1['nama']?></option>
											<?php 
										}?>

									</select>
								</div>
								<div class="form-group">
									<label>Alias COA</label>
									<input type="text" name="alias" class="form-control" value="<?php echo $alias;?>">
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $nama;?>">
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control select2" name="aktif" style="width: 100%;" required="">
										<option value="1" <?php if($status == "1"){echo "selected";}?>>Aktif</option>
										<option value="2" <?php if($status == "2"){echo "selected";}?>>Tidak Aktif</option>

									</select>
								</div>
								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<?php 
				$nama_modulnya = 'coa';
				include '../headfoot/history.php';
				?>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>