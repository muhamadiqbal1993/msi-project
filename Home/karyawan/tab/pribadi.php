<div class="row">
	<div class="col-md-12">
		<h1>Data Pribadi</h1>
		<hr class="abu">
	</div>
	<!-- <input type="hidden" name="s_pic" class="form-control" value="-">
	<input type="hidden" name="c_pic" class="form-control" value="-"> -->
	<div class="col-md-6">
		<div class="form-group">
			<label>No KTP</label>
			<input type="text" name="ktp" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Kelamin</label>
			<select class="form-control select2" name="kelamin" style="width: 100%;" required="">
				<option value="laki" >Laki - Laki</option>
				<option value="perempuan" >Perempuan</option>
			</select>
		</div>
		<div class="form-group">
			<label>Tempat Lahir</label>
			<input type="text" name="tempat_lahir" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Tanggal Lahir</label>
			<input type="text" name="tgl_lahir" id="datepicker" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Agama</label>
			<select class="form-control select2" name="agama" style="width: 100%;" required="">
				<option value="budha" >Budha</option>
				<option value="hindu" >Hindu</option>
				<option value="islam" >Islam</option>
				<option value="katholik" >Katholik</option>
				<option value="protestan" >Protestan</option>
			</select>
		</div>
		
		
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Status Pernikahan</label>
			<select class="form-control select2" name="nikah" style="width: 100%;" required="">
				<option value="lajang" >Lajang</option>
				<option value="menikah" >Menikah</option>
				<option value="bercerai" >Bercerai</option>
				<option value="pisah" >Pasangan Meninggal</option>
			</select>
		</div>
		<div class="form-group">
			<label>Phone 1</label>
			<input type="text" name="phone1" class="form-control">
		</div>
		<div class="form-group">
			<label>Phone 2</label>
			<input type="text" name="phone2" class="form-control">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control">
		</div>
		<div class="form-group">
			<label>Alamat</label>
			<input type="text" name="alamat" class="form-control">
		</div>
	</div>
</div>
