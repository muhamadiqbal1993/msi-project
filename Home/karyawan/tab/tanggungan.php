<div class="row">
	<div class="col-md-12">
		<h1>Tanggungan</h1>
		<hr class="abu">
	</div>
	<!-- <input type="hidden" name="s_pic" class="form-control" value="-">
	<input type="hidden" name="c_pic" class="form-control" value="-"> -->
	<div class="col-md-12">
		<input type="hidden" name="detTanggungan" id="detTanggungan" class="form-control">
		<a href="#" id="tambahBtnT" onclick="t_addLine()">+ Tambah Item</a>
		<p></p>
		<table id="tabTanggungan" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="width: 5%">No</th>
					<th>Nama</th>
					<th>NIK</th>
					<th>Hubungan</th>
					<th>Action</th>
				</tr>
			</thead>
			<script language="javascript" type="text/javascript">

				function t_view_total()
				{
					var detDimensi = "";
					var totals = 0;
					var rowCount = document.getElementById('tabTanggungan').rows.length;
					var table = document.getElementById('tabTanggungan');
					for(var a=1;a<rowCount;a++)
					{
						var nama = table.rows[a].cells[1].innerHTML;
						var phone = table.rows[a].cells[2].innerHTML;
						var hubungan = table.rows[a].cells[3].innerHTML;
						detDimensi = detDimensi+","+nama+"spasinya"+phone+"spasinya"+hubungan;
					}
					document.getElementById("detTanggungan").value = detDimensi;
				}

				function t_delete_row(abs)
				{
					document.getElementById("tabTanggungan").deleteRow(abs);
					var rowCount = document.getElementById('tabTanggungan').rows.length;
					var table = document.getElementById('tabTanggungan');
					for(var a=1;a<rowCount;a++)
					{
						table.rows[a].cells[0].innerHTML = a;
						table.rows[a].cells[4].innerHTML = "<a href='#' onclick='edit_row(\""+a+"\")'> Edit </a> |    <a href='#' onclick='t_delete_row(\""+a+"\")'> Hapus</a>";
						t_view_total();
					}
					t_view_total();
				}

				function t_edit_row(abs)
				{
					document.getElementById('tambahBtnT').style.display = "none"; 
					document.getElementById('tambahBtnT1').style.display = "none"; 
					var rowCount = document.getElementById('tabTanggungan').rows.length;
					var table = document.getElementById('tabTanggungan');
					for(var a=1;a<rowCount;a++)
					{
						if(abs == a)
						{
							var nama = table.rows[a].cells[1].innerHTML;
							var phone = table.rows[a].cells[2].innerHTML;
							var hubungan = table.rows[a].cells[3].innerHTML;

							table.rows[a].cells[1].innerHTML = '<input type="text" id="t_nama" class="form-control" value="'+nama+'">';
							table.rows[a].cells[2].innerHTML = '<input type="text" id="t_nik" class="form-control" value="'+phone+'">';
							table.rows[a].cells[3].innerHTML = '<input type="text" id="t_hubungan" class="form-control" value="'+hubungan+'">';
							table.rows[a].cells[4].innerHTML = "<button type='submit' onclick='t_save_row(\""+a+"\")' class='btn btn-primary'>Simpan</button>";
						}
						else
						{
							table.rows[a].cells[4].innerHTML = "";
						}
					}
				}

				function t_save_row(abs)
				{
					var rowCount = document.getElementById('tabTanggungan').rows.length;
					var table = document.getElementById('tabTanggungan');
					for(var a=1;a<rowCount;a++)
					{
						if(abs == a)
						{
							var nama = document.getElementById("t_nama").value;
							var phone = document.getElementById("t_nik").value;
							var hubungan = document.getElementById("t_hubungan").value;

							table.rows[abs].cells[1].innerHTML = nama;
							table.rows[abs].cells[2].innerHTML = phone;
							table.rows[abs].cells[3].innerHTML = hubungan;
							table.rows[abs].cells[4].innerHTML = "<a href='#' onclick='t_edit_row(\""+abs+"\")'> Edit </a> |    <a href='#' onclick='t_delete_row(\""+abs+"\")'> Hapus</a>";
						}
						else
						{
							table.rows[a].cells[4].innerHTML = "<a href='#' onclick='t_edit_row(\""+a+"\")'> Edit |    <a href='#' onclick='t_delete_row(\""+a+"\")'> Hapus</a></a>";
						}
					}
					document.getElementById('tambahBtnT').style.display = ""; 
					document.getElementById('tambahBtnT1').style.display = ""; 
					t_view_total();
				}

				function t_addLine()
				{

					document.getElementById('tambahBtnT').style.display = "none"; 
					document.getElementById('tambahBtnT1').style.display = "none"; 
					var rowCount = document.getElementById('tabTanggungan').rows.length;
					var table = document.getElementById('tabTanggungan');
					var row = table.insertRow(rowCount);
					var cell0 = row.insertCell(0);
					var cell1 = row.insertCell(1);
					var cell2 = row.insertCell(2);
					var cell3 = row.insertCell(3);
					var cell4 = row.insertCell(4);
					cell0.innerHTML = rowCount;
					cell1.innerHTML = '<input type="text" id="t_nama" class="form-control">';
					cell2.innerHTML = '<input type="text" id="t_nik" class="form-control">';
					cell3.innerHTML = '<input type="text" id="t_hubungan" class="form-control">';
					cell4.innerHTML = "<button type='submit' onclick='t_save_row(\""+rowCount+"\")' class='btn btn-primary'>Simpan</button>";

					for(var a=1;a<rowCount;a++)
					{
						if(a != rowCount)
						{
							table.rows[a].cells[4].innerHTML = "";
						}
					}
				}

			</script>
		</table>
		<a href="#" id="tambahBtnT1" onclick="t_addLine()">+ Tambah Item</a>
	</div>
</div>
