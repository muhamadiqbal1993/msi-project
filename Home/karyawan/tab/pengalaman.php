<div class="row">
	<div class="col-md-12">
		<h1>Pengalaman Kerja Dan Pelatihan</h1>
		<hr class="abu">
	</div>
	<!-- <input type="hidden" name="s_pic" class="form-control" value="-">
	<input type="hidden" name="c_pic" class="form-control" value="-"> -->
	<div class="col-md-12">
		<input type="hidden" name="detLatih" id="detLatih" class="form-control">
		<a href="#" id="tambahBtn_latih" onclick="addLine_latih()">+ Tambah Item</a>
		<p></p>
		<table id="tabLatih" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="width: 5%">No</th>
					<th>Jabatan / Pelatihan</th>
					<th>Tempat Pelatihan / Kerja</th>
					<th>Tahun Masuk</th>
					<th>Tahun Selesai</th>
					<th>Action</th>
				</tr>
			</thead>
			<script language="javascript" type="text/javascript">

				function view_total_latih()
				{
					var detDimensi = "";
					var totals = 0;
					var rowCount = document.getElementById('tabLatih').rows.length;
					var table = document.getElementById('tabLatih');
					for(var a=1;a<rowCount;a++)
					{
						var jenjang = table.rows[a].cells[1].innerHTML;
						var nama = table.rows[a].cells[2].innerHTML;
						var masuk = table.rows[a].cells[3].innerHTML;
						var keluar = table.rows[a].cells[3].innerHTML;
						detDimensi = detDimensi+","+jenjang+"spasinya"+nama+"spasinya"+masuk+"spasinya"+keluar;
					}
					document.getElementById("detLatih").value = detDimensi;
				}

				function delete_row_latih(abs)
				{
					document.getElementById("tabLatih").deleteRow(abs);
					var rowCount = document.getElementById('tabLatih').rows.length;
					var table = document.getElementById('tabLatih');
					for(var a=1;a<rowCount;a++)
					{
						table.rows[a].cells[0].innerHTML = a;
						table.rows[a].cells[5].innerHTML = "<a href='#' onclick='edit_row_latih(\""+a+"\")'> Edit </a> |    <a href='#' onclick='delete_row_latih(\""+a+"\")'> Hapus</a>";
						view_total_latih();
					}
					view_total_latih();
				}

				function edit_row_latih(abs)
				{
					document.getElementById('tambahBtn_latih').style.display = "none"; 
					document.getElementById('tambahBtn1_latih').style.display = "none"; 
					var rowCount = document.getElementById('tabLatih').rows.length;
					var table = document.getElementById('tabLatih');
					for(var a=1;a<rowCount;a++)
					{
						if(abs == a)
						{
							var jenjang = table.rows[a].cells[1].innerHTML;
							var nama = table.rows[a].cells[2].innerHTML;
							var masuk = table.rows[a].cells[3].innerHTML;
							var keluar = table.rows[a].cells[4].innerHTML;

							table.rows[a].cells[1].innerHTML = '<input type="text" id="editJenjang" class="form-control" value="'+jenjang+'">';
							table.rows[a].cells[2].innerHTML = '<input type="text" id="editNama" class="form-control" value="'+nama+'">';
							table.rows[a].cells[3].innerHTML = '<input type="number" id="editMasuk" class="form-control" value="'+masuk+'">';
							table.rows[a].cells[4].innerHTML = '<input type="number" id="editKeluar" class="form-control" value="'+keluar+'">';
							table.rows[a].cells[5].innerHTML = "<button type='submit' onclick='save_row_latih(\""+a+"\")' class='btn btn-primary'>Simpan</button>";
						}
						else
						{
							table.rows[a].cells[5].innerHTML = "";
						}
					}
				}

				function save_row_latih(abs)
				{
					var rowCount = document.getElementById('tabLatih').rows.length;
					var table = document.getElementById('tabLatih');
					for(var a=1;a<rowCount;a++)
					{
						if(abs == a)
						{
							var jenjang = document.getElementById("editJenjang").value;
							var nama = document.getElementById("editNama").value;
							var masuk = document.getElementById("editMasuk").value;
							var keluar = document.getElementById("editKeluar").value;

							table.rows[abs].cells[1].innerHTML = jenjang;
							table.rows[abs].cells[2].innerHTML = nama;
							table.rows[abs].cells[3].innerHTML = masuk;
							table.rows[abs].cells[4].innerHTML = keluar;
							table.rows[abs].cells[5].innerHTML = "<a href='#' onclick='edit_row_latih(\""+abs+"\")'> Edit </a> |    <a href='#' onclick='delete_row_latih(\""+abs+"\")'> Hapus</a>";
						}
						else
						{
							table.rows[a].cells[5].innerHTML = "<a href='#' onclick='edit_row_latih(\""+a+"\")'> Edit |    <a href='#' onclick='delete_row_latih(\""+a+"\")'> Hapus</a></a>";
						}
					}
					document.getElementById('tambahBtn_latih').style.display = ""; 
					document.getElementById('tambahBtn1_latih').style.display = ""; 
					view_total_latih();
				}

				function addLine_latih()
				{

					document.getElementById('tambahBtn_latih').style.display = "none"; 
					document.getElementById('tambahBtn1_latih').style.display = "none"; 
					var rowCount = document.getElementById('tabLatih').rows.length;
					var table = document.getElementById('tabLatih');
					var row = table.insertRow(rowCount);
					var cell0 = row.insertCell(0);
					var cell1 = row.insertCell(1);
					var cell2 = row.insertCell(2);
					var cell3 = row.insertCell(3);
					var cell4 = row.insertCell(4);
					var cell5 = row.insertCell(5);
					cell0.innerHTML = rowCount;
					cell1.innerHTML = '<input type="text" id="editJenjang" class="form-control">';
					cell2.innerHTML = '<input type="text" id="editNama" class="form-control">';
					cell3.innerHTML = '<input type="number" id="editMasuk" class="form-control">';
					cell4.innerHTML = '<input type="number" id="editKeluar" class="form-control">';
					cell5.innerHTML = "<button type='submit' onclick='save_row_latih(\""+rowCount+"\")' class='btn btn-primary'>Simpan</button>";

					for(var a=1;a<rowCount;a++)
					{
						if(a != rowCount)
						{
							table.rows[a].cells[5].innerHTML = "";
						}
					}
				}

			</script>
		</table>
		<a href="#" id="tambahBtn1_latih" onclick="addLine_latih()">+ Tambah Item</a>
	</div>
</div>
