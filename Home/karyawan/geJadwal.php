<?php
include 'conn.php';

$origin = $_GET['origin'];
$dest = $_GET['dest'];
$tanggal = $_GET['tanggal'];
$agent = $_GET['agent'];
$prefix = $_GET['prefix'];
$qry = "select tb4.*,IFNULL(BR.max, 0) as kilo, IFNULL(BR.vw, 0) as koli,A.awb_prefix,(select gw from space_allocation where agent = '$agent' and reg_number = tb4.reg_number) as spaceal,RN.kapasitas,
(select sum(B.charge) from flight_order FO
inner join booking B on B.no = FO.id_order
where FO.fl_no in (select fl_no from flight_summary where reg_number = tb4.reg_number and tanggal = tb4.tanggal)
and FO.tanggal = tb4.tanggals ) as space_kepake,
(select count(*) as totalawb from stock_awb SA inner join airlane A on A.no = SA.liner where A.carier_code = SUBSTRING_INDEX(fl_no,'-',1) and SA.agent = '$agent' and SA.status in ('0','1')) as totalAWB from (
select *,
(select sum(charge) as kepake from (
select FO.*,B.charge from flight_order FO
inner join booking B on B.no =  FO.id_order
) as tb5 where fl_no = tb3.fl_no and tanggal = tb3.tanggals and origin = tb3.origin and dest = tb3.dest and etd = tb3.etd and eta = tb3.eta) as totals,
(select TRUNCATE(sum(B.charge),2) as kepake from flight_order FO
inner join booking B on B.no = FO.id_order
where FO.fl_no in (select fl_no from flight_summary where reg_number = tb3.reg_number and tanggal = tb3.tanggal) and tanggal = tb3.tanggals) as total_reg_number
from (
select *,
(CASE
WHEN wkt <= now() and tanggal <= now() THEN 'Close'
WHEN wkt >= now() THEN 'Open'
WHEN tanggal > now() THEN 'Open'
END) as status_trb
from (
select *,date_add(CONVERT(etd, TIME),interval -bct minute) as wkt from (
select *,DATE_FORMAT(tanggal, '%d %M %Y') as tanggals,
(select TM.isi from time_managemnt TM 
inner join airlane A on A.no = TM.liner
where TM.port = '$origin' and A.carier_code = SUBSTRING_INDEX(fl_no,'-',1) and TM.type = 'BCT') as bct,
(select TM.isi from time_managemnt TM 
inner join airlane A on A.no = TM.liner
where TM.port = '$origin' and A.carier_code = SUBSTRING_INDEX(fl_no,'-',1) and TM.type = 'MCT') as mct
from flight_summary where origin = '$origin' and dest = '$dest' and tanggal >= curdate() and status = '1'
) as tb1
) as tb2
) as tb3
) as tb4
left outer join airlane A on A.carier_code = SUBSTRING_INDEX(fl_no,'-',1)
left outer join booking_rule BR on BR.liner = A.no
left outer join reg_number RN on RN.reg_number = tb4.reg_number
where A.awb_prefix like '%$prefix%'
";

if($tanggal != "%")
{
	$tanggal = explode("/", $_GET['tanggal'])[2].'-'.explode("/", $_GET['tanggal'])[0].'-'.explode("/", $_GET['tanggal'])[1];
	// $qry = "select *,DATE_FORMAT(tanggal, '%d %M %Y') as tanggals from flight_summary where origin = '$origin' and dest = '$dest' and tanggal = '$tanggal' and tanggal >= curdate()";
	$qry = "select tb4.*,IFNULL(BR.max, 0) as kilo, IFNULL(BR.vw, 0) as koli,A.awb_prefix,(select gw from space_allocation where agent = '$agent' and reg_number = tb4.reg_number) as spaceal,RN.kapasitas,
	(select sum(B.charge) from flight_order FO
	inner join booking B on B.no = FO.id_order
	where FO.fl_no in (select fl_no from flight_summary where reg_number = tb4.reg_number and tanggal = tb4.tanggal) and FO.tanggal = tb4.tanggals ) as space_kepake,
	(select count(*) as totalawb from stock_awb SA inner join airlane A on A.no = SA.liner where A.carier_code = SUBSTRING_INDEX(fl_no,'-',1) and SA.agent = '$agent' and SA.status in ('0','1')) as totalAWB from ( 
	select *,
	(select sum(charge) as kepake from (
	select FO.*,B.charge from flight_order FO
	inner join booking B on B.no =  FO.id_order
	) as tb5 where fl_no = tb3.fl_no and tanggal = tb3.tanggals and origin = tb3.origin and dest = tb3.dest and etd = tb3.etd and eta = tb3.eta) as totals,
	(select TRUNCATE(sum(B.charge),2) as kepake from flight_order FO
	inner join booking B on B.no = FO.id_order
	where FO.fl_no in (select fl_no from flight_summary where reg_number = tb3.reg_number and tanggal = tb3.tanggal) and tanggal = tb3.tanggals) as total_reg_number
	from (
	select *,
	(CASE
	WHEN wkt <= now() and tanggal <= now() THEN 'Close'
	WHEN wkt >= now() THEN 'Open'
	WHEN tanggal > now() THEN 'Open'
	END) as status_trb
	from (
	select *,date_add(CONVERT(etd, TIME),interval -bct minute) as wkt from (
	select *,DATE_FORMAT(tanggal, '%d %M %Y') as tanggals,
	(select TM.isi from time_managemnt TM 
	inner join airlane A on A.no = TM.liner
	where TM.port = '$origin' and A.carier_code = SUBSTRING_INDEX(fl_no,'-',1) and TM.type = 'BCT') as bct,
	(select TM.isi from time_managemnt TM 
	inner join airlane A on A.no = TM.liner
	where TM.port = '$origin' and A.carier_code = SUBSTRING_INDEX(fl_no,'-',1) and TM.type = 'MCT') as mct
	from flight_summary where origin = '$origin' and dest = '$dest' and tanggal = '$tanggal' and tanggal >= curdate() and status = '1'
	) as tb1
	) as tb2
	) as tb3 
	) as tb4
	left outer join airlane A on A.carier_code = SUBSTRING_INDEX(fl_no,'-',1)
	left outer join booking_rule BR on BR.liner = A.no
	left outer join reg_number RN on RN.reg_number = tb4.reg_number
	where A.awb_prefix like '%$prefix%'
	";
}
//echo $qry;
$queryResult=$connect->query($qry);

$result=array();

while($fetchData=$queryResult->fetch_assoc()){
	$result[]=$fetchData;
}

echo json_encode($result);

?>


