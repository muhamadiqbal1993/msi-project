

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>History <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<button type="submit" class="btn btn-warning" onclick="history.back()">  Kembali  </button>
						&nbsp;&nbsp;&nbsp;
						<a href="./?ids=<?php echo $id;?>"><button class="btn btn-primary">Tambah Data</button></a>
						
						
						
						
						<hr class="abu">
					</div>

					<div class="box-body">
						<div class="col-xs-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width: 5%">No</th>
										<th>Id Order</th>
										<th>Bank</th>
										<th>Tanggal</th>
										<th>Saldo Awal</th>
										<th>Saldo Akhir</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$not = 1;

									$sql1=mysql_query("select M.*,concat(K.bank,' ',K.no_acc,' - ',K.nama_acc) as nambank,DATE_FORMAT(M.tanggal, '%d %M %Y') as tgls from mutasi M
									inner join kasbank K on K.no = M.kasbank
									where M.kasbank = '$id' order by tanggal desc");
									while($data1=mysql_fetch_array($sql1))
									{  
										$status = 'Draft';
										$dis = $data1['id'];
										if($data1['status'] == "1")
										{
											$status = "Draft";
										}
										if($data1['status'] == "2" || $data1['status'] == "3")
										{
											$status = "Close";
										}
										$idbkm = $data1['id'];
										$row = mysql_fetch_array(mysql_query("select sum(nominal) as total from mutasi_detail where id_trx = '$idbkm'"));
										$total_nya = $row['total'];
										echo '
										<tr>
											<td>'.$not.'</td>
											<td>'.$data1['id'].'</td>
											<td>'.$data1['nambank'].'</td>
											<td>'.$data1['tgls'].'</td>
											<td>'.number_format($data1['saldo_awal']).'</td>
											<td>'.number_format($data1['saldo_akhir']).'</td>
											<td>'.$status.'</td>
											<td><a href="./?kd='.$data1['no'].'">Manage</a></td>
										</tr>
										';
										$not++;
									}
									?>
								</tbody>
							</table>
						</div>
						

						
					</div>
					<br>
					<br>
					<!-- /.box-body -->
				</div>

				

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function kbmProses()
	{
		document.getElementById("kbmProses").action = ".";
	}

</script>
