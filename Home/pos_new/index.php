<?php
include '../function/init.php';
if(empty($_SESSION['no']))
{
  header('Location:../../');
}
$divt = $user_data['groups'];
$divisinyadia = "";
$sql1=mysql_query("select * from group_divisi where no = '$divt'");
while($data1=mysql_fetch_array($sql1))
{  
  $divisinyadia = $data1['nama'];
}
$ids = date('ymdhis');
$tanggalSekarang = date('d F Y');
$tglCari = date('Y-m-d');
$dataJualanSales=array();
$sql1=mysql_query("select *,(select nama from level_sales where tb1.total between nilai_dari and nilai_sampai) as level
  ,(select urutan from level_sales where tb1.total between nilai_dari and nilai_sampai) as urutan_level from (
  select *,IFNULL((select sum(total) from pos_order where tanggal_order like '%".$tglCari."%' and sales = user.no group by sales),0) as total from user where status = '1' and groups = '9'
  ) as tb1 
  order by total desc
  ");
while($data1=mysql_fetch_array($sql1))
{ 
  $dataJualanSales[] = array("namaSales"=>$data1['nama'],"totalPenjualan"=>$data1['total'],"namaLevel"=>$data1['level'],"urutanLevel"=>$data1['urutan_level']);
}
$dataFinalJual = json_encode($dataJualanSales);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>POS</title>
  <link rel="shortcut icon" href="../logo.png"/>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <script src="js/jquery.js" type="text/javascript"></script>
</head>
<body class="skin-blue-light sidebar-collapse pos">
  <div class="wrapper">

    <header class="main-header">
      <a class="logo">
         POS
        <!-- <span class="logo-mini">POS</span> -->
      </a>
      <nav class="navbar navbar-static-top" role="navigation">
        <ul class="nav navbar-nav pull-left">
          <!-- <li><a href="#"><?php echo  date('d F Y')?></a></li> -->
          <li><a href="../" target="_blank">Dashboard</a></li>
          <li><a href="../pos_order" target="_blank">Penjualan</a></li>
          <!-- <li><a href="../stock_product" target="_blank">Stock</a></li>
            <li><a href="../keuangan" target="_blank">Keuangan</a></li> -->
            <li><a href="#" data-toggle="control-sidebar" class="sidebar-icon">Order Pending <span id="warningpending" class="label label-danger"></span></a></li>

          </ul>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="https://ecm.my.id/uploads/avatars/thumbs/male.png" class="user-image" alt="Avatar" />
                  <span><?php echo $user_data['nama'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="https://ecm.my.id/uploads/avatars/male.png" class="img-circle" alt="Avatar" />
                    <p>
                      <?php echo $user_data['nama'].' - '.$divisinyadia;?>                                           
                      <small><?php echo $user_data['email'];?> </small>
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="../profil" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="../function/F_logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        <!-- <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" data-toggle="modal" data-target="#popLevel" class="dropdown-toggle" data-toggle="dropdown">
                <img src="../images/level.png" class="user-image" alt="Avatar" />
                <span>Level Sales</span>
              </a>
              
            </li>
            
          </ul>
        </div> -->
      </nav>
    </header>

    <script type="text/javascript">
      $(document).keypress(function (e) {
        if (e.which == 13) {
          var kds = document.getElementById("barcode").value;
          inputProduct(kds);
          document.getElementById("barcode").focus();
        }
        if (e.which > 31 && (e.which < 48 || e.which > 57)) {
          if (e.which == 80) 
          {
            //printPage();
          }
          if (e.which == 83) 
          {
            //simpan();
          }
          if (e.which == 75) 
          {
            document.getElementById("kartu").className = "btn btn-primary btn-block btn-flat";
            document.getElementById("cash").className = "btn btn-default btn-block btn-flat";
            document.getElementById('pembayaran').value = "2";
          }
          if (e.which == 67) 
          {
            document.getElementById("cash").className = "btn btn-primary btn-block btn-flat";
            document.getElementById("kartu").className = "btn btn-default btn-block btn-flat";
            document.getElementById('pembayaran').value = "1";
          }
        }
        else
        {
          document.getElementById("barcode").focus();
        }
      });
    </script>
    <div class="content-wrapper">
      <table style="width:100%;" class="layout-table">
        <tr>
          <td style="width: 35%;">
            <div id="pos">
              <div class="well well-sm" id="leftdiv">
                <div id="lefttop" style="margin-bottom:5px;">
                  <form action="F_insertData.php" id="pos-sale-form" method="post" accept-charset="utf-8">
                    <div class="form-group" style="margin-bottom:5px;display: none;">
                      <div class="input-group">
                        <select name="sales" id="spos_customer" data-placeholder="Pilih Sales"  class="form-control select2" style="width:100%;position:absolute;">
                          <option value="0" selected="selected">----- Pilih Sales -----</option>
                          <?php
                          $sql1=mysql_query("select * from user where status = '1' and groups = '9'");
                          while($data1=mysql_fetch_array($sql1))
                          {
                            ?>
                            <option value="<?php echo $data1['no']?>" ><?php echo $data1['nama']?></option>

                            <?php 
                          }
                          ?>
                        </select>
                        <div class="input-group-addon no-print" style="padding: 2px 5px;">

                        </div>
                      </div>
                      <div style="clear:both;"></div>
                    </div>
                    <div class="form-group" style="margin-bottom:5px;">
                      <input type="text" name="hold_ref" value="" id="hold_ref" class="form-control kb-text" placeholder="Nama Pelanggan" />
                      <input type="hidden" name="id_order" id="id_order" class="form-control" required="" readonly="" value="<?php echo $ids?>">
                      <input type="hidden" name="user_idms" id="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
                      <input type="hidden" name="detail" id="detail" class="form-control" >
                      <input type="hidden" name="pembayaran" id="pembayaran" class="form-control" value="2">
                      <input type="hidden" name="total" id="total" class="form-control">
                    </div>
                  </form> 
                  <div class="form-group" style="margin-bottom:5px;">
                    <input type="text" name="barcode" id="barcode" onclick="onchange('-')" class="form-control" placeholder="Cari Berdasarkan Barcode / Kode Product" autofocus/>
                  </div>
                </div>
                <div id="printhead" class="print">
                  <p>Tanggal: Tue 23 Aug 2022</p>
                </div>
                <div id="print" class="fixed-table-container">
                  <div id="list-table-div">

                    <div class="fixed-table-headers" id="tblCongrats" style="display: none">
                      <img src="../images/congrats.gif" width="300px" height="300px" style="display: block;margin-left: auto;margin-right: auto;">
                      <h1 style="text-align: center;color: green" id="c_nama"><strong>-</strong></h1>
                      <br>
                      <h4 style="text-align: center;color: red" id="c_level"><strong>Telah Mencapai Level</strong></h4>
                      <h4 style="text-align: center;color: red" id="c_penjualan"><strong>Total Penjualan</strong></h4>
                    </div>

                    <div class="fixed-table-headers" id="tblListBeli">
                      <table id="listorder" class="table table-striped table-condensed table-hover list-table" style="overflow-y:auto">
                        <thead>
                          <tr class="success">
                            <th style="width: 5%;text-align:center;">No</th>
                            <th>Produk</th>
                            <th style="width: 15%;text-align:center;">Qty</th>
                            <th style="width: 15%;text-align:center;">Harga</th>
                            <th style="width: 20%;text-align:center;">Subtotal</th>
                            <th style="width: 20px;" class="satu"><a href="#" onclick="hapusSemua()"><i class="fa fa-trash-o"></i></a></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $no = "1";
                          $harga = 0;
                          $dets = "";
                          $sql1=mysql_query("select *,count(*) as qtys from (
                            select IFNULL(TM.id_prod,P.nama) as nama,IFNULL(TM.harga,P.harga) as harga,'0' as stat ,P.nama as nampro from tmp_order TM
                            left outer join product P on P.kode = TM.id_prod
                            where TM.id_order = '".$user_data['no']."') as tb1 group by nama
                            ");
                          while($data1=mysql_fetch_array($sql1))
                          {  
                            $hargs = $data1['harga'] * $data1['qtys'];
                            $apss = "<a href='javascript:void(0)' onclick='newDelete(".$no.")'><i class='fa fa-trash-o'></i></a>";
                            $nams = '['.$data1['nama'].'] '.$data1['nampro'];
                            if($data1['nampro'] == "")
                            {

                              $apss = "";
                              $nams = $data1['nama'];
                            }
                            $qtyss = '<input type="number" id="'.$no.'" oninput="editOrder('.$no.','.$data1['qtys'].')" onclick="kliks('.$no.','.$data1['qtys'].')" class="form-control" value="'.$data1['qtys'].'" />';
                            
                            if($data1['harga'] < 0)
                            {
                              $qtyss = '<input type="number" id="'.$no.'" oninput="editOrder('.$no.','.$data1['qtys'].')" onclick="kliks('.$no.','.$data1['qtys'].')" class="form-control" value="'.$data1['qtys'].'" readonly/>';
                              
                            }
                            echo '
                            <tr class="danger">
                            <td>'.$no.'</td> 
                            <td>'.$nams.'</td>
                            <td style="width: 15%;text-align:center;">'.$qtyss.'</td>
                            <td style="width: 15%;text-align:center;">'.number_format($data1['harga']).'</td>
                            <td style="width: 15%;text-align:center;">'.number_format($hargs).'</td>
                            <td>'.$apss.'</td>

                            </tr>
                            ';
                            $dets = $dets .','.$data1['nama']."spasinya".$data1['qtys']."spasinya".$data1['harga'];
                            $harga += $hargs;
                            $no++;
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                      <!-- <table id="posTable" class="table table-striped table-condensed table-hover list-table" style="margin:0px;" data-height="100">
                        <thead>
                          <tr class="success">
                            <th>Produk456</th>
                            <th style="width: 15%;text-align:center;">Harga</th>
                            <th style="width: 15%;text-align:center;">Jumlah</th>
                            <th style="width: 20%;text-align:center;">Subtotal</th>
                            <th style="width: 20px;" class="satu"><i class="fa fa-trash-o"></i></th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table> -->
                    </div>
                    <div style="clear:both;"></div>
                    <div id="totaldiv">
                      <table id="totaltbl" class="table table-condensed totals" style="margin-bottom:10px;">
                        <tbody>
                          <tr class="info">
                            <td width="25%"><strong>Total Item</strong></td>
                            <td class="text-right" style="padding-right:10px;"><span id="counts">0 Items</span></td>
                            <td width="25%"><strong>Total</strong></td>
                            <td class="text-right" colspan="2"><span id="totals">0</span></td>
                          </tr>
                          <tr class="info">
                            <td width="25%">Diskon</td>
                            <td class="text-right" style="padding-right:10px;"><span id="ds_cons">0</span></td>
                            <td width="25%">Pajak Pesanan</td>
                            <td class="text-right"><span id="ts_con">0</span></td>
                          </tr>
                          <tr class="success">
                            <td colspan="2" style="font-weight:bold;">
                              Total Dibayar                                                            <a role="button" data-toggle="modal" data-target="#noteModal">
                                <i class="fa fa-comment"></i>
                              </a>
                            </td>
                            <td class="text-right" colspan="2" style="font-weight:bold;"><span id="payables">0</span></td>
                          </tr>
                          <tr class="success">
                            <td colspan="4" style="text-align: center;"><a href="#" data-toggle="modal" data-target="#popKembalian" onclick="inputTotals()"><strong>Hitung Kembalian</strong></a></td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <div id="botbuttons" class="col-xs-12 text-center">
                    <div class="row">
                      <div class="col-xs-4" style="padding: 0;">
                        <div class="btn-group-vertical btn-block">
                          <button type="button" class="btn btn-primary btn-block btn-flat" id="kartu"
                          onclick="bayarType('kartu')" >Pakai QRIS</button>
                          <button type="button" class="btn btn-default btn-block btn-flat" id="cash" onclick="bayarType('cash')" >Bayar Cash</button>
                        </div>

                      </div>
                      <div class="col-xs-4" style="padding: 0;">
                        <button type="button" class="btn btn-warning btn-block btn-flat" onclick="simpan()" style="height:67px;">Hold / Simpan</button>
                      </div>
                      <div class="col-xs-4" style="padding: 0;">
                        <button type="button" class="btn btn-success btn-block btn-flat" onclick="printPage()" style="height:67px;">Cetak Struk</button>
                      </div>
                    </div>

                  </div>
                  <script type="text/javascript">
                    function hapusSemua()
                    {
                      var answer = window.confirm("Apakah anda yakin menghapus item pada order list ?");
                      if (answer) {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                          if (this.readyState == 4 && this.status == 200) 
                          {
                            var myArr = JSON.parse(this.responseText);
                            var totalList = myArr.length;
                            var table = document.getElementById('listorder');
                            for(var i = table.rows.length - 1; i > 0; i--)
                            {
                              table.deleteRow(i);
                            }
                            var today = new Date();
                            var hari = today.getDate();
                            var bulan = today.getMonth()+1; 
                            var jam = today.getHours();
                            var menit = today.getMinutes();
                            var detik = today.getSeconds();
                            var tahun = today.getFullYear();
                            todayss = tahun+''+bulan+''+hari+''+jam+''+menit+''+detik; 
                            document.getElementById("id_order").value = todayss;
                            document.getElementById("counts").innerHTML = "0 Items";
                            document.getElementById("totals").innerHTML = "0";
                            document.getElementById("ds_cons").innerHTML = "0";
                            document.getElementById("payables").innerHTML = 'Rp. 0';
                          }
                        };
                        xmlhttp.open("GET","deleteTmp.php?user=<?php echo $user_data['no']?>&ids=xxx",true);
                        xmlhttp.send();
                      }
                    }
                    function bayarType(types)
                    {
                      if(types == "cash")
                      {
                        document.getElementById("cash").className = "btn btn-primary btn-block btn-flat";
                        document.getElementById("kartu").className = "btn btn-default btn-block btn-flat";
                        document.getElementById('pembayaran').value = "1";
                      }
                      else
                      {
                        document.getElementById("kartu").className = "btn btn-primary btn-block btn-flat";
                        document.getElementById("cash").className = "btn btn-default btn-block btn-flat";
                        document.getElementById('pembayaran').value = "2";
                      }
                    }
                  </script>
                <!-- <div id="botbuttons" class="col-xs-12 text-center">
                  <div class="row">
                    <div class="col-xs-4" style="padding: 0;">
                      <div class="btn-group-vertical btn-block">
                        <button type="button" class="btn btn-warning btn-block btn-flat"
                        id="suspend">Hold</button>
                        <button type="button" class="btn btn-danger btn-block btn-flat"
                        id="reset">Batalkan</button>
                      </div>

                    </div>
                    <div class="col-xs-4" style="padding: 0 5px;">
                      <div class="btn-group-vertical btn-block">
                        <button type="button" class="btn bg-purple btn-block btn-flat" id="print_order">Cetak Pesanan</button>

                        <button type="button" class="btn bg-navy btn-block btn-flat" id="print_bill">Cetak Tagihan</button>
                      </div>
                    </div>
                    <div class="col-xs-4" style="padding: 0;">
                      <button type="button" class="btn btn-success btn-block btn-flat" id="payment" style="height:67px;">Pembayaran</button>
                    </div>
                  </div>

                </div> -->
                <div class="clearfix"></div>
                <span id="hidesuspend"></span>
                <input type="hidden" name="spos_note" value="" id="spos_note">

                <div id="payment-con">
                  <input type="hidden" name="amount" id="amount_val" value=""/>
                  <input type="hidden" name="balance_amount" id="balance_val" value=""/>
                  <input type="hidden" name="paid_by" id="paid_by_val" value="cash"/>
                  <input type="hidden" name="cc_no" id="cc_no_val" value=""/>
                  <input type="hidden" name="paying_gift_card_no" id="paying_gift_card_no_val" value=""/>
                  <input type="hidden" name="cc_holder" id="cc_holder_val" value=""/>
                  <input type="hidden" name="cheque_no" id="cheque_no_val" value=""/>
                  <input type="hidden" name="cc_month" id="cc_month_val" value=""/>
                  <input type="hidden" name="cc_year" id="cc_year_val" value=""/>
                  <input type="hidden" name="cc_type" id="cc_type_val" value=""/>
                  <input type="hidden" name="cc_cvv2" id="cc_cvv2_val" value=""/>
                  <input type="hidden" name="balance" id="balance_val" value=""/>
                  <input type="hidden" name="payment_note" id="payment_note_val" value=""/>
                </div>
                <input type="hidden" name="customer" id="customer" value="1" />
                <input type="hidden" name="order_tax" id="tax_val" value="" />
                <input type="hidden" name="order_discount" id="discount_val" value="" />
                <input type="hidden" name="count" id="total_item" value="" />
                <input type="hidden" name="did" id="is_delete" value="0" />
                <input type="hidden" name="eid" id="is_delete" value="0" />
                <input type="hidden" name="total_items" id="total_items" value="0" />
                <input type="hidden" name="total_quantity" id="total_quantity" value="0" />
                <input type="submit" id="submit" value="Submit Sale" style="display: none;" />
              </div>
            </div>

          </td>
          <td>
            <div class="contents" id="right-col">
              <div id="item-list">
                <div class="product-nav">
                </div>
                <div class="items">
                  <div>
                    <?php 
                    $sql1=mysql_query("select * from (
                      select P.*,SP.nama as pendek from product P
                      left outer join short_product SP on SP.kode = P.kode
                      where P.status = '1'
                      UNION
                      SELECT no,id,nama,harga,image as photo,id as barcode,status,id as kode,'paket' as kategory,'0' as harga_flyer,'' as type,nama as pendek FROM paket_header where status = '1'
                      ) as tb1
                      order by kategory desc");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      $namas = $data1['pendek'];
                      if(empty($data1['pendek']))
                      {
                        $namas = $data1['nama'];
                      }
                      $imgs = "product/".$data1['photo'];
                      if($data1['kategory'] == "paket")
                      {
                        $imgs = "paket/".$data1['photo'];
                      }
                      
                      ?>
                      <button type="button" 
                      onclick="inputProduct('<?php echo $data1['barcode']?>')"
                      id="<?php echo $data1['kode']?>" 
                      type="button" 
                      class="btn btn-both btn-flat product"><span class="bg-img"><img src="../images/<?php echo $imgs?>" alt="<?php echo $namas?>" style="width: 100px; height: 100px;"></span><span><span><?php echo $namas?></span></span></button>
                      <?php
                    }
                    ?>
                  </div>
                </div>
                <div class="product-nav">
                  <div class="btn-group">
                    <div class="btn-group">
                      <button id="semua" onclick="gantiWarna('semua')" class="btn btn-success pos-tip btn-flat" type="button" >Semua</button>
                    </div>

                    <div class="btn-group" style="padding-left: 20px">
                      <button id="car" onclick="gantiWarna('car')" class="btn btn-default pos-tip btn-flat" type="button" >Car Wash</button>
                    </div>
                    
                    <div class="btn-group" style="padding-left: 20px">
                      <button id="det" onclick="gantiWarna('det')" class="btn btn-default pos-tip btn-flat" type="button" >Detailing</button>
                    </div>

                    <div class="btn-group" style="padding-left: 20px">
                      <button id="mak" onclick="gantiWarna('mak')" class="btn btn-default pos-tip btn-flat" type="button" style="width: 100%">Makanan & Minuman</button>
                    </div>

                    <div class="btn-group" style="padding-left: 20px">
                      <button id="paket" onclick="gantiWarna('paket')" class="btn btn-default pos-tip btn-flat" type="button" >Paket</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>

  <script type="text/javascript">
    function inputProduct(kode)
    {
      var plats = document.getElementById('hold_ref').value
      if(plats != "")
      {
        if(kode != "")
        {
          var idss = kode;
          if(kode == "-")
          {
            idss = document.getElementById('user_idms').value;
          }

          var users = document.getElementById('user_idms').value;
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              var myArr = JSON.parse(this.responseText);

              var totalList = myArr.length;
              if (myArr.length == 0)
              {
                alert("Product Tidak ditemukan");
                document.getElementById('barcode').value = "";
              }
              else
              {
                if (myArr[0]['stat'] == "0")
                {
                  alert("Product Tidak ditemukan");
                  document.getElementById('barcode').value = "";
                }
                else
                {
                  var check = "0";
                  var table = document.getElementById('listorder');

                  if(check == "0")
                  {
                    for(var i = table.rows.length - 1; i > 0; i--)
                    {
                      table.deleteRow(i);
                    }

                    var rowCount = document.getElementById('listorder').rows.length;

                    getView(myArr);
                    document.getElementById('barcode').value = "";
                    view_total();
                  }
                  else
                  {
                    document.getElementById('barcode').value = "";
                  }
                  document.getElementById('tblCongrats').style.display = "none";
                  document.getElementById('tblListBeli').style.display = "";
                }
              }
            }
          };
          xmlhttp.open("GET","checkOrderBaru.php?ids="+idss+"&idorder="+users+"&plat="+plats,true);
          xmlhttp.send();
        }
      }
      else
      {
        document.getElementById("hold_ref").focus();
        alert("Harap Masukan Nama Pelanggan Terlebih Dahulu");
      }
      
    }

    function view_total()
    {
      var totalnya = 0;
      var disc = 0;
      var detail = "";
      var rowCount = document.getElementById('listorder').rows.length;
      var table = document.getElementById('listorder');
      var totalOrder = 0;
      for(var a=1;a<rowCount;a++)
      {
        var hargas = table.rows[a].cells[4].innerHTML.split(",").join("");
        if(parseInt(hargas) < 0)
        {
          disc += parseInt(hargas);
        }
        else
        {
          totalnya += parseInt(hargas);
          totalOrder++;
        }

        var prod = table.rows[a].cells[1].innerHTML.split("]")[0].replace("[","");
        var qty = document.getElementById(String(a)).value;
        var harga = table.rows[a].cells[3].innerHTML.split(",").join("");
        //totalnya += parseInt(qty)*parseInt(harga);
        detail = detail +','+prod+"spasinya"+qty+"spasinya"+harga;
      } 
      var semuaTotal = parseInt(totalnya) + parseInt(disc);; 
      document.getElementById("counts").innerHTML = totalOrder+" Items";
      document.getElementById("totals").innerHTML = totalnya.toLocaleString();
      document.getElementById("ds_cons").innerHTML = disc.toLocaleString();
      document.getElementById("payables").innerHTML = 'Rp. '+semuaTotal.toLocaleString();
      
      
      // document.getElementById("totalHarga").innerHTML = '<strong>Total : Rp. '+totalnya.toLocaleString()+'</strong>';
      document.getElementById("detail").value = detail;
      document.getElementById("total").value = semuaTotal;
      // document.getElementById("bayar").value = totalnya.toLocaleString();
    }

    function editOrder(ids,stock)
    {
      var qty = document.getElementById(ids).value;
      var rowCount = document.getElementById('listorder').rows.length;
      var table = document.getElementById('listorder');
      var users = document.getElementById('user_idms').value;
      var harga = table.rows[ids].cells[3].innerHTML.split(",").join("");
      var kode = table.rows[ids].cells[1].innerHTML.split("] ")[0].split("[")[1];
      var totalLooping = parseInt(qty) - parseInt(stock);
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var myArr = JSON.parse(this.responseText);

          var totalList = myArr.length;
          var check = "0";
          var table = document.getElementById('listorder');

          if(check == "0")
          {
            for(var i = table.rows.length - 1; i > 0; i--)
            {
              table.deleteRow(i);
            }

            var rowCount = document.getElementById('listorder').rows.length;
            getView(myArr);
            view_total();
          }
          view_total();
        }
      };
      xmlhttp.open("GET","updateOrder.php?product="+kode+"&qty="+totalLooping+"&idorder="+users+"&harga="+harga,true);
      xmlhttp.send();
    }

    function newDelete(abs)
    {
      var table = document.getElementById('listorder');
      var idss = table.rows[abs].cells[1].innerHTML.split("] ")[0].split("[")[1];
      var users = document.getElementById('user_idms').value;
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var myArr = JSON.parse(this.responseText);

          var totalList = myArr.length;
          var check = "0";
          var table = document.getElementById('listorder');

          if(check == "0")
          {
            for(var i = table.rows.length - 1; i > 0; i--)
            {
              table.deleteRow(i);
            }

            var rowCount = document.getElementById('listorder').rows.length;
            getView(myArr);
            
            view_total();
          }
          view_total();
        }
      };
      xmlhttp.open("GET","deleteOrderBaru.php?ids="+idss+"&idorder="+users,true);
      xmlhttp.send();
    }

    function getView(myArr)
    {
      var table = document.getElementById('listorder');
      for (var i = 0; i < myArr.length; i++) 
      {
        var row = table.insertRow(i + 1)
        row.setAttribute("class", "danger");

        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);

        var nomors = i + 1;
        var idss = "qtys"+nomors
        var namsa = "["+myArr[i]['nama']+"] "+myArr[i]['nampro'];
        var qtyss = '<input type="number" id="'+nomors+'" oninput="editOrder('+nomors+','+myArr[i]['qtys']+')" onclick="kliks('+nomors+','+myArr[i]['qtys']+')" class="form-control" value="'+myArr[i]['qtys']+'" />';
        if(parseInt(myArr[i]['harga']) < 0)
        {
          qtyss = '<input type="number" id="'+nomors+'" oninput="editOrder('+nomors+','+myArr[i]['qtys']+')" onclick="kliks('+nomors+','+myArr[i]['qtys']+')" class="form-control" value="'+myArr[i]['qtys']+'" readonly/>';
        }

        if(myArr[i]['nampro'] == null)
        {
          cell5.innerHTML =  "";
          namsa = myArr[i]['nama'];
        }
        else
        {
          cell5.innerHTML =  "<a href='javascript:void(0)' onclick='newDelete("+nomors+")' ><i class='fa fa-trash-o'></i></a>";
        }
        cell0.innerHTML =  nomors;
        cell1.innerHTML =  namsa;
        cell2.innerHTML =  qtyss;
        cell2.style.textAlign = "center";
        cell3.innerHTML =  parseInt(myArr[i]['harga']).toLocaleString();
        cell3.style.textAlign = "center";
        cell4.innerHTML =  (parseInt(myArr[i]['harga']) * parseInt(myArr[i]['qtys'])).toLocaleString();
        cell4.style.textAlign = "center";

        view_total();
      }
    }

    function kliks(ids)
    {
      var input = document.getElementById(ids);
      input.focus();
      //input.select();
    }

    function gantiWarna(nama)
    {
      var product = "semua";
      document.getElementById(nama).className = "btn btn-success pos-tip btn-flat";
      if(nama == "car")
      {
        product = "CAR";
        document.getElementById("det").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("paket").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("semua").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("mak").className = "btn btn-default pos-tip btn-flat";
      }
      if(nama == "det")
      {
        product = "DET";
        document.getElementById("car").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("paket").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("semua").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("mak").className = "btn btn-default pos-tip btn-flat";
      }
      if(nama == "mak")
      {
        product = "MAK";
        document.getElementById("car").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("paket").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("semua").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("det").className = "btn btn-default pos-tip btn-flat";
      }
      if(nama == "semua")
      {
        product = "semua";
        document.getElementById("car").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("paket").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("det").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("mak").className = "btn btn-default pos-tip btn-flat";
      }
      if(nama == "paket")
      {
        product = "paket";
        document.getElementById("car").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("semua").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("det").className = "btn btn-default pos-tip btn-flat";
        document.getElementById("mak").className = "btn btn-default pos-tip btn-flat";
      }

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() 
      {
        if (this.readyState == 4 && this.status == 200) {
          var myArr = JSON.parse(this.responseText);

          var totalList = myArr.length;
          for (var i = 0; i < myArr.length; i++) 
          {
            var dys = myArr[i]['kode'];
            document.getElementById(dys).style.display = myArr[i]['hasil'];
          }
        }
      };
      xmlhttp.open("GET","edit/cekProduct.php?id="+product,true);
      xmlhttp.send();
      //alert('edit/cekProduct.php?id='+product);
    }
  </script>
  <aside class="control-sidebar control-sidebar-dark" id="categories-list">
    <div class="tab-content sb">
      <div class="tab-pane active sb" id="control-sidebar-home-tab">
        <div class="clearfix sb"></div>
        <div id="category-sidebar-menu">
          <ul class="control-sidebar-menu" id="orderPending">
            <!-- <li>
              <a href="#" class="category" id="2"><div class="menu-icon"><img src="../logo.png" alt="" class="img-thumbnail img-responsive"></div><div class="menu-info"><h4 class="control-sidebar-subheading">Muhamad Iqbal</h4><p>Rp. 130.000</p></div>
              </a>
            </li>   
            <li>
              <a href="#" class="category" id="2"><div class="menu-icon"><img src="../logo.png" alt="" class="img-thumbnail img-responsive"></div><div class="menu-info"><h4 class="control-sidebar-subheading">Muhamad Iqbal</h4><p>Rp. 130.000</p></div>
              </a>
            </li>   
            <li>
              <a href="#" class="category" id="2"><div class="menu-icon"><img src="../logo.png" alt="" class="img-thumbnail img-responsive"></div><div class="menu-info"><h4 class="control-sidebar-subheading">Muhamad Iqbal</h4><p>Rp. 130.000</p></div>
              </a>
            </li>   -->                
          </ul>
        </div>
      </div>
    </div>
  </aside>
  <div class="control-sidebar-bg sb"></div>
</div>
</div>
<script type="text/javascript">
  orderPending();
  function orderPending()
  {
    var ul = document.getElementById("orderPending");
    ul.replaceChildren();
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {

        var myArr = JSON.parse(this.responseText);
        var totalList = myArr.length;
        for (var i = 0; i < totalList; i++) 
        {

          var li = document.createElement("li");
          var totals = parseInt(myArr[i]['total']).toLocaleString();
          var namas = myArr[i]['customer'];
          if(namas == "")
          {
            namas = "Tidak Ada Nama";
          }
          li.innerHTML = '<a href="#" onClick="viewPending('+myArr[i]['id_order']+',<?php echo $user_data['no']?>)" class="category"><div class="menu-icon"><img src="../logo.png" alt="" class="img-thumbnail img-responsive"></div><div class="menu-info"><h4 class="control-sidebar-subheading">'+namas+'</h4><p>Rp. '+totals+'</p></div></a>';
          ul.appendChild(li);
        }
        document.getElementById("warningpending").innerHTML = totalList;
      }
    };
    xmlhttp.open("GET","orderPending.php",true);
    xmlhttp.send();
  }

  function viewPending(ids,user)
  {
    var answer = window.confirm("Apakah anda yakin menghapus item pada order list ?");
    if (answer) {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) 
        {
          var myArr = JSON.parse(this.responseText);
          var totalList = myArr.length;
          for (var i = 0; i < totalList; i++) 
          {
            for (var ih = 0; ih < parseInt(myArr[i]['qty']); ih++) 
            {
              inputProduct(myArr[i]['barcode']);
            }
            document.getElementById("id_order").value = myArr[i]['id_order'];
          }
          orderPending();
        }
      };
      xmlhttp.open("GET","deleteTmp.php?user="+user+"&ids="+ids,true);
      xmlhttp.send();
    }
  }
</script>
<div id="order_tbl" style="display:none;"><span id="order_span"></span>
  <table id="order-table" class="prT table table-striped table-condensed" style="width:100%;margin-bottom:0;"></table>
</div>
<div id="bill_tbl" style="display:none;"><span id="bill_span"></span>
  <table id="bill-table" width="100%" class="prT table table-striped table-condensed" style="width:100%;margin-bottom:0;"></table>
  <table id="bill-total-table" width="100%" class="prT table table-striped table-condensed" style="width:100%;margin-bottom:0;"></table>
</div>

<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>

<div class="modal" data-easein="flipYIn" id="popKembalian" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="dsModalLabel">Input Kembalian</h4>
      </div>
      <div class="modal-body">
        <a style="font-size: 20px;color: black" id="txtBayars"><strong>Total Bayar : <span style="float: right;">Rp. 0</span></strong></a>
        <br>
        <a style="font-size: 15px;color: grey" id="txtTagihan"><strong>Total Tagihan : <span style="float: right;">Rp. 0</span></strong></a>
        <hr>
        <input type="hidden" id="txtTempKembalian" value="0">
        <div class="col-sm-6">
          <div class="col-sm-4">
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(1)">1</button>
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(4)">4</button>
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(7)">7</button>
            <button style="width: 80px;height: 50px" onclick="hapusSemuaNya()">AC</button>
          </div>
          <div class="col-sm-4">
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(2)">2</button>
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(5)">5</button>
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(8)">8</button>
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(0)">0</button>
          </div>
          <div class="col-sm-4">
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(3)">3</button>
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(6)">6</button>
            <button style="width: 80px;height: 50px" onclick="inputSatuSatu(9)">9</button>
            <button style="width: 80px;height: 50px" onclick="hapusSatu()">DEL</button>
          </div>
        </div>
        <div class="col-sm-6" >
          <div class="col-sm-4">
            <button style="width: 80px;height: 50px" onclick="totalBayar(20000)">20.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(100000)">100.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(250000)">250.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(400000)">400.000</button>
          </div>
          <div class="col-sm-4">
            <button style="width: 80px;height: 50px" onclick="totalBayar(50000)">50.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(150000)">150.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(300000)">300.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(450000)">450.000</button>
          </div>
          <div class="col-sm-4">
            <button style="width: 80px;height: 50px" onclick="totalBayar(75000)">75.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(200000)">200.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(350000)">350.000</button>
            <button style="width: 80px;height: 50px" onclick="totalBayar(500000)">500.000</button>
          </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <hr>
        <a style="font-size: 30px;color: red" id="txtKembalians"><strong>Total Kembalian : <span style="float: right;">Rp. 0</span></strong></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Tutup</button>
        <!-- <button type="button" id="updateDiscount" class="btn btn-primary btn-sm">Simpan</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal" data-easein="flipYIn" id="popLevel" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="dsModalLabel"><strong>Level Sales - <span style="color: red">Tanggal <?php echo $tanggalSekarang?></span></strong></h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-4">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[0]['urutanLevel'].' - '.$dataJualanSales[0]['namaLevel']?></strong></h5>
          <img src="../images/product/diamond.png" width="100px" height="100px" style="display: block;margin-left: auto;margin-right: auto;width: 50%;">
          <br>
          <h4 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[0]['namaSales']?></strong></h4>
          <h3 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[0]['totalPenjualan']?></strong></h3>
        </div>
        <div class="col-sm-4">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[1]['urutanLevel'].' - '.$dataJualanSales[1]['namaLevel']?></strong></h5>
          <img src="../images/product/ruby.png" width="100px" height="100px" style="display: block;margin-left: auto;margin-right: auto;width: 50%;">
          <br>
          <h4 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[1]['namaSales']?></strong></h4>
          <h3 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[1]['totalPenjualan']?></strong></h3>
        </div>
        <div class="col-sm-4">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[2]['urutanLevel'].' - '.$dataJualanSales[2]['namaLevel']?></strong></h5>
          <img src="../images/product/emerald.jpg" width="100px" height="100px" style="display: block;margin-left: auto;margin-right: auto;width: 50%;">
          <br>
          <h4 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[2]['namaSales']?></strong></h4>
          <h3 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[2]['totalPenjualan']?></strong></h3>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <hr>
        <div class="col-sm-2">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[3]['urutanLevel'].' - '.$dataJualanSales[3]['namaLevel']?></strong></h5>
          <h5 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[3]['namaSales']?></strong></h5>
          <h5 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[3]['totalPenjualan']?></strong></h5>
        </div>
        <div class="col-sm-2">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[4]['urutanLevel'].' - '.$dataJualanSales[4]['namaLevel']?></strong></h5>
          <h5 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[4]['namaSales']?></strong></h5>
          <h5 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[4]['totalPenjualan']?></strong></h5>
        </div>
        <div class="col-sm-2">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[5]['urutanLevel'].' - '.$dataJualanSales[5]['namaLevel']?></strong></h5>
          <h5 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[5]['namaSales']?></strong></h5>
          <h5 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[5]['totalPenjualan']?></strong></h5>
        </div>
        <div class="col-sm-2">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[6]['urutanLevel'].' - '.$dataJualanSales[6]['namaLevel']?></strong></h5>
          <h5 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[6]['namaSales']?></strong></h5>
          <h5 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[6]['totalPenjualan']?></strong></h5>
        </div>
        <div class="col-sm-2">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[7]['urutanLevel'].' - '.$dataJualanSales[7]['namaLevel']?></strong></h5>
          <h5 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[7]['namaSales']?></strong></h5>
          <h5 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[7]['totalPenjualan']?></strong></h5>
        </div>
        <div class="col-sm-2">
          <h5 style="text-align: center;color: black"><strong>Level <?php echo $dataJualanSales[8]['urutanLevel'].' - '.$dataJualanSales[8]['namaLevel']?></strong></h5>
          <h5 style="text-align: center;color: green"><strong><?php echo $dataJualanSales[8]['namaSales']?></strong></h5>
          <h5 style="text-align: center;color: red"><strong><?php echo $dataJualanSales[8]['totalPenjualan']?></strong></h5>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  function inputTotals()
  {
    var totals = document.getElementById("total").value;
    document.getElementById("txtTagihan").innerHTML = '<strong>Total Tagihan : <span style="float: right;">Rp. '+parseInt(totals).toLocaleString()+'</span></strong>';
    hapusSemuaNya();

  }
  function inputSatuSatu(nilai)
  {
    var totals = document.getElementById("total").value;
    if(document.getElementById("txtTempKembalian").value == "0")
    {
      var isiKemb = nilai;
      var kembali = parseInt(nilai) - parseInt(totals);
      document.getElementById("txtBayars").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+parseInt(isiKemb).toLocaleString()+'</span></strong>';
      document.getElementById("txtTempKembalian").value = isiKemb;
    }
    else
    {
      var isiKemb = document.getElementById("txtTempKembalian").value + nilai;
      var kembali = parseInt(isiKemb) - parseInt(totals);
      document.getElementById("txtBayars").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+parseInt(isiKemb).toLocaleString()+'</span></strong>';
      document.getElementById("txtKembalians").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+kembali.toLocaleString()+'</span></strong>';
      document.getElementById("txtTempKembalian").value = isiKemb;
    }
  }

  function totalBayar(nilai)
  {
    var totals = document.getElementById("total").value;
    document.getElementById("txtTempKembalian").value = nilai;
    var kembali = parseInt(nilai) - parseInt(totals);
    document.getElementById("txtBayars").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+nilai.toLocaleString()+'</span></strong>';
    document.getElementById("txtKembalians").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+kembali.toLocaleString()+'</span></strong>';
  }

  function hapusSemuaNya()
  {
    document.getElementById("txtTempKembalian").value = '0';
    document.getElementById("txtBayars").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. 0</span></strong>';
    document.getElementById("txtKembalians").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. 0</span></strong>';
  }

  function hapusSatu()
  {
    var isiKemb = document.getElementById("txtTempKembalian").value;
    var totals = document.getElementById("total").value;
    if(isiKemb.length != 1)
    {
      var finalTxt = isiKemb.substring(0, isiKemb.length - 1);
      var kembali = parseInt(finalTxt) - parseInt(totals);
      document.getElementById("txtTempKembalian").value = finalTxt;
      document.getElementById("txtBayars").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+parseInt(finalTxt).toLocaleString()+'</span></strong>';
      document.getElementById("txtKembalians").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+kembali.toLocaleString()+'</span></strong>';
    }
    else
    {
      document.getElementById("txtTempKembalian").value = '0';
      var kembali = parseInt("0") - parseInt(totals);
      document.getElementById("txtBayars").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. 0</span></strong>';
      document.getElementById("txtKembalians").innerHTML = '<strong>Total Bayar : <span style="float: right;">Rp. '+kembali.toLocaleString()+'</span></strong>';
    }

  }
</script>

<div class="modal" data-easein="flipYIn" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="myModalLabel">Jual Gift Card</h4>
      </div>
      <div class="modal-body">
        <p>Mohon isi informasi di bawah ini</p>

        <div class="alert alert-danger gcerror-con" style="display: none;">
          <button data-dismiss="alert" class="close" type="button">×</button>
          <span id="gcerror"></span>
        </div>
        <div class="form-group">
          <label for="gccard_no">Nomor Kartu</label> *
          <div class="input-group">
            <input type="text" name="gccard_no" value=""  class="form-control" id="gccard_no" />
            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;"><a href="#" id="genNo"><i class="fa fa-cogs"></i></a></div>
          </div>
        </div>
        <input type="hidden" name="gcname" value="Gift Card" id="gcname"/>
        <div class="form-group">
          <label for="gcvalue">Nilai</label> *
          <input type="text" name="gcvalue" value=""  class="form-control" id="gcvalue" />
        </div>
        <div class="form-group">
          <label for="gcprice">Harga</label> *
          <input type="text" name="gcprice" value=""  class="form-control" id="gcprice" />
        </div>
        <div class="form-group">
          <label for="gcexpiry">Tanggal Kadaluarsa</label>                    <input type="text" name="gcexpiry" value=""  class="form-control" id="gcexpiry" />
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="button" id="addGiftCard" class="btn btn-primary">Jual Gift Card</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" data-easein="flipYIn" id="dsModal" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="dsModalLabel">Diskon (5 atau 5%)</h4>
      </div>
      <div class="modal-body">
        <input type='text' class='form-control input-sm kb-pad' id='get_ds' onClick='this.select();' value=''>

        <label class="checkbox" for="apply_to_order">
          <input type="radio" name="apply_to" value="order" id="apply_to_order" checked="checked"/>
        Terapkan di total pesanan                </label>
        <label class="checkbox" for="apply_to_products">
          <input type="radio" name="apply_to" value="products" id="apply_to_products"/>
        Terapkan untuk semua item pesanan                </label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Tutup</button>
        <button type="button" id="updateDiscount" class="btn btn-primary btn-sm">Perbarui</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" data-easein="flipYIn" id="tsModal" tabindex="-1" role="dialog" aria-labelledby="tsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="tsModalLabel">Pajak (5 atau 5%)</h4>
      </div>
      <div class="modal-body">
        <input type='text' class='form-control input-sm kb-pad' id='get_ts' onClick='this.select();' value=''>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Tutup</button>
        <button type="button" id="updateTax" class="btn btn-primary btn-sm">Perbarui</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" data-easein="flipYIn" id="noteModal" tabindex="-1" role="dialog" aria-labelledby="noteModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="noteModalLabel">Catatan</h4>
      </div>
      <div class="modal-body">
        <textarea name="snote" id="snote" class="pa form-control kb-text"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Tutup</button>
        <button type="button" id="update-note" class="btn btn-primary btn-sm">Perbarui</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" data-easein="flipYIn" id="proModal" tabindex="-1" role="dialog" aria-labelledby="proModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-primary">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="proModalLabel">
        Pembayaran                </h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-striped">
          <tr>
            <th style="width:25%;">Harga Bersih</th>
            <th style="width:25%;"><span id="net_price"></span></th>
            <th style="width:25%;">Pajak Produk</th>
            <th style="width:25%;"><span id="pro_tax"></span> <span id="pro_tax_method"></span></th>
          </tr>
        </table>
        <input type="hidden" id="row_id" />
        <input type="hidden" id="item_id" />
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="nPrice">Harga Unit</label>                            <input type="text" class="form-control input-sm kb-pad" id="nPrice" onClick="this.select();" placeholder="Harga Baru">
            </div>
            <div class="form-group">
              <label for="nDiscount">Diskon</label>                            <input type="text" class="form-control input-sm kb-pad" id="nDiscount" onClick="this.select();" placeholder="Diskon">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="nQuantity">Jumlah</label>                            <input type="text" class="form-control input-sm kb-pad" id="nQuantity" onClick="this.select();" placeholder="Jumlah Saat Ini">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label for="nComment">Komen</label>                            <textarea class="form-control kb-text" id="nComment"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success" id="editItem">Perbarui</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" data-easein="flipYIn" id="susModal" tabindex="-1" role="dialog" aria-labelledby="susModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="susModalLabel">Penjualan Tertangguhkan</h4>
      </div>
      <div class="modal-body">
        <p>Tipe Catatan Referensi</p>

        <div class="form-group">
          <label for="reference_note">Catatan Referensi</label>                    <input type="text" name="reference_note" value=""  class="form-control kb-text" id="reference_note" />
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Tutup </button>
        <button type="button" id="suspend_sale" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>



<div class="modal" data-easein="flipYIn" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="saleModalLabel" aria-hidden="true"></div>
<div class="modal" data-easein="flipYIn" id="opModal" tabindex="-1" role="dialog" aria-labelledby="opModalLabel" aria-hidden="true"></div>

<div class="modal" data-easein="flipYIn" id="payModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-success">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="payModalLabel">
        Pembayaran                </h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-9">
            <div class="font16">
              <table class="table table-bordered table-condensed" style="margin-bottom: 0;">
                <tbody>
                  <tr>
                    <td width="25%" style="border-right-color: #FFF !important;">Total Item</td>
                    <td width="25%" class="text-right"><span id="item_counts">0 Items</span></td>
                    <td width="25%" style="border-right-color: #FFF !important;">Total Dibayar</td>
                    <td width="25%" class="text-right"><span id="twts">0.00</span></td>
                  </tr>
                  <tr>
                    <td style="border-right-color: #FFF !important;">Total Pembayaran</td>
                    <td class="text-right"><span id="total_paying">0.00</span></td>
                    <td style="border-right-color: #FFF !important;">Saldo</td>
                    <td class="text-right"><span id="balance">0.00</span></td>
                  </tr>
                </tbody>
              </table>
              <div class="clearfix"></div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group">
                  <label for="note">Catatan</label>                                    <textarea name="note" id="note" class="pa form-control kb-text"></textarea>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="amount">Bayar</label>                                    <input name="amount" type="text" id="amount"
                  class="pa form-control kb-pad amount"/>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="paid_by">Membayar dengan</label>                                    <select id="paid_by" class="form-control paid_by select2" style="width:100%;">
                    <option value="cash">Tunai</option>
                    <option value="CC">Kartu Kredit</option>
                    <option value="cheque">Cek</option>
                    <option value="gift_card">Gift Card</option>
                    <option value="stripe">Garis</option>                                        <option value="other">Lainnya</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group gc" style="display: none;">
                  <label for="gift_card_no">Nomor Gift Card</label>                                    <input type="text" id="gift_card_no"
                  class="pa form-control kb-pad gift_card_no gift_card_input"/>

                  <div id="gc_details"></div>
                </div>
                <div class="pcc" style="display:none;">
                  <div class="form-group">
                    <input type="text" id="swipe" class="form-control swipe swipe_input"
                    placeholder="Gesek kartu kemudian tulis kode keamanan secara manual"/>
                  </div>
                  <div class="row">
                    <div class="col-xs-6">
                      <div class="form-group">
                        <input type="text" id="pcc_no"
                        class="form-control kb-pad"
                        placeholder="No Kartu Kredit"/>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group">

                        <input type="text" id="pcc_holder"
                        class="form-control kb-text"
                        placeholder="Nama Pemegang Kartu"/>
                      </div>
                    </div>
                    <div class="col-xs-3">
                      <div class="form-group">
                        <select id="pcc_type"
                        class="form-control pcc_type select2"
                        placeholder="Tipe Kartu">
                        <option value="Visa">Visa</option>
                        <option
                        value="MasterCard">MasterCard</option>
                        <option value="Amex">Amex</option>
                        <option
                        value="Discover">Discover</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">
                      <input type="text" id="pcc_month"
                      class="form-control kb-pad"
                      placeholder="Bulan"/>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">

                      <input type="text" id="pcc_year"
                      class="form-control kb-pad"
                      placeholder="Tahun"/>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group">

                      <input type="text" id="pcc_cvv2"
                      class="form-control kb-pad"
                      placeholder="CVV2"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="pcheque" style="display:none;">
                <div class="form-group"><label for="cheque_no">Nomor Cek</label>                                    <input type="text" id="cheque_no"
                  class="form-control cheque_no kb-text"/>
                </div>
              </div>
              <div class="pcash">
                <div class="form-group"><label for="payment_note">Catatan Pembayaran</label>                                    <input type="text" id="payment_note"
                  class="form-control payment_note kb-text"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-3 text-center">
          <!-- <span style="font-size: 1.2em; font-weight: bold;">Quick Cash</span> -->

          <div class="btn-group btn-group-vertical" style="width:100%;">
            <button type="button" class="btn btn-info btn-block quick-cash" id="quick-payable">0.00
            </button>
            <button type="button" class="btn btn-block btn-warning quick-cash">500</button><button type="button" class="btn btn-block btn-warning quick-cash">5,000</button><button type="button" class="btn btn-block btn-warning quick-cash">25,000</button><button type="button" class="btn btn-block btn-warning quick-cash">50,000</button><button type="button" class="btn btn-block btn-warning quick-cash">100,000</button><button type="button" class="btn btn-block btn-warning quick-cash"></button>                        <button type="button" class="btn btn-block btn-danger"
            id="clear-cash-notes">Kosongkan</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Tutup </button>
      <button class="btn btn-primary" id="submit-sale">Submit</button>
    </div>
  </div>
</div>
</div>

<div class="modal" data-easein="flipYIn" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="cModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-primary">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="cModalLabel">
        Tambahkan Pelanggan                </h4>
      </div>
      <form action="https://ecm.my.id/pos/add_customer" id="customer-form" method="post" accept-charset="utf-8">
        <div class="modal-body">
          <div id="c-alert" class="alert alert-danger" style="display:none;"></div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                <label class="control-label" for="code">
                Nama                            </label>
                <input type="text" name="name" value=""  class="form-control input-sm kb-text" id="cname" />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="cemail">
                Alamat Email                            </label>
                <input type="text" name="email" value=""  class="form-control input-sm kb-text" id="cemail" />
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="phone">
                Telepon                            </label>
                <input type="text" name="phone" value=""  class="form-control input-sm kb-pad" id="cphone" />
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="cf1">
                Custom Field 1                            </label>
                <input type="text" name="cf1" value=""  class="form-control input-sm kb-text" id="cf1" />
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label class="control-label" for="cf2">
                Custom Field 2                            </label>
                <input type="text" name="cf2" value=""  class="form-control input-sm kb-text" id="cf2" />
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer" style="margin-top:0;">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"> Tutup </button>
          <button type="submit" class="btn btn-primary" id="add_customer"> Tambahkan Pelanggan </button>
        </div>
      </form>        </div>
    </div>
  </div>

  <div class="modal" data-easein="flipYIn" id="posModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
  <div class="modal" data-easein="flipYIn" id="posModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true"></div>

  <script type="text/javascript">
    view_total();
    var base_url = 'https://ecm.my.id/', assets = 'https://ecm.my.id/themes/default/assets/';
    var dateformat = 'D j M Y', timeformat = 'h:i A';
    var Settings = {"logo":"labella_22.png","site_name":"PT.EKA CIPTA MANDIRI","tel":"087874973000","dateformat":"D j M Y","timeformat":"h:i A","language":"english","theme":"default","mmode":"0","captcha":"0","currency_prefix":"IDR","default_customer":"1","default_tax_rate":"0%","rows_per_page":"10","total_rows":"30","header":"","footer":"","bsty":"3","display_kb":"0","default_category":"0","default_discount":"0","item_addition":"1","barcode_symbology":"","pro_limit":"10","decimals":"2","thousands_sep":",","decimals_sep":".","focus_add_item":"ALT+F1","add_customer":"ALT+F2","toggle_category_slider":"ALT+F10","cancel_sale":"ALT+F5","suspend_sale":"ALT+F6","print_order":"ALT+F11","print_bill":"ALT+F12","finalize_sale":"ALT+F8","today_sale":"Ctrl+F1","open_hold_bills":"Ctrl+F2","close_register":"ALT+F7","java_applet":"0","receipt_printer":"","pos_printers":"","cash_drawer_codes":"","char_per_line":"42","rounding":"1","pin_code":"abdbeb4d8dbe30df8430a8394b7218ef","purchase_code":"cb036771-3c08-4c1c-a028-78355d9abd6f","envato_username":"sumintar","theme_style":"blue-light","after_sale_page":"0","overselling":"1","multi_store":"1","qty_decimals":"2","symbol":"Rp.","sac":"0","display_symbol":"1","remote_printing":"1","printer":null,"order_printers":"null","auto_print":"0","local_printers":"1","selected_language":"indonesian"};
    var sid = false, username = 'admin', spositems = {};
    $(window).load(function () {
      $('#mm_pos').addClass('active');
      $('#pos_index').addClass('active');
    });
    var pro_limit = 10, java_applet = 0, count = 1, total = 0, an = 1, p_page = 0, page = 0, cat_id = 0, tcp = 35;
    var gtotal = 0, order_discount = 0, order_tax = 0, protect_delete = 0;
    var order_data = {}, bill_data = {};
    var lang = new Array();
    lang['code_error'] = 'Kode Error';
    lang['r_u_sure'] = '<strong>Apakah Anda yakin?</strong>';
    lang['please_add_product'] = 'Mohon tambahkan produk';
    lang['paid_less_than_amount'] = 'Jumlah dibayar lebih kecil dari pembayaran';
    lang['x_suspend'] = 'Penjualan tidak dapat ditangguhkan';
    lang['discount_title'] = 'Diskon (5 atau 5%)';
    lang['update'] = 'Perbarui';
    lang['tax_title'] = 'Pajak (5 atau 5%)';
    lang['leave_alert'] = 'Anda akan kehilangan data, apa Anda yakin?';
    lang['close'] = 'Tutup';
    lang['delete'] = 'Hapus';
    lang['no_match_found'] = 'Tidak ditemukan kecocokan';
    lang['wrong_pin'] = 'Pin Salah';
    lang['file_required_fields'] = 'Mohon isi field yang diharuskan';
    lang['enter_pin_code'] = 'Masukkan kode pin';
    lang['incorrect_gift_card'] = 'Nomor Gift card salah atau kartu telah digunakan.';
    lang['card_no'] = 'Nomor Kartu';
    lang['value'] = 'Nilai';
    lang['balance'] = 'Saldo';
    lang['unexpected_value'] = 'Tersedia Nilai Tak Terduga!';
    lang['inclusive'] = 'Inclusif';
    lang['exclusive'] = 'Eksklusif';
    lang['total'] = 'Total';
    lang['total_items'] = 'Total Item';
    lang['order_tax'] = 'Pajak Pesanan';
    lang['order_discount'] = 'Anda Hemat';
    lang['total_payable'] = 'Total Dibayar';
    lang['rounding'] = 'Pembulatan';
    lang['grand_total'] = 'Grand Total';
    lang['register_open_alert'] = 'Kasir belum di tutup, Anda yakin untuk keluar?';
    lang['discount'] = 'Diskon';
    lang['order'] = 'Pesanan';
    lang['bill'] = 'Tagihan';
    lang['merchant_copy'] = 'Merchant Copy';

    $(document).ready(function() {

      if (get('rmspos')) {
        if (get('spositems')) { remove('spositems'); }
        if (get('spos_discount')) { remove('spos_discount'); }
        if (get('spos_tax')) { remove('spos_tax'); }
        if (get('spos_note')) { remove('spos_note'); }
        if (get('spos_customer')) { remove('spos_customer'); }
        if (get('amount')) { remove('amount'); }
        remove('rmspos');
      }
      if (! get('spos_discount')) {
        store('spos_discount', '0');
        $('#discount_val').val('0');
      }
      if (! get('spos_tax')) {
        store('spos_tax', '0%');
        $('#tax_val').val('0%');
      }

      if (ots = get('spos_tax')) {
        $('#tax_val').val(ots);
      }
      if (ods = get('spos_discount')) {
        $('#discount_val').val(ods);
      }
      bootbox.addLocale('bl',{OK:'OK',CANCEL:'Tidak',CONFIRM:'Ya'});
      bootbox.setDefaults({closeButton:false,locale:"bl"});
    });
  </script>

  <script type="text/javascript">
    var socket = null;
    function printBill(bill) {
      if (Settings.remote_printing == 1) {
        Popup($('#bill_tbl').html());
      } else if (Settings.remote_printing == 2) {
        if (socket.readyState == 1) {
          var socket_data = {'printer': '', 'logo': 'https://ecm.my.id/uploads/f233a43217d25622a1ac1eaee0dcf637.png', 'text': bill};
          socket.send(JSON.stringify({
            type: 'print-receipt',
            data: socket_data
          }));
          return false;
        } else {
          bootbox.alert('Tidak dapat terhubung ke socket, mohon pastikan bahwa server berjalan dengan baik.');
          return false;
        }
      }
    }
    var order_printers = '';
    function printOrder(order) {
      if (Settings.remote_printing == 1) {
        Popup($('#order_tbl').html());
      } else if (Settings.remote_printing == 2) {
        if (socket.readyState == 1) {
          if (order_printers == '') {

            var socket_data = { 'printer': false, 'order': true,
            'logo': 'https://ecm.my.id/uploads/f233a43217d25622a1ac1eaee0dcf637.png',
            'text': order };
            socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));

          } else {

            $.each(order_printers, function() {
              var socket_data = {'printer': this, 'logo': 'https://ecm.my.id/uploads/f233a43217d25622a1ac1eaee0dcf637.png', 'text': order};
              socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));
            });

          }
          return false;
        } else {
          bootbox.alert('Tidak dapat terhubung ke socket, mohon pastikan bahwa server berjalan dengan baik.');
          return false;
        }
      }
    }

    function Popup(data) {
      var mywindow = window.open('', 'spos_print', 'height=700,width=500');
      mywindow.document.write('<html><head><title>Print</title>');
      mywindow.document.write('<link rel="stylesheet" href="https://ecm.my.id/themes/default/assets/bootstrap/css/bootstrap.min.css" type="text/css" />');
      mywindow.document.write('</head><body >');
      mywindow.document.write(data);
      mywindow.document.write('</body></html>');
      mywindow.print();
      mywindow.close();
      return true;
    }

    function printPage() {
      var rowCounts = document.getElementById('listorder').rows.length;
      if(rowCounts > 1)
      {
        var namaPembeli = document.getElementById('hold_ref').value;
        if(namaPembeli != "")
        {
          var id_order = document.getElementById("id_order").value;
          var nama = document.getElementById("hold_ref").value;
          var sales = document.getElementById("spos_customer").value;
          var pembayaran = document.getElementById("pembayaran").value;
          var detail = document.getElementById("detail").value;
          var total = document.getElementById("total").value;
          var prnt = "1";
          var user_idms = document.getElementById("user_idms").value;

          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

              var myArr = JSON.parse(this.responseText);
              var totalList = myArr.length;
            }
          };

          xmlhttp.open("GET","F_insertData.php?id_order="+id_order+"&nama="+nama+"&sales="+sales+"&pembayaran="+pembayaran+"&detail="+detail+"&total="+total+"&prnt="+prnt+"&user_idms="+user_idms,true);
          xmlhttp.onload = function() {
            var table = document.getElementById('listorder');
            for(var i = table.rows.length - 1; i > 0; i--)
            {
              table.deleteRow(i);
            }
		//cekLevel(sales,total);
            printPageLama(id_order);
            
          }
          xmlhttp.send();
        }
        else
        {
          document.getElementById("hold_ref").focus();
          alert("Harap Input Nama Pelanggan Terlebih Dahulu");
        }
      }
      else
      {
        alert("Input Order Terlebih Dahulu");
      }
    }

    function cekLevel(sales,total)
    {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

          var myArr = JSON.parse(this.responseText);
          var totalList = myArr.length;
          if(totalList != 0)
          {
            document.getElementById('tblCongrats').style.display = "";
            document.getElementById('tblListBeli').style.display = "none";
            document.getElementById('c_nama').innerHTML = "<strong>"+myArr[0]['namaSales']+"</strong>";
            document.getElementById('c_level').innerHTML = "<strong>Telah Mencapai Level "+myArr[0]['level']+"</strong>";
            document.getElementById('c_penjualan').innerHTML = "<strong>Total Penjualan "+myArr[0]['total']+"</strong>";
          }

        }
      };
      xmlhttp.open("GET","cekLevel.php?sales="+sales+"&total="+total,true);
      xmlhttp.send();
    }

    function simpan()
    {
      var rowCounts = document.getElementById('listorder').rows.length;
      var namaPembeli = document.getElementById('hold_ref').value;
      if(namaPembeli != "")
      {
        if(rowCounts > 1)
        {
          var id_order = document.getElementById("id_order").value;
          var nama = document.getElementById("hold_ref").value;
          var sales = document.getElementById("spos_customer").value;
          var pembayaran = document.getElementById("pembayaran").value;
          var detail = document.getElementById("detail").value;
          var total = document.getElementById("total").value;
          var prnt = "1";
          var user_idms = document.getElementById("user_idms").value;

          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

              var myArr = JSON.parse(this.responseText);
              var totalList = myArr.length;
            }
          };

          xmlhttp.open("GET","F_insertData.php?id_order="+id_order+"&nama="+nama+"&sales="+sales+"&pembayaran="+pembayaran+"&detail="+detail+"&total="+total+"&prnt="+prnt+"&user_idms="+user_idms,true);
          xmlhttp.onload = function() {
            var table = document.getElementById('listorder');
            for(var i = table.rows.length - 1; i > 0; i--)
            {
              table.deleteRow(i);
            }
            document.getElementById("hold_ref").value = "";
            var today = new Date();
            var hari = today.getDate();
            var bulan = today.getMonth()+1; 
            var jam = today.getHours();
            var menit = today.getMinutes();
            var detik = today.getSeconds();
            var tahun = today.getFullYear();
            todayss = tahun+''+bulan+''+hari+''+jam+''+menit+''+detik; 
            document.getElementById("id_order").value = todayss;
            document.getElementById("counts").innerHTML = "0 Items";
            document.getElementById("totals").innerHTML = "0";
            document.getElementById("ds_cons").innerHTML = "0";
            document.getElementById("payables").innerHTML = 'Rp. 0';
            orderPending();
          }
          xmlhttp.send();
        }
        else
        {
          alert("Input Order Terlebih Dahulu");
        }
      }
      else
      {
        document.getElementById("hold_ref").focus();
        alert("Sebelum Hold / Simpan, Masukan dahulu nama pembeli, untuk mempermudah pencarian");
      }

    }

    function printPageLama(ids) {
      var mywindow = window.open('../prints/?id='+ids, 'spos_print', 'height=600,width=370');

      setTimeout(function () { mywindow.print(); }, 100);
      mywindow.onfocus = function () { setTimeout(function () { mywindow.close(); }, 200); }
      var today = new Date();
      var hari = today.getDate();
      var bulan = today.getMonth()+1; 
      var jam = today.getHours();
      var menit = today.getMinutes();
      var detik = today.getSeconds();
      var tahun = today.getFullYear();
      todayss = tahun+''+bulan+''+hari+''+jam+''+menit+''+detik; 
      document.getElementById("id_order").value = todayss;
      document.getElementById("counts").innerHTML = "0 Items";
      document.getElementById("totals").innerHTML = "0";
      document.getElementById("ds_cons").innerHTML = "0";
      document.getElementById("payables").innerHTML = 'Rp. 0';
      return true;
    };


  </script>

  <script src="js/library.js" type="text/javascript"></script>
  <script src="js/script.js" type="text/javascript"></script>
  <script src="js/pos.js" type="text/javascript"></script>
</body>
</html>
