<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content">
    <div class="row">
      <div class="col-xs-3" >
        <div class="box">
          <div class="box-header">
          
          </div>
          <div class="box-body table-responsive no-padding">
          </div>
          <br>
          <br>

        </div>
        
      </div>
      <div class="col-xs-9" style="height:100%;">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <?php if($addPM == "1"){?>
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu">
            <?php }?>
          </div>


          <div class="box-body table-responsive no-padding">

            <div class="col-xs-12">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Order</th>
                    <th>Sales</th>
                    <th>Nama Pembeli</th>
                    <th>Tanggal</th>
                    <th>Pembayaran</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  $sql1=mysql_query("select PS.*,U.nama as namsal from pos_order PS
                    inner join user U on U.no = PS.sales
                    order by status asc
                    limit 10
                    ");
                  while($data1=mysql_fetch_array($sql1))
                  {  
                    $status = "Belum Print";
                    $bayar = "Cash";
                    $warna = 'style="background-color: pink"';
                    if($data1['status'] == "2")
                    {
                      $status = "Selesai";
                      $warna = "";
                    }
                    if($data1['status'] == "3")
                    {
                      $status = '<span class="label label-danger"><strong>Cancel</strong></span>';
                      $warna = "";
                    }
                    if($data1['pembayaran'] != "1")
                    {
                      $bayar = "Kartu";
                    }

                    echo '
                    <tr>
                      <td '.$warna.'>'.$no.'</td>
                      <td '.$warna.'>'.$data1['id_order'].'</td>
                      <td '.$warna.'>'.$data1['namsal'].'</td>
                      <td '.$warna.'>'.$data1['customer'].'</td>
                      <td '.$warna.'>'.$data1['tanggal_order'].'</td>
                      <td '.$warna.'>'.$bayar.'</td>
                      <td '.$warna.'>'.number_format($data1['total']).'</td>
                      <td '.$warna.'>'.$status.'</td>    
                      <td '.$warna.'><a href="./?id='.$data1['no'].'">Manage</a></td>
                    </tr>
                    ';
                    $no++;
                  }
                  ?>
                </tbody>
              </table>
            </div>

          </div>
          <br>
          <br>
          
        </div>
        
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>

</div>