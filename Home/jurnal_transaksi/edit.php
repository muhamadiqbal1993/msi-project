<?php

$dataEdit = mysql_fetch_array(mysql_query("select * from level_sales where no = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-md-6">

								<div class="form-group">
									<label>Nama Level</label>
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $dataEdit['nama'];?>">
								</div>
								<div class="form-group">
									<label>Icon</label>
									
									<p></p>
									<img id="photo1" src="../images/product/<?php echo $dataEdit['icon'];?>" style="height: 200px;width: 200px"/>
									<p></p>
									<div class="btn btn-default btn-file" id="photos">
										<i class="fa fa-image"></i> Pilih Photo
										<input type="file" name="photos" onchange="viewImage(this);">
									</div>
									<p></p>
									<p></p>
									<p></p>
									<p></p>
									
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Urutan Level</label>
									<input type="text" name="urutan" class="form-control" required="" value="<?php echo $dataEdit['urutan'];?>">
								</div>
								<div class="form-group">
									<label>Nominal Penjualan</label>
									<input type="number" name="nilai" class="form-control" required="" value="<?php echo $dataEdit['nilai'];?>">
								</div>
							</div>
						</div>
					</form>
					<br>
					<br>
				</div>
				<?php 
				$nama_modulnya = 'level_sales';
				include '../headfoot/history.php';
				?>
			</div>
		</div>
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}

	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(200)
				.height(200);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>