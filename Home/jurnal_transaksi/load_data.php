<?php
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_DEPRECATED);

include '../function/connect_ajax_lama.php';

$db = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$table = <<<EOT
(select JI.*,CONCAT(JI.account, " - ", C.nama) as namjur,DATE_FORMAT(JI.tanggal, '%d %M %Y') as new_tanggal,FORMAT(debit,0) as new_debit,FORMAT(abs(credit),0) as new_credit from jurnal_item JI
	inner join coa C on C.id = JI.account ) viewData
EOT;
$primaryKey = 'no';

$columns = array(
	array( 'db' => 'no',  'dt' => 0 ),
	array( 'db' => 'new_tanggal', 'dt' => 1 ),
	array( 'db' => 'namjur', 'dt' => 2 ),
	array( 'db' => 'ref',  'dt' => 3 ),
	array( 'db' => 'new_debit',  'dt' => 4 ),
	array( 'db' => 'new_credit',  'dt' => 5 ),
	);
// SQL server connection information
$sql_details = array(
	'user' => $dbuser,
	'pass' => $dbpass,
	'db'   => $dbname,
	'host' => $dbhost
	);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );
// require '../../config/ssp.class.php';
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	);

	?>


