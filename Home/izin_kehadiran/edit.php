<?php

$izin = "";
$dari = "";
$sampai = "";
$hari = "";
$keterangan = "";

$sql1=mysql_query("select *,DATE_FORMAT(dari, '%m/%d/%Y') as daris,DATE_FORMAT(sampai, '%m/%d/%Y') as sampais from izin where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$izin = $data1['id_izin'];
	$dari = $data1['daris'];
	$hari = $data1['hari'];
	$sampai = $data1['sampais'];
	$keterangan = $data1['keterangan'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>

						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<input type="hidden" name="input" class="form-control" value="2">
								<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
								<div class="form-group">
									<label>Jenis Izin</label>
									<select class="form-control select2" name="izin" style="width: 100%;" required="">
										<?php
										$sql1=mysql_query("select * from jenis_izin where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" <?php if($izin == $data1['no']){echo "selected";}?>><?php echo $data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								<div class="form-group">
									<label>Tanggal Dari</label>
									<input type="text" name="dari" id="datepicker" class="form-control" required="" value="<?php echo $dari?>">
								</div>
								<div class="form-group">
									<label>Tanggal Sampai</label>
									<input type="text" name="sampai" id="datepicker1" class="form-control" required="" value="<?php echo $sampai?>">
								</div>
								

								<!-- /.form-group -->
								
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Total Hari</label>
									<select class="form-control select2" name="hari" style="width: 100%;" required="">
										<option value="0.5" <?php if($hari == "0.5"){echo "selected";}?>>0.5 Hari</option>
										<option value="1" <?php if($hari == "1"){echo "selected";}?>>1 Hari</option>
										<option value="2" <?php if($hari == "2"){echo "selected";}?>>2 Hari</option>
										<option value="3" <?php if($hari == "3"){echo "selected";}?>>3 Hari</option>
										<option value="4" <?php if($hari == "4"){echo "selected";}?>>4 Hari</option>
										<option value="5" <?php if($hari == "5"){echo "selected";}?>>5 Hari</option>
										<option value="6" <?php if($hari == "6"){echo "selected";}?>>6 Hari</option>
										<option value="7" <?php if($hari == "7"){echo "selected";}?>>7 Hari</option>
										<option value="90" <?php if($hari == "90"){echo "selected";}?>>90 Hari</option>
									</select>
								</div>
								<div class="form-group">
									<label>Keterangan Izin</label>
									<input type="text" name="keterangan" class="form-control" required="" value="<?php echo $keterangan?>">
								</div>

								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'izin';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>