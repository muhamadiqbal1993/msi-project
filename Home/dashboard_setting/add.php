<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control" required="">
								</div>
								<div class="form-group">
									<label><strong>Department</strong></label>
									<select class="form-control select2" name="department[]" style="width: 100%;" multiple>
										
										<?php
										$sql1=mysql_query("select * from jabatan where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['kode'].' - '.$data1['nama']?></option>
											<?php 
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label>Angka Waktu</label>
									<input type="number" name="angka" class="form-control" required="" value="1">
								</div>
								<div class="form-group">
									<label><strong>Waktu</strong></label>
									<select class="form-control select2" name="waktu" style="width: 100%;" >
										<option value="Hari" >Hari</option>
										<option value="Minggu" >Minggu</option>
										<option value="Bulan" >Bulan</option>
										<option value="Tahun" >Tahun</option>
									</select>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Query</label>
									<textarea name="qry" class="form-control" rows="12" cols="50"></textarea>
								</div>
							</div>
							<!-- /.col -->

							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(150)
				.height(150);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
