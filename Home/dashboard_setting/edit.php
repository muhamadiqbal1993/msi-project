<?php
$dataEdit = mysql_fetch_array(mysql_query("select * from dashboard_setting where no = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $dataEdit['nama'];?>" readonly>
								</div>
								<div class="form-group">
									<label><strong>Department</strong></label>
                                    <select class="form-control select2"  name="department[]"style="width: 100%;" required="" multiple>
    
                                        <?php
										$sql1=mysql_query("select * from jabatan where status = '1' and no in (".$dataEdit['department'].")");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" selected><?php echo $data1['kode'].' - '.$data1['nama']?></option>
											<?php 
										}
										$sql1=mysql_query("select * from jabatan where status = '1' and no not in (".$dataEdit['department'].")");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>"><?php echo $data1['kode'].' - '.$data1['nama']?></option>
											<?php 
										}
										?>
                                    </select>
                                </div>
                                <div class="form-group">
									<label>Angka Waktu</label>
									<input type="number" name="angka" class="form-control" required="" value="<?php echo $dataEdit['angka'];?>">
								</div>
								<div class="form-group">
									<label><strong>Waktu</strong></label>
									<select class="form-control select2" name="waktu" style="width: 100%;" >
										<option value="Hari" <?php if($dataEdit['waktu'] == "Hari"){echo "selected";}?>>Hari</option>
										<option value="Minggu" <?php if($dataEdit['waktu'] == "Minggu"){echo "selected";}?>>Minggu</option>
										<option value="Bulan" <?php if($dataEdit['waktu'] == "Bulan"){echo "selected";}?>>Bulan</option>
										<option value="Tahun" <?php if($dataEdit['waktu'] == "Tahun"){echo "selected";}?>>Tahun</option>
									</select>
								</div>
                                <div class="form-group">
										<label>Status</label>
										<select class="form-control select2" name="aktif" style="width: 100%;" required="">
											<option value="1" <?php if($dataEdit['status'] == "1"){echo "selected";}?>>Aktif</option>
											<option value="2" <?php if($dataEdit['status'] == "2"){echo "selected";}?>>Tidak Aktif</option>

										</select>
									</div>
				
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Query</label>
									<textarea name="qry" class="form-control" rows="8" cols="50"><?php echo $dataEdit['query'];?></textarea>
								</div>
								
							</div>
						</div>
					</form>
					<br>
					<br>
				</div>
				<?php 
				$nama_modulnya = 'dashboard_setting';
				include '../headfoot/history.php';
				?>
			</div>
		</div>
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}

	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(200)
				.height(200);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
