<?php

$kode = "";
$nama = "";
$status = "";

$sql1=mysql_query("select * from m_kolam where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$nama = $data1['nama'];
	$kode = $data1['kode'];
	$status = $data1['status'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>

						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Kode</label>
									<input type="text" name="kode" class="form-control" required="" value="<?php echo $kode;?>" readonly>
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control select2" name="aktif" style="width: 100%;" required="">
										<option value="1" <?php if($status == "1"){echo "selected";}?>>Aktif</option>
										<option value="2" <?php if($status == "2"){echo "selected";}?>>Tidak Aktif</option>

									</select>
								</div>
								<!-- /.form-group -->

								<!-- /.form-group -->
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $nama;?>">
								</div>
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'm_kolam';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>