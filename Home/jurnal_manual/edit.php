<?php
$dataEdit = mysql_fetch_array(mysql_query("select *,DATE_FORMAT(tanggal, '%m/%d/%Y') as new_tanggal from jurnal_manual where no = '$id'"));

?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($dataEdit['status'] != "3"){?>
							<button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button>
							<a href="F_cancelPayment.php?id=<?php echo $id?>"><button type="submit" class="btn btn-danger" style="float: right;">Cancel Payment</button></a>
						<?php }?>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="2">
						<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
						<input type="hidden" name="kode" class="form-control" required="" value="<?php echo $dataEdit['kode'];?>">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">
						<input type="hidden" name="debit" id="debit" class="form-control" value="<?php echo $dataEdit['debit'];?>">
						<input type="hidden" name="credit" id="credit" class="form-control" value="<?php echo $dataEdit['credit'];?>">
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>No Payment</label>
									<input type="text" name="kode" class="form-control" value="<?php echo $dataEdit['kode'];?>" readonly>
								</div>
								<div class="form-group">
									<label>Tanggal</label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" value="<?php echo $dataEdit['new_tanggal'];?>">
								</div>

							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Keterangan</label>
									<input type="text" name="keterangan" id="keterangan" class="form-control" value="<?php echo $dataEdit['keterangan'];?>">
								</div>
								<!-- /.form-group -->
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
										<h1><?php echo $modulnya.' Line';?></h1>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >No</th>
													<th style="width: 20%">Account</th>
													<th>Keterangan</th>
													<th>Debit</th>
													<th>Credit</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$noms = 1;
												$sql1=mysql_query("select * from jurnal_manual_detail where kode = '".$dataEdit['kode']."'");
												while($data1=mysql_fetch_array($sql1))
												{
													$accr = '<select class="form-control select2" name="coa[]" id="coa'.$noms.'" style="width: 100%;" required="">';

													$sql12=mysql_query("select * from coa where status = '1'");
													while($data12=mysql_fetch_array($sql12))
													{
														$slct = "";
														if($data1['account'] == $data12['id'])
														{
															$slct = "selected";
														}
														$accr .= '<option value="'.$data12['no'].'" '.$slct.'>'.$data12['id'].' - '.$data12['nama'].'</option>';
													}
													$accr .= '</select>';
													echo '
													<tr>
													<td>'.$noms.'</td>
													<td>'.$accr.'</td>
													<td><input type="text" name="editketerangan[]" class="form-control" value="'.$data1['keterangan'].'"></td>
													<td><input type="text" name="editDebit[]" id="editDebit'.$noms.'" oninput="ubahDebitDesimal('.$noms.')" class="form-control" value="'.number_format($data1['debit']).'"></td>
													<td><input type="text" name="editCredit[]" id="editCredit'.$noms.'" oninput="ubahDebitDesimal('.$noms.')" class="form-control" value="'.number_format($data1['credit']).'"></td>  
													<td><button type="button" onclick="delete_row_didik('.$noms.')" class="btn btn-danger">Delete</button></td>
													</tr>
													';
													$noms++;
												}
												?>
												<tr>
													<th colspan="3" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button></th>
													<th style="background-color: grey"><span><h4 style="color: white" id="txtDebit"><strong>Rp. <?php echo number_format($dataEdit['debit'])?></strong></h4></span></th>
													<th colspan="1" style="background-color: grey"><span><h4 style="color: white" id="txtCredit"><strong>Rp. <?php echo number_format($dataEdit['credit'])?></strong></h4></span></th>
													<th colspan="1" style="background-color: grey"><span><h4 style="color: white" id="txtMinus"><strong>(Rp. 0)</strong></h4></span></th>
												</tr>
											</tbody>
											<script language="javascript" type="text/javascript">

												function getProduct(abs)
												{
													var products = '<select class="form-control select2" name="coa[]" id="coa'+abs+'" style="width: 100%;" required="">'+
													<?php

													$sql1=mysql_query("select * from coa where status = '1'");
													while($data1=mysql_fetch_array($sql1))
													{
														?>
														'<option value="<?php echo $data1['no'];?>"><?php echo $data1['id'].' - '.$data1['nama']?></option>'+

														<?php 
													}?>

													'</select>';

													return products;
												}

												function delete_row_didik(abs)
												{
													document.getElementById("tabDidik").deleteRow(abs);
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													for(var a=1;a<rowCount;a++)
													{
														table.rows[a].cells[0].innerHTML = a;
														table.rows[a].cells[5].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
													}
													ubahTotal();
												}

												function addLine_didik()
												{ 
													var keterangan = document.getElementById('keterangan').value;
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													//alert(rowCount);
													var row = table.insertRow(rowCount);
													var cell0 = row.insertCell(0);
													var cell1 = row.insertCell(1);
													var cell2 = row.insertCell(2);
													var cell3 = row.insertCell(3);
													var cell4 = row.insertCell(4);
													var cell5 = row.insertCell(5);
													cell0.innerHTML = rowCount;
													cell1.innerHTML = getProduct(rowCount);
													cell2.innerHTML = '<input type="text" name="editketerangan[]" class="form-control" value="'+keterangan+'">';
													cell3.innerHTML = '<input type="text" name="editDebit[]" id="editDebit'+rowCount+'" oninput="ubahDebitDesimal('+rowCount+')" class="form-control" value="0">';
													cell4.innerHTML = '<input type="text" name="editCredit[]" id="editCredit'+rowCount+'" oninput="ubahCreditDesimal('+rowCount+')" class="form-control" value="0">';
													cell5.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

													var namaField = "#coa"+rowCount;
													$(namaField).select2();
												}

												function ubahDebitDesimal(ids)
												{
													var nams = "editDebit"+ids;
													var val = document.getElementById(nams).value.split(",").join("");
													document.getElementById(nams).value = parseInt(val).toLocaleString('en-US');
													ubahTotal();
												}


												function ubahCreditDesimal(ids)
												{
													var nams = "editCredit"+ids;
													var val = document.getElementById(nams).value.split(",").join("");
													document.getElementById(nams).value = parseInt(val).toLocaleString('en-US');
													ubahTotal();
												}

												function ubahTotal()
												{
													var rowCount = document.getElementById('tabDidik').rows.length - 1;
													var table = document.getElementById('tabDidik');
													var debit = 0;
													var credit = 0;
													for(var a=1;a<rowCount;a++)
													{
														var editDebit = 'editDebit'+a;
														var editCredit = 'editCredit'+a;
														var debits = document.getElementById(editDebit).value.split(",").join("");
														var credits = document.getElementById(editCredit).value.split(",").join("");
														debit = parseInt(debit) + parseInt(debits);
														credit = parseInt(credit) + parseInt(credits);
													}
													//alert(debit+" - "+credit);
													document.getElementById("debit").value = debit;
													document.getElementById("credit").value = credit;
													var txtMinus = parseInt(debit) - parseInt(credit);
													document.getElementById('txtDebit').innerHTML = "<strong>Rp. "+debit.toLocaleString('en-US')+"</strong>";
													document.getElementById('txtCredit').innerHTML = "<strong>Rp. "+credit.toLocaleString('en-US')+"</strong>";
													document.getElementById('txtMinus').innerHTML = "<strong>Rp. "+txtMinus.toLocaleString('en-US')+"</strong>";
												}
											</script>
										</table>
									</div>
								</div>
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<?php 
				$nama_modulnya = 'jurnal_manual';
				include '../headfoot/history.php';
				?>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>
