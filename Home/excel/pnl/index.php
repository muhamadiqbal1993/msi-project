<?php
include "../..//function/init.php";
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

// $id = $_GET['id'];
// $tgl_dari = $_GET['dari'];
// $tgl_sampai = $_GET['sampai'];
$id = "1";
$tgl_dari = "20 Juni 2021";
$tgl_sampai = "21 Juni 2021";
//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();
//set the value of cell a1 to "Hello World!"
$sheet->mergeCells("A1:D1");
$sheet->mergeCells("A2:D2");
$sheet->setCellValue('A1','Profit And Loss');
$sheet->setCellValue('A2','Periode : '.$tgl_dari .' s/d '.$tgl_sampai);
$sheet->setCellValue('A4', 'Account');
$sheet->setCellValue('B4', 'Debit');
$sheet->setCellValue('C4', 'Credit');
$sheet->setCellValue('D4', 'Total');

$g_total_debit = 0;
$g_total_credit = 0;
//$limits = "tanggal between '".$tgl_dari."' and '".$tgl_sampai."'";
$lv = 5;
$spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$sql1=mysql_query("select TL.*,C.nama as namco from t_pnl TL
	inner join coa C on C.id = TL.coa
	where TL.parent = '' order by seq asc");
while($data1=mysql_fetch_array($sql1))
{  
	if($data1['type'] == "")
	{

		$total_debit = 0;
		$total_credit = 0;

		$a = "A".$lv;
		$b = "B".$lv;
		$c = "C".$lv;
		$d = "D".$lv;
		$mrg = $a.':'.$d;
		$bold = $a.':'.$d;

	//$sheet->getStyle($bold)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('E3E1DA');

		$sheet->getStyle($bold)->getFont()->setBold(true);

		$sheet->mergeCells($mrg);
		$sheet->setCellValue($a, $data1['coa'].' - '.$data1['namco']);
		$lv++;
		$sql12=mysql_query("select TL.*,C.nama as namco from t_pnl TL
			inner join coa C on C.id = TL.coa
			where TL.parent = '".$data1['coa']."' order by seq asc");
		while($data12=mysql_fetch_array($sql12))
		{ 
			$total_12 = mysql_fetch_array(mysql_query("select IFNULL(sum(debit),0) as total_debit,IFNULL(sum(credit),0) as total_credit from ledger where coa = '".$data12['coa']."'"));
			$total_debit += $total_12['total_debit'];
			$total_credit += $total_12['total_credit'];
			$a = "A".$lv;
			$b = "B".$lv;
			$c = "C".$lv;
			$d = "D".$lv;
			$mrg = $a.':'.$d;
			$bold = $a.':'.$d;
			$sheet->getStyle($bold)->getFont()->setBold(true);
			$sheet->setCellValue($a, "            ".$data12['coa'].' - '.$data12['namco'].' Total : ');
			$sheet->setCellValue($b, number_format($total_12['total_debit']));
			$sheet->setCellValue($c, number_format($total_12['total_credit']));
			$sheet->setCellValue($d, number_format($total_12['total_debit']-$total_12['total_credit']));
			$lv++;
			$sql123=mysql_query("select TL.*,C.nama as namco from t_pnl TL
				inner join coa C on C.id = TL.coa
				where TL.parent = '".$data12['coa']."' order by seq asc");
			while($data123=mysql_fetch_array($sql123))
			{ 
				$total_123 = mysql_fetch_array(mysql_query("select IFNULL(sum(debit),0) as total_debit,IFNULL(sum(credit),0) as total_credit from ledger where coa = '".$data123['coa']."'"));
				$total_debit += $total_123['total_debit'];
				$total_credit += $total_123['total_credit'];
				$a = "A".$lv;
				$b = "B".$lv;
				$c = "C".$lv;
				$d = "D".$lv;
				$mrg = $a.':'.$d;
				$bold = $a.':'.$d;
				$sheet->setCellValue($a, "                        ".$data123['coa'].' - '.$data123['namco']);
				$sheet->setCellValue($b, number_format($total_123['total_debit']));
				$sheet->setCellValue($c, number_format($total_123['total_credit']));
				$sheet->setCellValue($d, number_format($total_123['total_debit']-$total_123['total_credit']));
				$lv++;
			}
		}

		$g_total_debit += $total_debit;
		$g_total_credit += $total_credit;

		$a = "A".$lv;
		$b = "B".$lv;
		$c = "C".$lv;
		$d = "D".$lv;
		$mrg = $a.':'.$d;
		$bold = $a.':'.$d;

		$sheet->getStyle($bold)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('E3E1DA');
		$sheet->getStyle($bold)->getFont()->setBold(true);
		$sheet->setCellValue($a, $data1['coa'].' - '.$data1['namco'].' Total : ');
		$sheet->setCellValue($b, number_format($total_debit));
		$sheet->setCellValue($c, number_format($total_credit));
		$sheet->setCellValue($d, number_format($total_debit-$total_credit));
		$lv++;
	}
	else
	{
		$a = "A".$lv;
		$b = "B".$lv;
		$c = "C".$lv;
		$d = "D".$lv;
		$mrg = $a.':'.$d;
		$bold = $a.':'.$d;

		$sheet->getStyle($bold)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00FFFF');

		$sheet->getStyle($bold)->getFont()->setBold(true);

		//$sheet->mergeCells($mrg);
		$sheet->setCellValue($a, $data1['type']);
		$sheet->setCellValue($b, number_format($g_total_debit));
		$sheet->setCellValue($c, number_format($g_total_credit));
		$sheet->setCellValue($d, number_format($g_total_debit-$g_total_credit));
		$lv++;
		$a = "A".$lv;
		$b = "B".$lv;
		$c = "C".$lv;
		$d = "D".$lv;
		$sheet->setCellValue($a, "");
		$sheet->setCellValue($b, "");
		$sheet->setCellValue($c, "");
		$sheet->setCellValue($d, "");
		$lv++;
		$g_total_debit = 0;
		$g_total_credit = 0;
	}

	// $a = "A".$lv;
	// $b = "B".$lv;
	// $c = "C".$lv;
	// $d = "D".$lv;
	// $e = "E".$lv;
	// $f = "F".$lv;
	// $mrg = $a.':'.$c;

	// $sheet->getStyle($mrg)->getFont()->setBold(true);

	// $sheet->setCellValue($a, "");
	// $sheet->setCellValue($b, "");
	// $sheet->setCellValue($c, "Saldo Awal");
	// $sheet->setCellValue($d, number_format($nilaiDebit_saldo['total_debit']));
	// $sheet->setCellValue($e, number_format($nilaiCredit_saldo['total_credit']));
	// $sheet->setCellValue($f, number_format($nilaiDebit_saldo['total_debit'] - $nilaiCredit_saldo['total_credit']));
	// $lv++;

	// $s_total_debit += $nilaiDebit['total_debit']+$nilaiDebit_saldo['total_debit'];
	// $s_total_credit += $nilaiCredit['total_credit']+$nilaiCredit_saldo['total_credit'];
	// $balances = $nilaiDebit_saldo['total_debit'] - $nilaiCredit_saldo['total_credit'];
	// $sql12=mysql_query("select *,DATE_FORMAT(tanggal, '%d-%m-%Y') as tanggal_new from ledger where coa = '".$data1['coa']."' and $limits order by tanggal asc");
	// while($data12=mysql_fetch_array($sql12))
	// {
	// 	$a = "A".$lv;
	// 	$b = "B".$lv;
	// 	$c = "C".$lv;
	// 	$d = "D".$lv;
	// 	$e = "E".$lv;
	// 	$f = "F".$lv;
	// 	$mrg = $a.':'.$c;
	// 	$bold = $a.':'.$f;
	// 	$sheet->getStyle($bold)->getFont()->setBold(false);

	// 	$balances += $data12['debit'] - $data12['credit'];
	// 	$sheet->setCellValue($a, $data12['tanggal_new']);
	// 	$sheet->setCellValue($b, $data12['ref']);
	// 	$sheet->setCellValue($c, $data12['ket']);
	// 	$sheet->setCellValue($d, number_format($data12['debit']));
	// 	$sheet->setCellValue($e, number_format($data12['credit']));
	// 	$sheet->setCellValue($f, number_format($balances));

	// 	$lv++;
	// }
	
}
// $s_total_balance = $s_total_debit - $s_total_credit;
// $a = "A".$lv;
// $b = "B".$lv;
// $c = "C".$lv;
// $d = "D".$lv;
// $e = "E".$lv;
// $f = "F".$lv;
// $mrg = $a.':'.$c;
// $bold = $a.':'.$f;

// $sheet->getStyle($bold)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('34A1EB');

// $sheet->getStyle($bold)->getFont()->setBold(true);

// $sheet->mergeCells($mrg);
// $sheet->setCellValue($a, "Grand Total");
// $sheet->setCellValue($d, number_format($s_total_debit));
// $sheet->setCellValue($e, number_format($s_total_credit));
// $sheet->setCellValue($f, number_format($s_total_balance));


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
header('Content-Disposition: attachment;filename="Profit And Loss.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
?>
