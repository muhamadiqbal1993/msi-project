<?php
include "conn.php";
require 'vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

$id = $_POST['id'];
//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();
//set the value of cell a1 to "Hello World!"
$sheet->setCellValue('A1', 'NOWB');
$sheet->setCellValue('B1', 'CreateDate');
$sheet->setCellValue('C1', 'STT');
$sheet->setCellValue('D1', 'Seal');
$sheet->setCellValue('E1', 'Destination');
$sheet->setCellValue('F1', 'PCS');
$sheet->setCellValue('G1', 'CW');
$sheet->setCellValue('H1', 'Note');

$lv = 2;
$sqlu=mysql_query("select * from reject where awb in ($id)");
while($datau=mysql_fetch_array($sqlu))
{
	$a = "A".$lv;
	$b = "B".$lv;
	$c = "C".$lv;
	$d = "D".$lv;
	$e = "E".$lv;
	$f = "F".$lv;
	$g = "G".$lv;
	$h = "H".$lv;

	$sheet->setCellValue($a, $datau['awb']);
	$sheet->setCellValue($b, $datau['tanggal']);
	$sheet->setCellValue($c, $datau['stt']);
	$sheet->setCellValue($d, $datau['seal']);
	$sheet->setCellValue($e, $datau['dest']);
	$sheet->setCellValue($f, $datau['pcs']);
	$sheet->setCellValue($g, $datau['berat']);
	$sheet->setCellValue($h, $datau['alasan']);
	$lv++;
}

//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
header('Content-Disposition: attachment;filename="reject.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
?>
