<?php
include "conn.php";
require 'vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

$id = $_GET['id'];
//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();
//set the value of cell a1 to "Hello World!"
$sheet->setCellValue('A1', 'Customer');
$sheet->setCellValue('B1', 'Destination');
$sheet->setCellValue('C1', 'Reff No');
$sheet->setCellValue('D1', 'SMU No');
$sheet->setCellValue('E1', 'Seal No');
$sheet->setCellValue('F1', 'Weight');

$lv = 2;
$sqlu=mysql_query("select AH.*,S.stt_no,S.berat from awb_header AH 
	inner join stt S on S.awb_no = AH.id
	where AH.no in ($id)");
while($datau=mysql_fetch_array($sqlu))
{
	$a = "A".$lv;
	$b = "B".$lv;
	$c = "C".$lv;
	$d = "D".$lv;
	$e = "E".$lv;
	$f = "F".$lv;

	$sheet->setCellValue($a, $datau['custoomer']);
	$sheet->setCellValue($b, $datau['dest']);
	$sheet->setCellValue($c, $datau['ref']);
	$sheet->setCellValue($d, $datau['awb_no']);
	$sheet->setCellValue($e, $datau['stt_no']);
	$sheet->setCellValue($f, $datau['berat']);
	$lv++;
}

//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
header('Content-Disposition: attachment;filename="smu_list.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
?>
