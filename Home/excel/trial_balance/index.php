<?php
include "../..//function/init.php";
require '../vendor/autoload.php';
//load phpspreadsheet class using namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;

$id = $_GET['id'];
$tgl_dari = $_GET['dari'];
$tgl_sampai = $_GET['sampai'];
//make a new spreadsheet object
$spreadsheet = new Spreadsheet();
//get current active sheet (first sheet)
$sheet = $spreadsheet->getActiveSheet();
//set the value of cell a1 to "Hello World!"
$sheet->mergeCells("A1:F1");
$sheet->mergeCells("A2:F2");
$sheet->setCellValue('A1','Report Ledger');
$sheet->setCellValue('A2','Periode : '.$_GET['dari'].' s/d '.$_GET['sampai']);

$sheet->getStyle("A4:E4")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('E3E1DA');
$sheet->getStyle("A5:E5")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('E3E1DA');

$sheet->mergeCells("A4:A5");
$sheet->setCellValue('A4', 'Account');

$sheet->mergeCells("B4:C4");
$sheet->mergeCells("D4:E4");

$sheet->setCellValue('B4', 'Saldo Awal');
$sheet->setCellValue('D4', 'Transaksi');

$sheet->setCellValue('B5', 'Debit');
$sheet->setCellValue('C5', 'Credit');
$sheet->setCellValue('D5', 'Debit');
$sheet->setCellValue('E5', 'Credit');

$s_total_debit = 0;
$s_total_credit = 0;
$s_total_balance = 0;
$limits = "tanggal between '".$tgl_dari."' and '".$tgl_sampai."'";
$spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$lv = 6;

$sql1=mysql_query("select * from ledger group by coa order by coa asc");
while($data1=mysql_fetch_array($sql1))
{  
	$dataCoa = mysql_fetch_array(mysql_query("select * from coa where id = '".$data1['coa']."'"));
	$nilaiDebit = mysql_fetch_array(mysql_query("select sum(debit) as total_debit from ledger where coa = '".$data1['coa']."' and ".$limits));
	$nilaiCredit = mysql_fetch_array(mysql_query("select sum(credit) as total_credit from ledger where coa = '".$data1['coa']."' and ".$limits));

	$nilaiDebit_saldo = mysql_fetch_array(mysql_query("select sum(debit) as total_debit from ledger where coa = '".$data1['coa']."' and tanggal < '$tgl_dari'"));
	$nilaiCredit_saldo = mysql_fetch_array(mysql_query("select sum(credit) as total_credit from ledger where coa = '".$data1['coa']."' and tanggal < '$tgl_dari'"));

	$nilaiBalance = ($nilaiDebit['total_debit']+$nilaiDebit_saldo['total_debit']) - ($nilaiCredit['total_credit']+$nilaiCredit_saldo['total_credit']);

	$a = "A".$lv;
	$b = "B".$lv;
	$c = "C".$lv;
	$d = "D".$lv;
	$e = "E".$lv;
	$mrg = $a.':'.$c;

	$sheet->setCellValue($a, $data1['coa'].' - '.$dataCoa['nama']);
	$sheet->setCellValue($b, number_format($nilaiDebit_saldo['total_debit']));
	$sheet->setCellValue($c, number_format($nilaiDebit_saldo['total_debit']));
	$sheet->setCellValue($d, number_format($nilaiDebit['total_debit']));
	$sheet->setCellValue($e, number_format($nilaiCredit['total_credit']));

	$lv++;
}
// $sql1=mysql_query("select * from ledger group by coa order by coa asc");
// while($data1=mysql_fetch_array($sql1))
// {  
// 	$dataCoa = mysql_fetch_array(mysql_query("select * from coa where id = '".$data1['coa']."'"));
// 	$nilaiDebit = mysql_fetch_array(mysql_query("select sum(debit) as total_debit from ledger where coa = '".$data1['coa']."' and ".$limits));
// 	$nilaiCredit = mysql_fetch_array(mysql_query("select sum(credit) as total_credit from ledger where coa = '".$data1['coa']."' and ".$limits));

// 	$nilaiDebit_saldo = mysql_fetch_array(mysql_query("select sum(debit) as total_debit from ledger where coa = '".$data1['coa']."' and tanggal < '$tgl_dari'"));
// 	$nilaiCredit_saldo = mysql_fetch_array(mysql_query("select sum(credit) as total_credit from ledger where coa = '".$data1['coa']."' and tanggal < '$tgl_dari'"));

// 	$nilaiBalance = ($nilaiDebit['total_debit']+$nilaiDebit_saldo['total_debit']) - ($nilaiCredit['total_credit']+$nilaiCredit_saldo['total_credit']);

// 	$a = "A".$lv;
// 	$b = "B".$lv;
// 	$c = "C".$lv;
// 	$d = "D".$lv;
// 	$e = "E".$lv;
// 	$f = "F".$lv;
// 	$mrg = $a.':'.$c;
// 	$bold = $a.':'.$f;

// 	$sheet->getStyle($bold)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('E3E1DA');

// 	$sheet->getStyle($bold)->getFont()->setBold(true);

// 	$sheet->mergeCells($mrg);
// 	$sheet->setCellValue($a, $data1['coa'].' - '.$dataCoa['nama']);
// 	$sheet->setCellValue($d, number_format($nilaiDebit['total_debit']+$nilaiDebit_saldo['total_debit']));
// 	$sheet->setCellValue($e, number_format($nilaiCredit['total_credit']+$nilaiCredit_saldo['total_credit']));
// 	$sheet->setCellValue($f, number_format($nilaiBalance));
// 	$lv++;

// 	$a = "A".$lv;
// 	$b = "B".$lv;
// 	$c = "C".$lv;
// 	$d = "D".$lv;
// 	$e = "E".$lv;
// 	$f = "F".$lv;
// 	$mrg = $a.':'.$c;

// 	$sheet->getStyle($mrg)->getFont()->setBold(true);

// 	$sheet->setCellValue($a, "");
// 	$sheet->setCellValue($b, "");
// 	$sheet->setCellValue($c, "Saldo Awal");
// 	$sheet->setCellValue($d, number_format($nilaiDebit_saldo['total_debit']));
// 	$sheet->setCellValue($e, number_format($nilaiCredit_saldo['total_credit']));
// 	$sheet->setCellValue($f, number_format($nilaiDebit_saldo['total_debit'] - $nilaiCredit_saldo['total_credit']));
// 	$lv++;

// 	$s_total_debit += $nilaiDebit['total_debit']+$nilaiDebit_saldo['total_debit'];
// 	$s_total_credit += $nilaiCredit['total_credit']+$nilaiCredit_saldo['total_credit'];
// 	$balances = $nilaiDebit_saldo['total_debit'] - $nilaiCredit_saldo['total_credit'];
// 	$sql12=mysql_query("select *,DATE_FORMAT(tanggal, '%d-%m-%Y') as tanggal_new from ledger where coa = '".$data1['coa']."' and $limits order by tanggal asc");
// 	while($data12=mysql_fetch_array($sql12))
// 	{
// 		$a = "A".$lv;
// 		$b = "B".$lv;
// 		$c = "C".$lv;
// 		$d = "D".$lv;
// 		$e = "E".$lv;
// 		$f = "F".$lv;
// 		$mrg = $a.':'.$c;
// 		$bold = $a.':'.$f;
// 		$sheet->getStyle($bold)->getFont()->setBold(false);

// 		$balances += $data12['debit'] - $data12['credit'];
// 		$sheet->setCellValue($a, $data12['tanggal_new']);
// 		$sheet->setCellValue($b, $data12['ref']);
// 		$sheet->setCellValue($c, $data12['ket']);
// 		$sheet->setCellValue($d, number_format($data12['debit']));
// 		$sheet->setCellValue($e, number_format($data12['credit']));
// 		$sheet->setCellValue($f, number_format($balances));

// 		$lv++;
// 	}

// }
// $s_total_balance = $s_total_debit - $s_total_credit;
// $a = "A".$lv;
// $b = "B".$lv;
// $c = "C".$lv;
// $d = "D".$lv;
// $e = "E".$lv;
// $f = "F".$lv;
// $mrg = $a.':'.$c;
// $bold = $a.':'.$f;

// $sheet->getStyle($bold)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('34A1EB');

// $sheet->getStyle($bold)->getFont()->setBold(true);

// $sheet->mergeCells($mrg);
// $sheet->setCellValue($a, "Grand Total");
// $sheet->setCellValue($d, number_format($s_total_debit));
// $sheet->setCellValue($e, number_format($s_total_credit));
// $sheet->setCellValue($f, number_format($s_total_balance));


//set the header first, so the result will be treated as an xlsx file.
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

//make it an attachment so we can define filename
header('Content-Disposition: attachment;filename="Trial_Balance.xlsx"');

//create IOFactory object
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//save into php output
$writer->save('php://output');
?>
