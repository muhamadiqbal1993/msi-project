<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<div class="box-body">
							<div class="col-xs-12">
								<h3>Group Header</h3>
								<table id="revenue" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th>Urutan Posisi</th>
											<th>Nama Group</th>
											<th>Hapus</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$noms = 1;
										$sql1=mysql_query("select * from template_bs_header where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											echo '
											<tr>
											<td>'.$noms.'</td>
											<td><input type="number" name="urutanGroup[]" class="form-control" value="'.$data1['urutan'].'"></td>
											<td><input type="text" name="namaGroup[]" class="form-control" value="'.$data1['nama'].'"></td>
											<td><button type="button" onclick="delete_rev('.$noms.')" class="btn btn-danger">Delete</button></td>
											</tr>
											';
											$noms++;
										}
										?>
										<tr>
											<td colspan="4" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_rev()">Tambah Data</button></td>

										</tr>
									</tbody>

								</table>
								<script language="javascript" type="text/javascript">

									function delete_rev(abs)
									{
										document.getElementById("revenue").deleteRow(abs);
										var rowCount = document.getElementById('revenue').rows.length - 1;
										var table = document.getElementById('revenue');
										for(var a=1;a<rowCount;a++)
										{
											table.rows[a].cells[0].innerHTML = a;
											table.rows[a].cells[3].innerHTML = "<button type='submit' onclick='delete_rev(\""+a+"\")' class='btn btn-danger'>Delete</button>";
										}
									}

									function addLine_rev()
									{ 
										var rowCount = document.getElementById('revenue').rows.length - 1;
										var table = document.getElementById('revenue');
										var row = table.insertRow(rowCount);
										var cell0 = row.insertCell(0);
										var cell1 = row.insertCell(1);
										var cell2 = row.insertCell(2);
										var cell3 = row.insertCell(3);

										cell0.innerHTML = rowCount;
										cell1.innerHTML = '<input type="number" name="urutanGroup[]" class="form-control" value="">';
										cell2.innerHTML = '<input type="text" name="namaGroup[]" class="form-control" value="">';
										cell3.innerHTML = "<button type='submit' onclick='delete_rev(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";
									}
								</script>
							</div>
							<!-- ================================================================================================ -->
							<div class="col-xs-12">
								<h3>Group Detail</h3>
								<table id="exp" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th>Group</th>
											<th>Urutan</th>
											<th>Account Header</th>
											<th>Hapus</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$noms = 1;
										$sql1=mysql_query("select * from template_bs_detail where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											$accr = '<select class="form-control select2" name="coa[]" id="coa'.$noms.'" style="width: 100%;" required="">';
											$sql12=mysql_query("select * from coa where status = '1'");
											while($data12=mysql_fetch_array($sql12))
											{
												$slct = "";
												if($data1['account'] == $data12['no'])
												{
													$slct = "selected";
												}
												$accr .= '<option value="'.$data12['no'].'" '.$slct.'>'.$data12['id'].' - '.$data12['nama'].'</option>';
											}
											$accr .= '</select>';


											$grp = '<select class="form-control select2" name="n_group[]" id="n_group'.$noms.'" style="width: 100%;" required="">';
											$sql12=mysql_query("select * from template_bs_header where status = '1'");
											while($data12=mysql_fetch_array($sql12))
											{
												$slct2 = "";
												if($data1['n_group'] == $data12['no'])
												{
													$slct2 = "selected";
												}
												$grp .= '<option value="'.$data12['no'].'" '.$slct2.'>'.$data12['nama'].'</option>';
											}
											$grp .= '</select>';


											echo '
											<tr>
											<td>'.$noms.'</td>
											<td>'.$grp.'</td>
											<td><input type="number" name="urutanDetail[]" class="form-control" value="'.$data1['urutan'].'"></td>
											<td>'.$accr.'</td>
											<td><button type="button" onclick="delete_exp('.$noms.')" class="btn btn-danger">Delete</button></td>
											</tr>
											';
											$noms++;
										}
										?>
										<tr>
											<td colspan="5" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_exp()">Tambah Data</button></td>

										</tr>
									</tbody>

								</table>
								<script language="javascript" type="text/javascript">

									function getProduct_exp(abs)
									{
										var products = '<select class="form-control select2" name="n_group[]" id="n_group'+abs+'" style="width: 100%;" required="">'+
										<?php

										$sql1=mysql_query("select * from template_bs_header where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											'<option value="<?php echo $data1['no'];?>"><?php echo $data1['nama']?></option>'+

											<?php 
										}?>

										'</select>';

										return products;
									}

									function getCoa_exp(abs)
									{
										var products = '<select class="form-control select2" name="coa[]" id="coa'+abs+'" style="width: 100%;" required="">'+
										<?php

										$sql1=mysql_query("select * from coa where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											'<option value="<?php echo $data1['no'];?>"><?php echo $data1['id'].' - '.$data1['nama']?></option>'+

											<?php 
										}?>

										'</select>';

										return products;
									}

									function delete_exp(abs)
									{
										document.getElementById("exp").deleteRow(abs);
										var rowCount = document.getElementById('exp').rows.length - 1;
										var table = document.getElementById('exp');
										for(var a=1;a<rowCount;a++)
										{
											table.rows[a].cells[0].innerHTML = a;
											table.rows[a].cells[2].innerHTML = "<button type='submit' onclick='delete_exp(\""+a+"\")' class='btn btn-danger'>Delete</button>";
										}

									}

									function addLine_exp()
									{ 
										var rowCount = document.getElementById('exp').rows.length - 1;
										var table = document.getElementById('exp');
										var row = table.insertRow(rowCount);
										var cell0 = row.insertCell(0);
										var cell1 = row.insertCell(1);
										var cell2 = row.insertCell(2);
										var cell3 = row.insertCell(3);
										var cell4 = row.insertCell(4);

										cell0.innerHTML = rowCount;
										cell1.innerHTML = getProduct_exp(rowCount);
										cell2.innerHTML = '<input type="number" name="urutanDetail[]" class="form-control" value="">';
										cell3.innerHTML = getCoa_exp(rowCount);
										cell4.innerHTML = "<button type='submit' onclick='delete_exp(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

										var namaField = "#n_group"+rowCount;
										$(namaField).select2();
										var namaField2 = "#coa"+rowCount;
										$(namaField2).select2();
									}
								</script>
							</div>
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>
