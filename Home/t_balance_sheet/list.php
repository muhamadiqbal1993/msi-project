<?php 
$customer = "";
$payment = "";
$client = "";
$wheres = "";
$dari = date('m/d/Y');
$sampai = date('m/d/Y');
$drs = explode("/", $dari)[2].'-'.explode("/", $dari)[0].'-'.explode("/", $dari)[1];
$smps = explode("/", $sampai)[2].'-'.explode("/", $sampai)[0].'-'.explode("/", $sampai)[1];
$caris = "";
if(!empty($_POST['cek']))
{
  $dari = $_POST['dari'];
  $sampai = $_POST['sampai'];
  $drs = explode("/", $dari)[2].'-'.explode("/", $dari)[0].'-'.explode("/", $dari)[1];
  $smps = explode("/", $sampai)[2].'-'.explode("/", $sampai)[0].'-'.explode("/", $sampai)[1];
}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <!-- <?php if($addPM == "1"){?>
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu">
            <?php }?> -->
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">  
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
              Data '.$modulnya.' Berhasil Dibuat.
              </div>
              </div>';
            }
            if($status == "2")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
              </div>
              </div>';
            }
            if($status == "3")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Warning!</h4>
              Data '.$modulnya.' sudah ada yang menggunakan , silahkan daftarkan dengan data yang lain
              </div>
              </div>';
            }
            ?>
            <div id="pencarian">
              <!-- <form action="../excel/ledger/index.php" id="search_data" method="POST">  -->
                <form id="search_data" method="POST"> 
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>Dari Tanggal</label>
                      <input type="hidden" name="cek" class="form-control" required="" value="ada">
                      <input type="text" name="dari" id="datepicker" class="form-control" required="" value="<?php echo $dari?>">
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                      <label>Sampai Tanggal</label>
                      <input type="text" name="sampai" id="datepicker1" class="form-control" required="" value="<?php echo $sampai?>">
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Account (COA)</label>
                      <select class="form-control select2" name="coa[]" multiple="" style="width: 100%;">
                        <?php
                        $sql1=mysql_query("select * from coa where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          <option value="<?php echo $data1['id']?>" <?php if($data1['id'] == $customer){echo "selected";}?>><?php echo $data1['id'].' - '.$data1['nama']?></option>
                          <?php 
                        }
                        ?>

                      </select>
                    </div>
                  </div>
                </form>
                <div class="col-xs-12">
                  <button type="submit" class="btn btn-primary" onclick="localForm()">Import Excel</button>
                  <!-- <button type="submit" onclick="pencarian()" class="btn btn-warning">Sembunyikan Pencarian</button> -->
                  <button type="submit" class="btn btn-success" style="float: right;" onclick="downloadExcel()">Download Excel</button>
                </div>
              </div>

              <script type="text/javascript">

                function localForm()
                {
                  document.getElementById("search_data").action = ".";
                  document.getElementById("search_data").submit();
                }
                function downloadExcel()
                {
                  document.getElementById('search_data').action = "../excel/ledger/index.php"; 
                  document.getElementById("search_data").submit();
                }
                function pencarian()
                {
                  document.getElementById('pencarian').style.display = "none"; 
                  document.getElementById('buka').style.display = ""; 
                }
                function buka()
                {
                  document.getElementById('pencarian').style.display = ""; 
                  document.getElementById('buka').style.display = "none";
                }
              </script>

              <div class="col-xs-12" id="buka" style="display: none">
                <button type="submit" onclick="buka()" class="btn btn-primary">Buka Pencarian</button>
              </div>

              <div class="col-xs-12">
                <hr class="abu">
              </div>

              <div class="col-xs-12">
                <table id="example3" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Tanggal</th>
                      <th>Reference</th>
                      <th>Description</th>
                      <th>Debit</th>
                      <th>Credit</th>
                      <th>Balance</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $pencarian = "";
                    if(empty($_POST['coa']))
                    {
                      $pencarian = " status = '1'";
                    }
                    else
                    {
                      $arrayCoa = join(",",$_POST['coa']);
                      $pencarian = " id in (".$arrayCoa.") and status = '1'";
                    }
                    $tgl_saldo = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $dari) ) ));
                    $sql1=mysql_query("select * from coa where $pencarian");
                    while($dataCoa=mysql_fetch_array($sql1))
                    {
                      echo '
                      <tr>
                      <td colspan="6" style="background-color: grey;">'.$dataCoa['id'].' - '.$dataCoa['nama'].'</td>
                      </tr>
                      ';
                      echo '
                      <tr>
                      <td colspan="2">'.$tgl_saldo.'</td>
                      <td>Saldo Awal</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      </tr>
                      ';
                      $sql12=mysql_query("select PCD.*,PC.tanggal from p_cost_detail PCD
                        inner join p_cost PC on PC.kode = PCD.kode_header
                        inner join coa C on C.no = PCD.account
                        where C.id = '".$dataCoa['id']."' and PC.status != '3'");
                      while($data12=mysql_fetch_array($sql12))
                      { 
                        echo '
                        <tr>
                        <td>'.explode(" ", $data12['tanggal'])[0].'</td>
                        <td>PC-'.$data12['kode_header'].'</td>
                        <td>'.$data12['keterangan'].'</td>
                        <td>'.number_fromat($data12['harga']).'</td>
                        <td>0</td>
                        <td>0</td>
                        </tr>
                        ';
                      }

                      $sql12=mysql_query("select PC.* from p_cost PC 
                        inner join coa C on C.no = PC.account
                        where C.id = '".$dataCoa['id']."' and PC.status != '3'");
                      while($data12=mysql_fetch_array($sql12))
                      { 

                        echo '
                        <tr>
                        <td>'.explode(" ", $data12['tanggal'])[0].'</td>
                        <td>PC-'.$data12['kode_header'].'</td>
                        <td>'.$data12['keterangan'].'</td>
                        <td>0</td>
                        <td>'.number_fromat($data12['harga']).'</td>
                        <td>0</td>
                        </tr>
                        ';
                      }

                      $sql12=mysql_query("select PC.* from p_sales_invoice PC 
                        inner join coa C on C.no = PC.account
                        where C.id = '".$dataCoa['id']."' and PC.status != '3'");
                      while($data12=mysql_fetch_array($sql12))
                      { 

                        echo '
                        <tr>
                        <td>'.explode(" ", $data12['tanggal'])[0].'</td>
                        <td>PSI-'.$data12['kode'].'</td>
                        <td>'.$data12['keterangan'].'</td>
                        <td>0</td>
                        <td>'.number_fromat($data12['total']).'</td>
                        <td>0</td>
                        </tr>
                        ';
                      }

                      $sql12=mysql_query("select PC.* from p_agent_invoice PC 
                        inner join coa C on C.no = PC.account
                        where C.id = '".$dataCoa['id']."' and PC.status != '3'");
                      while($data12=mysql_fetch_array($sql12))
                      { 

                        echo '
                        <tr>
                        <td>'.explode(" ", $data12['tanggal'])[0].'</td>
                        <td>PAI-'.$data12['kode'].'</td>
                        <td>'.$data12['keterangan'].'</td>
                        <td>0</td>
                        <td>'.number_fromat($data12['total']).'</td>
                        <td>0</td>
                        </tr>
                        ';
                      }

                      $sql12=mysql_query("select PC.* from p_customer_invoice PC 
                        inner join coa C on C.no = PC.account
                        where C.id = '".$dataCoa['id']."' and PC.status != '3'");
                      while($data12=mysql_fetch_array($sql12))
                      { 
                        echo '
                        <tr>
                        <td>'.explode(" ", $data12['tanggal'])[0].'</td>
                        <td>PCI-'.$data12['kode'].'</td>
                        <td>'.$data12['keterangan'].'</td>
                        <td>'.number_fromat($data12['total']).'</td>
                        <td>0</td>
                        <td>0</td>
                        </tr>
                        ';
                      }

                      $sql12=mysql_query("select PCD.*,PC.tanggal from jurnal_manual_detail PCD
                        inner join jurnal_manual PC on PC.kode = PCD.kode
                        inner join coa C on C.no = PCD.account
                        where C.id = '".$dataCoa['id']."' and PC.status != '3'");
                      while($data12=mysql_fetch_array($sql12))
                      { 

                        echo '
                        <tr>
                        <td>'.explode(" ", $data12['tanggal'])[0].'</td>
                        <td>PC-'.$data12['kode'].'</td>
                        <td>'.$data12['keterangan'].'</td>
                        <td>'.number_fromat($data12['debit']).'</td>
                        <td>'.number_fromat($data12['credit']).'</td>
                        <td>0</td>
                        </tr>
                        ';
                      }

  // ==========================================================================


                      echo '
                      <tr>
                      <td colspan="3" style="background-color: cyan;">Total '.$dataCoa['id'].' - '.$dataCoa['nama'].'</td>
                      <td style="background-color: cyan;">0</td>
                      <td style="background-color: cyan;">0</td>
                      <td style="background-color: cyan;">0</td>
                      </tr>
                      ';
                      echo '
                      <tr>
                      <td colspan="6"></td>

                      </tr>
                      ';
                    }
                    
                    ?>
                  </tbody>
                </table>
              </div>

            </div>
            <br>
            <br>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

  </div>

