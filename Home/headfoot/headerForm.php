<?php
include '../function/init.php';
$data_web = mysql_fetch_array(mysql_query("select * from setting_web limit 1"));
if(empty($_SESSION['no']))
{
	header('Location:../../');
}
$divt = $user_data['groups'];
$divisinyadia = "";
$madep = "";
$idDep = "";
$sql1=mysql_query("select GD.*,J.nama as madep,J.no as idDeps from group_divisi GD 
	inner join jabatan J on J.no = GD.departemen
	where GD.no = '$divt'");
while($data1=mysql_fetch_array($sql1))
{  
	$divisinyadia = $data1['nama'];
	$madep = $data1['madep'];
	$idDep = $data1['idDeps'];
}
$notifJumlah = 0;
$totalNotif = mysql_fetch_array(mysql_query("select count(*) as totals from izin I
	inner join karyawan K on K.no = I.id_kar
	inner join group_divisi GD on GD.no = K.jabatan
	where I.status = '1' and GD.atasan = '$idDep'"));

$notifJumlah += $totalNotif['totals'];

if($divt == "10")
{
	$totalNotif2 = mysql_fetch_array(mysql_query("select count(*) as totals from izin where status = '2'"));
	$notifJumlah += $totalNotif2['totals'];
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $data_web['nama']?></title>
	<link rel="icon" type="image/png" href="../images/<?php echo $data_web['logo']?>"/>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
	<!-- daterange picker -->
	<link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" href="../plugins/iCheck/all.css">
	<!-- Bootstrap Color Picker -->
	<link rel="stylesheet" href="../bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
	<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="../bower_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <style>
  	/* Red border */
  	hr.abu {
  		border-top: 1px solid silver;
  	}
  }
</style>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="../https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="../https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="." class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b><?php echo $data_web['inisial']?></b></span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b><?php echo $data_web['inisial']?></b></span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="../#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<!-- <li class="dropdown user user-menu"><marquee width="100%"><strong style="color:white;"><?php echo $kataNews;?></strong></marquee></li> -->

						<!-- Notifications: style can be found in dropdown.less -->
						<li class="dropdown notifications-menu">
							<a href="../#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-bell-o"></i>
								<?php 
								if($notifJumlah != "0")
								{
									?>
									<span class="label label-danger"><?php echo $notifJumlah?></span>
									<?php 
								}
								?>
							</a>
							<ul class="dropdown-menu">
								<li>
									<!-- inner menu: contains the actual data -->
									<ul class="menu">
										<?php 
										if($notifJumlah != "0")
										{
											?>
											<li>
												<a href="../app_hadir">
													<i class="fa fa-users text-aqua"></i> Kamu Mempunyai <?php echo $notifJumlah;?> Approval Izin
												</a>
											</li>
											<?php 
										}
										?>
									</ul>
								</li>
							</ul>
						</li>

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="../#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="../images/<?php echo $user_data['photo'];?>" class="user-image" alt="User Image">
								<span class="hidden-xs"><?php echo $user_data['nama'];?></span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="../images/<?php echo $user_data['photo'];?>" class="img-circle" alt="User Image">

									<p>
										<?php echo $user_data['nama']?> 
										<small><?php echo $divisinyadia;?> </small>
										<small><?php echo $madep;?> </small>
									</p>
								</li>
								<!-- Menu Body -->
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="../profil" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="../function/F_logout.php" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<!-- Control Sidebar Toggle Button -->

					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<?php
		include 'menu.php';
		?>
