  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user_data['nama'];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">

        <li class="header">Main Navigation</li>
        <li>
          <a href="../dashboard">
            <i class="fa fa-home"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cube"></i> <span>Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-user"></i> User
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="../user_data"> User Data</a></li>
                <li><a href="../group"> Group User</a></li>
                <li><a href="../otorisasi"> Otorisasi</a></li>
              </ul>
            </li>

            <li class="treeview">
            <a href="#"><i class="fa fa-map"></i> Geographical
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"> Modul</a></li>
                <li><a href="#"> Sub Modul</a></li>
              </ul>
            </li>

            <li>
              <a href="../dashboard">
                <i class="fa fa-users"></i> <span>Agent</span>
              </a>
            </li>

            <li class="treeview">
            <a href="#"><i class="fa fa-road"></i> Moda
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"> Udara</a></li>
                <li><a href="#"> Darat</a></li>
                <li><a href="#"> Laut</a></li>
              </ul>
            </li>

            <li class="treeview">
            <a href="#"><i class="fa fa-calendar"></i> Schedule
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"> Schedule</a></li>
                <li><a href="#"> Route</a></li>
              </ul>
            </li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Reservation</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-cart-plus"></i> Direct Sales
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"> Sales Order</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-cart-plus"></i> Sales Agent
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"> Sales Order</a></li>
              </ul>
            </li>
          </ul>
        </li>

        <li class="header">Settings</li>
        <li>
          <a href="../parent">
            <i class="fa fa-th"></i> <span>Parent Modul</span>
          </a>
        </li>
        <li>
          <a href="../modul">
            <i class="fa fa-th"></i> <span>Modul</span>
          </a>
        </li>
        <li>
          <a href="../sub_modul">
            <i class="fa fa-th"></i> <span>Sub Modul</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>