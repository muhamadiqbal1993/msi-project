<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <?php if($addPM == "1"){?>
            <form action="." method="post">
              <input type="hidden" name="type" value="input">
              <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

            <hr class="abu">
            <?php }?>
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Berhasil Dibuat.
              </div>
            </div>';
          }
          if($status == "2")
          {
            echo '<div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
            </div>
          </div>';
        }
        if($status == "3")
        {
          echo '<div class="col-xs-12">
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Warning!</h4>
            Username sudah ada yang menggunakan , silahkan daftarkan dengan username lain
          </div>
        </div>';
      }
      ?>


      <div class="col-xs-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Karyawan</th>
              <th>Jenis Izin</th>
              <th>Dari Tanggal</th>
              <th>Sampai Tanggal</th>
              <th>Total Hari</th>
              <th>Keterangan</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 

            $sql1=mysql_query("select I.*,K.nama as namkar,JI.nama as namjin,DATE_FORMAT(I.dari, '%d %M %Y') as daris,DATE_FORMAT(I.sampai, '%d %M %Y') as sampais from izin I
              inner join jenis_izin JI on JI.no = I.id_izin
              left outer join karyawan K on K.no = I.id_kar
              ");
            while($data1=mysql_fetch_array($sql1))
            {  
              $status = "Menunggu Approval Atasan";
              $btn = '<a href="./?id='.$data1['no'].'">Manage</a>';
              if($data1['status'] == "2")
              {
                $status = "Menunggu Approval HRD";
                $btn = "";
              }
              if($data1['status'] == "3")
              {
                $status = "Di Tolak";
                $btn = "";
              }
              if($data1['status'] == "4")
              {
                $status = "Di Setujui";
                $btn = "";
              }

              echo '
              <tr>
                <td>'.$data1['namkar'].'</td>
                <td>'.$data1['namjin'].'</td>
                <td>'.$data1['daris'].'</td>
                <td>'.$data1['sampais'].'</td>
                <td>'.$data1['hari'].' Hari</td>
                <td>'.$data1['keterangan'].'</td>
                <td>'.$status.'</td>      
                <td><a href="./?id='.$data1['no'].'">View</a></td>
              </tr>
              ';
            }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <br>
    <br>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>

</div>