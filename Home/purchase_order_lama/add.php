<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>

					</div>

					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<form id="create_data" method="POST"> 
			<input type="hidden" name="input" class="form-control" value="1">
			<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Vendor</strong></label>
									<input type="hidden" name="no_order" class="form-control" value="<?php echo date('YmdHis')?>">
									<select class="form-control select2" name="vendor" id="vendor" style="width: 100%;" required="">
										<option value="" >--- Pilih Vendor ---</option>
										<?php
										$sql1=mysql_query("select * from vendor where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['kode'].' - '.$data1['nama']?></option>
											<?php 
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label><strong>Tanggal Invoice</strong></label>
									<input type="text" name="tanggal" id="datepicker" class="form-control" value="<?php echo date('m/d/Y');?>" required>
								</div>
								<div class="form-group">
									<label><strong>Jatuh Tempo</strong></label>
									<input type="text" name="due" id="datepicker1" class="form-control" value="<?php echo date('m/d/Y');?>">
								</div>
							</div>
							<div class="col-md-6">
								
								<div class="form-group">
									<label><strong>Account</strong></label>
									<select class="form-control select2" name="account" style="width: 100%;" >
										<option value="" >--- Pilih Account ---</option>
										<?php
										$sql1=mysql_query("select * from coa where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['id']?>" ><?php echo $data1['id'].' - '.$data1['nama']?></option>
											<?php 
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<label><strong>Keterangan</strong></label>
									<input type="text" name="keterangan" class="form-control" required>
								</div>
							</div>
						
						<div class="col-md-12">
							<hr class="abu">
						</div>
						<div class="col-md-12">
							<div class="box-body table-responsive no-padding">
								<table id="so_table" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th style="width: 20%">Item Product</th>
											<th>Qty</th>
											<th>Harga</th>
											<th>Keterangan</th>
											<th >Total</th>
											<th style="width: 5%"><i class="fa fa-trash" aria-hidden="true"></i></th>
										</tr>
									</thead>
								</table>
							</div>
							<br>
							<a href="javascript:void(0)" id="tambahBtn1" onclick="addLine()">+ Tambah Item</a>
						</div>

						<script language="javascript" type="text/javascript">

							function addLine()
							{
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');

								var row = table.insertRow(rowCount);
								var cell0 = row.insertCell(0);
								var cell1 = row.insertCell(1);
								var cell2 = row.insertCell(2);
								var cell3 = row.insertCell(3);
								var cell4 = row.insertCell(4);
								var cell5 = row.insertCell(5);
								var cell6 = row.insertCell(6);
							

								cell0.innerHTML = rowCount;
								cell1.innerHTML = '<input type="text" id="editItem'+rowCount+'" name="editItem[]" class="form-control" >';
								cell2.innerHTML = '<input type="number" id="editQty'+rowCount+'" name="editQty[]" oninput="getNilaiTotal('+rowCount+')" class="form-control" value="0">';
								cell3.innerHTML = '<input type="number" id="editHarga'+rowCount+'" name="editHarga[]" oninput="getNilaiTotal('+rowCount+')" class="form-control" value="0">';
								cell4.innerHTML = '<input type="text" id="editKeterangan'+rowCount+'" name="editKeterangan[]" class="form-control" >';
								cell5.innerHTML = '';
								cell6.innerHTML = '<a href="javascript:void(0)" onclick="delete_row('+rowCount+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
							}

							function getNilaiTotal(abs)
							{
								var table = document.getElementById('so_table');

								var tBerat = "editQty"+abs
								var tHarga = "editHarga"+abs
								
								var nBerat = document.getElementById(tBerat).value;
								var nHarga = document.getElementById(tHarga).value;

								var totalSemua = (parseFloat(nBerat) * parseFloat(nHarga));

								table.rows[abs].cells[5].innerHTML = parseInt(totalSemua).toLocaleString('en-US');

								totalPajak();
								
							}

							function viewTotal()
							{
								var table = document.getElementById('so_table');
								var rowCount = document.getElementById('so_table').rows.length;
								var subTotal = 0;
								for(var a=1;a<rowCount;a++)
								{
									subTotal += parseInt(table.rows[a].cells[5].innerHTML.split(",").join(""));
								}
								document.getElementById("subtotal").value = subTotal.toLocaleString('en-US');
								var nilaiPajak = subTotal + parseInt(document.getElementById("lainya").value.split(",").join(""));
								document.getElementById("grandtotal").value = nilaiPajak.toLocaleString('en-US');
							}

							function delete_row(abs)
							{
								document.getElementById("so_table").deleteRow(abs);
								var rowCount = document.getElementById('so_table').rows.length;
								var table = document.getElementById('so_table');
								for(var a=1;a<rowCount;a++)
								{
									var ab = a;

									if(abs <= a)
									{
										ab = a + 1;
									}

									var n_editItem = "editItem"+ab;
									var n_editQty= "editQty"+ab;
									var n_editHarga= "editHarga"+ab;
									var n_editKeterangan= "editKeterangan"+ab;

									var editItem = document.getElementById(n_editItem).value;
									var editQty = document.getElementById(n_editQty).value;
									var editHarga = document.getElementById(n_editHarga).value;
									var editKeterangan = document.getElementById(n_editKeterangan).value;
									var editTotal = 0;

									table.rows[a].cells[0].innerHTML = a;
									table.rows[a].cells[1].innerHTML = '<input type="text" id="editItem'+a+'" name="editItem[]" class="form-control" value="'+editItem+'">';
									table.rows[a].cells[2].innerHTML = '<input type="text" id="editQty'+a+'" name="editQty[]" oninput="getNilaiTotal('+a+')" class="form-control" value="'+editQty+'">';
									table.rows[a].cells[3].innerHTML = '<input type="number" id="editHarga'+a+'" name="editHarga[]" oninput="getNilaiTotal('+a+')" class="form-control" value="'+editHarga+'">';
									table.rows[a].cells[4].innerHTML = '<input type="text" id="editKeterangan'+a+'" name="editKeterangan[]" class="form-control" value="'+editKeterangan+'">';
									table.rows[a].cells[6].innerHTML = '<a href="javascript:void(0)" onclick="delete_row('+a+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
									totalPajak();
								}
								totalPajak();
							}

							function delete_row_pajak(abs)
							{
								document.getElementById("pajak_table").deleteRow(abs);
								var rowCount = document.getElementById('pajak_table').rows.length;
								var table = document.getElementById('pajak_table');
								for(var a=1;a<rowCount;a++)
								{
									table.rows[a].cells[0].innerHTML = a;
									table.rows[a].cells[3].innerHTML = '<a href="javascript:void(0)" onclick="delete_row_pajak('+a+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
									totalPajak();
								}
								totalPajak();
							}

							function getProductPajak(abs)
							{
								var pajaks = '<select class="form-control select2" id="product_editss'+abs+'" name="editPajak[]" onchange="getNilaiPajak('+abs+')" style="width: 100%;" required="">'+
								<?php

								$sql1=mysql_query("select * from pajak where status = '1'");
								while($data1=mysql_fetch_array($sql1))
								{
									?>
									'<option value="<?php echo $data1['no'].'##'.$data1['nilai'];?>"><?php echo $data1['nama']?></option>'+

									<?php 
								}?>

								'</select>';

								return pajaks;
							}

							function getNilaiPajak(abs)
							{
								//alert(abs);
								var cellValue = "product_editss"+abs
								var table = document.getElementById('pajak_table');
								var isiNilai = document.getElementById(cellValue).value;
								if(isiNilai == "")
								{
									table.rows[abs].cells[2].innerHTML = 0;
								}
								else
								{
									table.rows[abs].cells[2].innerHTML = isiNilai.split("##")[1];
								}
								

								totalPajak();

							}

							function totalPajak()
							{
								var nilaiSubtotal = parseInt(document.getElementById("subtotal").value.split(",").join(""));

								var rowCount = document.getElementById('pajak_table').rows.length;
								var table = document.getElementById('pajak_table');
								var nilaiPajak = 0;
								for(var a=1;a<rowCount;a++)
								{
									var persentasePajak = nilaiSubtotal * (parseFloat(table.rows[a].cells[2].innerHTML) / 100);
									nilaiPajak += persentasePajak ;
									//viewTotal();
								}
								document.getElementById("lainya").value = nilaiPajak.toLocaleString('en-US');
								viewTotal();
							}

							function addLinePajak()
							{
								var rowCount = document.getElementById('pajak_table').rows.length;
								var table = document.getElementById('pajak_table');

								var row = table.insertRow(rowCount);
								var cell0 = row.insertCell(0);
								var cell1 = row.insertCell(1);
								var cell2 = row.insertCell(2);
								var cell3 = row.insertCell(3);


								cell0.innerHTML = rowCount;
								cell1.innerHTML = getProductPajak(rowCount);
								cell2.innerHTML = '';
								cell3.innerHTML = '<a href="javascript:void(0)" onclick="delete_row_pajak('+rowCount+')" ><i class="fa fa-trash" aria-hidden="true"></i></a>';
								var nams = '#product_editss'+rowCount;
								$(nams).select2();
								$(nams).val(""); 
								$(nams).change();
							}

						</script>
						<div class="col-md-12">
							<br>
						</div>
					</div>
					
				</div>
				
			</div>

		</div>

		<div class="row">
			<div class="col-xs-7">
				<div class="box">
					<div class="box-body">
						<div class="col-md-12">
							<h5><strong>Pajak Tambahan</strong></h5>
							<p></p>
							<div class="box-body table-responsive no-padding">
								<table id="pajak_table" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>No</th>
											<th style="width: 40%">Nama Pajak</th>
											<th>Nilai (Dalam %)</th>
											<th><i class="fa fa-trash" aria-hidden="true"></i></th>
										</tr>
									</thead>
								</table>
							</div>
							<br>
							<a href="javascript:void(0)" id="tambahBtn22" onclick="addLinePajak()">+ Tambah Item</a>
						</div>
					</div>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<div class="col-xs-5">
				<div class="box">
					<div class="box-body">
						<div class="col-md-12">
							<table width="100%">
								<tbody>
									<tr>
										<td width="30%"><input type="text" class="form-control" value="SUBTOTAL :" readonly></td>
										<td width="70%"><input type="text" id="subtotal" name="subtotal" class="form-control" value="0" readonly style="text-align: right; "></td>
									</tr>
									<tr>
										<td width="30%"><input type="text" class="form-control" value="BIAYA LAINYA :" readonly></td>
										<td width="70%"><input type="text" id="lainya" name="lainya" class="form-control" value="0" readonly style="text-align: right; " ></td>
									</tr>
								</tbody>
							</table>
							<hr class="abu">
							<table width="100%">
								<tbody>
									<tr>
										<td width="30%"><input type="text" class="form-control" value="GRAND TOTAL :" readonly style="font-weight:bold;"></td>
										<td width="70%"><input type="text" id="grandtotal" name="grandtotal" class="form-control" value="0" readonly style="text-align: right;font-weight:bold; "></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<br>
					
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>

		</div>
	</form>
</section>


</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>

