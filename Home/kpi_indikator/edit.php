<?php
$dataEdit = mysql_fetch_array(mysql_query("select * from kpi_indikator where no = '$id'"));
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<input type="hidden" name="input" class="form-control" value="2">
								<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
								<input type="hidden" name="kode" class="form-control" required="" value="<?php echo $kode;?>" >	
								<div class="form-group">
									<label>Aspek</label>
									<select class="form-control select2" name="aspek" style="width: 100%;" required="">
										<option value="1" <?php if($dataEdit['aspek'] == "1"){echo "selected";}?>>Aspek Individu</option>
										<option value="2" <?php if($dataEdit['aspek'] == "2"){echo "selected";}?>>Aspek Kinerja</option>
									</select>
								</div>
								<div class="form-group">
									<label>Aspek Untuk Jabatan</label>
									<select class="form-control select2" name="jabatan" style="width: 100%;" required="">
										<option value="" >--- Pilih Jabtan ---</option>
										<?php
										$sql1=mysql_query("select * from group_divisi where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" <?php if($dataEdit['jabatan'] == $data1['no']){echo "selected";}?>><?php echo $data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control select2" name="aktif" style="width: 100%;" required="">
										<option value="1" <?php if($dataEdit['status'] == "1"){echo "selected";}?>>Aktif</option>
										<option value="2" <?php if($dataEdit['status'] == "2"){echo "selected";}?>>Tidak Aktif</option>

									</select>
								</div>	
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Target Nilai</label>
									<input type="number" name="target" class="form-control" value="<?php echo $dataEdit['target']?>">
								</div>	
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control" value="<?php echo $dataEdit['nama']?>">
								</div>
								
								
								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<?php 
				$nama_modulnya = 'kpi_indikator';
				include '../headfoot/history.php';
				?>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function bukaPassword()
	{
		var aa = document.getElementById("bukaPas").readOnly;
		if(aa)
		{
			document.getElementById("bukaPas").readOnly = false;
		}
		else
		{
			document.getElementById("bukaPas").readOnly = true;
		}
	}
</script>