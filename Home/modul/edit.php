<?php

$parent = "";
$nama = "";
$ids = "";
$urutan = "";
$logo = "";
$status = "";

$sql1=mysql_query("select * from modul where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$parent = $data1['parent'];
	$nama = $data1['nama'];
	$ids = $data1['id'];
	$urutan = $data1['sequence'];
	$logo = $data1['logo'];
	$status = $data1['status'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Modul
			<small>Edit Modul</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> Modul</a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Group User</label>
									<select class="form-control select2" name="parent" style="width: 100%;" required="">
										<?php
										$sql1=mysql_query("select * from parent_modul where status = '1'");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $parent){echo "selected";}?>><?php echo $data1['nama']?></option>

											<?php }?>

										</select>
									</div>
									<div class="form-group">
										<label>Id</label>
										<input type="hidden" name="input" class="form-control" value="2">
										<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
										<input type="text" name="id" class="form-control" required="" value="<?php echo $ids;?>" readonly>
									</div>
									<div class="form-group">
										<label>Urutan:</label>
										<input type="number" name="urutan" class="form-control" value="<?php echo $urutan;?>">
									</div>
									
								</div>
								<!-- /.col -->
								<div class="col-md-6">
									<div class="form-group">
										<label>Nama Parent Modul</label>
										<input type="text" name="nama" class="form-control" required="" value="<?php echo $nama;?>">
									</div>
									<div class="form-group">
										<label>Logo</label>
										<input type="text" name="logo" class="form-control" required="" value="<?php echo $logo;?>">
									</div>
									<!-- /.form-group -->
									<div class="form-group">
										<label>Status</label>
										<select class="form-control select2" name="aktif" style="width: 100%;" required="">
											<option value="1" <?php if($status == "1"){echo "selected";}?>>Aktif</option>
											<option value="2" <?php if($status == "2"){echo "selected";}?>>Tidak Aktif</option>

										</select>
									</div>
									
									<!-- /.form-group -->
								</div>
								<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
							</div>
						</form>
						<br>
						<br>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
	</div>

	<script>
		function save_modul()
		{
			document.getElementById("create_data").action = ".";
		}
		function bukaPassword()
		{
			var aa = document.getElementById("bukaPas").readOnly;
			if(aa)
			{
				document.getElementById("bukaPas").readOnly = false;
			}
			else
			{
				document.getElementById("bukaPas").readOnly = true;
			}
		}
	</script>