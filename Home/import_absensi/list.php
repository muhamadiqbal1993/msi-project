<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php 
      echo $modulnya;

      // $row = mysql_fetch_array(mysql_query("select count(*) as totalnya from awb_header"));
      // $next_id = $row['totalnya'] + 1;
      ?>
      <small>Import Absensi</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active">Import Absensi</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <!-- <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a> -->
            &nbsp;&nbsp;&nbsp;
            
            <!-- <hr class="abu"> -->
          </div>
          
          <div class="box-body">
            <form id="create_data" method="POST" enctype="multipart/form-data">
              <!-- <div class="col-md-12">
                <div class="form-group">
                  <label>Customer</label>
                  <select class="form-control select2" name="customer" style="width: 100%;" required="">
                    <?php
                    $sql1=mysql_query("select * from (
                      select *,(select no from account_airport where airport = customer.no and account = '".$user_data['no']."' and modul = 'customer_user') as isi from customer where status = '1' ) as tb1 where isi != ''");
                    while($data1=mysql_fetch_array($sql1))
                    {
                      ?>
                      <option value="<?php echo $data1['nama']?>" ><?php echo $data1['nama']?></option>
                      <?php 
                    }?>

                  </select>
                </div>
              </div> -->
              <div class="col-md-12">
                <label>Input File</label>
                <input type="file" name="dataExcel" required="">
              </div>
              <div class="col-md-1">
                <br>
                <button type="submit" onclick="save_modul()" form="create_data" class="btn btn-success">Import File</button>
              </div>

            </form>
            <div class="col-md-2">
              <br>
              <a href="upload/template/template.xls" target="_blank"><button type="submit" class="btn btn-primary">Download Format</button></a>
            </div>
          </div>

          <br>
          <br>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Hasil Export</h3>
            <div class="box-tools">
              <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                <!-- <button type="submit" form="create_data" class="btn btn-primary"></button> -->
                <a></a>

                <!-- <div class="input-group-btn">
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div> -->
              </div>
            </div>
            <hr class="abu">
          </div>

          <!-- /.box-header -->
          
          <div class="box-body table-responsive no-padding">
            <div class="col-xs-12">
              <table class="table table-hover">
                <th>Kode Karyawan</th>
                <th>Nama Karyawan</th>
                <th>Tanggal Absen</th>
                <th>Jam Absen</th>
                <?php
                if (!empty($_FILES['dataExcel']['tmp_name'])) {
                  set_time_limit(300);

                  $target_dir = "upload/".basename($_FILES['dataExcel']['name']);

                  move_uploaded_file($_FILES['dataExcel']['tmp_name'],$target_dir);

                  //mysql_query("delete from jadwal");
                  require('import-excel/spreadsheet-reader-master/php-excel-reader/excel_reader2.php');
                  require('import-excel/spreadsheet-reader-master/SpreadsheetReader.php');
                  $account = "";
                  $batch = date("Ymd");
                  $Reader = new SpreadsheetReader($target_dir);
                  $sukses = 0;
                  $gagal = 0;
                  $no = 1;
                  $tgls = date('d-m-Y');
                  foreach ($Reader as $Key => $Row)
                  {
                    if ($Key < 2) continue; 

                    $dataKaryawan = mysql_fetch_array(mysql_query("select * from karyawan where kode = '$Row[0]'"));

                    echo '
                    <tr>
                      <td>'.$Row[0].'</td>
                      <td>'.$dataKaryawan['nama'].'</td>
                      <td>'.$Row[1].'</td>
                      <td>'.$Row[2].'</td>
                    </tr>';
                    $ckData = mysql_fetch_array(mysql_query("select * from absensi where id_kar = '".$dataKaryawan['no']."' and tanggal like '%$Row[1]%' and jam = '$Row[2]'"));
                    if(empty($ckData))
                    {
                      mysql_query("insert into absensi values(null,'".$dataKaryawan['no']."','$Row[1]','$Row[2]')");
                    }
                    
                    $no++;
                  }
                }

                ?>
              </table>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
</div>

<script>
  function save_modul()
  {
    document.getElementById("create_data").action = ".";
  }
</script>