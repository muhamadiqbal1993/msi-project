<?php

$nama = "";
$isi = "";

$sql1=mysql_query("select * from setting where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$nama = $data1['nama'];
	$isi = $data1['isi'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>

						<hr class="abu">
					</div>
					<form id="create_data" method="POST"> 
						<div class="box-body">
							<div class="col-md-6">
								<input type="hidden" name="input" class="form-control" value="2">
								<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control" required="" value="<?php echo $nama;?>" readonly>
								</div>
								

								<!-- /.form-group -->
								
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Isi</label>
									<input type="text" name="isi" class="form-control timepicker1" required="" value="<?php echo $isi;?>">
								</div>


								<!-- /.form-group -->
							</div>
							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'datang_pulang';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
</script>