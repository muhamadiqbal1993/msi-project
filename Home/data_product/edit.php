<?php

$nama = "";
$ids = "";
$harga = "";
$flyer = "";
$brand = "";
$barcode = "";
$photo = "";
$id_odoo = "";
$status = "";

$rak = "";
$satuan = "";
$hpp = "";
$stock = "";
$spesifikasi = "";

$sql1=mysql_query("select * from product where no = '$id'");
while($data1=mysql_fetch_array($sql1))
{ 
	$nama = $data1['nama'];
	$ids = $data1['kode'];
	$harga =$data1['harga'];
	$flyer =$data1['harga_flyer'];
	$brand =$data1['kategory'];
	$barcode = $data1['barcode'];
	$photo = $data1['photo'];
	$status = $data1['status'];
	$id_odoo = $data1['id'];

	$rak = $data1['rak'];
	$satuan = $data1['satuan'];
	$hpp = $data1['harga_flyer'];
	$stock = $data1['type'];
	$spesifikasi = $data1['spesifikasi'];
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Edit <?php echo $modulnya;?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Edit Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<?php if($editPM == "1"){?>
							<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<?php }?>
						<hr class="abu">
					</div>
					
					<div class="box-body">
						<form id="create_data" method="POST" enctype="multipart/form-data">
							<div class="col-md-2">
								<p></p>
								<p></p>
								<img id="photo1" src="../images/product/<?php echo $photo;?>" style="height: 200px;width: 200px"/>
								<p></p>
								<div class="btn btn-default btn-file" id="photo62">
									<i class="fa fa-image"></i> Pilih Photo
									<input type="file" name="photos" onchange="viewImage(this);">
								</div>
								<p></p>
								<p></p>
								<p></p>
								<p></p>
							</div>
							<div class="col-md-5">
								<input type="hidden" name="input" class="form-control" value="4">
								<input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
								<div class="form-group">
									<label>Kode Product</label>
									<input type="text" name="id" class="form-control" required="" value="<?php echo $ids;?>" readonly>
								</div>
								
								<div class="form-group">
									<label>Nama</label>
									<input type="text" name="nama" class="form-control" value="<?php echo $nama;?>">
								</div>
								<div class="form-group">
									<label>Satuan Product</label>
									<select class="form-control select2" name="satuan" style="width: 100%;" required="">
										<option value="" >--- Pilih Satuan ---</option>
										<?php
										$sql1=mysql_query("select * from satuan_product");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['kode']?>" <?php if($satuan == $data1['kode']){echo "selected";}?>><?php echo $data1['nama'].' -'.$data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								<div class="form-group">
									<label>Rak Gudang</label>
									<select class="form-control select2" name="rak" style="width: 100%;" required="">
										<option value="" >--- Pilih Rak Gudang ---</option>
										<?php
										$sql1=mysql_query("select R.*,G.nama as namgud from rak R
											inner join gudang G on G.no = R.gudang");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" <?php if($rak == $data1['no']){echo "selected";}?>><?php echo $data1['namgud'].' - '.$data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								<div class="form-group">
									<label>Type</label>
									<input type="text" name="tipe" class="form-control" value="<?php echo $barcode;?>">
								</div>
								<div class="form-group">
									<label>Status</label>
									<select class="form-control select2" name="aktif" style="width: 100%;" required="">
										<option value="1" <?php if($status == "1"){echo "selected";}?>>Aktif</option>
										<option value="2" <?php if($status == "2"){echo "selected";}?>>Tidak Aktif</option>
									</select>
								</div>
								
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<label>Harga Jual</label>
									<input type="number" name="harga" class="form-control" required="" value="<?php echo $harga;?>">
								</div>
								<div class="form-group">
									<label>Hpp</label>
									<input type="text" name="hpp" class="form-control" readonly="" value="<?php echo number_format($hpp);?>">
								</div>
								<div class="form-group">
									<label>Total Stock <a href="../stock_product/?id=<?php echo $id?>" target="_blank">(View History)</a></label>
									<input type="text" name="stock" class="form-control" readonly="" value="<?php echo number_format($stock);?>">
								</div>
								
								<div class="form-group">
									<label>Brand</label>
									<select class="form-control select2" name="kategory" style="width: 100%;" required="">
										<option value="" >--- Pilih Brand ---</option>
										<?php
										$sql1=mysql_query("select * from brand");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" <?php if($brand == $data1['no']){echo "selected";}?>><?php echo $data1['nama']?></option>
										<?php }?>

									</select>
								</div>
								<div class="form-group">
									<label>Spesifikasi Product (PDF)</label>
									<br>
									<a href="../images/spesifikasi/<?php echo $spesifikasi?>"><?php echo $spesifikasi?></a>
									<br>
									<br>
									<input type="file" name="pdfs">
								</div>
								
								
							</div>
						</form>
						<div class="col-xs-12">
							<hr class="abu">
							<h4><strong>History Vendor</strong></h4>
							<hr class="abu">
						</div>
						<div class="col-xs-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Vendor</th>
										<th>No PO</th>
										<th>Tanggal Transaksi</th>
										<th>Qty Dibeli</th>
										<th>Harga Dibeli</th>
									
									</tr>
								</thead>
								<tbody>
									<?php 

									$sql1=mysql_query("select BD.*,V.nama as namven,B.tanggal from bill_detail BD 
										inner join bill B on B.bill = BD.bill
										inner join vendor V on V.no = B.vendor
										where BD.product = '$id'
										and B.status != '3'
										group by B.vendor
										");
									while($data1=mysql_fetch_array($sql1))
									{  
			

										echo '
										<tr>
								
										<td>'.$data1['namven'].'</td>
										<td>'.$data1['bill'].'</td>
										<td>'.$data1['tanggal'].'</td>
										
										<td>'.$data1['qty'].'</td>
										<td>'.number_format($data1['harga_jual']).'</td> 
										
									
										</tr>
										';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
					<br>
					<br>
					
					<!-- /.box-body -->
				</div>

				<?php 
				$nama_modulnya = 'product';
				include '../headfoot/history.php';
				?>

				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}
	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(200)
				.height(200);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
