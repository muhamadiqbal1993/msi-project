<?php
$dataEdit = mysql_fetch_array(mysql_query("select DATE_FORMAT(tanggal, '%m/%d/%Y') as new_tanggal from setting_account where no = '1'"));
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?></a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">

						<button type="button" onclick="save_modul()" class="btn btn-primary">Simpan</button>

					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="box">
					<form id="create_data" method="POST"> 
						<input type="hidden" name="input" class="form-control" value="1">
						<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 
						<input type="hidden" name="debit" id="debit" class="form-control"> 
						<input type="hidden" name="credit" id="credit" class="form-control"> 
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal Close Transaksi (Close Accounting)</label>
									<input type="text" name="tanggal" id="datepicker1" class="form-control" value="<?php echo $dataEdit['new_tanggal']?>">
								</div>
							</div>
							<div class="col-md-6">
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-12">
										<hr class="abu">
										<h1><?php echo $modulnya.' Line';?></h1>
										<hr class="abu">
									</div>
									<div class="col-md-12">
										<table id="tabDidik" class="table table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%" >Kode</th>
													<th style="width: 20%">Jenis Transaksi</th>
													<th>Jurnal Debit</th>
													<th>Jurnal Credit</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$noms = 1;
												$sql1=mysql_query("select * from setting_account where status = '2'");
												while($data1=mysql_fetch_array($sql1))
												{

													$acc_debit = '<select class="form-control select2" name="coa_debit[]" id="coa_debit'.$noms.'" style="width: 100%;" required="">';

													$sql12=mysql_query("select * from coa where status = '1'");
													while($data12=mysql_fetch_array($sql12))
													{
														$slct = "";
														if($data1['debit'] == $data12['id'])
														{
															$slct = "selected";
														}
														$acc_debit .= '<option value="'.$data12['id'].'" '.$slct.'>'.$data12['id'].' - '.$data12['nama'].'</option>';
													}
													$acc_debit .= '</select>';

													$acc_credit = '<select class="form-control select2" name="coa_credit[]" id="coa_credit'.$noms.'" style="width: 100%;" required="">';

													$sql12=mysql_query("select * from coa where status = '1'");
													while($data12=mysql_fetch_array($sql12))
													{
														$slct = "";
														if($data1['credit'] == $data12['id'])
														{
															$slct = "selected";
														}
														$acc_credit .= '<option value="'.$data12['id'].'" '.$slct.'>'.$data12['id'].' - '.$data12['nama'].'</option>';
													}
													$acc_credit .= '</select>';

													echo '
													<tr>
													<td>'.$data1['no'].'</td>
													<td><input type="text" name="nama[]" class="form-control" value="'.$data1['nama'].'" readonly></td>
													<td>'.$acc_debit.'</td>
													<td>'.$acc_credit.'</td>
													</tr>
													';
													$noms++;
												}
												?>
											</tbody>

										</table>
									</div>
								</div>
							</div>

						</div>
					</form>
					<br>
					<br>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
</section>
</div>

<script>
	function save_modul()
	{
		var debs = document.getElementById("debit").value;
		var cres = document.getElementById("credit").value;
		if(debs != cres)
		{
			//alert("Total Credit Dan Debit Harus Sama");
			document.getElementById("create_data").submit();
		}
		else
		{
			document.getElementById("create_data").submit();
		}

	}
</script>
