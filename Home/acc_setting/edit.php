<?php
$dataEdit = mysql_fetch_array(mysql_query("select * from bom where no = '$id'"));

?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
      <small>Edit <?php echo $modulnya;?></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li><a href="."> <?php echo $modulnya;?></a></li>
      <li class="active">Edit Data</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
            &nbsp;&nbsp;&nbsp;
            <?php if($dataEdit['status'] != "3"){?>
             <button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
             
           <?php }?>
           <hr class="abu">
         </div>
         <form id="create_data" method="POST"> 
          <input type="hidden" name="input" class="form-control" value="2">
          <input type="hidden" name="no" class="form-control" required="" value="<?php echo $id;?>">
          <input type="hidden" name="kode" class="form-control" required="" value="<?php echo $dataEdit['id'];?>">
          <input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>">

          <div class="box-body">
            <div class="col-md-6">
              <div class="form-group">
                <label>Id Bom</label>
                <input type="text" name="kode" class="form-control" value="<?php echo $dataEdit['id'];?>" readonly>
              </div>
              <div class="form-group">
                <label>Product</label>
                <select class="form-control select2" name="product" style="width: 100%;">
                  <option value="" >----- Pilih Product Jadi -----</option>
                  <?php
                  $sql1=mysql_query("select * from product");
                  while($data1=mysql_fetch_array($sql1))
                  {
                    ?>
                    <option value="<?php echo $data1['no']?>" <?php if($dataEdit['product'] == $data1['no']){echo "selected";}?>><?php echo $data1['kode'].' - '.$data1['nama']?></option>
                    <?php 
                  }?>

                </select>
              </div>
              <div class="form-group">
                <label>Qty Produksi</label>
                <input type="text" name="qty" id="qty" class="form-control" value="<?php echo $dataEdit['qty'];?>">
              </div>
              <div class="form-group">
                <label>Status</label>
                <select class="form-control select2" name="aktif" style="width: 100%;" required="">
                  <option value="1" <?php if($dataEdit['status'] == "1"){echo "selected";}?>>Aktif</option>
                  <option value="2" <?php if($dataEdit['status'] == "2"){echo "selected";}?>>Tidak Aktif</option>

                </select>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Gudang Raw Material</label>
                <select class="form-control select2" name="g_rawmat" style="width: 100%;">
                  <option value="" >----- Pilih Gudang Raw Material -----</option>
                  <?php
                  $sql1=mysql_query("select * from gudang");
                  while($data1=mysql_fetch_array($sql1))
                  {
                    ?>
                    <option value="<?php echo $data1['no']?>" <?php if($dataEdit['gudang_rawmat'] == $data1['no']){echo "selected";}?>><?php echo $data1['id'].' - '.$data1['nama']?></option>
                    <?php 
                  }?>

                </select>
              </div>
              <div class="form-group">
                <label>Gudang Penerimaan Barang Jadi </label>
                <select class="form-control select2" name="g_barang_jadi" style="width: 100%;">
                  <option value="" >----- Pilih Gudang Penerimaan Barang Jadi -----</option>
                  <?php
                  $sql1=mysql_query("select * from gudang");
                  while($data1=mysql_fetch_array($sql1))
                  {
                    ?>
                    <option value="<?php echo $data1['no']?>" <?php if($dataEdit['gudang_barang_jadi'] == $data1['no']){echo "selected";}?>><?php echo $data1['id'].' - '.$data1['nama']?></option>
                    <?php 
                  }?>

                </select>
              </div>
              <div class="form-group">
                <label>Keterangan</label>
                <input type="text" name="keterangan" id="keterangan" class="form-control" value="<?php echo $dataEdit['keterangan'];?>">
              </div>
              <!-- /.form-group -->
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-12">
                  <hr class="abu">
                  <h1><?php echo $modulnya.' Line';?></h1>
                  <hr class="abu">
                </div>
                <div class="col-md-12">
                  <table id="tabDidik" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width: 5%" >No</th>
                        <th style="width: 20%">Product</th>
                        <th>UOM</th>
                        <th>Qty</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $noms = 1;
                      $sql1=mysql_query("select * from bom_detail where id_bom = '".$dataEdit['id']."'");
                      while($data1=mysql_fetch_array($sql1))
                      {
                        $satuans = "";
                        $accr = '<select class="form-control select2" name="r_product[]" id="r_product'.$noms.'"" onchange="getUom('.$noms.')" style="width: 100%;" required=""><option value="" >----- Pilih Product Raw Material -----</option>';

                        $sql12=mysql_query("select * from product_raw_mat where status = '1'");
                        while($data12=mysql_fetch_array($sql12))
                        {
                          $slct = "";
                          if($data1['raw_mat'] == $data12['no'])
                          {
                            $slct = "selected";
                            $satuans = $data12['satuan'];
                          }
                          $accr .= '<option value="'.$data12['no'].'#'.$data1['satuan'].'" '.$slct.'>'.$data12['kode'].' - '.$data12['nama'].'</option>';
                        }
                        $accr .= '</select>';
                        echo '
                        <tr>
                        <td>'.$noms.'</td>
                        <td>'.$accr.'</td>
                        <td><input type="text" name="editUom[]" id="editUom'.$noms.'" class="form-control" readonly value="'.$satuans.'"></td>
                        <td><input type="text" name="editQty[]" class="form-control" value="'.$data1['qty'].'"></td>

                        <td><button type="submit" onclick="delete_row_didik('.$noms.')" class="btn btn-danger">Delete</button></td>
                        </tr>
                        ';
                        $noms++;
                      }
                      ?>
                      <tr>
                        <th colspan="5" style="background-color: grey"><button type="button" class="btn btn-default" onclick="addLine_didik()">Tambah <?php echo $modulnya.' Line'?></button></th>

                      </tr>
                    </tbody>
                    <script language="javascript" type="text/javascript">

                      function getProduct(abs)
                      {
                        var products = '<select class="form-control select2" name="r_product[]" id="r_product'+abs+'" onchange="getUom('+abs+')" style="width: 100%;" required=""><option value="" >----- Pilih Product Raw Material -----</option>'+
                        <?php

                        $sql1=mysql_query("select * from product_raw_mat where status = '1'");
                        while($data1=mysql_fetch_array($sql1))
                        {
                          ?>
                          '<option value="<?php echo $data1['no'].'#'.$data1['satuan'];?>"><?php echo $data1['kode'].' - '.$data1['nama']?></option>'+

                          <?php 
                        }?>

                        '</select>';

                        return products;
                      }

                      function getUom(abs)
                      {
                        var idUom = "r_product"+abs;
                        var valueUom = "editUom"+abs;
                        var dataUom = document.getElementById(idUom).value.split("#");

                        document.getElementById(valueUom).value = dataUom[1];
                      }

                      function delete_row_didik(abs)
                      {
                        document.getElementById("tabDidik").deleteRow(abs);
                        var rowCount = document.getElementById('tabDidik').rows.length - 1;
                        var table = document.getElementById('tabDidik');
                        for(var a=1;a<rowCount;a++)
                        {
                          table.rows[a].cells[0].innerHTML = a;
                          table.rows[a].cells[4].innerHTML = "<button type='submit' onclick='delete_row_didik(\""+a+"\")' class='btn btn-danger'>Delete</button>";
                        }
                        ubahTotal();
                      }

                      function addLine_didik()
                      { 

                        var rowCount = document.getElementById('tabDidik').rows.length - 1;
                        var table = document.getElementById('tabDidik');
                        var row = table.insertRow(rowCount);
                        var cell0 = row.insertCell(0);
                        var cell1 = row.insertCell(1);
                        var cell2 = row.insertCell(2);
                        var cell3 = row.insertCell(3);
                        var cell4 = row.insertCell(4);

                        cell0.innerHTML = rowCount;
                        cell1.innerHTML = getProduct(rowCount);
                        cell2.innerHTML = '<input type="text" name="editUom[]" id="editUom'+rowCount+'" class="form-control" readonly>';
                        cell3.innerHTML = '<input type="text" name="editQty[]" class="form-control" value="0">';
                        cell4.innerHTML = "<button type='submit' onclick='delete_row_didik(\""+rowCount+"\")' class='btn btn-danger'>Delete</button>";

                        var namaField = "#r_product"+rowCount;
                        $(namaField).select2();
                      }

                      
                    </script>
                  </table>
                </div>
              </div>
            </div>
            <!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
          </div>
        </form>
        <br>
        <br>
        <!-- /.box-body -->
      </div>
      <?php 
      $nama_modulnya = 'bom';
      include '../headfoot/history.php';
      ?>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
</div>

<script>
  function save_modul()
  {
   document.getElementById("create_data").submit();
 }
 function bukaPassword()
 {
  var aa = document.getElementById("bukaPas").readOnly;
  if(aa)
  {
    document.getElementById("bukaPas").readOnly = false;
  }
  else
  {
    document.getElementById("bukaPas").readOnly = true;
  }
}
</script>
