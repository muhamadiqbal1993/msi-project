<?php 
$brand = "";
$rak = "";
$gudang = "";
$kodnamket = "";
$wheres = "";

if(!empty($_POST['kodnamket']))
{
  $kodnamket = $_POST['kodnamket'];
  $wheres .= " and KP.no = '$kodnamket'";
}
if(!empty($_POST['brand']))
{
 $brand = $_POST['brand'];
 $wheres .= " and B.no = '$brand'";
}
if(!empty($_POST['gudang']))
{
 $gudang = $_POST['gudang'];
 $wheres .= " and R.gudang = '$gudang'";
}
if(!empty($_POST['rak']))
{
 $rak = $_POST['rak'];
 $wheres .= " and rak = '$rak'";
}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $modulnya;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="../dashboard"> Dashboard</a></li>
      <li class="active"><?php echo $modulnya;?></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
           <div class="row">
            <!-- <form action="../excel/ledger/index.php" id="search_data" method="POST">  -->
              <form id="search_data" method="POST"> 

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Kategory</label>

                    <select class="form-control select2" name="kodnamket" style="width: 100%;">
                      <option value="">--- Semua Kategory ---</option>
                      <?php
                      $sql1=mysql_query("select * from kategory_product where status = '1'");
                      while($data1=mysql_fetch_array($sql1))
                      {
                        ?>
                        <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $kodnamket){echo "selected";}?>><?php echo $data1['nama']?></option>
                        <?php 
                      }
                      ?>

                    </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Brand</label>
                    <select class="form-control select2" name="brand" style="width: 100%;">
                      <option value="">--- Semua Brand ---</option>
                      <?php
                      $sql1=mysql_query("select * from brand where status = '1'");
                      while($data1=mysql_fetch_array($sql1))
                      {
                        ?>
                        <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $brand){echo "selected";}?>><?php echo $data1['nama']?></option>
                        <?php 
                      }
                      ?>

                    </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Lokasi</label>
                    <select class="form-control select2" name="rak" style="width: 100%;">
                      <option value="">--- Semua Lokasi ---</option>
                      <?php
                      $sql1=mysql_query("select * from rak where status = '1'");
                      while($data1=mysql_fetch_array($sql1))
                      {
                        ?>
                        <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $rak){echo "selected";}?>><?php echo $data1['nama']?></option>
                        <?php 
                      }
                      ?>

                    </select>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Gudang</label>
                    <select class="form-control select2" name="gudang" style="width: 100%;">
                      <option value="">--- Semua Gudang ---</option>
                      <?php
                      $sql1=mysql_query("select * from gudang where status = '1'");
                      while($data1=mysql_fetch_array($sql1))
                      {
                        ?>
                        <option value="<?php echo $data1['no']?>" <?php if($data1['no'] == $gudang){echo "selected";}?>><?php echo $data1['nama']?></option>
                        <?php 
                      }
                      ?>

                    </select>
                  </div>
                </div>
              </form>
              <div class="col-xs-12">
                <button type="submit" class="btn btn-success" form="search_data">Cari Data</button>
                <button type="submit" form="tambahData" class="btn btn-primary">Tambah Data</button>
                <!-- <button type="submit" onclick="pencarian()" class="btn btn-warning">Sembunyikan Pencarian</button> -->
                <a href="../function/laporan/label"><button type="button" class="btn btn-warning" style="float: right;">Print Label</button></a>

              </div>
            </div>

          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">


          </div>
          <br>

          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
      <div class="col-xs-12">

        <!-- /.box -->

        <div class="box">
          <div class="box-header">
            <div class="row">
            <!--   <div class="col-md-12">
                <?php if($addPM == "1"){?>
                  <button type="submit" form="tambahData" class="btn btn-primary">Tambah Data</button>
                  <a href="../function/laporan/label"><button type="button" class="btn btn-warning" style="float: right;">Print Label</button></a>
                <?php }?>
              </div> -->
              <form action="." id="tambahData" method="post">
                <input type="hidden" name="type" value="input">
              </form>


            </div>
          </div>

          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <?php 
            if($status == "1")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
              Data <?php echo $modulnya;?> Berhasil Dibuat.
              </div>
              </div>';
            }
            if($status == "2")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              Ada Masalah Dengan Server , Segera Hubungi Administrator.
              </div>
              </div>';
            }
            if($status == "3")
            {
              echo '<div class="col-xs-12">
              <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Warning!</h4>
              Data Product sudah ada yang menggunakan , silahkan buat dengan data yang berbeda
              </div>
              </div>';
            }
            ?>


            <div class="col-xs-12">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Photo</th>
                    <th>Kode Product</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Type</th>
                    <th>Stock</th>
                    <th>Brand</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 

                  $sql1=mysql_query("select P.*,R.nama as namrak,B.nama as nambrand,KP.nama as namkat,G.nama as namgud from product P
                    left outer join rak R on R.no = P.rak
                    left outer join brand B on B.no = P.kategory
                    left outer join kategory_product KP on KP.no = B.kategory
                    left outer join gudang G on G.no = R.gudang
                    where P.status = '1' and P.rawmat = '2' $wheres limit 100");
                  while($data1=mysql_fetch_array($sql1))
                  {  
                    $status = "Aktif";
                    if($data1['status'] == "2")
                    {
                      $status = "Tidak Aktif";
                    }

                    echo '
                    <tr>
                    <td><img src="../images/product/'.$data1['photo'].'" class="img" alt="Photo Product" style="width:70px;height:70px;"></td>
                    <td>'.$data1['kode'].'</td>
                    <td>'.$data1['nama'].'</td>
                    <td>'.number_format($data1['harga']).'</td> 
                    <td>'.$data1['barcode'].'</td>
                    <td>'.$data1['type'].' '.$data1['satuan'].'</td>
                    <td>'.$data1['nambrand'].'</td>
                    <td>'.$status.'</td>    
                    <td><a href="./?id='.$data1['no'].'">Manage</a></td>
                    </tr>
                    ';
                  }
                  ?>
                </tbody>
              </table>
            </div>

          </div>
          <br>
          <br>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>

</div>