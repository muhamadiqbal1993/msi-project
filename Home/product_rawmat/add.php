<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $modulnya;?>
			<small>Tambah <?php echo $modulnya;?> Baru</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="../dashboard"> Dashboard</a></li>
			<li><a href="."> <?php echo $modulnya;?> Data</a></li>
			<li class="active">Tambah Data</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="."><button type="submit" class="btn btn-warning">Kembali</button></a>
						&nbsp;&nbsp;&nbsp;
						<button type="submit" onclick="save_modul()" form="create_data" class="btn btn-primary">Simpan</button>
						<hr class="abu">
					</div>
					<form id="create_data" method="POST" enctype="multipart/form-data">
						<div class="box-body">
							<div class="col-md-2">
								<p></p>
								<p></p>
								<img id="photo1"/>
								<p></p>
								<div class="btn btn-default btn-file" id="photo62">
									<i class="fa fa-image"></i> Pilih Photo
									<input type="file" name="photos" onchange="viewImage(this);">

								</div>
								<p></p>
								<p></p>
								<p></p>
								<p></p>
							</div>
							<div class="col-md-5">
								<input type="hidden" name="input" class="form-control" value="1">
								<input type="hidden" name="user_idms" class="form-control" value="<?php echo $user_data['no'];?>"> 

								<div class="form-group">
									<label>Kode product</label>
									<input type="text" name="kode" class="form-control" >
								</div>
								<div class="form-group">
									<label>Nama product</label>
									<input type="text" name="nama" class="form-control" >
								</div>
								<div class="form-group">
									<label>Satuan Product</label>
									<select class="form-control select2" name="satuan" style="width: 100%;" required="">
										<option value="" >--- Pilih Satuan ---</option>
										<?php
										$sql1=mysql_query("select * from satuan_product");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['kode']?>" ><?php echo $data1['nama'].' -'.$data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								


								<!-- /.form-group -->

								<!-- /.form-group -->
							</div>
							<div class="col-md-5">
								
								<div class="form-group">
									<label>Brand</label>
									<select class="form-control select2" name="kategory" style="width: 100%;" required="">
										<option value="" >--- Pilih Brand ---</option>
										<?php
										$sql1=mysql_query("select * from brand");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								<div class="form-group">
									<label>Harga Beli</label>
									<input type="number" name="harga" class="form-control" value="0">
								</div>
								<div class="form-group">
									<label>Rak Gudang</label>
									<select class="form-control select2" name="rak" style="width: 100%;" required="">
										<option value="" >--- Pilih Rak Gudang ---</option>
										<?php
										$sql1=mysql_query("select R.*,G.nama as namgud from rak R
											inner join gudang G on G.no = R.gudang");
										while($data1=mysql_fetch_array($sql1))
										{
											?>
											<option value="<?php echo $data1['no']?>" ><?php echo $data1['namgud'].' - '.$data1['nama']?></option>

											<?php 
										}
										?>

									</select>
								</div>
								<!-- <div class="form-group">
									<label>Harga Flyer</label>
									<input type="number" name="flyer" class="form-control" >
								</div> -->

							</div>
							<!-- /.col -->

							<!-- <button type="submit" onclick="save_modul()" class="btn btn-primary">Simpan</button> -->
						</div>
					</form>
					<br>
					<br>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
</div>

<script>
	function save_modul()
	{
		document.getElementById("create_data").action = ".";
	}

	function viewImage(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#photo1')
				.attr('src', e.target.result)
				.width(150)
				.height(150);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
