<?php

include 'Home/function/init.php';

$error = "";
$username = "";

$data_web = mysql_fetch_array(mysql_query("select * from setting_web limit 1"));

if(!empty($_POST['Username']))
{
  $username = $_POST['Username'];
  $userna = $_POST['Username'];
  $pass = $_POST['Password'];

  $data_user = mysql_fetch_array(mysql_query("select * from user where user = '$userna' and password = '$pass'"));
  if(empty($data_user['no']))
  {
    $error = "Akun Anda Tidak Terdaftar";
    $username = "";
    $ck_user = mysql_fetch_array(mysql_query("select * from user where user = '$userna'"));
    if(!empty($ck_user))
    {
      $username = $userna;
      $nous = $ck_user['no'];
      mysql_query("insert into history_login values(null,'$nous',now(),'2')");
      $ttl_salah = mysql_fetch_array(mysql_query("select count(*) as ttls from history_login where user = '$nous' and status = '2'"));
      if($ttl_salah['ttls'] == "2")
      {
        $error = "Hallo, ".$userna." Anda sudah gagal login sebanyak ".$ttl_salah['ttls']." Kali, 1 kesempatan lagi agar akun anda tidak terkunci";
      }
      if($ttl_salah['ttls'] == "1")
      {
        $error = "Hallo, ".$userna." Anda sudah gagal login sebanyak ".$ttl_salah['ttls']." Kali";
      }
      if($ttl_salah['ttls'] == "3")
      {
        $error = "Hallo ".$userna.", Karena anda sudah gagal login sebanyak 3 kali, maka akun anda akan kami lock, silahkan hubungi Administrator untuk membuka kembali akun anda";
      }
      if($ttl_salah['ttls'] > 3)
      {
        $error = "Hallo ".$userna.", Karena anda sudah gagal login sebanyak 3 kali, maka akun anda akan kami lock, silahkan hubungi Administrator untuk membuka kembali akun anda";
      }
    }
  }
  else
  {
    $nous = $data_user['no'];
    $ttl_salah = mysql_fetch_array(mysql_query("select count(*) as ttls from history_login where user = '$nous' and status = '2'"));
    if($ttl_salah['ttls'] >= 3)
    {
      $error = "Hallo ".$userna.", Karena anda sudah gagal login sebanyak 3 kali, maka akun anda akan kami lock, silahkan hubungi Administrator untuk membuka kembali akun anda";
    }
    else
    {
      $nous = $data_user['no'];
      mysql_query("insert into history_login values(null,'$nous',now(),'1')");
      mysql_query("delete from history_login where user = '$nous' and status = '2'");
      header('Location:Home/function/F_login.php?Username='.$userna.'&Password='.$pass);
    }
    
  }
}

if(!empty($_SESSION['no']))
{
  header('Location:Home/dashboard');
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $data_web['title']?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" type="image/png" href="Home/images/<?php echo $data_web['logo']?>"/>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="Home/bower_components/select2/dist/css/select2.min.css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="Home/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="Home/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="Home/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="Home/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="Home/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <img src="Home/images/<?php echo $data_web['logo']?>" alt="lionel" width="300" height="100">
    </div>
    <?php
    if ($error != "")
    {
      echo '
      <div class="alert alert-danger" role="alert">
        '.$error.'
      </div>';

    }
    ?>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <!-- <form action="Home/function/F_login.php" method="post"> -->
      <form action="." method="post">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" name="Username" placeholder="Username" required="" value="<?php echo $username?>"> 
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="Password" placeholder="Password" required="">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>

          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
      <a href="#">I forgot my password</a><br><br>
      
      <!-- <img src="andro.png" class="rounded" alt="Mobile Apps Download" width="100px" height="50px"> -->
      <!-- <div class="text-left">
        <a href="LCM 1.0.apk">
          <img src="andro.png" class="rounded" alt="Mobile Apps Download" width="100px" height="50px">
        </a>
      </div> -->
      
      <!-- <div class="col-xs-6">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
      </div>
      <div class="col-xs-6">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
      </div> -->


    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <!-- jQuery 3 -->
  <script src="Home/bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="Home/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="Home/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="Home/plugins/iCheck/icheck.min.js"></script>
  <script>
    $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  });
    $(function () {

      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  </script>
</body>
</html>